package mx.isban.flujogd.notification.listener;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hazelcast.core.MemberAttributeEvent;
import com.hazelcast.core.MembershipEvent;
import com.hazelcast.core.MembershipListener;

import mx.isban.flujogd.common.bean.MemberBean;
import mx.isban.flujogd.common.bean.NetConfigBean;
import mx.isban.flujogd.common.util.InetAddressUtil;
import mx.isban.flujogd.common.util.MapEnum;
import mx.isban.flujogd.notification.configuration.HazelcastNotificationConfig;
import mx.isban.flujogd.persistence.PersistenceManagerSubscriptor;
import mx.isban.flujogd.persistence.impl.PersistenceManagerSubscriptorImpl;

/**
 * The listener interface for receiving clusterMembership events.
 * The class that is interested in processing a clusterMembership
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addClusterMembershipListener<code> method. When
 * the clusterMembership event occurs, that object's appropriate
 * method is invoked.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase ClusterMembershipListener
 */
public class ClusterMembershipListener implements MembershipListener{
	
	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(ClusterMembershipListener.class);
	
	/** nombreSubscriptor. */
	private static String nombreSubscriptor;
	
	/** componente. */
	private static String componente;
	
	/** hazelcastConfig. */
	private static HazelcastNotificationConfig hazelcastConfig;

	/**
	 * memberAdded.
	 * Metodo para agregar miembros  al servicio hazelcast
	 *
	 * @param membershipEvent the membership event
	 */
	@Override
	public void memberAdded(MembershipEvent membershipEvent) {
		String uuid = membershipEvent.getMember().getUuid();
		String uuidLocal = membershipEvent.getCluster().getLocalMember().getUuid();
		LOG.info("Se grego un nuevo miembro: "+uuid);
		if(uuid.equals(uuidLocal)) {
			LOG.info("flujogd-validation: "+uuid);
			notifyMemberAddNotification(uuid);
		}
	}

	/**
	 * memberRemoved.
	 * Metodo para remover miembros  al servicio hazelcast
	 *
	 * @param membershipEvent the membership event
	 */
	@Override
	public void memberRemoved(MembershipEvent membershipEvent) {
		LOG.info("Se elimino el miembro: "+membershipEvent.getMember().getUuid());
		notifyMemberRemovedNotification(membershipEvent.getMember().getUuid());
	}
	
	/**
	 * Notifica al cluster que flujo fue agregado.
	 *
	 * @param uuid the uuid
	 */
	private void notifyMemberAddNotification(String uuid) {
		LOG.info("Notificamos al cluster que flujo fue agregado");
		PersistenceManagerSubscriptor persistenceManagerNotification = new PersistenceManagerSubscriptorImpl();
		InetAddressUtil inetUtilNotification = new InetAddressUtil();
		String ipNetNotification = inetUtilNotification.getLocalNet();
		if(!"".equals(ipNetNotification)) {
			//Configurar el bean con los datos del subscriptor
			NetConfigBean netConfigBeanNotification = persistenceManagerNotification.retrieveNetConfigByIp(nombreSubscriptor,ipNetNotification);
			if(netConfigBeanNotification!=null){
				Map<String, MemberBean> members = hazelcastConfig.getMapMembers(MapEnum.MAP_MEMBER_STATUS.getName());
				MemberBean memberNotification = new MemberBean();
				memberNotification.setActive(true);
				memberNotification.setIp(netConfigBeanNotification.getIp());
				memberNotification.setPort(netConfigBeanNotification.getPort());
				memberNotification.setName(componente);
				memberNotification.setUuid(uuid);
				members.put(uuid, memberNotification);
				LOG.info("La notificacion se realizo con exito");
			}
		}else {
			LOG.info("Ocurrio un problema al recuperar la IP de este servidor, sin ella no es posible notificar la baja del nodo");
		}
	}

	/**
	 * Notifica al cluster que flujo se dio de baja.
	 *
	 * @param uuid the uuid
	 */
	private void notifyMemberRemovedNotification(String uuid) {
		LOG.info("Notificamos al cluster que flujo se dio de baja");
		PersistenceManagerSubscriptor persistenceManagerNotification = new PersistenceManagerSubscriptorImpl();
		InetAddressUtil inetUtilNotification = new InetAddressUtil();
		String ipNetNotification = inetUtilNotification.getLocalNet();
		if(!"".equals(ipNetNotification)) {
			//Configurar el bean con los datos del subscriptor
			NetConfigBean netConfigBeanNotification = persistenceManagerNotification.retrieveNetConfigByIp(nombreSubscriptor,ipNetNotification);
			if(netConfigBeanNotification!=null){
				Map<String, MemberBean> members = hazelcastConfig.getMapMembers(MapEnum.MAP_MEMBER_STATUS.getName());
				MemberBean memberNotification = members.get(uuid);
				if(memberNotification!=null) {
					memberNotification.setActive(false);
					members.replace(uuid, memberNotification);
				}
				LOG.info("La notificacion se realizo con exito");
			}
		}else {
			LOG.info("Ocurrio un problema al recuperar la IP de este servidor, sin ella no es posible notificar la baja del nodo");
		}
	}

	/**
	 * Metodo para notificar el cambio de atributos en los miembros
	 * memberAttributeChanged.
	 *
	 * @param memberAttributeEvent the member attribute event
	 */
	@Override
	public void memberAttributeChanged(MemberAttributeEvent memberAttributeEvent) {
		LOG.info("Cambio un atributo de uno de los miembros");
	}

	/**
	 * Sets the nombre subscriptor notification.
	 *
	 * @param nombreSubscriptor the nombreSubscriptor to set
	 */
	public static void setNombreSubscriptorNotification(String nombreSubscriptor) {
		ClusterMembershipListener.nombreSubscriptor = nombreSubscriptor;
	}

	/**
	 * Sets the componente notification.
	 *
	 * @param componente the componente to set
	 */
	public static void setComponenteNotification(String componente) {
		ClusterMembershipListener.componente = componente;
	}

	/**
	 * Sets the hazelcast config.
	 *
	 * @param hazelcastConfig the hazelcastConfig to set
	 */
	public static void setHazelcastConfig(HazelcastNotificationConfig hazelcastConfig) {
		ClusterMembershipListener.hazelcastConfig = hazelcastConfig;
	}

}
