package mx.isban.flujogd.notification.configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.support.transaction.ResourcelessTransactionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.notification.listener.JobNotificationListener;
import mx.isban.flujogd.notification.step.BatchNotificationItemProcessor;
import mx.isban.flujogd.notification.step.BatchNotificationItemReader;
import mx.isban.flujogd.notification.step.BatchNotificationItemWriter;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase BatchNotificationConfig
 *
 */
@EnableBatchProcessing(modular = true)
@Configuration
public class BatchNotificationConfig {

	//Inicializamos un log para guardar la bitacora
	private static final Logger LOG = LogManager.getLogger(BatchNotificationConfig.class);

	//Crear un job builder factory
	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	//Crear un step builder factory
	@Autowired
	private StepBuilderFactory stepBuilderFactory;
	
	//Crear un hazelcast notification config
	@Autowired
	private HazelcastNotificationConfig hazelcastNotificationConfig; 

	/**
	 * job
	 * @return	Job
	 */
	@Bean
	public Job processJob() {
		//Mostrar en bitacora el inicio del job
		LOG.info("execute job");
		return jobBuilderFactory.get("jobBuilderFactory")
				.incrementer(new RunIdIncrementer())
				.listener(listener())
				//Flujo del step
				.flow(orderStep())
				//Fin del step
				.end()
				//Compilar
				.build();
	}

	/**
	 * To create a step, reader, processor and writer has been passed serially
	 * 
	 * @return	Step
	 */
	@Bean
	public Step orderStep() {
		//Mostrar en bitacora el inicio del step
		LOG.info("step de job");
		return stepBuilderFactory.get("orderStep1")
				.<ExpedienteBean, ExpedienteBean> chunk(1)
				//Leer la configuracion de hazelcast
				.reader(new BatchNotificationItemReader(this.hazelcastNotificationConfig))
				//Procesar la configuracion
				.processor(new BatchNotificationItemProcessor(this.hazelcastNotificationConfig))
				.writer(new BatchNotificationItemWriter(this.hazelcastNotificationConfig))
				//Compilar
				.build();
	}

	/**
	 * @return {@link JobExecutionListener}
	 */
	@Bean
	public JobExecutionListener listener() {
		return new JobNotificationListener();
	}

	/**
	 * @return {@link ResourcelessTransactionManager}
	 */
	@Bean
	public ResourcelessTransactionManager transactionManager() {
		return new ResourcelessTransactionManager();
	}
}
