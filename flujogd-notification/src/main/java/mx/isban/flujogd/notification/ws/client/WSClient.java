package mx.isban.flujogd.notification.ws.client;

import java.util.List;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;

import mx.isban.flujogd.common.bean.AccentureNotificacionResponse;
import mx.isban.flujogd.common.bean.AccentureNotificationRequest;
import mx.isban.flujogd.common.bean.AccentureTokenRequest;
import mx.isban.flujogd.common.bean.AccentureTokenResponse;
import mx.isban.flujogd.common.bean.ParametroBean;
import mx.isban.flujogd.common.util.MapEnum;
import mx.isban.flujogd.common.util.ParametroEnum;
import mx.isban.flujogd.common.util.QueueEnum;
import mx.isban.flujogd.notification.configuration.HazelcastNotificationConfig;
import mx.isban.flujogd.notification.json.manager.JsonManager;
import mx.isban.flujogd.persistence.PersistenceManager;
import mx.isban.flujogd.persistence.impl.PersistenceManagerImpl;

/**
 * The Class WSClient.
 *
 * @author Alvaro Zamorano azamorano@serviciosexternos.isban.mx Clase WSClient
 */
public class WSClient extends WSClientData {
	/** The proxy name host. */
	private String proxyNameHost;

	/** The proxy value host. */
	private String proxyValueHost;

	/** The proxy name port. */
	private String proxyNamePort;

	/** The proxy value port. */
	private String proxyValuePort;

	/** The proxy name disabled schemes. */
	private String proxyNameDisabledSchemes;

	/** The proxy value disabled schemes. */
	private String proxyValueDisabledSchemes;

	// Crear un hazelcast notification config
	private HazelcastNotificationConfig hazelcastValidationConfig;

	/**
	 * Metodo para enviar configuracion de servicio hazelcast wsclient. q
	 *
	 * @param idExpediente              Id del expediente
	 * @param hazelcastValidationConfig Configuracion de validacion de hazelcast
	 */
	public WSClient(HazelcastNotificationConfig hazelcastValidationConfig) {		
		this.hazelcastValidationConfig = hazelcastValidationConfig;
		
	}

	private void prepareObjects(String idExpediente) {
		// Obtener los parametros del expediente
		String[] urls = retrieveParams(idExpediente);
		this.urlNoti = urls[0];
		this.urlToken = urls[1];
		this.challenge = urls[2];
		this.channel = urls[3];
		this.password = urls[4];
		this.username = urls[5];
		this.version = urls[6];
		this.proxyNameHost = urls[7];
		this.proxyValueHost = urls[8];
		this.proxyNamePort = urls[9];
		this.proxyValuePort = urls[10];
		this.proxyNameDisabledSchemes = urls[11];
		this.proxyValueDisabledSchemes = urls[12];
		if (this.proxyValueDisabledSchemes == null) {
			this.proxyValueDisabledSchemes = "";
		}
		System.setProperty(this.proxyNameHost, this.proxyValueHost);
		System.setProperty(this.proxyNamePort, this.proxyValuePort);
		System.setProperty(this.proxyNameDisabledSchemes, this.proxyValueDisabledSchemes);
	}

	/**
	 * Metodo para obtener el token del servicio de notificacion getToken.
	 *
	 * @param request the request Parametro para hacer la petici�n
	 * @return the token Regresa el token de la petici�n
	 * @throws JSONException             the JSON exception Lanza un error de tipo
	 *                                   JSONException
	 * @throws UniformInterfaceException the uniform interface exception
	 */
	public AccentureTokenResponse getToken(AccentureTokenRequest request) throws JSONException {
		AccentureTokenResponse resp = new AccentureTokenResponse();
		JsonManager manager = new JsonManager();
		String req = manager.getJsonToken(request);
		Client clientToken = ClientBuilder.newClient();
		WebTarget webTarget = clientToken.target(this.urlToken);
		Invocation.Builder inBuilder = webTarget.request(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION,
				"Basic UE9SVEFMX0NEQ19BUFBfQ0xJRU5UOkMmTGZeaGt0UFlNVnB1aFZ6UW5YaHZSNyFBUl4yMlhFQVU4cDMhNG0=");
		Response response = inBuilder.post(Entity.entity(req, MediaType.APPLICATION_JSON));
		String json = response.readEntity(String.class);
		JSONObject object = new JSONObject(json);
		resp.setAccessToken(object.getString("access_token"));
		resp.setTokenType(object.getString("token_type"));
		resp.setExpiresIn(object.getString("expires_in"));
		return resp;
	}

	/**
	 * Metodo para enviar la notificacion del proceso setNotification.
	 *
	 * @param request the request Parametro de la notificacion
	 * @param idExpediente the idExpediente
	 * @return the accenture notificacion response Regresa el valor de la
	 *         notificacion
	 * @throws JSONException             Lanza un error de tipo JSONException
	 * @throws UniformInterfaceException the uniform interface exception
	 */
	public AccentureNotificacionResponse setNotification(AccentureNotificationRequest request, String idExpediente) throws JSONException {
		prepareObjects(idExpediente);
		// Recuperamos el token
		Map<String, String> mapa = this.hazelcastValidationConfig.getMapToken(MapEnum.MAP_TOKEN.getName());
		String token = mapa.get(MapEnum.MAP_TOKEN.getName());
		if (token == null) {
			token = getNewToken();
			mapa.put(MapEnum.MAP_TOKEN.getName(), token);
		}
		AccentureNotificacionResponse accentureResponseNotificacion = sendNotification(request, token);
		if ("406".equals(accentureResponseNotificacion.getCode())) {
			token = getNewToken();
			mapa.put(MapEnum.MAP_TOKEN.getName(), token);
			accentureResponseNotificacion = sendNotification(request, token);
		}
		return accentureResponseNotificacion;
	}

	/**
	 * Metodo para ejecutar la notificacion setNotification.
	 *
	 * @param request the request Parametro para hacer la peticion
	 * @param token   the token Parametro token de tipo String
	 * @return the accenture notificacion response Regresa la respuesta de la
	 *         notificacion
	 * @throws JSONException             Arroja el JSON Exception
	 * @throws UniformInterfaceException the uniform interface exception
	 */
	private AccentureNotificacionResponse sendNotification(AccentureNotificationRequest request, String token) throws JSONException {
		AccentureNotificacionResponse resp = new AccentureNotificacionResponse();
		JsonManager manager = new JsonManager();
		String req = manager.getJsonNotification(request);
		Client clientNoti = ClientBuilder.newClient();
		WebTarget webTarget = clientNoti.target(this.urlNoti);
		Invocation.Builder inBuilder = webTarget.request(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, "Bearer " + token);

		Response response = inBuilder.post(Entity.entity(req, MediaType.APPLICATION_JSON));
		String json = response.readEntity(String.class);

		resp.setBody(validDataResponse(json, "body"));
		resp.setCode(validDataResponse(json, "code"));
		resp.setMessage(validDataResponse(json, "message"));
		return resp;

	}

	/**
	 * Valid data response.
	 *
	 * @param json the json
	 * @param data the data
	 * @return the string
	 * @throws JSONException the JSON exception
	 */
	private String validDataResponse(String json, String data) throws JSONException {
		JSONObject object = new JSONObject(json);
		if (json.indexOf(data) >= 0) {
			return object.getString(data);
		}
		return null;
	}

	/**
	 * Metodo para renovar el token de acceso al servicio de notificacion
	 * getNewToken.
	 *
	 * @return the new token
	 * @throws JSONException the JSON exception
	 */
	private String getNewToken() throws JSONException {
		AccentureTokenRequest requestToken = new AccentureTokenRequest();
		requestToken.setChallenge(this.challenge);
		requestToken.setChannel(this.channel);
		requestToken.setPassword(this.password);
		requestToken.setUsername(this.username);
		requestToken.setVersion(this.version);
		AccentureTokenResponse response = getToken(requestToken);
		return response.getAccessToken();
	}

	/**
	 * Metodo para obtener la ruta del IDC retrieveIDCPath.
	 *
	 * @param idExpediente the id expediente
	 * @return the string[]
	 */
	private String[] retrieveParams(String idExpediente) {
		PersistenceManager persistenceManager = new PersistenceManagerImpl();
		// Guardar los parametros en una lista
		List<ParametroBean> parametros = persistenceManager.retrieveParametrosBloque(idExpediente, QueueEnum.QUEUE_NOTIFICATION.getName());
		String[] urls = new String[13];
		if (parametros != null) {
			for (ParametroBean bean : parametros) {
				// Configurar las urls de acuerdo a los parametros
				urls = validarParametros(bean, urls);
				urls = validarParametrosComplemento(bean, urls);
			}
		}
		return urls;
	}

	/**
	 * Validar parametros. Metodo para setear parametros para peticion del servicio
	 * de notificacion
	 *
	 * @param bean   the bean
	 * @param urlsOr the urls or
	 * @return the string[]
	 */
	private String[] validarParametros(ParametroBean bean, String[] urlsOr) {
		String[] urls = urlsOr.clone();
		if (bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_NOTIFICATION_URL_NOTI.getName())) {
			urls[0] = bean.getValorParametro();
		} else if (bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_NOTIFICATION_URL_TOKEN.getName())) {
			urls[1] = bean.getValorParametro();
		} else if (bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_NOTIFICATION_TOKEN_CHALLENGE.getName())) {
			urls[2] = bean.getValorParametro();
		} else if (bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_NOTIFICATION_TOKEN_CHANNEL.getName())) {
			urls[3] = bean.getValorParametro();
		} else if (bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_NOTIFICATION_TOKEN_PASSWORD.getName())) {
			urls[4] = bean.getValorParametro();
		} else if (bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_NOTIFICATION_TOKEN_USERNAME.getName())) {
			urls[5] = bean.getValorParametro();
		} else if (bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_NOTIFICATION_TOKEN_VERSION.getName())) {
			urls[6] = bean.getValorParametro();
		}
		return urls;
	}

	/**
	 * Validar parametros complemento. Metodo para setear parametros para peticion
	 * del servicio de notificacion
	 *
	 * @param bean   the bean
	 * @param urlsOr the urls or
	 * @return the string[]
	 */
	private String[] validarParametrosComplemento(ParametroBean bean, String[] urlsOr) {
		String[] urls = urlsOr.clone();
		if (bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_NOTIFICATION_PROXY_NAM_HOST.getName())) {
			urls[7] = bean.getValorParametro();
		} else if (bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_NOTIFICATION_PROXY_VAL_HOST.getName())) {
			urls[8] = bean.getValorParametro();
		} else if (bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_NOTIFICATION_PROXY_NAM_PORT.getName())) {
			urls[9] = bean.getValorParametro();
		} else if (bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_NOTIFICATION_PROXY_VAL_PORT.getName())) {
			urls[10] = bean.getValorParametro();
		} else if (bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_NOTIFICATION_PROXY_NAM_DISABLED_SCHEMES.getName())) {
			urls[11] = bean.getValorParametro();
		} else if (bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_NOTIFICATION_PROXY_VAL_DISABLED_SCHEMES.getName())) {
			urls[12] = bean.getValorParametro();
		}
		return urls;
	}

}
