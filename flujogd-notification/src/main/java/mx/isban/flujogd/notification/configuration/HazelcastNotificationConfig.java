package mx.isban.flujogd.notification.configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.hazelcast.config.Config;
import com.hazelcast.config.ListenerConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IQueue;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.bean.MemberBean;
import mx.isban.flujogd.common.generic.HazelcastGenericUtils;
import mx.isban.flujogd.notification.listener.ClusterMembershipListener;

/**
 * The Class HazelcastNotificationConfig.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase HazelcastNotificationConfig
 */
@Configuration
public class HazelcastNotificationConfig {
	
	/** ips. */
	//Crear una lista de strings para guardar ls ips
	private static List<String> ips;
	
	/** port. */
	//Crear un string para guardar el puerto
	private static String port;
	
	/**
	 * Constructor.
	 */
	public HazelcastNotificationConfig() {
		ClusterMembershipListener.setHazelcastConfig(this);
	}
	
	
	/**
	 * Gets the single instance of HazelcastNotificationConfig.
	 *
	 * @return single instance of HazelcastNotificationConfig
	 */
	@Bean
	public HazelcastInstance getInstanceNotification() {
		return Hazelcast.newHazelcastInstance(getHazelcastConfig());
	}

	/**
	 * getqueue.
	 *
	 * @param name Nombre del expediente
	 * @return Instancia del expediente
	 */
	public IQueue<ExpedienteBean> getQueue(String name) {
		return getInstanceNotification().getQueue(name);
	}
	
	/**
	 * getmapmembers.
	 *
	 * @param name Nombre del mapa de miembros
	 * @return Instancia del mapa de miembros
	 */
	public Map<String, MemberBean> getMapMembers(String name) {
		return getInstanceNotification().getMap(name);
	}
	
	/**
	 * getmapexpediente.
	 *
	 * @param name Nombre del mapa de expedientes
	 * @return Instancia del mapa de expedientes
	 */
	public Map<String, Boolean> getMapExpedientes(String name) {
		return getInstanceNotification().getMap(name);
	}
	
	/**
	 * getmapnotificaciones.
	 *
	 * @param name Nombre del mapa de notificaciones
	 * @return Instancia del mapa de notificaciones
	 */
	public Map<String, Boolean> getMapNotificaciones(String name) {
		return getInstanceNotification().getMap(name);
	}
	
	/**
	 * getmaptoken.
	 *
	 * @param name Nombre del mapa de tokens
	 * @return Instancia del mapa de tokens
	 */
	public Map<String, String> getMapToken(String name) {
		return getInstanceNotification().getMap(name);
	}

	/**
	 * Config cluster network and discovery mechanism.
	 *
	 * @return Config
	 */
	@Bean
	public Config getHazelcastConfig() {
		Config config = new Config();
		HazelcastGenericUtils util = new HazelcastGenericUtils();
		config.addListenerConfig(new ListenerConfig("mx.isban.flujogd.notification.listener.ClusterMembershipListener"));
		config.setInstanceName("FlujoGD-Notification");
		return util.setConfigHazelcast(port, ips, config);

	}

	/**
	 * Sets the ips.
	 *
	 * @param ips the ips to set
	 */
	public static void setIps(List<String> ips) {
		List<String> copia = new ArrayList<>();
		copia.addAll(ips);
		HazelcastNotificationConfig.ips = copia;
	}

	/**
	 * Sets the port.
	 *
	 * @param port the port to set
	 */
	public static void setPort(String port) {
		HazelcastNotificationConfig.port = port;
	}

}
