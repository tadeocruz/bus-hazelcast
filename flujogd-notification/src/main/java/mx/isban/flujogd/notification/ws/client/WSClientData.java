/**
 * 
 */
package mx.isban.flujogd.notification.ws.client;

/**
 * The Class WSClientToken.
 * Clase para obtener token para notificacion
 *
 * @author jccalderon
 * Stefanini
 * 02/08/2019
 */
public class WSClientData {

	
	//Crear un string para guardar la url de la notificacion
	protected String urlNoti;
	
	//Crear un string para guardar la url del token
	protected String urlToken;
	
	//Crear un string para guardar el challenge
	protected String challenge;
	
	//Crear un string para guardar el canal
	protected String channel;
	
	//Crear un string para guardar la contrasena
	protected String password;
	
	//Crear un string para guardar el nombre de usuario
	protected String username;
	
	//Crear un string para guardar la version
	protected String version;
	
}
