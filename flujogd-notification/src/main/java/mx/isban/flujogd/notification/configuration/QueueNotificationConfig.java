package mx.isban.flujogd.notification.configuration;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import mx.isban.flujogd.common.util.QueueEnum;
import mx.isban.flujogd.notification.listener.QueueNotificationListener;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase QueueNotificationConfig
 *
 */
@Configuration
public class QueueNotificationConfig {
	
	/**
	 * log
	 */
	//Inicializamos un log para guardar la bitacora
	private static final Logger LOG = LogManager.getLogger(QueueNotificationConfig.class);
	
	//Crear un hazelcast notification config
	@Autowired
	private HazelcastNotificationConfig hazelcastConfig;
	
	//Crear un queue notificacion listener
	@Autowired
	private QueueNotificationListener queueNotificationListener;

	/**
	 * postConstruct
	 */
	@PostConstruct
	public void postConstruct() {
		hazelcastConfig.getQueue( QueueEnum.QUEUE_NOTIFICATION.getName() ).addItemListener( queueNotificationListener, true );
		LOG.info( "FlujoGD-Notification  - Listener Start" );
	}
}
