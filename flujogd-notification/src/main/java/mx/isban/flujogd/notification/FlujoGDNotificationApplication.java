package mx.isban.flujogd.notification;


import java.io.File;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.system.ApplicationPidFileWriter;
import org.springframework.scheduling.annotation.EnableAsync;

import mx.isban.flujogd.common.generic.CommonsUtils;
import mx.isban.flujogd.notification.configuration.HazelcastNotificationConfig;
import mx.isban.flujogd.notification.listener.ClusterMembershipListener;

/**
 * The Class FlujoGDNotificationApplication.
 * Metodo principal que ejecutara el proceso de Notification.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase FlujoGDNotificationApplication
 */
@SpringBootApplication
@EnableAsync
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
public class FlujoGDNotificationApplication implements CommandLineRunner {
	
	//Inicializamos un log para guardar la bitacora
	private static final Logger LOG = LogManager.getLogger(FlujoGDNotificationApplication.class);
	
	/**
	 * main
	 * Metodo principal que inicia el flujo notification.
	 * 
	 * @param args Argumentos
	 */
    public static void main(String[] args) {
		CommonsUtils utils = new CommonsUtils();
		// Validar que existan los parametros requeridos para el proceso
    	if(args.length<5) {
    		//Mostrar en bitacora la falta de parametros
    		LOG.error("No es posible inicar el componente sin los parametros 'IPs','port','path','componente' y 'Nombre subscriptor'");
    		return;
    	}else{
    		//Colocar la ip de la configuracion de hazelcast
			HazelcastNotificationConfig.setIps(utils.getListaIp(args));
			//Colocar el puerto de la configuracion de hazelcast
			HazelcastNotificationConfig.setPort(args[1]);
			//Colocar el nombre del subscriptor del cluster de membresias
			ClusterMembershipListener.setNombreSubscriptorNotification(args[4]);
			//Colocar el componente del cluster de membresias
			ClusterMembershipListener.setComponenteNotification(args[3]);
			SpringApplication sp = new SpringApplication(FlujoGDNotificationApplication.class);
			//Agregar los disparadores al spring application
			sp.addListeners(new ApplicationPidFileWriter(args[2]+File.separator+args[3]+".pid"));
			//Correr los argumentos
			sp.run(args);
		}
    }

    /**
     * Metodo nativo hazelcast que dispara el proceso de arranque.
     *
     * @param args the args Parametro argumento varuable de tipo String
     * @throws Exception the exception Lnaza error de tipo exception
     */
	@Override
	public void run(String... args) throws Exception {
		//Mostrar en bitacora el inicio del proceso
		LOG.info("FlujoGDNotificationApplication - run");
	}

}
