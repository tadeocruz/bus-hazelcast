package mx.isban.flujogd.notification.bean;

import lombok.Getter;
import lombok.Setter;

/**
 * Respuesta genérica de los servicios, incluye una respuesta y un response
 * 
 * @author Heimat
 *
 */
@Getter
@Setter
public class GenericResponse {
	/**
	 * Objeto respuesta
	 */
	private Respuesta respuesta;
	
	/**
	 * Objeto response
	 */
	private Object response;
	
	public GenericResponse() {}
	
	
	/**
	 * Constructor para crear un objeto GenericResponse con base en los parámetros proporcionados
	 * 
	 * @param code código de respuesta
	 * @param mensaje mensaje de respuesta
	 * @param response objeto que se desea regresar como respuesta
	 */
	public GenericResponse(String code, String mensaje, Object response) {
		super();
		this.respuesta = new Respuesta(code, mensaje);
		this.response = response;
	}
}
