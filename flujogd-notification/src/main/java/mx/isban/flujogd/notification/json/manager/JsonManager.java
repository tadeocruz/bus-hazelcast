package mx.isban.flujogd.notification.json.manager;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import mx.isban.flujogd.common.bean.AccentureDocumentoRequest;
import mx.isban.flujogd.common.bean.AccentureNotificationRequest;
import mx.isban.flujogd.common.bean.AccentureTokenRequest;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase JsonManager
 *
 */
public class JsonManager {

	/**
	 * getjsonnotification
	 * @param request	Peticion
	 * @return	JSON convertio en string
	 * @throws JSONException	Excepcion de JSON
	 */
	public String getJsonNotification(AccentureNotificationRequest request) throws JSONException {
		//Crear un objeto JSON
		JSONObject json = new JSONObject();
		//Crear un arreglo JSON
		JSONArray array = new JSONArray();
		//Colocar la referencia externa en el objeto JSON
		json.put("REFERENCIA_EXTERNA",request.getReferenciaExterna());
		//Colocar el id de peticion en el objeto JSON
		json.put("REQUEST_ID",request.getRequestId());
		//Colocar el buc en el objeto JSON
		json.put("BUC",request.getBuc());
		//Colocar la fecha de proceso en el objeto JSON
		json.put("FECHA_PROCESO",request.getFechaProceso());
		//Crear una lista para guardar los archivos
		List<AccentureDocumentoRequest> lista = request.getArchivos();
		for(AccentureDocumentoRequest doc:lista) {
			//Crear un objeto JSON
			JSONObject jsonDoc = new JSONObject();
			//Colocar el id del documento en el objeto JSON
			jsonDoc.put("DOC_ID", doc.getDocId());
			//Colocar el nombre del documento en el objeto JSON
			jsonDoc.put("DOC_NOMBRE", doc.getDocNombre());
			//Colocar el checksum del documento en el objeto JSON
			jsonDoc.put("CHECKSUM", doc.getChecksum());
			//Colocar el estatus del documento en el objeto JSON
			jsonDoc.put("ESTADO", doc.getEstado());
			//Colocar el id de error del documento en el objeto JSON
			jsonDoc.put("ERROR_ID", doc.getDetalle().getErrorId());
			//Colocar el error del documento en el objeto JSON
			jsonDoc.put("ERROR", doc.getDetalle().getError());
			//Colocar el reenvio del documento en el objeto JSON
			jsonDoc.put("REENVIO", doc.getDetalle().getReenvio());
			array.put(jsonDoc);
		}
		//Colocar la lista de archivos en el objeto JSON
		json.put("ARCHIVOS_LIST", array);
		//Convertir el objeto JSON a string
		return json.toString();
	}
	
	/**
	 * getjsontoken
	 * @param request	Peticion
	 * @return	JSON convertido en string
	 * @throws JSONException	Excepcion JSON
	 */
	public String getJsonToken(AccentureTokenRequest request) throws JSONException {
		JSONObject json = new JSONObject();
		json.put("username",request.getUsername());
		json.put("password",request.getPassword());
		json.put("grant_type","password");
		json.put("channel",request.getChannel());
		json.put("version",request.getVersion());
		JSONObject json2 = new JSONObject();
		json2.put("response","");
		json2.put("challenge",request.getChallenge());
		json.put("captcha", json2);
		return json.toString();
	}

}
