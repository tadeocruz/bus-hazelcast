package mx.isban.flujogd.notification.step;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.item.ItemReader;

import com.hazelcast.core.IQueue;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.util.QueueEnum;
import mx.isban.flujogd.notification.configuration.HazelcastNotificationConfig;

/**
 * The Class BatchNotificationItemReader.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase inicial BatchNotificationItemReader
 */
public class BatchNotificationItemReader implements  ItemReader<ExpedienteBean>{
	
	//Crear un hazelcast notification config
	private HazelcastNotificationConfig hazelcastNotificationConfig;
	
	//Crear un expediente bean
	private ExpedienteBean expedienteBean;

	//Inicializamos un log para guardar la bitacora
	private static final Logger LOG = LogManager.getLogger(BatchNotificationItemReader.class);

	/**
	 * Constructor.
	 *
	 * @param hazelcastNotificationConfig Configuracion de la notificacion de hazelcast
	 */
	public BatchNotificationItemReader(HazelcastNotificationConfig hazelcastNotificationConfig){
		this.hazelcastNotificationConfig = hazelcastNotificationConfig;
	}


	/**
	 * Metodo que lee de la queue el siguiente dato a ser procesado.
	 *
	 * @return the expediente bean
	 * @throws Exception the exception
	 */
	@Override
	public ExpedienteBean read() throws Exception {
		IQueue<ExpedienteBean> queueNotification = this.hazelcastNotificationConfig.getQueue(QueueEnum.QUEUE_NOTIFICATION.getName());
		try {
			expedienteBean = queueNotification.take();
			if (expedienteBean != null) {
				//Mostrar en bitacora el expediente recuperado
				LOG.info("=== SE RECUPERA EXPEDIENTE: "+expedienteBean.getIdExpediente() + " - "+ expedienteBean.getNombreExpediente() + " - " + expedienteBean.getDetalle().getDetalle2().getReferenciaExterna());
				return expedienteBean;
			}else{
				return null;
			}
		} catch (InterruptedException e) {
			LOG.error(e);
			//Interrumpir el hilo actual
			Thread.currentThread().interrupt();
		}
		return null;
	}
}
