package mx.isban.flujogd.notification.step;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.item.ItemWriter;

import com.hazelcast.core.IQueue;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.notification.configuration.HazelcastNotificationConfig;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase BatchNotificationItemWriter
 *
 */
public class BatchNotificationItemWriter implements ItemWriter<ExpedienteBean>{
	//Crear un hazel cast notificacion config
	private HazelcastNotificationConfig hazelcastNotificationConfig;

	//Inicializamos un log para guardar la bitacora
	private static final Logger LOG = LogManager.getLogger(BatchNotificationItemWriter.class);

	/**
	 * Constructor
	 * @param hazelcastNotificationConfig	Configuracion de la notificacion de hazelcast
	 */
	public BatchNotificationItemWriter(HazelcastNotificationConfig hazelcastNotificationConfig){
		this.hazelcastNotificationConfig = hazelcastNotificationConfig;
	}


	/**
	 * write
	 * @param items
	 */
	@Override
	public void write(List<? extends ExpedienteBean> items) throws Exception {
		String nextQueue = items.get(0).getDetalle().getSiguienteQueue();
		if(!"".equals(nextQueue)) {
			LOG.info("Siguiente queue: " + nextQueue + " - "+ items.get(0).getNombreExpediente() + " - " + items.get(0).getDetalle().getDetalle2().getReferenciaExterna());
			IQueue<ExpedienteBean> queue = hazelcastNotificationConfig.getQueue(nextQueue);
			queue.put(items.get(0));
		}else {
			//Si ocurrio un error en el proceso se elimina el expediente del mapa
			LOG.info("No hay otra queue a procesar");
		}
	}

}
