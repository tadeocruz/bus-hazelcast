package mx.isban.flujogd.notification.listener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hazelcast.core.ItemEvent;
import com.hazelcast.core.ItemListener;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.notification.trigger.ExecuteJobNotificationTrigger;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase QueueNotificationListener
 *
 */
@Component
public class QueueNotificationListener implements ItemListener<ExpedienteBean> {

	//Inicializamos un log para guardar la bitacora
	private static final Logger LOG = LogManager.getLogger(QueueNotificationListener.class);

	//Crear un execute job notification trigger
	@Autowired
	private ExecuteJobNotificationTrigger jobBatch;

	/**
	 * itemAdded
	 * @param item
	 */
	@Override
	public void itemAdded(ItemEvent<ExpedienteBean> item) {
		LOG.info("Se detecta objeto agregado a la queue");
		jobBatch.executeJobNotification();
	}

	/**
	 * itemRemoved
	 * @param item
	 */
	@Override
	public void itemRemoved(ItemEvent<ExpedienteBean> item) {
		LOG.info("Se detecta objeto removido de la queue");
	}

}