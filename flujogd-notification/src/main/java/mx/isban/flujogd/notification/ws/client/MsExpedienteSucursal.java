package mx.isban.flujogd.notification.ws.client;

import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.bean.ParametroBean;
import mx.isban.flujogd.common.exceptions.BusinessException;
import mx.isban.flujogd.common.util.ParametroEnum;
import mx.isban.flujogd.notification.bean.GenericResponse;
import mx.isban.flujogd.persistence.PersistenceManagerSubscriptor;
import mx.isban.flujogd.persistence.impl.PersistenceManagerSubscriptorImpl;

/**
 * Clase MsExpedienteSucursal para realizar las notificaciones correspondientes
 * al Microservicio de Expedientes Sucursales
 * 
 * @author DELL
 *
 */
public class MsExpedienteSucursal {
	/** Inicializamos un log para guardar la bitacora */
	private static final Logger LOG = LogManager.getLogger(MsExpedienteSucursal.class);

	/**
	 * url del ced para obtener un token de sesión
	 */
	private String urlToken;

	/**
	 * Url para obtener las reglas de validación de metadatos desde el ced
	 */
	private String urlNotificar;

	/**
	 * objeto para realizar parseo
	 */
	private JSONParser parser = new JSONParser();

	/**
	 * objeto para realizar las peticiones a servicios rest
	 */
	private RestTemplate restTemplate = new RestTemplate();

	/**
	 * Objeto de persistencia en base de datos, utilizado para recuperar la url del
	 * token y la url del ced
	 */
	private PersistenceManagerSubscriptor persistenceManager = new PersistenceManagerSubscriptorImpl();

	/**
	 * Constructor con nombre de subscriptor como parámetro de entrada
	 */
	public MsExpedienteSucursal() {
	}

	/**
	 * Constructor con los parámetros específicados
	 * 
	 * @param restTemplate       objeto para realizar las peticiones a servicios
	 *                           rest
	 * @param persistenceManager Objeto de persistencia en base de datos, utilizado
	 *                           para recuperar la url del token y la url del ced
	 */
	public MsExpedienteSucursal(RestTemplate restTemplate, PersistenceManagerSubscriptor persistenceManager) {
		parser = new JSONParser();
		this.restTemplate = restTemplate;
		this.persistenceManager = persistenceManager;
	}

	/**
	 * Método para obtener la lista de validación de metadatos correspondiente al
	 * nombre de subscriptor y tipo documental proporcionado
	 * 
	 * @param expedienteBean expediente con la información necesaria para realizar
	 *                       la notificación
	 * @return ResponseEntity<GenericResponse> with status code and response body
	 * @throws ParseException    en caso de ocurrir un error al parsear la respuesta
	 *                           del ced
	 * @throws BusinessException en caso de que la respuesta del ced no sea un
	 *                           código ok
	 */
	public ResponseEntity<GenericResponse> notificar(ExpedienteBean expedienteBean) throws BusinessException {
		String token = "";
		try {
			prepareObjects(expedienteBean);
			token = getToken();
		} catch (ParseException | BusinessException e) {
			throw new BusinessException("Ocurrió un error al recuperar el token", e);
		}
		try {
			LOG.info("preparando notificación para la cuenta " + expedienteBean.getExpedienteSucursal().getCuenta());
			LOG.debug("Errores fileNet: {}", expedienteBean.getExpedienteSucursal().getErroresFilenet());
			HashMap<String, Object> bodyRequest = new HashMap<>();
			bodyRequest.put("cuenta", expedienteBean.getExpedienteSucursal().getCuenta());
			if (expedienteBean.getExpedienteSucursal().isErrorExistenciaParidad()) {
				bodyRequest.put("errorExistenciaParidad", true);
			} else if (expedienteBean.getExpedienteSucursal().getErroresFilenet() != null && !expedienteBean.getExpedienteSucursal().getErroresFilenet().isEmpty()) {
				bodyRequest.put("aplicativo", expedienteBean.getExpedienteSucursal().getAplicativo());
				bodyRequest.put("erroresFileNet", expedienteBean.getExpedienteSucursal().getErroresFilenet());
			} else {
				bodyRequest.put("aplicativo", expedienteBean.getExpedienteSucursal().getAplicativo());
				bodyRequest.put("erroresDocumentos", expedienteBean.getExpedienteSucursal().getErroresDocumento());
			}
			LOG.debug("agregando token: " + token);
			HttpHeaders headers = new HttpHeaders();
			headers.set("Authorization", token);

			HttpEntity<HashMap<String, Object>> request = new HttpEntity<>(bodyRequest, headers);
			LOG.debug("notificando request: " + new Gson().toJson(bodyRequest));
			return restTemplate.postForEntity(urlNotificar, request, GenericResponse.class);
		} catch (RestClientException e) {
			LOG.error(e.getMessage(), e);
			throw new BusinessException("Ocurrió un error al realizar la notificación", e);
		}

	}

	/**
	 * Método para obtener un token de autenticación y poder consumir el servicio
	 * del ced
	 * 
	 * @return token de autenticación
	 * @throws ParseException    en caso de ocurrir un error al parsear la respuesta
	 *                           del ced
	 * @throws BusinessException en caso de que la respuesta del ced no sea un
	 *                           código ok
	 */
	private String getToken() throws ParseException, BusinessException {
		LOG.info("Obteniendo token");
		try {
			ResponseEntity<String> response = restTemplate.postForEntity(urlToken, null, String.class);
			LOG.debug(response.getBody());
			if (response.getStatusCode().is2xxSuccessful()) {
				JSONObject jsonObject = (JSONObject) parser.parse(response.getBody());
				return jsonObject.get("access_token").toString();
			}
			throw new BusinessException(response.getBody());
		} catch (ParseException | BusinessException e) {
			LOG.error(e.getMessage(), e);
			throw e;
		} catch (RestClientException e) {
			LOG.error(e.getMessage(), e);
			throw new BusinessException(e.getMessage(), e);
		}
	}

	/**
	 * Método para prepara las urls con base en un expediente proporcionado
	 * 
	 * @param expedienteBean
	 */
	private void prepareObjects(ExpedienteBean expedienteBean) {
		LOG.info("Recuperando parámetro de notificación");
		String nombreSubscriptor = persistenceManager.retrieveSubscriptorByIdExpediente(expedienteBean.getIdExpediente());

		ParametroBean urlTokenCedParam = persistenceManager.retrieveParam(nombreSubscriptor, ParametroEnum.PARAMETRO_EXP_SUC_TOKEN_URL.getName());
		urlToken = urlTokenCedParam.getValorParametro();

		ParametroBean urlGetMetadatosConfigurationParam = persistenceManager.retrieveParam(nombreSubscriptor, ParametroEnum.PARAMETRO_EXP_SUC_VALIDATION_URL.getName());
		urlNotificar = urlGetMetadatosConfigurationParam.getValorParametro();
	}
}
