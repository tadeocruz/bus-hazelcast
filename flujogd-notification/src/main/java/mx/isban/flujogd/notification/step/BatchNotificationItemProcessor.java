package mx.isban.flujogd.notification.step;

import java.io.IOException;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.http.ResponseEntity;

import mx.isban.flujogd.common.bean.AccentureNotificacionResponse;
import mx.isban.flujogd.common.bean.AccentureNotificationRequest;
import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.exceptions.BusinessException;
import mx.isban.flujogd.common.util.MapEnum;
import mx.isban.flujogd.notification.bean.GenericResponse;
import mx.isban.flujogd.notification.configuration.HazelcastNotificationConfig;
import mx.isban.flujogd.notification.ws.client.MsExpedienteSucursal;
import mx.isban.flujogd.notification.ws.client.WSClient;

/**
 * The Class BatchNotificationItemProcessor.
 *
 * @author Alvaro Zamorano azamorano@serviciosexternos.isban.mx
 */
public class BatchNotificationItemProcessor implements ItemProcessor<ExpedienteBean, ExpedienteBean> {

	/** Inicializamos un log para guardar la bitacora */
	private static final Logger LOG = LogManager.getLogger(BatchNotificationItemProcessor.class);

	/** Crear un hazelcast notification config */
	private HazelcastNotificationConfig hazelcastValidationConfig;

	/**
	 * ms object
	 */
	private MsExpedienteSucursal ms = new MsExpedienteSucursal();
	
	/**
	 * WSClient Accenture
	 */
	private WSClient client;

	/**
	 * NotificationUtils
	 */
	private NotificationUtils notificationUtils = new NotificationUtils();

	/**
	 * Constructor.
	 *
	 * @param hazelcastValidationConfig Configuracion de la validacion de hazelcast
	 */
	public BatchNotificationItemProcessor(HazelcastNotificationConfig hazelcastValidationConfig) {
		this.hazelcastValidationConfig = hazelcastValidationConfig;
		client = new WSClient(this.hazelcastValidationConfig);
	}

	/**
	 * Constructor con los parámetros específicados
	 * 
	 * @param hazelcastValidationConfig hz config
	 * @param ms                        ms object
	 * @param notificationUtils notificationUtils object
	 * @param client the accenture client
	 */
	public BatchNotificationItemProcessor(HazelcastNotificationConfig hazelcastValidationConfig, MsExpedienteSucursal ms, NotificationUtils notificationUtils, WSClient client) {
		this.hazelcastValidationConfig = hazelcastValidationConfig;
		this.ms = ms;
		this.notificationUtils = notificationUtils;
		this.client = client;
	}

	/**
	 * Método process para realizar la notificación de término de proceso
	 * correspondiente al servicio según sea la implementación(En este momento
	 * contempla Vivere(Accenture) y Expedientes sucursales) independientemente de
	 * si el resultado contiene errores o no se debe notificar al subscriptor.
	 *
	 * @param expedienteBean Parametro para los expedientes
	 * @return the expediente bean Regresa los valores del parametro
	 * @throws Exception Lanza un error de tipo Exception
	 */
	@Override
	public synchronized ExpedienteBean process(ExpedienteBean expedienteBean) throws BusinessException, JSONException, IOException {
		String code = null;
		LOG.info("Comienza proceso de notificación");
		if (expedienteBean.getExpedienteSucursal() != null) {
			LOG.info("preparando notificación a expedientes sucursales");
			ResponseEntity<GenericResponse> response = ms.notificar(expedienteBean);
			LOG.debug("=== WS RESPONSE: {}", response);
			if (response.getStatusCode().is2xxSuccessful()) {
				LOG.info("notificación exitosa");
				code = "200";
			} else {
				code = "" + response.getStatusCode().value();
				LOG.info("Ocurrió un error al realizar la notificación");
				LOG.debug(response.getBody());
			}
			notificationUtils.getPm().insertOrUpdateNotification(notificationUtils.getNotificationBeanFromExpSuc(expedienteBean, response));
		} else {
			AccentureNotificationRequest request = expedienteBean.getDetalle().getDetalle2().getAccenturRequest();
			LOG.info("preparando notidicación a Vivere"+request.getBuc());
			AccentureNotificacionResponse response = client.setNotification(request, expedienteBean.getIdExpediente());
			LOG.info("=== WS RESPONSE: {}", response);
			code = response.getCode();

			// Actualizar el estatus de la notificacion y su response en DB
			notificationUtils.getPm().insertOrUpdateNotification(notificationUtils.getNotificationBeanFromAccenture(expedienteBean, request, response));
		}

		// Se elimina la notificacion del mapa
		Map<String, Boolean> notificacionesMap = this.hazelcastValidationConfig.getMapNotificaciones(MapEnum.MAP_NOTIFICACIONES_IN_PROCESS.getName());
		notificacionesMap.remove(expedienteBean.getIdExpediente(), true);
		expedienteBean.getDetalle().setSiguienteQueue("");
		if ("200".equals(code)) {
			// Borrar el expediente
			notificationUtils.borrarArchivo(expedienteBean);
		}
		// Regresar la vaina del expediente
		return expedienteBean;
	}

}
