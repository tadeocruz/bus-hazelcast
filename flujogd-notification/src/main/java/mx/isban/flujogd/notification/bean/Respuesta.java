package mx.isban.flujogd.notification.bean;

import lombok.Getter;
import lombok.Setter;

/**
 * Respuesta general para los servicios
 * 
 * @author Heimat
 *
 */
@Getter
@Setter
public class Respuesta {
	
	/**
	 * Código de respuesta
	 */
	private String code;
	
	/**
	 * Mensaje de respuesta 
	 */
	private String mensaje;

	/**
	 * Constructor por defecto 
	 */
	public Respuesta() {
	}

	/**
	 * Constructor para crear un objeto Respuesta con base en los parámetros proporcionados
	 * 
	 * @param code código de respuesta
	 * @param mensaje mensaje de respuesta
	 */
	public Respuesta(String code, String mensaje) {
		super();
		this.code = code;
		this.mensaje = mensaje;
	}

}
