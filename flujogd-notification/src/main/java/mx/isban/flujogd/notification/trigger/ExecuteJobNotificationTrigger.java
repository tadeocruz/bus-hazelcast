package mx.isban.flujogd.notification.trigger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase ExecuteJobNotificationTrigger
 *
 */
@Component
public class ExecuteJobNotificationTrigger {
	
	//Inicializamos un log para guardar la bitacora
	private static final Logger LOG = LogManager.getLogger(ExecuteJobNotificationTrigger.class);
	
	//Crear un job launcher
	@Autowired
	private JobLauncher jobLauncherNotification;
	//Crear un job
	@Autowired
	private Job processJobNotification;

	/**
	 * Ejecuta el job batch
	 */
	public void executeJobNotification() {
		try {
			//Configurar los parametros deljob
			JobParameters jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis()).toJobParameters();
			//Ejecutar el proceso
			jobLauncherNotification.run(processJobNotification, jobParameters);
		} catch (JobParametersInvalidException | JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException e) {
			LOG.error(e);
		}
	}

}
