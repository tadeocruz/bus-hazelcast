package mx.isban.flujogd.notification.step;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import mx.isban.flujogd.common.bean.AccentureNotificacionResponse;
import mx.isban.flujogd.common.bean.AccentureNotificationRequest;
import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.bean.NotificationBean;
import mx.isban.flujogd.common.bean.NotificationBean2;
import mx.isban.flujogd.common.bean.NotificationBean3;
import mx.isban.flujogd.notification.bean.GenericResponse;
import mx.isban.flujogd.notification.json.manager.JsonManager;
import mx.isban.flujogd.persistence.PersistenceManagerNotificacion;
import mx.isban.flujogd.persistence.impl.PersistenceManagerNotificacionImpl;

/**
 * Clase NotificationBeanManipulator para la obtención de un objeto
 * NotificationBean con base en los parámetros proporcionados
 * 
 * @author DELL
 *
 */
public class NotificationUtils {
	
	/** Inicializamos un log para guardar la bitacora */
	private static final Logger LOG = LogManager.getLogger(NotificationUtils.class);

	/**
	 * Json Manager
	 */
	private JsonManager manager;

	/**
	 * ObjectMapper
	 */
	private ObjectMapper mapper;

	/**
	 * PersistenceManagerNotificacion
	 */
	private PersistenceManagerNotificacion pm;

	/**
	 * Default constructor
	 */
	public NotificationUtils() {
		manager = new JsonManager();
		mapper = new ObjectMapper();
		pm = new PersistenceManagerNotificacionImpl();
	}

	/**
	 * Constructor with PersistenceManagerNotificacion
	 * @param pm persistence manager object
	 */
	public NotificationUtils(PersistenceManagerNotificacion pm) {
		manager = new JsonManager();
		mapper = new ObjectMapper();
		this.pm = pm;
	}

	/**
	 * PersistenceManagerNotificacion getter
	 * 
	 * @return PersistenceManagerNotificacion object
	 */
	public PersistenceManagerNotificacion getPm() {
		return pm;
	}

	public void setPm(PersistenceManagerNotificacion pm) {
		this.pm = pm;
	}

	/**
	 * Método getNotificationBeanFromExpSuc para obtener un NotificationBean desde
	 * un response de Expedientes Sucursales
	 * 
	 * @param expedienteBean expedienteBean
	 * @param response       Response de Expedientes Sucursales
	 * @return NotificationBean NotificationBean
	 * @throws JsonProcessingException Excepción JsonProcessingException
	 */
	public NotificationBean getNotificationBeanFromExpSuc(ExpedienteBean expedienteBean, ResponseEntity<GenericResponse> response) throws JsonProcessingException {
		NotificationBean bean = new NotificationBean();
		NotificationBean2 bean2 = new NotificationBean2();
		// Configurar los parametros de cada bean
		bean2.setBean3(new NotificationBean3());
		bean.setBean2(bean2);
		bean.setIdExpediente(expedienteBean.getIdExpediente());
		bean.setResponseCode("" + response.getStatusCode().value());
		bean.setResponseMsg(mapper.writeValueAsString(response.getBody()));
		return bean;
	}

	/**
	 * Método getNotificationBeanFromAccenture para obtener un NotificationBean
	 * desde un response de Accenture
	 * 
	 * @param expedienteBean expedienteBean
	 * @param request        Request de Accenture
	 * @param response       Response de Accenture
	 * @return NotificationBean NotificationBean
	 * @throws JSONException Excepción JSONException
	 */
	public NotificationBean getNotificationBeanFromAccenture(ExpedienteBean expedienteBean, AccentureNotificationRequest request, AccentureNotificacionResponse response)
			throws JSONException {
		NotificationBean bean = new NotificationBean();
		NotificationBean2 bean2 = new NotificationBean2();
		// Configurar los parametros de cada bean
		bean2.setBean3(new NotificationBean3());
		bean.setBean2(bean2);
		bean.setIdExpediente(expedienteBean.getIdExpediente());
		bean.setRequest(manager.getJsonNotification(request));
		bean.setResponseCode(response.getCode());
		bean.setResponseMsg(response.getBody());

		return bean;
	}

	/**
	 * borrarArchivo.
	 *
	 * @param expedienteBean the expediente bean Parametro de tipo expedienteBean
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void borrarArchivo(ExpedienteBean expedienteBean) throws IOException {
		if (expedienteBean.getPathZIPExpediente() != null && !"".equals(expedienteBean.getPathZIPExpediente())) {
			Files.delete(Paths.get(expedienteBean.getPathZIPExpediente() + File.separator + expedienteBean.getNombreExpediente()));
		}
		if (expedienteBean.getPathUNZIPExpediente() != null && !"".equals(expedienteBean.getPathUNZIPExpediente())) {
			recorrerDirectorio(expedienteBean.getPathUNZIPExpediente());
		}

	}

	/**
	 * recorrerDirectorio.
	 *
	 * @param path the path Parametro para almacenar una nueva ruta
	 * @return true, if successful
	 */
	private void recorrerDirectorio(String path) {
		File folder = new File(path);
		// Obtener la lista de archivos en la ruta
		File[] listOfFiles = folder.listFiles();
		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				// Eliminar el archivo
				eliminaArchivo(listOfFiles[i]);
			} else {
				recorrerDirectorio(listOfFiles[i].getAbsolutePath());
			}
		}
		// Borrar el directorio de la ruta
		Path p = Paths.get(path);
		try {
			Files.delete(p);
		} catch (IOException e) {
			LOG.error("Erro al eliminar el directorio :: " + path, e);
		}
	}

	/**
	 * eliminaArchivo.
	 *
	 * @param file the file
	 */
	private void eliminaArchivo(File file) {
		try {
			Files.deleteIfExists(Paths.get(file.getAbsolutePath()));
		} catch (IOException e) {
			LOG.error("Error al intentar borrar el archivo: " + file.getAbsolutePath() + " Cause :: ", e);
		}
	}

}
