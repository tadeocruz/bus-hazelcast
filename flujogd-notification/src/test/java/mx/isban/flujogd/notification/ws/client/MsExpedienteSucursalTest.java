package mx.isban.flujogd.notification.ws.client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.bean.ExpedienteBeanDetalle;
import mx.isban.flujogd.common.bean.ExpedienteSucursal;
import mx.isban.flujogd.common.bean.ParametroBean;
import mx.isban.flujogd.common.exceptions.BusinessException;
import mx.isban.flujogd.common.util.ParametroEnum;
import mx.isban.flujogd.notification.bean.GenericResponse;
import mx.isban.flujogd.persistence.PersistenceManagerSubscriptor;

public class MsExpedienteSucursalTest {

	private static final String reponse = "{\r\n" + "    \"respuesta\": {\r\n" + "        \"codigo\": \"A001\",\r\n" + "        \"mensaje\": \"Consulta Exitosa.\"\r\n"
			+ "    },\r\n" + "    \"response\": {\r\n" + "        \"data\": [\r\n" + "            {\r\n" + "                \"paramsRequired\": [\r\n" + "                    {\r\n"
			+ "                        \"param\": {\r\n" + "                            \"tipo\": \"Varchar(12)\",\r\n"
			+ "                            \"validacion\": \"\\\\^([A-ZÑ&amp;]{3,4}) ?(?:- ?)?(\\\\\\\\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\\\\\\\\d|3[01])) ?(?:- ?)?([A-Z\\\\\\\\d]{2})([A\\\\\\\\d])\\\\$\",\r\n"
			+ "                            \"valordefecto\": 1,\r\n" + "                            \"displayname\": \"RFC \",\r\n"
			+ "                            \"symbolicname\": \"RFC\",\r\n" + "                            \"requerido\": 0\r\n" + "                        }\r\n"
			+ "                    },\r\n" + "                    {\r\n" + "                        \"param\": {\r\n" + "                            \"tipo\": \"Char(8)\",\r\n"
			+ "                            \"validacion\": \"^[0-9]{8}$\",\r\n" + "                            \"valordefecto\": 1,\r\n"
			+ "                            \"displayname\": \"BUC \",\r\n" + "                            \"symbolicname\": \"BUC\",\r\n"
			+ "                            \"requerido\": 1\r\n" + "                        }\r\n" + "                    },\r\n" + "                    {\r\n"
			+ "                        \"param\": {\r\n" + "                            \"tipo\": \"Varchar(25)\",\r\n"
			+ "                            \"validacion\": \"\\\\^[0-9, \\\\]\\\\{12\\\\}\\\\$\",\r\n" + "                            \"valordefecto\": 1,\r\n"
			+ "                            \"displayname\": \"CodigoBarras \",\r\n" + "                            \"symbolicname\": \"CodigoBarras\",\r\n"
			+ "                            \"requerido\": 0\r\n" + "                        }\r\n" + "                    },\r\n" + "                    {\r\n"
			+ "                        \"param\": {\r\n" + "                            \"tipo\": \"Boolean\",\r\n"
			+ "                            \"validacion\": \"\\\\^[01]\\\\$\",\r\n" + "                            \"valordefecto\": 1,\r\n"
			+ "                            \"displayname\": \"DocActualizado \",\r\n" + "                            \"symbolicname\": \"DocActualizado\",\r\n"
			+ "                            \"requerido\": 0\r\n" + "                        }\r\n" + "                    }\r\n" + "                ],\r\n"
			+ "                \"properties\": {\r\n" + "                    \"ID_QUERY\": \"PosicionFinanciera\",\r\n"
			+ "                    \"displayname\": \"INFO_FINANCIERA-Posición Financiera\",\r\n" + "                    \"requerido\": \"0\",\r\n"
			+ "                    \"nivel\": \"Apertura Cuentas - Empresa\"\r\n" + "                }\r\n" + "            }\r\n" + "        ],\r\n"
			+ "        \"responseData\": {}\r\n" + "    }\r\n" + "}\r\n" + "";

	private static final String MS_TOKEN_RESPONSE = "{" + "    \"access-token\": \"eb44fa09-9eba-4ac3-a60a-4275e1b0106d\"," + "    \"token_type\": \"bearer\","
			+ "    \"expires_in\": 599," + "    \"scope\": \"basic\"" + "}";

	@Mock
	private RestTemplate restTemplate;

	@Mock
	private PersistenceManagerSubscriptor persistenceManager;

	private MsExpedienteSucursal ws;

	@BeforeEach
	public void Setup() {
		MockitoAnnotations.initMocks(this);
		ws = new MsExpedienteSucursal(restTemplate, persistenceManager);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void cuandoSeIntentaNotificarAExpedientesSucursalesYElResultadoEsCorrectoEntoncesSeDevuelveLaListaDeValidacionesMetadato() throws BusinessException {

		when(persistenceManager.retrieveSubscriptorByIdExpediente("98124")).thenReturn("EXP_SUC");

		when(persistenceManager.retrieveParam("EXP_SUC", ParametroEnum.PARAMETRO_EXP_SUC_TOKEN_URL.getName()))
				.thenReturn(new ParametroBean(ParametroEnum.PARAMETRO_EXP_SUC_TOKEN_URL.getName(), "urlToken", "false"));

		when(persistenceManager.retrieveParam("EXP_SUC", ParametroEnum.PARAMETRO_EXP_SUC_VALIDATION_URL.getName()))
				.thenReturn(new ParametroBean(ParametroEnum.PARAMETRO_EXP_SUC_VALIDATION_URL.getName(), "urlCed", "false"));

		when(restTemplate.postForEntity(anyString(), any(), any(Class.class))).thenReturn(new ResponseEntity<String>(MS_TOKEN_RESPONSE, HttpStatus.OK));
		when(restTemplate.postForEntity(anyString(), any(HttpEntity.class), any(Class.class))).thenReturn(new ResponseEntity<>(reponse, HttpStatus.OK));

		ExpedienteSucursal expedienteSucursal = new ExpedienteSucursal();
		expedienteSucursal.setCuenta(new Long("22345678923"));
		expedienteSucursal.setAplicativo("TF");
		expedienteSucursal.setErroresDocumento(new ArrayList<>());

		ExpedienteBean expedienteBean = new ExpedienteBean();
		expedienteBean.setIdExpediente("98124");
		expedienteBean.setPathUNZIPExpediente("src/test/resources");
		expedienteBean.setNombreExpediente("TEST-001");
		expedienteBean.setDetalle(new ExpedienteBeanDetalle());
		expedienteBean.setExpedienteSucursal(expedienteSucursal);

		ResponseEntity<GenericResponse> result = ws.notificar(expedienteBean);
		assertTrue(result.getStatusCode().is2xxSuccessful());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void cuandoSeNotificaUnErrorPorExistencoaParidadYElResultadoEsCorrectoEntoncesElCodigoDeRespuestaEs2xx() throws BusinessException {

		when(persistenceManager.retrieveSubscriptorByIdExpediente("98124")).thenReturn("EXP_SUC");

		when(persistenceManager.retrieveParam("EXP_SUC", ParametroEnum.PARAMETRO_EXP_SUC_TOKEN_URL.getName()))
				.thenReturn(new ParametroBean(ParametroEnum.PARAMETRO_EXP_SUC_TOKEN_URL.getName(), "urlToken", "false"));

		when(persistenceManager.retrieveParam("EXP_SUC", ParametroEnum.PARAMETRO_EXP_SUC_VALIDATION_URL.getName()))
				.thenReturn(new ParametroBean(ParametroEnum.PARAMETRO_EXP_SUC_VALIDATION_URL.getName(), "urlCed", "false"));

		when(restTemplate.postForEntity(anyString(), any(), any(Class.class))).thenReturn(new ResponseEntity<String>(MS_TOKEN_RESPONSE, HttpStatus.OK));
		when(restTemplate.postForEntity(anyString(), any(HttpEntity.class), any(Class.class))).thenReturn(new ResponseEntity<>(reponse, HttpStatus.OK));

		ExpedienteSucursal expedienteSucursal = new ExpedienteSucursal();
		expedienteSucursal.setCuenta(new Long("22345678923"));
		expedienteSucursal.setAplicativo("TF");
		expedienteSucursal.setErrorExistenciaParidad(true);

		ExpedienteBean expedienteBean = new ExpedienteBean();
		expedienteBean.setIdExpediente("98124");
		expedienteBean.setPathUNZIPExpediente("src/test/resources");
		expedienteBean.setNombreExpediente("TEST-001");
		expedienteBean.setDetalle(new ExpedienteBeanDetalle());
		expedienteBean.setExpedienteSucursal(expedienteSucursal);

		ResponseEntity<GenericResponse> result = ws.notificar(expedienteBean);
		assertTrue(result.getStatusCode().is2xxSuccessful());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void cuandoSeIntentaNotificarAExpedientesSucursalesYOcurreUnErrorAlRecuperarElTokenEntoncesSeLanzaUnaBusinessException() throws BusinessException {

		when(persistenceManager.retrieveSubscriptorByIdExpediente("98124")).thenReturn("EXP_SUC");

		when(persistenceManager.retrieveParam("EXP_SUC", ParametroEnum.PARAMETRO_EXP_SUC_TOKEN_URL.getName()))
				.thenReturn(new ParametroBean(ParametroEnum.PARAMETRO_EXP_SUC_TOKEN_URL.getName(), "urlToken", "false"));

		when(persistenceManager.retrieveParam("EXP_SUC", ParametroEnum.PARAMETRO_EXP_SUC_VALIDATION_URL.getName()))
				.thenReturn(new ParametroBean(ParametroEnum.PARAMETRO_EXP_SUC_VALIDATION_URL.getName(), "urlCed", "false"));

		when(restTemplate.postForEntity(anyString(), any(), any(Class.class))).thenReturn(new ResponseEntity<String>("Invalid request", HttpStatus.BAD_REQUEST));
		when(restTemplate.postForEntity(anyString(), any(HttpEntity.class), any(Class.class))).thenReturn(new ResponseEntity<>(reponse, HttpStatus.OK));

		ExpedienteSucursal expedienteSucursal = new ExpedienteSucursal();
		expedienteSucursal.setCuenta(new Long("22345678923"));
		expedienteSucursal.setAplicativo("TF");
		expedienteSucursal.setErrorExistenciaParidad(true);

		ExpedienteBean expedienteBean = new ExpedienteBean();
		expedienteBean.setIdExpediente("98124");
		expedienteBean.setPathUNZIPExpediente("src/test/resources");
		expedienteBean.setNombreExpediente("TEST-001");
		expedienteBean.setDetalle(new ExpedienteBeanDetalle());
		expedienteBean.setExpedienteSucursal(expedienteSucursal);

		BusinessException e = assertThrows(BusinessException.class, () -> {
			ws.notificar(expedienteBean);
		});
		assertEquals("Invalid request", e.getMessage());
	}

}
