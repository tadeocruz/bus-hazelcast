package mx.isban.flujogd.notification.tc;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;

import com.google.gson.Gson;

import mx.isban.flujogd.common.bean.AccentureDocumentoDetalleRequest;
import mx.isban.flujogd.common.bean.AccentureDocumentoRequest;
import mx.isban.flujogd.common.bean.AccentureNotificationRequest;
import mx.isban.flujogd.common.bean.AccentureNotificacionResponse;
import mx.isban.flujogd.common.bean.AccentureTokenResponse;
import mx.isban.flujogd.common.bean.AccentureTokenRequest;
import mx.isban.flujogd.notification.json.manager.JsonManager;

public class JsonTest {
	
	private static final Logger LOG = LogManager.getLogger(JsonTest.class);

	public static void main(String[] args) throws JSONException {
		JsonTest test = new JsonTest();
//		test.getResponseToken();
//		AccentureResponseNotificacion resp = test.getResponseNotification();
//		JsonManager manager = new JsonManager();
//		String json = manager.getJsonNotification(test.getNotificationBean());
//		String json = manager.getJsonToken(test.getTokenBean());
		AccentureTokenResponse response = test.getResponseToken();
		LOG.info("done");
	}

	public AccentureNotificationRequest getNotificationBean() {
		AccentureNotificationRequest request = new AccentureNotificationRequest();
		request.setBuc("123456789");
		request.setFechaProceso("29-10-1982");
		request.setReferenciaExterna("REFEXT01");
		request.setRequestId("IDREQ-12345");
		List<AccentureDocumentoRequest> docs = new ArrayList<AccentureDocumentoRequest>();
		AccentureDocumentoRequest doc1 = new AccentureDocumentoRequest();
		AccentureDocumentoDetalleRequest detalle = new  AccentureDocumentoDetalleRequest();
		doc1.setDetalle(detalle);
		doc1.setChecksum("1e9c6be772877932eeb5f31642839c1a2b1f5270daff4c393f23313823a0bec9");
		doc1.setDocId("123456789");
		doc1.setDocNombre("IFE");
		doc1.getDetalle().setError("Some Error");
		doc1.getDetalle().setErrorId("01");
		doc1.setEstado("E01");
		doc1.getDetalle().setReenvio("False");
		docs.add(doc1);
		AccentureDocumentoRequest doc2 = new AccentureDocumentoRequest();
		AccentureDocumentoDetalleRequest detalle2 = new  AccentureDocumentoDetalleRequest();
		doc2.setDetalle(detalle2);
		doc2.setChecksum("1e9c6be772877932eeb5f31642839c1a2b1f5270daff4c393f23313823a0bec9");
		doc2.setDocId("987654321");
		doc2.setDocNombre("INE");
		doc2.getDetalle().setError("Some Error 2");
		doc2.getDetalle().setErrorId("02");
		doc2.setEstado("E02");
		doc2.getDetalle().setReenvio("False");
		docs.add(doc2);
		request.setArchivos(docs);
		return request;
	}

	public AccentureTokenRequest getTokenBean() {
		AccentureTokenRequest request = new AccentureTokenRequest();
		request.setChallenge("");
		request.setChannel("");
		request.setPassword("");
		request.setUsername("");
		request.setVersion("");
		return request;
	}
	
	public AccentureTokenResponse getResponseToken() {
		String json = "{\"access_token\":\"eyJlbmMiOiJBMTI4R0NNIiwiYWxnIjoiUlNBLU9BRVAtMjU2In0.ftokLofBKN124-rXtTKfIOMe8oyywdPPLNGBKZwmAGTi7pU6njDrLFcdgZgYdAJazauj7f_vJ6d2u2bqoTg2HPIW_Vo9_qVWME0OfKTdJmvZ_aiYL4qenKb8wPcmoKnY0BMZDB2_ZuakkuhzPeLj0J0keG50fiWwNRtClP6ELJC7XMTH5nFol8XcWsWQvhS1TH4to4Qcd4IiVc4KJ6llxjBQOJjjaJojHo32yX6MW70U-HER8ifeP0ju4-QFsI0E4hSfbn5dT8z95Zgz5rv_Ws2uDCOVErMekOEt0gQm1honGwmg0iHPIEq1yPswfJoprDw0q4cytSRlSX_tvlzFew.xuDER8rfB8suoPzt.73fRlPdVXd0PjC84p9aLODRtQMC1zTvpD_kiIXO9O3aDUnpcpzhDZQ3QXwwgw2Ctp7hG-fII_otHVV3ME2BihWAd4IhkX04nVPy7bLyoiubecj_wSIGU-XJNEmSYs3VMpN9dcHDymrraYKi8GZTdTpE1LCeR6IiXUCAuBOGeEI-sp2V_NeT8F-0IyAKCCWXHDxN9ErywmpyuJTOQ_JPFJJE5rFZvuikX9N6UX-PtJ4WLo-Mir3cVcfQoisXd3UrWS8iYzKjq84hvJ-f2mpLaFwcIrIR33donYiAliNm1UrnX9WAdtNA4ItpYBZfT26fhnZHtpzWeKVLLGa7GJCTOZC1KbVYSHWOSJ_ZDJ77IUFBFuZm0iE5bKfrBpNmMfXcIMAJyW1g-rT2lshODf1njHyhxJJHbMv3OB1Nw4Uf-rekq7-B9mn8WZLVGRq9PbMWzHnOkKgdit7S1uvN-w5EneqUxX0fX-jxXl5PCDYxIrBunlWYenMEK7ppKkLkSl8PEvkYlJQIX0vzshw3xadqeD70DJJdWtwoT4iRyXL8KF7vaNdnnnnsM-ohmbiELpY_r63De2EAdzYFLm6H8iOqPUzTDtExvGvqbA9ZH9elNWgLS_R6pLVJqCTdfjnzc-YuDrIXJrSn9Ls0e68C5S-hkLI8wxd_Zd8nWPXdLtbO-Ii_ioD26xybmhTTRpjSFxVpO5wSk__BFmqVXtN0.vf_yGpD8ycXa7iJAgkKSNA\",\"token_type\":\"bearer\",\"expires_in\":1539899802315}";
		AccentureTokenResponse token = new Gson().fromJson(json, AccentureTokenResponse.class);
		return token;
	}
	
	public AccentureNotificacionResponse getResponseNotification() {
		String json = "{\r\n" + 
				"    \"code\": 406,\r\n" + 
				"    \"errors\": [\r\n" + 
				"        \"Usuario no autenticado.\"\r\n" + 
				"    ]\r\n" + 
				"}";
		AccentureNotificacionResponse token = new Gson().fromJson(json, AccentureNotificacionResponse.class);
		return token;
	}

}
