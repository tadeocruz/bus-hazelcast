package mx.isban.flujogd.notification.step;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import mx.isban.flujogd.common.bean.AccentureNotificacionResponse;
import mx.isban.flujogd.common.bean.AccentureNotificationRequest;
import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.bean.ExpedienteBeanDetalle;
import mx.isban.flujogd.common.bean.ExpedienteBeanDetalle2;
import mx.isban.flujogd.common.bean.ExpedienteSucursal;
import mx.isban.flujogd.common.bean.NotificationBean;
import mx.isban.flujogd.notification.bean.GenericResponse;
import mx.isban.flujogd.notification.configuration.HazelcastNotificationConfig;
import mx.isban.flujogd.notification.ws.client.MsExpedienteSucursal;
import mx.isban.flujogd.notification.ws.client.WSClient;
import mx.isban.flujogd.persistence.PersistenceManagerNotificacion;

public class BatchNotificationItemProcessorTest {

	@Mock
	private HazelcastNotificationConfig hazelCastConfig;

	@Mock
	private MsExpedienteSucursal ms;
	
	@Mock
	private PersistenceManagerNotificacion pm;
	
	@Mock
	private NotificationUtils utils;
	
	@Mock
	private WSClient client;

	private BatchNotificationItemProcessor batch;

	@BeforeEach
	public void Setup() {
		MockitoAnnotations.initMocks(this);
		batch = new BatchNotificationItemProcessor(hazelCastConfig, ms, utils, client);
	}

	@Test
	public void cuandoSeNotificaAExpSucYElResultadoEsCorrectoEntoncesTerminaElFlujoConLaSiguienteColaVacia() throws Exception {

		ExpedienteSucursal expedienteSucursal = new ExpedienteSucursal();
		expedienteSucursal.setCuenta(new Long("22345678923"));
		expedienteSucursal.setAplicativo("TF");
		expedienteSucursal.setErroresDocumento(new ArrayList<>());

		ExpedienteBean expedienteBean = new ExpedienteBean();
		expedienteBean.setIdExpediente("98124");
		expedienteBean.setPathUNZIPExpediente("src/test/resources");
		expedienteBean.setNombreExpediente("TEST-001");
		expedienteBean.setDetalle(new ExpedienteBeanDetalle());
		expedienteBean.setExpedienteSucursal(expedienteSucursal);

		GenericResponse genericResponse = new GenericResponse("200","Notificación realizada correctamente", "Ok");

		ResponseEntity<GenericResponse> responseEntity = new ResponseEntity<GenericResponse>(genericResponse, HttpStatus.OK);
		when(ms.notificar(expedienteBean)).thenReturn(responseEntity );
		NotificationBean notification = new NotificationBean();
		when(utils.getNotificationBeanFromExpSuc(expedienteBean, responseEntity)).thenReturn(notification);
		when(utils.getPm()).thenReturn(pm);
		when(pm.insertOrUpdateNotification(notification)).thenReturn(true);

		ExpedienteBean result = batch.process(expedienteBean);
		assertTrue(result.getDetalle().getSiguienteQueue().isEmpty());
		verify(pm).insertOrUpdateNotification(notification);
	}
	
	@Test
	public void cuandoSeNotificaAVivereYElResultadoEsCorrectoEntoncesTerminaElFlujoConLaSiguienteColaVacia() throws Exception {

		AccentureNotificationRequest accenturRequest = new AccentureNotificationRequest();
		
		ExpedienteBeanDetalle2 detalle2 = new ExpedienteBeanDetalle2();				
		detalle2.setAccenturRequest(accenturRequest);
		
		ExpedienteBeanDetalle detalle  = new ExpedienteBeanDetalle();		
		detalle.setDetalle2(detalle2);
		
		ExpedienteBean expedienteBean = new ExpedienteBean();
		expedienteBean.setIdExpediente("98124");
		expedienteBean.setPathUNZIPExpediente("src/test/resources");
		expedienteBean.setNombreExpediente("TEST-001");

		expedienteBean.setDetalle(detalle);

		AccentureNotificacionResponse accentureResponse = new AccentureNotificacionResponse();
		accentureResponse.setCode("200");
		when(client.setNotification(accenturRequest, expedienteBean.getIdExpediente())).thenReturn(accentureResponse  );
		NotificationBean notification = new NotificationBean();
		when(utils.getNotificationBeanFromAccenture(expedienteBean, accenturRequest, accentureResponse)).thenReturn(notification);
		when(utils.getPm()).thenReturn(pm);
		when(pm.insertOrUpdateNotification(notification)).thenReturn(true);

		ExpedienteBean result = batch.process(expedienteBean);
		assertTrue(result.getDetalle().getSiguienteQueue().isEmpty());
		verify(pm).insertOrUpdateNotification(notification);
	}

}
