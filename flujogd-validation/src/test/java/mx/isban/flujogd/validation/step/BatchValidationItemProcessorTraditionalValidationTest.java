package mx.isban.flujogd.validation.step;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.IExecutorService;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.bean.ExpedienteBeanDetalle;
import mx.isban.flujogd.common.bean.ExpedienteBeanDetalle2;
import mx.isban.flujogd.common.bean.ExpedienteBeanDetalle3;
import mx.isban.flujogd.common.bean.FlujoBean;
import mx.isban.flujogd.common.bean.ParametroBean;
import mx.isban.flujogd.common.util.ExpedienteProcessTypeEnum;
import mx.isban.flujogd.common.util.ExpedienteUtil;
import mx.isban.flujogd.common.util.ParametroEnum;
import mx.isban.flujogd.common.util.QueueEnum;
import mx.isban.flujogd.common.util.XMLManager;
import mx.isban.flujogd.persistence.PersistenceManagerBloque;
import mx.isban.flujogd.persistence.PersistenceManagerBloqueDetail;
import mx.isban.flujogd.persistence.PersistenceManagerDocumento;
import mx.isban.flujogd.persistence.PersistenceManagerExpediente;
import mx.isban.flujogd.persistence.PersistenceManagerSubscriptor;
import mx.isban.flujogd.persistence.callable.BloquePersistenceTask;
import mx.isban.flujogd.persistence.callable.DocumentoPersistenceTask;
import mx.isban.flujogd.persistence.callable.ExpedientePersistenceTask;
import mx.isban.flujogd.validation.configuration.HazelcastValidationConfig;
import mx.isban.flujogd.validation.manager.ValidationManagerUtils;
import mx.isban.flujogd.validation.ws.WsCed;

public class BatchValidationItemProcessorTraditionalValidationTest {

	private static final String SUBSCRIPTOR = "VIVERE";

	@Mock
	private HazelcastValidationConfig hazelCastConfig;

	@Mock
	private ValidationProcesorObjectsContainerContainer pm;

	@Mock
	private ValidationManagerUtils utils;

	@Mock
	private PersistenceManagerExpediente pme;

	@Mock
	private PersistenceManagerDocumento pmd;

	@Mock
	private PersistenceManagerSubscriptor persistenceManagerSub;

	@Mock
	private PersistenceManagerBloqueDetail persistenceManagerValidation;

	@Mock
	private WsCed ws;

	@Mock
	private ValidationsCommon validation;

	@Mock
	private BatchValidationItemProcessorUtils validationUtils;

	@Mock
	private IExecutorService executorService;

	@Mock
	private PersistenceManagerBloque pmb;

	private BatchValidationItemProcessor procesor;

	@BeforeEach
	public void Setup() {
		MockitoAnnotations.initMocks(this);
		utils = new ValidationManagerUtils(new XMLManager(), persistenceManagerSub, persistenceManagerValidation);
		pm = new ValidationProcesorObjectsContainerContainer(pme, pmd, persistenceManagerSub, utils);
		when(hazelCastConfig.getInstanceValidation()).thenReturn(Hazelcast.newHazelcastInstance());
		validationUtils = new BatchValidationItemProcessorUtils(hazelCastConfig, pme, pmb, executorService);
		validation = new ValidationsCommon(hazelCastConfig, validationUtils, new ExpedienteUtil());
		procesor = new BatchValidationItemProcessor(hazelCastConfig, pm, new CedValidation(hazelCastConfig), executorService, validation);
	}

	@Test
	public void CuandoSeValidaUnNuevoExpedienteYEsteSeEncuentraDuplicadoEntoncesLaListaDeErroreNosEsVacia() throws Exception {

		List<String> expedienteDuplicado = new ArrayList<>();
		expedienteDuplicado.add("TEST-001");

		when(pm.getPersistenceManager().retrieveSubscriptorByIdExpediente(any(String.class))).thenReturn(SUBSCRIPTOR);
		when(pm.getPersistenceManager().retrieveParam(SUBSCRIPTOR, ParametroEnum.PARAMETRO_GRAL_VALIDAR_CED.getName()))
				.thenReturn(new ParametroBean(ParametroEnum.PARAMETRO_GRAL_VALIDAR_CED.getName(), "false", "false"));
		when(persistenceManagerSub.retrieveParam(SUBSCRIPTOR, ParametroEnum.PARAMETRO_GRAL_FILE_DESCRIPTOR.getName()))
				.thenReturn(new ParametroBean(ParametroEnum.PARAMETRO_GRAL_VALIDAR_CED.getName(), "expediente.xml", "false"));
		when(pm.getPme().validarExpediente("TEST-001")).thenReturn(expedienteDuplicado);
		when(pmd.retrieveDocumento(any(String.class), any(String.class), any(String.class))).thenReturn(null);
		when(hazelCastConfig.getInstanceValidation()).thenReturn(Hazelcast.newHazelcastInstance());
		when(pmd.insertDocumento(any(String.class), any(String.class), any(String.class), any(String.class), any(String.class))).thenReturn(true);

		when(executorService.submit(any(BloquePersistenceTask.class))).thenReturn(CompletableFuture.completedFuture(true));
		when(executorService.submit(any(ExpedientePersistenceTask.class))).thenReturn(CompletableFuture.completedFuture(true));
		when(executorService.submit(any(DocumentoPersistenceTask.class))).thenReturn(CompletableFuture.completedFuture(true));

		Map<Integer, FlujoBean> flujos = new HashMap<>();
		flujos.put(0, new FlujoBean(QueueEnum.QUEUE_VALIDATION.getName()));
		flujos.put(1, new FlujoBean(QueueEnum.QUEUE_FILENET.getName()));

		ExpedienteBeanDetalle3 detalle3 = new ExpedienteBeanDetalle3();
		detalle3.setTipoProceso(ExpedienteProcessTypeEnum.PROCESO_TYPE_NUEVO.getName());

		ExpedienteBeanDetalle2 detalle2 = new ExpedienteBeanDetalle2();
		detalle2.setDetalle3(detalle3);

		ExpedienteBeanDetalle detalle = new ExpedienteBeanDetalle();
		detalle.setFlujos(flujos);
		detalle.setDetalle2(detalle2);

		ExpedienteBean expedienteBean = new ExpedienteBean();
		expedienteBean.setIdExpediente("98124");
		expedienteBean.setPathUNZIPExpediente("src/test/resources");
		expedienteBean.setNombreExpediente("TEST-001");
		expedienteBean.setDetalle(detalle);

		ExpedienteBean result = procesor.process(expedienteBean);
		assertEquals(QueueEnum.QUEUE_NOTIFICATION.getName(), result.getDetalle().getSiguienteQueue());
	}

	@Test
	public void CuandoSeValidaUnNuevoExpedienteYEsValidoEntoncesLaListaDeErroresEsVacia() throws Exception {

		when(pm.getPersistenceManager().retrieveSubscriptorByIdExpediente(any(String.class))).thenReturn(SUBSCRIPTOR);
		when(pm.getPersistenceManager().retrieveParam(SUBSCRIPTOR, ParametroEnum.PARAMETRO_GRAL_VALIDAR_CED.getName()))
				.thenReturn(new ParametroBean(ParametroEnum.PARAMETRO_GRAL_VALIDAR_CED.getName(), "false", "false"));
		when(persistenceManagerSub.retrieveParam(SUBSCRIPTOR, ParametroEnum.PARAMETRO_GRAL_FILE_DESCRIPTOR.getName()))
				.thenReturn(new ParametroBean(ParametroEnum.PARAMETRO_GRAL_VALIDAR_CED.getName(), "expediente.xml", "false"));
		when(pm.getPme().validarExpediente("TEST-001")).thenReturn(new ArrayList<>());
		when(pmd.retrieveDocumento(any(String.class), any(String.class), any(String.class))).thenReturn(null);
		when(hazelCastConfig.getInstanceValidation()).thenReturn(Hazelcast.newHazelcastInstance());
		when(pmd.insertDocumento(any(String.class), any(String.class), any(String.class), any(String.class), any(String.class))).thenReturn(true);

		when(executorService.submit(any(BloquePersistenceTask.class))).thenReturn(CompletableFuture.completedFuture(true));
		when(executorService.submit(any(ExpedientePersistenceTask.class))).thenReturn(CompletableFuture.completedFuture(true));
		when(executorService.submit(any(DocumentoPersistenceTask.class))).thenReturn(CompletableFuture.completedFuture(true));

		Map<Integer, FlujoBean> flujos = new HashMap<>();
		flujos.put(0, new FlujoBean(QueueEnum.QUEUE_VALIDATION.getName()));
		flujos.put(1, new FlujoBean(QueueEnum.QUEUE_FILENET.getName()));

		ExpedienteBeanDetalle3 detalle3 = new ExpedienteBeanDetalle3();
		detalle3.setTipoProceso(ExpedienteProcessTypeEnum.PROCESO_TYPE_NUEVO.getName());

		ExpedienteBeanDetalle2 detalle2 = new ExpedienteBeanDetalle2();
		detalle2.setDetalle3(detalle3);

		ExpedienteBeanDetalle detalle = new ExpedienteBeanDetalle();
		detalle.setFlujos(flujos);
		detalle.setDetalle2(detalle2);

		ExpedienteBean expedienteBean = new ExpedienteBean();
		expedienteBean.setIdExpediente("98124");
		expedienteBean.setPathUNZIPExpediente("src/test/resources");
		expedienteBean.setNombreExpediente("TEST-001");
		expedienteBean.setDetalle(detalle);

		ExpedienteBean result = procesor.process(expedienteBean);
		assertTrue(!result.getDetalle().getSiguienteQueue().isEmpty());
	}

	@Test
	public void CuandoSeValidaUnNuevoExpedienteYAlMenosUnDocumentoPdfNoExisteEntonces() throws Exception {

		when(pm.getPersistenceManager().retrieveSubscriptorByIdExpediente(any(String.class))).thenReturn(SUBSCRIPTOR);
		when(pm.getPersistenceManager().retrieveParam(SUBSCRIPTOR, ParametroEnum.PARAMETRO_GRAL_VALIDAR_CED.getName()))
				.thenReturn(new ParametroBean(ParametroEnum.PARAMETRO_GRAL_VALIDAR_CED.getName(), "false", "false"));
		when(persistenceManagerSub.retrieveParam(SUBSCRIPTOR, ParametroEnum.PARAMETRO_GRAL_FILE_DESCRIPTOR.getName()))
				.thenReturn(new ParametroBean(ParametroEnum.PARAMETRO_GRAL_VALIDAR_CED.getName(), "expediente-no-pdf.xml", "false"));
		when(pm.getPme().validarExpediente(any(String.class))).thenReturn(new ArrayList<>());
		when(pmd.retrieveDocumento(any(String.class), any(String.class), any(String.class))).thenReturn(null);
		when(hazelCastConfig.getInstanceValidation()).thenReturn(Hazelcast.newHazelcastInstance());
		when(pmd.insertDocumento(any(String.class), any(String.class), any(String.class), any(String.class), any(String.class))).thenReturn(true);

		when(executorService.submit(any(BloquePersistenceTask.class))).thenReturn(CompletableFuture.completedFuture(true));
		when(executorService.submit(any(ExpedientePersistenceTask.class))).thenReturn(CompletableFuture.completedFuture(true));
		when(executorService.submit(any(DocumentoPersistenceTask.class))).thenReturn(CompletableFuture.completedFuture(true));
		
		List<ParametroBean> parametrosBloque = new ArrayList<>();
		parametrosBloque.add(new ParametroBean(ParametroEnum.PARAMETRO_GRAL_VALIDAR_CED.getName(), "src/test/resources", "false"));
		when(persistenceManagerValidation.retrieveParametroBloque("98124",QueueEnum.QUEUE_SFTP.getName(), ParametroEnum.PARAMETRO_SFTP_OUTPUT_PATH.getName()))
		.thenReturn(parametrosBloque );

		Map<Integer, FlujoBean> flujos = new HashMap<>();
		flujos.put(0, new FlujoBean(QueueEnum.QUEUE_VALIDATION.getName()));
		flujos.put(1, new FlujoBean(QueueEnum.QUEUE_FILENET.getName()));

		ExpedienteBeanDetalle3 detalle3 = new ExpedienteBeanDetalle3();
		detalle3.setTipoProceso(ExpedienteProcessTypeEnum.PROCESO_TYPE_NUEVO.getName());

		ExpedienteBeanDetalle2 detalle2 = new ExpedienteBeanDetalle2();
		detalle2.setDetalle3(detalle3);

		ExpedienteBeanDetalle detalle = new ExpedienteBeanDetalle();
		detalle.setFlujos(flujos);
		detalle.setDetalle2(detalle2);

		ExpedienteBean expedienteBean = new ExpedienteBean();
		expedienteBean.setIdExpediente("98124");
		expedienteBean.setNombreExpediente(".zip");
		expedienteBean.setDetalle(detalle);

		ExpedienteBean result = procesor.process(expedienteBean);
		assertEquals(QueueEnum.QUEUE_NOTIFICATION.getName(), result.getDetalle().getSiguienteQueue());
	}

}
