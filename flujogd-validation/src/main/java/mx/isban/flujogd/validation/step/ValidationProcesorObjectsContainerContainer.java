package mx.isban.flujogd.validation.step;

import lombok.Getter;
import mx.isban.flujogd.persistence.PersistenceManagerDocumento;
import mx.isban.flujogd.persistence.PersistenceManagerExpediente;
import mx.isban.flujogd.persistence.PersistenceManagerSubscriptor;
import mx.isban.flujogd.persistence.impl.PersistenceManagerDocumentoImpl;
import mx.isban.flujogd.persistence.impl.PersistenceManagerExpedienteImpl;
import mx.isban.flujogd.persistence.impl.PersistenceManagerSubscriptorImpl;
import mx.isban.flujogd.validation.manager.ValidationManagerUtils;

/**
 * Clase con los objetos necesarios para realizar la persistencia en base de
 * datos
 * 
 * @author DELL
 *
 */
@Getter
public class ValidationProcesorObjectsContainerContainer {

	/**
	 * PersistenceManagerExpediente
	 */
	private PersistenceManagerExpediente pme;

	/**
	 * PersistenceManagerDocumento
	 */
	private PersistenceManagerDocumento pmd;

	/**
	 * PersistenceManagerSubscriptor
	 */
	private PersistenceManagerSubscriptor persistenceManager;
	
	/**
	 * utils
	 */
	private ValidationManagerUtils utils;
	
	/**
	 * Default constructor
	 */
	public ValidationProcesorObjectsContainerContainer() {
		pme = new PersistenceManagerExpedienteImpl();
		pmd = new PersistenceManagerDocumentoImpl();
		persistenceManager = new PersistenceManagerSubscriptorImpl();
		utils = new ValidationManagerUtils();
	}

	/**
	 * Constructor con todos los parámetros
	 * @param pme PersistenceManagerExpediente
	 * @param pmd PersistenceManagerDocumento
	 * @param persistenceManager PersistenceManagerSubscriptor
	 * @param utils           validationManager utils
	 */
	public ValidationProcesorObjectsContainerContainer(PersistenceManagerExpediente pme, PersistenceManagerDocumento pmd, PersistenceManagerSubscriptor persistenceManager, ValidationManagerUtils utils) {
		super();
		this.pme = pme;
		this.pmd = pmd;
		this.persistenceManager = persistenceManager;
		this.utils = utils;
	}
	
	

}
