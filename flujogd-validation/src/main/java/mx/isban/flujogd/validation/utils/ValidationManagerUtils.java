package mx.isban.flujogd.validation.utils;

import java.io.File;
import java.util.List;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.bean.ParametroBean;
import mx.isban.flujogd.common.bean.XMLManagerResponse;
import mx.isban.flujogd.common.util.ParametroEnum;
import mx.isban.flujogd.common.util.QueueEnum;
import mx.isban.flujogd.common.util.XMLManager;
import mx.isban.flujogd.persistence.PersistenceManagerBloqueDetail;
import mx.isban.flujogd.persistence.PersistenceManagerSubscriptor;
import mx.isban.flujogd.persistence.impl.PersistenceManagerBloqueDetailImpl;
import mx.isban.flujogd.persistence.impl.PersistenceManagerSubscriptorImpl;

/**
 * Clase de utileria para validar las rutas de los expedientes The Class
 * ValidationManagerUtils.
 */
public class ValidationManagerUtils {

	/**
	 * XMLManager
	 */
	private XMLManager xmlManager = new XMLManager();

	/**
	 * PersistenceManagerSubscriptor
	 */
	private PersistenceManagerSubscriptor persistenceManagerSub = new PersistenceManagerSubscriptorImpl();

	/**
	 * PersistenceManagerBloqueDetail
	 */
	private PersistenceManagerBloqueDetail persistenceManagerValidation = new PersistenceManagerBloqueDetailImpl();

	/**
	 * Default constructor
	 */
	public ValidationManagerUtils() {
	}

	/**
	 * Constructor con todos los parámetros
	 * 
	 * @param xmlManager                   manipulador de xmls
	 * @param persistenceManagerSub        objeto para la persistencia de
	 *                                     subscriptores
	 * @param persistenceManagerValidation objeto para la persistencia relacionada
	 *                                     con la validación
	 */
	public ValidationManagerUtils(XMLManager xmlManager, PersistenceManagerSubscriptor persistenceManagerSub, PersistenceManagerBloqueDetail persistenceManagerValidation) {
		super();
		this.xmlManager = xmlManager;
		this.persistenceManagerSub = persistenceManagerSub;
		this.persistenceManagerValidation = persistenceManagerValidation;
	}

	/**
	 * Metodo para crear la ruta a depositar el expediente Crear path.
	 *
	 * @param expedienteBean the expediente bean
	 * @return pathUnzip for exp
	 */
	public String crearPath(ExpedienteBean expedienteBean) {
		List<ParametroBean> listSftpOutputPathByIdExpediente = persistenceManagerValidation.retrieveParametroBloque(expedienteBean.getIdExpediente(),
				QueueEnum.QUEUE_SFTP.getName(), ParametroEnum.PARAMETRO_SFTP_OUTPUT_PATH.getName());
		expedienteBean.setPathZIPExpediente(listSftpOutputPathByIdExpediente.get(0).getValorParametro());
		// Generar el path para archivo .zip
		String pathUnzip = expedienteBean.getPathZIPExpediente() + File.separator + expedienteBean.getNombreExpediente();
		return pathUnzip.substring(0, pathUnzip.length() - 4);
	}

	/**
	 * Metodo para obteber los expedientes en .xml Recuperar documentos.
	 *
	 * @param expedienteBean the expediente bean
	 * @return the XML manager response
	 */
	public XMLManagerResponse recuperarDocumentos(ExpedienteBean expedienteBean) {
		String nombreSubscriptor = persistenceManagerSub.retrieveSubscriptorByIdExpediente(expedienteBean.getIdExpediente());
		ParametroBean hazelcastPathParam = persistenceManagerSub.retrieveParam(nombreSubscriptor, ParametroEnum.PARAMETRO_GRAL_FILE_DESCRIPTOR.getName());
		return xmlManager.getDocumentos(expedienteBean.getPathUNZIPExpediente(), hazelcastPathParam.getValorParametro());
	}

}
