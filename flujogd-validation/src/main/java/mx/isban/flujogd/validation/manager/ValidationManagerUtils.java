package mx.isban.flujogd.validation.manager;

import java.io.File;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.bean.ParametroBean;
import mx.isban.flujogd.common.bean.XMLManagerResponse;
import mx.isban.flujogd.common.util.ParametroEnum;
import mx.isban.flujogd.common.util.QueueEnum;
import mx.isban.flujogd.common.util.XMLManager;
import mx.isban.flujogd.persistence.PersistenceManagerBloqueDetail;
import mx.isban.flujogd.persistence.PersistenceManagerSubscriptor;
import mx.isban.flujogd.persistence.impl.PersistenceManagerBloqueDetailImpl;
import mx.isban.flujogd.persistence.impl.PersistenceManagerSubscriptorImpl;

/**
 * Clase de utileria para validar las rutas de los expedientes
 * The Class ValidationManagerUtils.
 */
public class ValidationManagerUtils {
	
	/**
	 * Metodo para crear la ruta a depositar el expediente
	 * Crear path.
	 *
	 * @param expedienteBean the expediente bean
	 */
	public void crearPath(ExpedienteBean expedienteBean) {
		PersistenceManagerBloqueDetail persistenceManagerValidation = new PersistenceManagerBloqueDetailImpl();
		expedienteBean.setPathZIPExpediente(persistenceManagerValidation.retrieveParametroBloque(expedienteBean.getIdExpediente(),  QueueEnum.QUEUE_SFTP.getName(), ParametroEnum.PARAMETRO_SFTP_OUTPUT_PATH.getName()).get(0).getValorParametro());
		// Generar el path para archivo .zip
		String pathUnzip = expedienteBean.getPathZIPExpediente()+File.separator+expedienteBean.getNombreExpediente();
		pathUnzip = pathUnzip.substring(0, pathUnzip.length()-4);
		expedienteBean.setPathUNZIPExpediente(pathUnzip);
	}
	
	/**
	 * Metodo para obteber los expedientes en .xml
	 * Recuperar documentos.
	 *
	 * @param expedienteBean the expediente bean
	 * @return the XML manager response
	 */
	public XMLManagerResponse recuperarDocumentos(ExpedienteBean expedienteBean) {
		XMLManager xmlManager = new XMLManager();
		PersistenceManagerSubscriptor persistenceManagerValidation = new PersistenceManagerSubscriptorImpl();
		String nombreSubscriptor = persistenceManagerValidation.retrieveSubscriptorByIdExpediente(expedienteBean.getIdExpediente());
		ParametroBean hazelcastPathParam = persistenceManagerValidation.retrieveParam(nombreSubscriptor, ParametroEnum.PARAMETRO_GRAL_FILE_DESCRIPTOR.getName());
		return xmlManager.getDocumentos(expedienteBean.getPathUNZIPExpediente(),hazelcastPathParam.getValorParametro());
	}

}
