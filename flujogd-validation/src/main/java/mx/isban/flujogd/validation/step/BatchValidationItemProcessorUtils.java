package mx.isban.flujogd.validation.step;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hazelcast.core.IExecutorService;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.bean.NotificationBean;
import mx.isban.flujogd.common.bean.NotificationBean2;
import mx.isban.flujogd.common.bean.NotificationBean3;
import mx.isban.flujogd.common.util.ExpedienteStatusEnum;
import mx.isban.flujogd.common.util.NotificationManager;
import mx.isban.flujogd.persistence.PersistenceManagerExpediente;
import mx.isban.flujogd.persistence.callable.BloquePersistenceTask;
import mx.isban.flujogd.persistence.callable.ExpedientePersistenceTask;
import mx.isban.flujogd.persistence.callable.NotificacionPersistenceTask;
import mx.isban.flujogd.persistence.impl.PersistenceManagerExpedienteImpl;
import mx.isban.flujogd.validation.configuration.HazelcastValidationConfig;

/**
 * The Class BatchValidationItemProcessorUtils.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase BatchValidationItemProcessorUtils
 */
public class BatchValidationItemProcessorUtils{

	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(BatchValidationItemProcessorUtils.class);

	/** The Constant EXECUTOR_SERVICE. */
	private static final String EXECUTOR_SERVICE  = "executorService";

	/**
	 * Metodo que realiza la actualizacion del estatus para los expedientes
	 * updateStausBloqueExpediente.
	 *
	 * @param hazelcastValidationConfig the hazelcast validation config
	 * @param expedienteBean the expediente bean
	 * @param statusBloque the status bloque
	 * @param queueName the queue name
	 * @param nombreError the nombre error
	 * @return true, if successful
	 */
	public boolean updateStausBloqueExpediente(HazelcastValidationConfig hazelcastValidationConfig,ExpedienteBean expedienteBean, String statusBloque, String queueName, String nombreError) {
		PersistenceManagerExpediente persistence = new PersistenceManagerExpedienteImpl();
		IExecutorService executorService = hazelcastValidationConfig.getInstanceValidation().getExecutorService( EXECUTOR_SERVICE );
		Future<Object> future = executorService.submit( new BloquePersistenceTask(expedienteBean.getIdExpediente(), statusBloque, queueName,nombreError) );
		boolean ok = false;
		try {
			ok = (Boolean)future.get(); 
			if(ok){
				String estatusExpediente = ExpedienteStatusEnum.STATUS_ARCHIVO_VALIDO.getName();
				if(nombreError!=null) {
					estatusExpediente = ocurrioUnError(hazelcastValidationConfig,persistence, expedienteBean, queueName, nombreError);
				}
				ok = updateStausExpediente(hazelcastValidationConfig,expedienteBean.getIdExpediente(), estatusExpediente, nombreError);
			}
		} catch (Exception e) {
			LOG.error(e);
		}
		return ok;
	}

	/**
	 * Metodo para actuliar el estatus del expediente
	 * updateStausExpediente.
	 *
	 * @param hazelcastValidationConfig the hazelcast validation config
	 * @param idExpediente the id expediente
	 * @param status the status
	 * @param idError the id error
	 * @return true, if successful
	 */
	private boolean updateStausExpediente(HazelcastValidationConfig hazelcastValidationConfig,String idExpediente, String status, String idError) {
		IExecutorService executorServiceValidation = hazelcastValidationConfig.getInstanceValidation().getExecutorService( EXECUTOR_SERVICE );
		Future<Object> future = executorServiceValidation.submit( new ExpedientePersistenceTask(idExpediente, status, idError) );
		boolean okValidation = false;
		try {
			okValidation = (Boolean)future.get();
		} catch (InterruptedException e) {
			LOG.error(e);
			Thread.currentThread().interrupt();
		} catch (ExecutionException e) {
			LOG.error(e);
		}
		return okValidation;
	}

	/**
	 * Metodo para lanzar la queue de notificacion de expedientes
	 * insertNotificacion.
	 *
	 * @param hazelcastValidationConfig the hazelcast validation config
	 * @param expedienteBean the expediente bean
	 * @param nombreError the nombre error
	 * @return true, if successful
	 */
	private boolean insertNotificacion(HazelcastValidationConfig hazelcastValidationConfig,ExpedienteBean expedienteBeanValidation, String nombreError) {
		IExecutorService executorServiceValidation = hazelcastValidationConfig.getInstanceValidation().getExecutorService( EXECUTOR_SERVICE );
		NotificationBean bean = new NotificationBean();
		NotificationBean2 bean2 = new NotificationBean2();
		NotificationBean3 bean3 = new NotificationBean3();
		bean2.setBean3(bean3);
		bean.setBean2(bean2);
		bean.setIdExpediente(expedienteBeanValidation.getIdExpediente());
		bean.setRequest(null);
		bean.setResponseCode(null);
		bean.setResponseMsg(null);
		bean2.setRefExterna(null);
		bean2.setReqId(null);
		bean2.setErrorId(nombreError);
		bean2.setErrorMsg(nombreError);
		bean3.setBuc(null);
		bean3.setPathZip(expedienteBeanValidation.getPathZIPExpediente());
		bean3.setFileName(expedienteBeanValidation.getNombreExpediente());
		bean3.setPathUnzip(expedienteBeanValidation.getPathUNZIPExpediente());
		Future<Object> future = executorServiceValidation.submit( new NotificacionPersistenceTask(bean) );
		boolean okValidation = false;
		try {
			okValidation = (Boolean)future.get();
		} catch (InterruptedException e) {
			LOG.error(e);
			Thread.currentThread().interrupt();
		} catch (ExecutionException e) {
			LOG.error(e);
		}
		return okValidation;
	}

	/**
	 * Metodo para manejar los errores en el procesamiento de la validacion de los expedientes
	 * ocurrioUnError.
	 *
	 * @param hazelcastValidationConfig the hazelcast validation config
	 * @param persistence the persistence
	 * @param expedienteBean the expediente bean
	 * @param queueName the queue name
	 * @param nombreError the nombre error
	 * @return the string
	 */
	private String ocurrioUnError(HazelcastValidationConfig hazelcastValidationConfig,PersistenceManagerExpediente persistence,ExpedienteBean expedienteBean,String queueName,String nombreError) {
		String estatusExpedienteValidation = ExpedienteStatusEnum.STATUS_ERROR_VALIDACION.getName();
		//Nueva validacion para numero de reintentos y envio de notificaciones
		Integer intentosValidation = persistence.retrieveIntentosTipoError(expedienteBean.getIdExpediente(), queueName, nombreError);
		Integer reintentosPermitidos = persistence.retrieveReintentosByNombreError(nombreError);
		if(intentosValidation>=reintentosPermitidos) {
			estatusExpedienteValidation = ExpedienteStatusEnum.STATUS_PROCESO_CANCELADO.getName();
		}
		NotificationManager notification = new NotificationManager();
		notification.preparaEnvioNotificacion(expedienteBean, nombreError, nombreError,expedienteBean.getNombreExpediente().substring(0, expedienteBean.getNombreExpediente().length()-4));
		insertNotificacion(hazelcastValidationConfig,expedienteBean,nombreError);
		return estatusExpedienteValidation;
	}

}
