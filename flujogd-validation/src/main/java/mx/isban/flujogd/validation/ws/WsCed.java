package mx.isban.flujogd.validation.ws;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.SSLContext;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.TrustStrategy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import mx.isban.flujogd.common.bean.ParametroBean;
import mx.isban.flujogd.common.bean.ValidacionMetadato;
import mx.isban.flujogd.common.exceptions.BusinessException;
import mx.isban.flujogd.common.util.ParametroEnum;
import mx.isban.flujogd.persistence.PersistenceManagerSubscriptor;
import mx.isban.flujogd.persistence.impl.PersistenceManagerSubscriptorImpl;
import mx.isban.flujogd.validation.bean.CedParamsBean;

/**
 * Clase WsCed para comunicarse con el servicio de Ced el cuál se encarga de
 * proporcionar la información necesaria para validar los metadatos
 * 
 * @author DELL
 *
 */
public class WsCed {
	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(WsCed.class);

	/**
	 * objeto para realizar parseo
	 */
	private JSONParser parser = new JSONParser();

	/**
	 * objeto para realizar las peticiones a servicios rest
	 */
	private RestTemplate restTemplate;

	/**
	 * Objeto de persistencia en base de datos, utilizado para recuperar la url del
	 * token y la url del ced
	 */
	private PersistenceManagerSubscriptor persistenceManager = new PersistenceManagerSubscriptorImpl();

	/**
	 * Constructor Por default
	 */
	public WsCed() {
		restTemplate = getRestTemplate();
	}

	/**
	 * Constructor con lo necesario para manejar el restTemplate y el persistence
	 * manager
	 * 
	 * @param restTemplate       resttemplate object
	 * @param persistenceManager persistenceManager object
	 */
	public WsCed(RestTemplate restTemplate, PersistenceManagerSubscriptor persistenceManager) {
		super();
		this.restTemplate = restTemplate;
		this.persistenceManager = persistenceManager;
	}

	/**
	 * Método para obtener un resttemplate que no valida el certificado Ssl(por el
	 * tema de que se utilizan certificados autofirmados en lugar de los expedidos
	 * por una tienda autorizada)
	 * 
	 * @return RestTemplate
	 */
	private RestTemplate getRestTemplate() {
		TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;

		SSLContext sslContext;
		try {
			sslContext = org.apache.http.ssl.SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
			SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);

			CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(csf).build();

			HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();

			requestFactory.setHttpClient(httpClient);

			return new RestTemplate(requestFactory);
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException e) {
			LOG.error(e.getMessage(), e);
			return new RestTemplate();
		}
	}

	/**
	 * Método para obtener la lista de validación de metadatos correspondiente al
	 * nombre de subscriptor y tipo documental proporcionado
	 * 
	 * @param documentalTipe    tipo documental del que deseamos obtener las
	 *                          validaciones
	 * @param nombreSubscriptor nombre del subscriptor
	 * @return List<ValidacionMetadato> lista de validaciones de metadatos
	 * @throws ParseException    en caso de ocurrir un error al parsear la respuesta
	 *                           del ced
	 * @throws BusinessException en caso de que la respuesta del ced no sea un
	 *                           código ok
	 */
	public List<ValidacionMetadato> getParamsFromCedByDocumentalTipe(String documentalTipe, String nombreSubscriptor) throws BusinessException {
		JSONArray metadataParams;
		try {
			metadataParams = getJsonArrayParamsFromCedByDocumentalTipe(documentalTipe, nombreSubscriptor);
		} catch (ParseException | ClassCastException e) {
			throw new BusinessException("ocurrió un error al recuperar las validaciones del ced", e);
		}

		List<ValidacionMetadato> list = new ArrayList<>();
		for (int i = 0; i < metadataParams.size(); i++) {
			JSONObject objParam = (JSONObject) metadataParams.get(i);
			JSONObject metaValid = (JSONObject) objParam.get("param");
			list.add(new ValidacionMetadato(metaValid.get("tipo").toString(), metaValid.get("validacion").toString(), Boolean.parseBoolean(metaValid.get("isRequerido").toString()),
					metaValid.get("symbolicName").toString()));
		}
		return list;
	}

	/**
	 * Método para obtener los parámetros necesarios para realizar la consulta al
	 * servicio ced
	 * 
	 * @param subscriptor
	 * @return
	 */
	private CedParamsBean getCedParams(String subscriptor) {
		ParametroBean urlCed = persistenceManager.retrieveParam(subscriptor, ParametroEnum.PARAMETRO_CED_VALIDATION_URL.getName());
		ParametroBean canalCed = persistenceManager.retrieveParam(subscriptor, ParametroEnum.PARAMETRO_CED_CANAL.getName());
		ParametroBean appCed = persistenceManager.retrieveParam(subscriptor, ParametroEnum.PARAMETRO_CED_APP.getName());
		ParametroBean nivelCed = persistenceManager.retrieveParam(subscriptor, ParametroEnum.PARAMETRO_CED_NIVEL.getName());
		ParametroBean productoCed = persistenceManager.retrieveParam(subscriptor, ParametroEnum.PARAMETRO_CED_PRODUCTO.getName());
		ParametroBean tipoConsultaCed = persistenceManager.retrieveParam(subscriptor, ParametroEnum.PARAMETRO_CED_TIPO_CONSULTA.getName());
		ParametroBean tipoOperacionCed = persistenceManager.retrieveParam(subscriptor, ParametroEnum.PARAMETRO_CED_TIPO_OPERACION.getName());

		CedParamsBean params = new CedParamsBean();
		params.setUrl(urlCed.getValorParametro());
		params.setCanal(canalCed.getValorParametro());
		params.setApp(appCed.getValorParametro());
		if (nivelCed.getValorParametro() != null) {
			params.setNivel(nivelCed.getValorParametro());
		} else {
			params.setNivel("");
		}
		params.setProducto(productoCed.getValorParametro());
		params.setTipoConsulta(tipoConsultaCed.getValorParametro());
		params.setTipoOperacion(tipoOperacionCed.getValorParametro());

		return params;
	}

	/**
	 * Método para obtener el array de parámetros de validación desde el CED, este
	 * método se encarga de consumir el ws del ced por medio del request que debe
	 * contener múltiples parámetros pero el que principalmente necesitamos es el
	 * tipoDocumental, al cuál pertenecen los metadatos
	 * 
	 * @param documentalTipe tipo documental por el cuál se solicita los parámetros
	 *                       de validación
	 * 
	 * @return JsonArray con los parámetros de validación
	 * @throws ParseException    en caso de ocurrir un error al parsear la respuesta
	 *                           del ced
	 * @throws BusinessException en caso de que la respuesta del ced no sea un
	 *                           código ok
	 */
	private JSONArray getJsonArrayParamsFromCedByDocumentalTipe(String documentalTipe, String nombreSubscriptor) throws BusinessException, ParseException {

		CedParamsBean params = getCedParams(nombreSubscriptor);

		HashMap<String, String> bodyRequest = new HashMap<>();
		bodyRequest.put("appOrigen", params.getApp());
		bodyRequest.put("canal", params.getCanal());
		bodyRequest.put("nivel", params.getNivel());
		bodyRequest.put("producto", params.getProducto());
		bodyRequest.put("tipoConsulta", params.getTipoConsulta());
		bodyRequest.put("tipoOper", params.getTipoOperacion());
		bodyRequest.put("tipoDocumento", documentalTipe);

		ResponseEntity<String> response = restTemplate.postForEntity(params.getUrl(), bodyRequest, String.class);
		if (response.getStatusCode().is2xxSuccessful()) {
			JSONObject jsonObject = (JSONObject) parser.parse(response.getBody());
			LOG.debug(jsonObject.toString());
			JSONObject remoteResponse = (JSONObject) jsonObject.get("response");
			JSONArray data = (JSONArray) remoteResponse.get("data");
			if (data.isEmpty()) {
				LOG.warn("No se encontró resultados del ced para el tipo documental: " + documentalTipe);
				return new JSONArray();
			}
			JSONObject paramsRequired = (JSONObject) data.get(0);
			return (JSONArray) paramsRequired.get("paramsRequired");
		}
		throw new BusinessException(response.getBody());
	}

}
