package mx.isban.flujogd.validation.listener;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hazelcast.core.MemberAttributeEvent;
import com.hazelcast.core.MembershipEvent;
import com.hazelcast.core.MembershipListener;

import mx.isban.flujogd.common.bean.MemberBean;
import mx.isban.flujogd.common.bean.NetConfigBean;
import mx.isban.flujogd.common.util.InetAddressUtil;
import mx.isban.flujogd.common.util.MapEnum;
import mx.isban.flujogd.persistence.PersistenceManagerSubscriptor;
import mx.isban.flujogd.persistence.impl.PersistenceManagerSubscriptorImpl;
import mx.isban.flujogd.validation.configuration.HazelcastValidationConfig;

/**
 * The listener interface for receiving clusterMembership events.
 * The class that is interested in processing a clusterMembership
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addClusterMembershipListener<code> method. When
 * the clusterMembership event occurs, that object's appropriate
 * method is invoked.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase ClusterMembershipListener
 */
public class ClusterMembershipListener implements MembershipListener{
	
	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(ClusterMembershipListener.class);
	
	/** nombreSubscriptor. */
	private static String nombreSubscriptor;
	
	/** componente. */
	private static String componente;
	
	/** hazelcastConfig. */
	private static HazelcastValidationConfig hazelcastConfig;

	/**
	 * memberAdded.
	 *
	 * @param membershipEvent the membership event
	 */
	@Override
	public void memberAdded(MembershipEvent membershipEvent) {
		String uuid = membershipEvent.getMember().getUuid();
		String uuidLocal = membershipEvent.getCluster().getLocalMember().getUuid();
		LOG.info("Se grego un nuevo miembro: "+uuid);
		if(uuid.equals(uuidLocal)) {
			LOG.info("flujogd-validation: "+uuid);
			notifyMemberAdd(uuid);
		}
	}

	/**
	 * memberRemoved.
	 *
	 * @param membershipEvent the membership event
	 */
	@Override
	public void memberRemoved(MembershipEvent membershipEvent) {
		LOG.info("Se elimino el miembro: "+membershipEvent.getMember().getUuid());
		notifyMemberRemoved(membershipEvent.getMember().getUuid());
	}
	
	/**
	 * Notifica al cluster que flujo fue agregado.
	 *
	 * @param uuid the uuid
	 */
	private void notifyMemberAdd(String uuid) {
		LOG.info("Notificamos al cluster que flujo fue agregado");
		PersistenceManagerSubscriptor persistenceManagerValidation = new PersistenceManagerSubscriptorImpl();
		InetAddressUtil inetUtilValidation = new InetAddressUtil();
		String ipNetValidation = inetUtilValidation.getLocalNet();
		if(!"".equals(ipNetValidation)) {
			//Configurar el bean con los datos del subscriptor
			NetConfigBean netConfigBeanValidation = persistenceManagerValidation.retrieveNetConfigByIp(nombreSubscriptor,ipNetValidation);
			if(netConfigBeanValidation!=null){
				Map<String, MemberBean> members = hazelcastConfig.getMapMembers(MapEnum.MAP_MEMBER_STATUS.getName());
				MemberBean memberValidation = new MemberBean();
				memberValidation.setActive(true);
				memberValidation.setIp(netConfigBeanValidation.getIp());
				memberValidation.setPort(netConfigBeanValidation.getPort());
				memberValidation.setName(componente);
				memberValidation.setUuid(uuid);
				members.put(uuid, memberValidation);
				LOG.info("La notificacion se realizo con exito");
			}
		}else {
			LOG.info("Ocurrio un problema al recuperar la IP de este servidor, sin ella no es posible notificar la baja del nodo");
		}
	}

	/**
	 * Notifica al cluster que flujo se dio de baja.
	 *
	 * @param uuid the uuid
	 */
	private void notifyMemberRemoved(String uuid) {
		LOG.info("Notificamos al cluster que flujo se dio de baja");
		PersistenceManagerSubscriptor persistenceManagerValidation = new PersistenceManagerSubscriptorImpl();
		InetAddressUtil inetUtilValidation = new InetAddressUtil();
		String ipNetValidation = inetUtilValidation.getLocalNet();
		if(!"".equals(ipNetValidation)) {
			//Configurar el bean con los datos del subscriptor
			NetConfigBean netConfigBeanValidation = persistenceManagerValidation.retrieveNetConfigByIp(nombreSubscriptor,ipNetValidation);
			if(netConfigBeanValidation!=null){
				Map<String, MemberBean> members = hazelcastConfig.getMapMembers(MapEnum.MAP_MEMBER_STATUS.getName());
				MemberBean memberValidation = members.get(uuid);
				if(memberValidation!=null) {
					memberValidation.setActive(false);
					members.replace(uuid, memberValidation);
				}
				LOG.info("La notificacion se realizo con exito");
			}
		}else {
			LOG.info("Ocurrio un problema al recuperar la IP de este servidor, sin ella no es posible notificar la baja del nodo");
		}
	}

	/**
	 * memberAttributeChanged.
	 *
	 * @param memberAttributeEvent the member attribute event
	 */
	@Override
	public void memberAttributeChanged(MemberAttributeEvent memberAttributeEvent) {
		LOG.info("Cambio un atributo de uno de los miembros");
	}

	/**
	 * Sets the nombre subscriptor.
	 *
	 * @param nombreSubscriptor the nombreSubscriptor to set
	 */
	public static void setNombreSubscriptorValidation(String nombreSubscriptor) {
		ClusterMembershipListener.nombreSubscriptor = nombreSubscriptor;
	}

	/**
	 * Sets the componente.
	 *
	 * @param componente the componente to set
	 */
	public static void setComponenteValidation(String componente) {
		ClusterMembershipListener.componente = componente;
	}

	/**
	 * Sets the hazelcast config.
	 *
	 * @param hazelcastConfig the hazelcastConfig to set
	 */
	public static void setHazelcastConfig(HazelcastValidationConfig hazelcastConfig) {
		ClusterMembershipListener.hazelcastConfig = hazelcastConfig;
	}

}
