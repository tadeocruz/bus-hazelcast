package mx.isban.flujogd.validation.trigger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Clase que ejecutara las tareas de Validation
 * The Class ExecuteJobValidationTrigger.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase ExecuteJobValidationTrigger
 */
@Component
public class ExecuteJobValidationTrigger {
	
	/** Inicializar un log para guardar la bitacora */
	private static final Logger LOG = LogManager.getLogger(ExecuteJobValidationTrigger.class);
	
	/** Crear un job launcher */
	@Autowired
	private JobLauncher jobLauncherValidation;
	
	/** Crear un job */
	@Autowired
	private Job processJobValidation;

	/**
	 * Ejecuta el job batch.
	 */
	public void executeJobValidation() {
		//Configurar los parametros del job
		try {
			JobParameters jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis()).toJobParameters();
			jobLauncherValidation.run(processJobValidation, jobParameters);
		} catch (JobParametersInvalidException | JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException e) {
			LOG.error(e);
		}
	}

}
