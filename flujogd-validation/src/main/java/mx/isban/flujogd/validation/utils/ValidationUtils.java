package mx.isban.flujogd.validation.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.hazelcast.core.IExecutorService;

import mx.isban.flujogd.common.bean.DocumentoBean;
import mx.isban.flujogd.common.bean.DocumentoBeanDetalle;
import mx.isban.flujogd.common.bean.ErrorDocumentoBean;
import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.bean.ParametroBean;
import mx.isban.flujogd.common.util.ExpedienteErrorEnum;
import mx.isban.flujogd.persistence.PersistenceManagerSubscriptor;
import mx.isban.flujogd.persistence.callable.DocumentoPersistenceTask;
import mx.isban.flujogd.persistence.impl.PersistenceManagerSubscriptorImpl;
import mx.isban.flujogd.validation.configuration.HazelcastValidationConfig;
import mx.isban.flujogd.validation.exceptions.ExistenciaParidadException;

/**
 * Clase para realizar el dlujo de validación con el CED
 * 
 * @author DELL
 *
 */
public class ValidationUtils {

	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(ValidationUtils.class);

	/**
	 * dbFactory
	 */
	private DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();

	/**
	 * The manager subscriptor
	 */
	private PersistenceManagerSubscriptor pmSub = new PersistenceManagerSubscriptorImpl();

	/**
	 * the HazelcastValidationConfig
	 */
	private HazelcastValidationConfig hazelcastValidationConfig;

	/**
	 * Default constructor
	 *
	 * @param hazelcastValidationConfig hz config
	 */
	public ValidationUtils(HazelcastValidationConfig hazelcastValidationConfig) {
		this.hazelcastValidationConfig = hazelcastValidationConfig;
	}

	/**
	 * Método para obtener el nombre de la aplicacón que invocó el proceso del
	 * bus(En este caso TF o CG) y la cuenta correspondiente al expediente que se
	 * encuentra validando, los valores se encuentran en el nodo
	 * "referencia-externa" separados por un guión
	 * 
	 * @param det                 detale del documento
	 * @param pathUNZIPExpediente path donde se encuentra el expediente
	 * @return lista de 2 elementos(aplicación y cuenta)
	 * @throws ExistenciaParidadException En caso de no encontrar el archivo
	 */
	public List<String> obtenerLaAplicacionYcuentaDesdeLaReferenciaExterna(DocumentoBeanDetalle det, String pathUNZIPExpediente) throws ExistenciaParidadException {
		String nombreArchivo = det.getMetadato().substring(0, det.getMetadato().length() - 3) + "xml";
		File fXmlFile = new File(pathUNZIPExpediente + File.separator + nombreArchivo);
		if (!fXmlFile.exists()) {
			throw new ExistenciaParidadException(String.format("No se pudo recuperar el archivo %s", nombreArchivo));
		}
		Document doc;
		try {
			DocumentBuilder builder = dbFactory.newDocumentBuilder();
			doc = builder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
			Element element = (Element) doc.getElementsByTagName("referencia-externa").item(0);
			String result = element.getTextContent();
			return Arrays.asList(result.split("\\s*-\\s*"));
		} catch (SAXException | IOException | ParserConfigurationException e) {
			throw new ExistenciaParidadException(e.getMessage(), e);
		}
	}

	/**
	 * Método encargado de validar un documento, revisando que exista y que tenga su
	 * xml correspondiente
	 * 
	 * @param documentos documentos a validar
	 * @param exp        expediente con el cuál está relacionado el documento
	 * @return lista de errores
	 */
	public List<ErrorDocumentoBean> validarExistenciaYParidad(List<DocumentoBean> documentos, ExpedienteBean exp) {
		List<ErrorDocumentoBean> errors = new ArrayList<>();
		for (DocumentoBean doc : documentos) {
			if (doc.getDetalle().getAtributos().isEmpty()) {
				LOG.warn("El detalle del documento no tiene atributos");
				errors.add(new ErrorDocumentoBean(doc.getDetalle().getNombreDocumento(), ExpedienteErrorEnum.ERROR_XML_DOC_NO_EXISTE.getName()));
			} else if ("pdf".equalsIgnoreCase(doc.getDetalle().getExtension()) && isErrorParidad(doc, exp)) {
				errors.add(new ErrorDocumentoBean(doc.getDetalle().getNombreDocumento(), ExpedienteErrorEnum.ERROR_ARCHIVO_PARIDAD.getName()));
			} else {
				saveDocument(exp, doc.getDetalle().getNombreDocumento());
			}
		}
		return errors;

	}

	/**
	 * Metodo para obtener los parametros del subscripto
	 * 
	 * @param nombreSubscriptor el nombre del subscriptor por el cuál se buscará
	 * @param nombreParametro   el nombre del parámetro que se busca en la bd
	 * @return ParametroBean con el resultado
	 */
	public ParametroBean retrieveParam(String nombreSubscriptor, String nombreParametro) {

		return pmSub.retrieveParam(nombreSubscriptor, nombreParametro);
	}

	
	/**
	 * Guardando el documento en la base de datos
	 * 
	 * @param exp expediente del cuál se obtendrá la información para guardar el
	 *            documento en base de datos
	 * @param doc nombre del documento
	 */
	private void saveDocument(ExpedienteBean exp, String doc) {
		LOG.info("Guardando el documento {} ", doc);
		IExecutorService executorService = this.hazelcastValidationConfig.getInstanceValidation().getExecutorService("executorService");
		try {
			String tmp = doc.split("_")[1];
			String tipoDocumento = tmp.substring(0, tmp.lastIndexOf("."));
			LOG.debug("Tipo de documento: {}", tipoDocumento);
			if (Boolean.TRUE.equals(executorService.submit(new DocumentoPersistenceTask(exp.getIdExpediente(), tipoDocumento, doc, "", "DOC_VALID_OK")).get())) {
				LOG.debug("Documento guardado correctamente");
			} else {
				LOG.error("ERROR AL INSERTAR DOCUEMNTO EN DB - ID_EXPEDIENTE: " + exp.getIdExpediente() + " - DOCUMENTO: " + doc);
			}
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(e.getMessage(), e);
		}
	}

	/**
	 * Método que valida si hay error de paridad, primero se valida que el documento
	 * pdf exista, posteriormente se busca el xml correspondiente(que contiene los
	 * metadatos que servirán para realizar la ingesta, si alguno de los dos no
	 * existe entonces se regresará un error de existencia/paridad)
	 * 
	 * @param doc documento a validar
	 * @param exp expediente con el cuál está relacionado el documento
	 * @return si existe error de paridad
	 */
	private boolean isErrorParidad(DocumentoBean doc, ExpedienteBean exp) {
		String pathFile = exp.getPathUNZIPExpediente() + File.separator;
		File fichero = new File(pathFile + doc.getDetalle().getNombreDocumento());
		if (fichero.exists()) {
			fichero = new File(pathFile + doc.getDetalle().getMetadato().substring(0, doc.getDetalle().getMetadato().length() - 3) + "xml");
			if (!fichero.exists()) {
				LOG.warn("No existe el archivo: " + pathFile + doc.getDetalle().getMetadato().substring(0, doc.getDetalle().getMetadato().length() - 3) + "xml");
				return true;
			}
		} else {
			LOG.warn("No existe el archivo: " + pathFile);
			return true;
		}
		return false;
	}

}
