package mx.isban.flujogd.validation.configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.hazelcast.config.Config;
import com.hazelcast.config.ListenerConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IQueue;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.bean.MemberBean;
import mx.isban.flujogd.common.generic.HazelcastGenericUtils;
import mx.isban.flujogd.validation.listener.ClusterMembershipListener;

/**
 * The Class HazelcastValidationConfig.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase HazelcastValidationConfig
 */
@Configuration
public class HazelcastValidationConfig {

	/** ips. */
	private static List<String> ips;
	
	/** port. */
	private static String port;
	
	/**
	 * Constructor.
	 */
	public HazelcastValidationConfig() {
		ClusterMembershipListener.setHazelcastConfig(this);
	}
	
	
	/**
	 * Gets the single instance of HazelcastValidationConfig.
	 *
	 * @return single instance of HazelcastValidationConfig
	 */
	@Bean
	public HazelcastInstance getInstanceValidation() {
		return Hazelcast.newHazelcastInstance(getHazelcastConfig());
	}

	/**
	 * Gets the queue.
	 *
	 * @param name the name
	 * @return the queue
	 */
	public IQueue<ExpedienteBean> getQueue(String name) {
		return getInstanceValidation().getQueue(name);
	}
	
	/**
	 * Gets the map members.
	 *
	 * @param name the name
	 * @return the map members
	 */
	public Map<String, MemberBean> getMapMembers(String name) {
		return getInstanceValidation().getMap(name);
	}
	
	/**
	 * Gets the map expedientes.
	 *
	 * @param name the name
	 * @return the map expedientes
	 */
	public Map<String, Boolean> getMapExpedientes(String name) {
		return getInstanceValidation().getMap(name);
	}
	
	/**
	 * Gets the map notificaciones.
	 *
	 * @param name the name
	 * @return the map notificaciones
	 */
	public Map<String, Boolean> getMapNotificaciones(String name) {
		return getInstanceValidation().getMap(name);
	}

	/**
	 * Config cluster network and discovery mechanism.
	 *
	 * @return Config
	 */
	@Bean
	public Config getHazelcastConfig() {
		Config configValidation = new Config();
		HazelcastGenericUtils util = new HazelcastGenericUtils();
		configValidation.addListenerConfig(new ListenerConfig("mx.isban.flujogd.validation.listener.ClusterMembershipListener"));
		configValidation.setInstanceName("FlujoGD-Validation");
		return util.setConfigHazelcast(port, ips, configValidation);
	}

	/**
	 * Sets the ips.
	 *
	 * @param ips the ips to set
	 */
	public static void setIps(List<String> ips) {
		List<String> copiaValidation = new ArrayList<>();
		copiaValidation.addAll(ips);
		HazelcastValidationConfig.ips = copiaValidation;
	}

	/**
	 * Sets the port.
	 *
	 * @param port the port to set
	 */
	public static void setPort(String port) {
		HazelcastValidationConfig.port = port;
	}

}
