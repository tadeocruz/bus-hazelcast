package mx.isban.flujogd.validation.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import mx.isban.flujogd.common.bean.DocumentoBean;
import mx.isban.flujogd.common.bean.ErrorDocumentoBean;
import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.bean.ExpedienteSucursal;
import mx.isban.flujogd.common.bean.ParametroBean;
import mx.isban.flujogd.common.bean.ValidacionMetadato;
import mx.isban.flujogd.common.exceptions.BusinessException;
import mx.isban.flujogd.common.util.ExpedienteErrorEnum;
import mx.isban.flujogd.common.util.ParametroEnum;
import mx.isban.flujogd.common.util.QueueEnum;
import mx.isban.flujogd.common.util.QueueStatusEnum;
import mx.isban.flujogd.validation.configuration.HazelcastValidationConfig;
import mx.isban.flujogd.validation.exceptions.ExistenciaParidadException;
import mx.isban.flujogd.validation.ws.WsCed;

/**
 * Clase para realizar el dlujo de validación con el CED
 * 
 * @author DELL
 *
 */
public class CedValidation {

	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(CedValidation.class);

	/**
	 * dbFactory
	 */
	private DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();

	/**
	 * dBuilder
	 */
	private DocumentBuilder dBuilder;

	/**
	 * Objeto con las propiedades(ValidationUtils y ExpedienteUtil) y métodos
	 * comúnes para las clases de validación
	 */
	private ValidationsObjectsContainer validation;

	/**
	 * Objeto con métodos útiles utilizados por esta clase
	 */
	private ValidationUtils vUtils;

	/**
	 * WsCed
	 */
	private WsCed ws = new WsCed();

	/**
	 * Default constructor
	 *
	 * @param hazelcastValidationConfig hz config
	 */
	public CedValidation(HazelcastValidationConfig hazelcastValidationConfig) {
		prepareBuilder(this.dBuilder);
		validation = new ValidationsObjectsContainer(hazelcastValidationConfig);
		vUtils = new ValidationUtils(hazelcastValidationConfig);
	}

	/**
	 * Constructor con los parámetros indicados
	 * 
	 * @param ws                        Ws Ced
	 * @param hazelcastValidationConfig hz config
	 * @param validation                ValidationsCommon object
	 * @param dBuilder                  dBuilder object
	 */
	public CedValidation(WsCed ws, HazelcastValidationConfig hazelcastValidationConfig, ValidationsObjectsContainer validation, DocumentBuilder dBuilder) {
		prepareBuilder(dBuilder);
		this.validation = validation;
		vUtils = new ValidationUtils(hazelcastValidationConfig);
	}

	/**
	 * Método para validar los documentos de un expediente, que se encuentren todos
	 * los pdfs listados en expediente.xml, que para cada pdf exista un xml el cual
	 * contiene los metadatos necesarios para realizar la ingesta y finalmente que
	 * estos metadatos contengan los valores correctos de acuerdo con el CED
	 * 
	 * @param expedienteBean    expediente al cuál se encuentran relacionados los
	 *                          documentos
	 * @param documentos        documentos a los cuales se validará su existencia y
	 *                          metadatos
	 * @param nombreSubscriptor nombre del subscriptor con el cuál obtendremos la
	 *                          información correspondiente desde el ced
	 * @return ExpedienteBean expediente validado
	 */
	public ExpedienteBean validarDocumentoConCed(ExpedienteBean expedienteBean, List<DocumentoBean> documentos, String nombreSubscriptor) {
		ExpedienteSucursal expSuc = new ExpedienteSucursal();
		try {
			LOG.info("Validando errores de existencia/paridad");
			List<ErrorDocumentoBean> errors = vUtils.validarExistenciaYParidad(documentos, expedienteBean);
			expSuc.setErroresDocumento(errors);
			List<String> appCuenta = new ArrayList<>();

			appCuenta.addAll(vUtils.obtenerLaAplicacionYcuentaDesdeLaReferenciaExterna(documentos.get(0).getDetalle(), expedienteBean.getPathUNZIPExpediente()));
			expSuc.setAplicativo(appCuenta.get(0));
			expSuc.setCuenta(new Long(appCuenta.get(1)));

			siExistenErroresPrepararNotificacion(expedienteBean, expSuc, errors);

			ParametroBean validarCedParam = vUtils.retrieveParam(nombreSubscriptor, ParametroEnum.PARAMETRO_GRAL_VALIDAR_CED.getName());

			if ("true".equalsIgnoreCase(validarCedParam.getValorParametro())) {
				LOG.info("Validando errores de metadatos");
				errors = validateMetadatosFromDocuments(documentos, expedienteBean, nombreSubscriptor);
				if (!errors.isEmpty()) {
					LOG.warn("Existen errores de metadatos");
					expSuc.setErroresDocumento(errors);
					expedienteBean.setExpedienteSucursal(expSuc);
					validation.getExpedienteUtil().cambiaEstausQueue(expedienteBean);
					validation.error(expedienteBean, ExpedienteErrorEnum.ERROR_FORMATO_INVALIDO.getName());
					return expedienteBean;
				}
			}
			siNoExistenErroresPrepararLaSiguienteCola(expedienteBean, errors);

		} catch (SAXException | IOException e) {
			LOG.error(e.getMessage(), e);
			return validation.error(expedienteBean, e.getMessage());
		} catch (ExistenciaParidadException e) {
			LOG.warn("Existen errores de existencia/paridad siguiente queue notificacion");
			LOG.error(e.getMessage(), e);
			expSuc.setErrorExistenciaParidad(true);
			expedienteBean.getDetalle().setSiguienteQueue(QueueEnum.QUEUE_VALIDATION.getName());
		} catch (BusinessException e) {
			LOG.error("Ocurrió un error al validar los metadatos", e);
		}
		expedienteBean.setExpedienteSucursal(expSuc);
		return expedienteBean;
	}

	/**
	 * Método para preparar la siguiente cola de ejecución en caso de que todos los
	 * documentos y metadatos hayan sido validados y no existan errores
	 * 
	 * @param expedienteBean expediente
	 * @param errors lista de errores
	 */
	private void siNoExistenErroresPrepararLaSiguienteCola(ExpedienteBean expedienteBean, List<ErrorDocumentoBean> errors) {
		if (errors.isEmpty()) {
			LOG.debug("Realizando updateStausBloqueExpediente");
			validation.getValidationUtils().updateStausBloqueExpediente(expedienteBean, QueueStatusEnum.STATUS_OK.getName(), QueueEnum.QUEUE_VALIDATION.getName(), null);
			LOG.debug("Realizando cambiaEstausQueue");
			validation.getExpedienteUtil().cambiaEstausQueue(expedienteBean);
			LOG.debug("Realizando determinaSiguienteQueue");
			validation.getExpedienteUtil().determinaSiguienteQueue(expedienteBean);
			LOG.info("Expediente válido, siguiente queue: " + expedienteBean.getDetalle().getSiguienteQueue());
		}
	}

	/**
	 * Método para preparar la notificación de que existen errores de
	 * existencia/paridad
	 * 
	 * @param expedienteBean expediente
	 * @param expSuc         Objeto de expediente sucursal
	 * @param errors         errores
	 */
	private void siExistenErroresPrepararNotificacion(ExpedienteBean expedienteBean, ExpedienteSucursal expSuc, List<ErrorDocumentoBean> errors) {
		if (!errors.isEmpty()) {
			LOG.warn("Existen errores de existencia/paridad siguiente queue notificacion");
			expedienteBean.setExpedienteSucursal(expSuc);
			expedienteBean.getDetalle().setSiguienteQueue(QueueEnum.QUEUE_NOTIFICATION.getName());
		}
	}

	/**
	 * Método para validar los metadatos de los documentos proporcionados
	 * 
	 * @param documentos        documentos a los cuales se validará su existencia y
	 *                          metadatos
	 * @param exp               expediente al cuál se encuentran relacionados los
	 *                          documentos
	 * @param nombreSubscriptor nombre del subscriptor con el cuál obtendremos la
	 *                          información correspondiente desde el ced
	 * @return List<ErrorDocumentoBean> lista con los errores de metadatos
	 *         relacionados a los documentos
	 * @throws IOException  Excepción IOException
	 * @throws SAXException Excepción SAXException
	 */
	private List<ErrorDocumentoBean> validateMetadatosFromDocuments(List<DocumentoBean> documentos, ExpedienteBean exp, String nombreSubscriptor)
			throws BusinessException, SAXException, IOException {
		List<ErrorDocumentoBean> errores = new ArrayList<>();
		for (DocumentoBean docs : documentos) {
			String pathFile = exp.getPathUNZIPExpediente() + File.separator + docs.getDetalle().getMetadato().substring(0, docs.getDetalle().getMetadato().length() - 3) + "xml";
			File fXmlFile = new File(pathFile);

			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();

			LOG.info("Tipo documental : {}", doc.getDocumentElement().getAttribute("tipoDocumental"));

			List<ValidacionMetadato> params = ws.getParamsFromCedByDocumentalTipe(doc.getDocumentElement().getAttribute("tipoDocumental"), nombreSubscriptor);

			Element nodeAtributes = (Element) doc.getElementsByTagName("atributos").item(0);

			HashMap<String, String> metadatos = getMetadatos(nodeAtributes.getElementsByTagName("atributo"));
			errores.addAll(getErroresMetadatos(params, metadatos, docs));
		}
		return errores;
	}

	/**
	 * Método para obtener los errores de metadatos, una vez que se obtuvieron los
	 * metadatos del ced y del documento es necesario realizar una comparación para
	 * saber si los metadatos del documento son correctos o no
	 * 
	 * @param params    parámetros con los cuales comparar
	 * @param metadatos metadatos a validar
	 * @param doc       documento a validar
	 * @return lista de errores
	 */
	private List<ErrorDocumentoBean> getErroresMetadatos(List<ValidacionMetadato> params, HashMap<String, String> metadatos, DocumentoBean doc) {
		List<ErrorDocumentoBean> errores = new ArrayList<>();
		for (ValidacionMetadato parametro : params) {
			if (parametro.isRequerido()) {
				HashMap<String, String> metadatosErrores = new HashMap<>();
				if (metadatos.get(parametro.getSymbolicname()) == null || metadatos.get(parametro.getSymbolicname()).isEmpty()) {
					LOG.debug(String.format("El metadato %s es requerido", parametro.getSymbolicname()));
					metadatosErrores.put(parametro.getSymbolicname(), String.format("El metadato %s es requerido", parametro.getSymbolicname()));
					errores.add(new ErrorDocumentoBean(doc.getDetalle().getNombreDocumento(), "Metadatos inválidos", metadatosErrores));

				} else if (!metadatos.get(parametro.getSymbolicname()).matches(parametro.getValidacion())) {
					LOG.debug(String.format("Metadato: \"%s\" Valor: \"%s\" Regexp: \"%s\" ", parametro.getSymbolicname(), metadatos.get(parametro.getSymbolicname()),
							parametro.getValidacion()));
					metadatosErrores.put(parametro.getSymbolicname(),
							String.format("El valor de \"%s\" debe de coincidir con la expresión regular \"%s\"", parametro.getSymbolicname(), parametro.getValidacion()));
					errores.add(new ErrorDocumentoBean(doc.getDetalle().getNombreDocumento(), "Metadatos inválidos", metadatosErrores));
				}
			}

		}
		return errores;
	}

	/**
	 * Método para obtener los metadatos de un nodo proporcionado(que vienen desde
	 * el documento)
	 * 
	 * @param nList
	 * @return
	 */
	private static HashMap<String, String> getMetadatos(NodeList nList) {
		HashMap<String, String> metadatos = new HashMap<>();
		for (int temp = 0; temp < nList.getLength(); temp++) {
			Element eElement = (Element) nList.item(temp);
			metadatos.put(eElement.getAttribute("id"), eElement.getTextContent());

		}
		return metadatos;
	}

	/**
	 * Método para preparar el builder
	 */
	private void prepareBuilder(DocumentBuilder dBuilder) {
		try {
			dbFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
			if (dBuilder == null) {
				this.dBuilder = dbFactory.newDocumentBuilder();
			} else {
				this.dBuilder = dBuilder;
			}

		} catch (ParserConfigurationException e) {
			LOG.error(e.getMessage(), e);
		}
	}

}
