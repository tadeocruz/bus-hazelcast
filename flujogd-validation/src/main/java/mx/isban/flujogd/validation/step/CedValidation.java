package mx.isban.flujogd.validation.step;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import mx.isban.flujogd.common.bean.DocumentoBean;
import mx.isban.flujogd.common.bean.DocumentoBeanDetalle;
import mx.isban.flujogd.common.bean.ErrorDocumentoBean;
import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.bean.ExpedienteSucursal;
import mx.isban.flujogd.common.bean.ValidacionMetadato;
import mx.isban.flujogd.common.exceptions.BusinessException;
import mx.isban.flujogd.common.util.ExpedienteErrorEnum;
import mx.isban.flujogd.common.util.QueueEnum;
import mx.isban.flujogd.common.util.QueueStatusEnum;
import mx.isban.flujogd.validation.configuration.HazelcastValidationConfig;
import mx.isban.flujogd.validation.exceptions.ExistenciaParidadException;
import mx.isban.flujogd.validation.ws.WsCed;

/**
 * Clase para realizar el dlujo de validación con el CED
 * 
 * @author DELL
 *
 */
public class CedValidation {

	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(CedValidation.class);

	/**
	 * dbFactory
	 */
	private DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();

	/**
	 * dBuilder
	 */
	private DocumentBuilder dBuilder;

	/**
	 * Objeto con las propiedades y métodos comúnes para las clases de validación
	 */
	private ValidationsCommon validation;

	/**
	 * WsCed
	 */
	private WsCed ws;

	/**
	 * Default constructor
	 *
	 * @param hazelcastValidationConfig hz config
	 */
	public CedValidation(HazelcastValidationConfig hazelcastValidationConfig) {
		prepareBuilder();
		ws = new WsCed();
		validation = new ValidationsCommon(hazelcastValidationConfig);
	}

	/**
	 * Constructor con los parámetros indicados
	 * 
	 * @param ws                        Ws Ced
	 * @param hazelcastValidationConfig hz config
	 * @param validation                ValidationsCommon object
	 */
	public CedValidation(WsCed ws, HazelcastValidationConfig hazelcastValidationConfig, ValidationsCommon validation) {
		prepareBuilder();
		this.ws = ws;
		this.validation = validation;
	}

	/**
	 * Método para validar que los documentos de un expediente proporcionado existen
	 * y sus metadatos son correctos
	 * 
	 * @param expedienteBean    expediente al cuál se encuentran relacionados los
	 *                          documentos
	 * @param documentos        documentos a los cuales se validará su existencia y
	 *                          metadatos
	 * @param nombreSubscriptor nombre del subscriptor con el cuál obtendremos la
	 *                          información correspondiente desde el ced
	 * @return ExpedienteBean expediente validado
	 */
	public ExpedienteBean validarDocumentoConCed(ExpedienteBean expedienteBean, List<DocumentoBean> documentos, String nombreSubscriptor) {
		ExpedienteSucursal expSuc = new ExpedienteSucursal();
		try {
			List<ErrorDocumentoBean> errors = validarExistenciaYParidad(documentos, expedienteBean);
			expSuc.setErroresDocumento(errors);
			List<String> appCuenta = new ArrayList<>();

			appCuenta.addAll(obtenerLaAplicacionYcuentaDesdeLaReferenciaExterna(documentos.get(0).getDetalle(), expedienteBean.getPathUNZIPExpediente()));
			expSuc.setAplicativo(appCuenta.get(0));
			expSuc.setCuenta(new Long(appCuenta.get(1)));

			if (!errors.isEmpty()) {
				expedienteBean.setExpedienteSucursal(expSuc);
				expedienteBean.getDetalle().setSiguienteQueue(QueueEnum.QUEUE_NOTIFICATION.getName());
			} else {
				errors = validateMetadatosFromDocuments(documentos, expedienteBean, nombreSubscriptor);
				if (!errors.isEmpty()) {
					expSuc.setErroresDocumento(errors);
					expedienteBean.setExpedienteSucursal(expSuc);
					expedienteBean.getDetalle().setSiguienteQueue(QueueEnum.QUEUE_NOTIFICATION.getName());
				}
			}
			if (errors.isEmpty()) {
				validation.getValidationUtils().updateStausBloqueExpediente(expedienteBean, QueueStatusEnum.STATUS_OK.getName(), QueueEnum.QUEUE_VALIDATION.getName(), null);
				validation.getExpedienteUtil().cambiaEstausQueue(expedienteBean);
				validation.getExpedienteUtil().determinaSiguienteQueue(expedienteBean);
			}

		} catch (SAXException | IOException | BusinessException e) {
			LOG.error(e.getMessage(), e);
			return validation.error(expedienteBean, e.getMessage());
		} catch (ExistenciaParidadException e) {
			LOG.error(e.getMessage(), e);
			expSuc.setErrorExistenciaParidad(true);
			expedienteBean.getDetalle().setSiguienteQueue(QueueEnum.QUEUE_VALIDATION.getName());
		}
		expedienteBean.setExpedienteSucursal(expSuc);
		return expedienteBean;
	}

	/**
	 * Método para obtener el nombre de la aplicacón y la cuenta correspondiente al
	 * expediente validado
	 * 
	 * @param det                 detale del documento
	 * @param pathUNZIPExpediente path donde se encuentra el expediente
	 * @return lista de 2 elementos(aplicación y cuenta)
	 * @throws ExistenciaParidadException En caso de no encontrar el archivo
	 */
	private List<String> obtenerLaAplicacionYcuentaDesdeLaReferenciaExterna(DocumentoBeanDetalle det, String pathUNZIPExpediente) throws ExistenciaParidadException {
		String nombreArchivo = det.getMetadato().substring(0, det.getMetadato().length() - 3) + "xml";
		File fXmlFile = new File(pathUNZIPExpediente + File.separator + nombreArchivo);
		if (!fXmlFile.exists()) {
			throw new ExistenciaParidadException(String.format("No se pudo recuperar el archivo %s", nombreArchivo));
		}
		Document doc;
		try {
			doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
			Element element = (Element) doc.getElementsByTagName("referencia-externa").item(0);
			String result = element.getTextContent();
			return Arrays.asList(result.split("\\s*-\\s*"));
		} catch (SAXException | IOException e) {
			throw new ExistenciaParidadException(e.getMessage(), e);
		}
	}

	/**
	 * Método para validar los metadatos de los documentos proporcionados
	 * 
	 * @param documentos        documentos a los cuales se validará su existencia y
	 *                          metadatos
	 * @param exp               expediente al cuál se encuentran relacionados los
	 *                          documentos
	 * @param nombreSubscriptor nombre del subscriptor con el cuál obtendremos la
	 *                          información correspondiente desde el ced
	 * @return List<ErrorDocumentoBean> lista con los errores de metadatos
	 *         relacionados a los documentos
	 * @throws IOException  Excepción IOException
	 * @throws SAXException Excepción SAXException
	 */
	private List<ErrorDocumentoBean> validateMetadatosFromDocuments(List<DocumentoBean> documentos, ExpedienteBean exp, String nombreSubscriptor)
			throws BusinessException, SAXException, IOException {
		List<ErrorDocumentoBean> errores = new ArrayList<>();
		for (DocumentoBean docs : documentos) {
			File fXmlFile = new File(
					exp.getPathUNZIPExpediente() + File.separator + docs.getDetalle().getMetadato().substring(0, docs.getDetalle().getMetadato().length() - 3) + "xml");

			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();

			LOG.info("Tipo documental : {}", doc.getDocumentElement().getAttribute("tipoDocumental"));

			List<ValidacionMetadato> params = ws.getParamsFromCedByDocumentalTipe(doc.getDocumentElement().getAttribute("tipoDocumental"), nombreSubscriptor);

			Element nodeAtributes = (Element) doc.getElementsByTagName("atributos").item(0);

			HashMap<String, String> metadatos = getMetadatos(nodeAtributes.getElementsByTagName("atributo"));
			errores.addAll(getErroresMetadatos(params, metadatos, docs));
		}
		return errores;
	}

	/**
	 * Método para obtener los errores de metadatos
	 * 
	 * @param params    parámetros con los cuales comparar
	 * @param metadatos metadatos a validar
	 * @param doc       documento a validar
	 * @return lista de errores
	 */
	private List<ErrorDocumentoBean> getErroresMetadatos(List<ValidacionMetadato> params, HashMap<String, String> metadatos, DocumentoBean doc) {
		List<ErrorDocumentoBean> errores = new ArrayList<>();
		for (ValidacionMetadato parametro : params) {
			if (parametro.getRequerido() == 1) {
				HashMap<String, String> metadatosErrores = new HashMap<>();
				if (metadatos.get(parametro.getSymbolicname()) == null || metadatos.get(parametro.getSymbolicname()).isEmpty()) {
					metadatosErrores.put(parametro.getSymbolicname(), String.format("El metadato %s es requerido", parametro.getSymbolicname()));
					errores.add(new ErrorDocumentoBean(doc.getDetalle().getNombreDocumento(), "Metadatos inválidos", metadatosErrores));

				} else if (!metadatos.get(parametro.getSymbolicname()).matches(parametro.getValidacion())) {
					metadatosErrores.put(parametro.getSymbolicname(), String.format("El valor debe de coincidir con la expresión regular \"%s\"", parametro.getValidacion()));
					errores.add(new ErrorDocumentoBean(doc.getDetalle().getNombreDocumento(), "Metadatos inválidos", metadatosErrores));
				}
			}

		}
		return errores;
	}

	/**
	 * Método para obtener los metadatos de un nodo proporcionado
	 * 
	 * @param nList
	 * @return
	 */
	private static HashMap<String, String> getMetadatos(NodeList nList) {
		HashMap<String, String> metadatos = new HashMap<>();
		for (int temp = 0; temp < nList.getLength(); temp++) {
			Element eElement = (Element) nList.item(temp);
			metadatos.put(eElement.getAttribute("id"), eElement.getTextContent());

		}
		return metadatos;
	}

	/**
	 * Método encargado de validar un documento, revisando que exista y que tenga su
	 * xml correspondiente
	 * 
	 * @param doc documento a validar
	 * @param exp expediente con el cuál está relacionado el documento
	 * @return lista de errores
	 */
	private List<ErrorDocumentoBean> validarExistenciaYParidad(List<DocumentoBean> documentos, ExpedienteBean exp) {
		List<ErrorDocumentoBean> errors = new ArrayList<>();
		for (DocumentoBean doc : documentos) {
			if (doc.getDetalle().getAtributos().isEmpty()) {
				errors.add(new ErrorDocumentoBean(doc.getDetalle().getNombreDocumento(), ExpedienteErrorEnum.ERROR_XML_DOC_NO_EXISTE.getName()));
			} else if ("pdf".equalsIgnoreCase(doc.getDetalle().getExtension()) && isErrorParidad(doc, exp)) {
				errors.add(new ErrorDocumentoBean(doc.getDetalle().getNombreDocumento(), ExpedienteErrorEnum.ERROR_ARCHIVO_PARIDAD.getName()));
			}
		}
		return errors;

	}

	/**
	 * Método que valida si hay error por paridad, es decir que un archivo pdf no
	 * tenga el archivo xml correspondiente
	 * 
	 * @param doc documento a validar
	 * @param exp expediente con el cuál está relacionado el documento
	 * @return si existe error de paridad
	 */
	private boolean isErrorParidad(DocumentoBean doc, ExpedienteBean exp) {
		String pathFile = exp.getPathUNZIPExpediente() + File.separator;
		File fichero = new File(pathFile + doc.getDetalle().getNombreDocumento());
		if (fichero.exists()) {
			fichero = new File(pathFile + doc.getDetalle().getMetadato().substring(0, doc.getDetalle().getMetadato().length() - 3) + "xml");
			if (!fichero.exists()) {
				return true;
			}
		} else {
			return true;
		}
		return false;
	}

	/**
	 * Método para preparar el builder
	 */
	private void prepareBuilder() {
		try {
			dbFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
			dBuilder = dbFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			LOG.error(e.getMessage(), e);
		}
	}

}
