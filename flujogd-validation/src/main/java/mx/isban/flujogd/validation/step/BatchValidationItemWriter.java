package mx.isban.flujogd.validation.step;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.item.ItemWriter;

import com.hazelcast.core.IQueue;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.util.MapEnum;
import mx.isban.flujogd.common.util.QueueEnum;
import mx.isban.flujogd.validation.configuration.HazelcastValidationConfig;

/**
 * The Class BatchValidationItemWriter.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase BatchValidationItemWriter
 */
public class BatchValidationItemWriter implements ItemWriter<ExpedienteBean>{
	
	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(BatchValidationItemWriter.class);
	
	/** The hazelcast validation config. */
	private HazelcastValidationConfig hazelcastValidationConfig;
	
	/**
	 * Constructor.
	 *
	 * @param hazelcastValidationConfig the hazelcast validation config
	 */
	public BatchValidationItemWriter(HazelcastValidationConfig hazelcastValidationConfig){
		this.hazelcastValidationConfig = hazelcastValidationConfig;
	}
	

	/**
	 * write.
	 *
	 * @param items the items
	 * @throws Exception the exception
	 */
	@Override
	public void write(List<? extends ExpedienteBean> items) throws Exception {
		String nextQueue = items.get(0).getDetalle().getSiguienteQueue();
		if(!"".equals(nextQueue)) {
			LOG.info("Siguiente queue: " + nextQueue + " - "+ items.get(0).getNombreExpediente() + " - " + items.get(0).getDetalle().getDetalle2().getReferenciaExterna());
			if(QueueEnum.QUEUE_NOTIFICATION.getName().equals(nextQueue)) {
				//Si se va a enviar a la queue de notificaciones se debe de eliminar el mapa de expedientes
				eliminarExpedienteDelMapa(items.get(0));
				Map<String, Boolean> notificacionesMap = this.hazelcastValidationConfig.getMapNotificaciones(MapEnum.MAP_NOTIFICACIONES_IN_PROCESS.getName());
				if(notificacionesMap.get(items.get(0).getIdExpediente())==null) {
					IQueue<ExpedienteBean> queue = hazelcastValidationConfig.getQueue(nextQueue);
					queue.put(items.get(0));
					notificacionesMap.put(items.get(0).getIdExpediente(), true);
				}
			}else {
				IQueue<ExpedienteBean> queue = hazelcastValidationConfig.getQueue(nextQueue);
				queue.put(items.get(0));
			}
		}else {
			//Si ocurrio un error en el proceso se elimina el expediente del mapa
			eliminarExpedienteDelMapa(items.get(0));
			LOG.info("No hay otra queue a procesar");
		}
	}
	
	/**
	 * Metodo para eliminar los expedietes ya procesados por las queues
	 * eliminarExpedienteDelMapa.
	 *
	 * @param expedienteBean the expediente bean
	 */
	private void eliminarExpedienteDelMapa(ExpedienteBean expedienteBean) {
		Map<String, Boolean> expedientesMap = this.hazelcastValidationConfig.getMapExpedientes(MapEnum.MAP_EXPEDIENTES_IN_PROCESS.getName());
		if(expedientesMap.remove(expedienteBean.getDetalle().getDetalle2().getDetalle3().getKeyMap(), true)) {
			LOG.info("BatchValidationItemWriter - SE ELIMINO EXPEDIENTE DEL MAPA CON EXITO - "+expedienteBean.getIdExpediente() + " - "+ expedienteBean.getNombreExpediente());
		}else {
			LOG.error("BatchValidationItemWriter - NO SE PUDO ELIMINAR EXPEDIENTE DEL MAPA - "+expedienteBean.getIdExpediente() + " - "+ expedienteBean.getNombreExpediente());
		}
	}
	
}
