package mx.isban.flujogd.validation.step;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.item.ItemProcessor;

import mx.isban.flujogd.common.bean.DocumentoBean;
import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.bean.XMLManagerResponse;
import mx.isban.flujogd.persistence.PersistenceManagerSubscriptor;
import mx.isban.flujogd.persistence.impl.PersistenceManagerSubscriptorImpl;
import mx.isban.flujogd.validation.configuration.HazelcastValidationConfig;
import mx.isban.flujogd.validation.utils.CedValidation;
import mx.isban.flujogd.validation.utils.ValidationManagerUtils;

/**
 * The Class BatchValidationItemProcessor.
 * 
 * @author DELL
 *
 */
public class BatchValidationItemProcessor implements ItemProcessor<ExpedienteBean, ExpedienteBean> {

	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(BatchValidationItemProcessor.class);

	/**
	 * ced
	 */
	private CedValidation ced;

	/**
	 * persistenceManagerSubscriptor
	 */
	private PersistenceManagerSubscriptor persistenceManagerSubscriptor = new PersistenceManagerSubscriptorImpl();

	/**
	 * validationManagerUtils
	 */
	private ValidationManagerUtils validationManagerUtils = new ValidationManagerUtils();

	/**
	 * Constructor.
	 *
	 * @param hazelcastValidationConfig the hazelcast validation config
	 */
	public BatchValidationItemProcessor(HazelcastValidationConfig hazelcastValidationConfig) {
		ced = new CedValidation(hazelcastValidationConfig);
	}

	/**
	 * Constructor con los parámetros indicados
	 * 
	 * @param persistenceManagerSubscriptor objeto para manipular los datos
	 *                                      relacionados relacionados con el
	 *                                      subscriptor
	 * @param validationManagerUtils        objeto con funciones útiles para el
	 *                                      proceso de validación
	 * @param ced                           objeto para realizar las validaciones de
	 *                                      los archivos
	 */
	public BatchValidationItemProcessor(PersistenceManagerSubscriptor persistenceManagerSubscriptor, ValidationManagerUtils validationManagerUtils, CedValidation ced) {
		this.persistenceManagerSubscriptor = persistenceManagerSubscriptor;
		this.validationManagerUtils = validationManagerUtils;
		this.ced = ced;
	}

	/**
	 * Metodo para seguiento del proceso de validacion process.
	 *
	 * @param expedienteBean the expediente bean
	 * @return the expediente bean
	 * @throws Exception the exception
	 */
	@Override
	public synchronized ExpedienteBean process(ExpedienteBean expedienteBean) throws Exception {
		LOG.info("Inicia proceso de validacion de documentos");
		if (expedienteBean.getPathUNZIPExpediente() == null) {
			LOG.info("Creando el path para el archivo del expediente");
			expedienteBean.setPathUNZIPExpediente(validationManagerUtils.crearPath(expedienteBean));
		}
		LOG.info("Recuperando los documentos del expediente");
		XMLManagerResponse xmlResponse = validationManagerUtils.recuperarDocumentos(expedienteBean);
		List<DocumentoBean> documentos = xmlResponse.getDocumentos();
		LOG.info("Documentos recuperados: " + documentos.size());
		LOG.debug("Documentos: {}", documentos);
		LOG.info("Recuperando el nombre del subscriptor");
		String nombreSubscriptor = persistenceManagerSubscriptor.retrieveSubscriptorByIdExpediente(expedienteBean.getIdExpediente());
		LOG.info("Validando metadatos con el ced");
		return ced.validarDocumentoConCed(expedienteBean, documentos, nombreSubscriptor);
	}
}
