package mx.isban.flujogd.validation;


import java.io.File;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.web.EmbeddedServletContainerAutoConfiguration;
import org.springframework.boot.autoconfigure.web.WebMvcAutoConfiguration;
import org.springframework.boot.system.ApplicationPidFileWriter;
import org.springframework.scheduling.annotation.EnableAsync;

import mx.isban.flujogd.common.generic.CommonsUtils;
import mx.isban.flujogd.validation.configuration.HazelcastValidationConfig;
import mx.isban.flujogd.validation.listener.ClusterMembershipListener;

/**
 * The Class FlujoGDValidationApplication.
 * Metodo principal que ejecutara el proceso de Validation.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase FlujoGDValidationApplication
 */
@SpringBootApplication(exclude = {EmbeddedServletContainerAutoConfiguration.class, 
		WebMvcAutoConfiguration.class})
@EnableAsync
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
public class FlujoGDValidationApplication implements CommandLineRunner {
	
	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(FlujoGDValidationApplication.class);
	
    /**
	 * Metodo principal que inicia el flujo Validation.
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
		CommonsUtils utils = new CommonsUtils();
		// Validar que existan los parametros requeridos para el proceso
    	if(args.length<5) {
    		LOG.error("No es posible inicar el componente sin los parametros 'IPs','port','path','componente' y 'Nombre subscriptor'");
    		return;
    	}else{
			//Colocar la ip de la configuracion de hazelcast
			HazelcastValidationConfig.setIps(utils.getListaIp(args));
			//Colocar el puerto de la configuracion de hazelcast
			HazelcastValidationConfig.setPort(args[1]);
			//Colocar el nombre del subscriptor del cluster de membresias
			ClusterMembershipListener.setNombreSubscriptorValidation(args[4]);
			//Colocar el componente del cluster de membresias
			ClusterMembershipListener.setComponenteValidation(args[3]);
			SpringApplication sp = new SpringApplication(FlujoGDValidationApplication.class);
			//Agregar los disparadores
			sp.addListeners(new ApplicationPidFileWriter(args[2]+File.separator+args[3]+".pid"));
			//Correr los argumentos
			sp.run(args);
		}
    }

    /**
     * Metodo nativo hazelcast que dispara el proceso de arranque.
     *
     * @param args the args
     * @throws Exception the exception
     */
	@Override
	public void run(String... args) throws Exception {
		LOG.info("FlujoGDValidationApplication - run");
	}

}
