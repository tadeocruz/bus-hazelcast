package mx.isban.flujogd.validation.bean;

import lombok.Getter;
import lombok.Setter;

/**
 * Bean con las propiedades necesarias para realizar la validación mediante el
 * ced(sin el tipo documental debido a que este es muy variable)
 * 
 * @author DELL
 *
 */
@Getter
@Setter
public class CedParamsBean {
	/**
	 * Url para obtener token de sesión
	 */
	private String url;
	/**
	 * Canal digital para ced
	 */
	private String canal;
	/**
	 * Aplicación origen para ced
	 */
	private String app;
	/**
	 * Nivel para ced
	 */
	private String nivel;
	/**
	 * producto (para físicas o morales)
	 */
	private String producto;
	/**
	 * Tipo de consulta
	 */
	private String tipoConsulta;
	/**
	 * Tipo de operación
	 */
	private String tipoOperacion;
}
