package mx.isban.flujogd.validation.step;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.item.ItemReader;

import com.hazelcast.core.IQueue;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.util.QueueEnum;
import mx.isban.flujogd.validation.configuration.HazelcastValidationConfig;

/**
 * The Class BatchValidationItemReader.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase inicial BatchValidationItemReader
 */
public class BatchValidationItemReader implements  ItemReader<ExpedienteBean>{

	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(BatchValidationItemReader.class);

	/** The hazelcast validation config. */
	private HazelcastValidationConfig hazelcastValidationConfig;
	
	/** The expediente bean. */
	private ExpedienteBean expedienteBean;
	
	/**
	 * Constructor.
	 *
	 * @param hazelcastValidationConfig the hazelcast validation config
	 */
	public BatchValidationItemReader(HazelcastValidationConfig hazelcastValidationConfig){
		this.hazelcastValidationConfig = hazelcastValidationConfig;
	}


	/**
	 * Metodo que lee de la queue el siguiente dato a ser procesado.
	 *
	 * @return the expediente bean
	 * @throws Exception the exception
	 */
	@Override
	public ExpedienteBean read() throws Exception {
		IQueue<ExpedienteBean> queue = this.hazelcastValidationConfig.getQueue(QueueEnum.QUEUE_VALIDATION.getName());
		try {
			expedienteBean = queue.take();
			if (expedienteBean != null) {
				LOG.info("=== SE RECUPERA EXPEDIENTE: "+expedienteBean.getIdExpediente() + " - "+ expedienteBean.getNombreExpediente() + " - " + expedienteBean.getDetalle().getDetalle2().getReferenciaExterna());
				return expedienteBean;
			}else{
				return null;
			}
		} catch (InterruptedException e1) {
			LOG.error(e1);
			Thread.currentThread().interrupt();
		}
		return null;
	}
}
