package mx.isban.flujogd.validation.configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.support.transaction.ResourcelessTransactionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.validation.listener.JobValidationListener;
import mx.isban.flujogd.validation.step.BatchValidationItemProcessor;
import mx.isban.flujogd.validation.step.BatchValidationItemReader;
import mx.isban.flujogd.validation.step.BatchValidationItemWriter;

/**
 * Clase para obtener las configuraciones y parametros para la ejecucion de Validation-Batch
 * The Class BatchValidationConfig.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase BatchValidationConfig
 */
@EnableBatchProcessing(modular = true)
@Configuration
public class BatchValidationConfig {

	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(BatchValidationConfig.class);

	/** The job builder factory. */
	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	/** The step builder factory. */
	@Autowired
	private StepBuilderFactory stepBuilderFactory;
	
	/** The hazelcast validation config. */
	@Autowired
	private HazelcastValidationConfig hazelcastValidationConfig; 

	/**
	 * Metodo para ejecutar proceso de validacion
	 * Process job.
	 *
	 * @return Job
	 */
	@Bean
	public Job processJob() {
		LOG.info("execute job");
		return jobBuilderFactory.get("jobBuilderFactory")
				.incrementer(new RunIdIncrementer())
				.listener(listener())
				.flow(orderStep())
				.end()
				.build();
	}

	/**
	 * Metodo para obtener los pasos de procesamiento
	 * To create a step, reader, processor and writer has been passed serially.
	 *
	 * @return the step
	 */
	@Bean
	public Step orderStep() {
		LOG.info("step de job");
		// Devolver pasos de proceso
		return stepBuilderFactory.get("orderStep1")
				.<ExpedienteBean, ExpedienteBean> chunk(1)
				.reader(new BatchValidationItemReader(this.hazelcastValidationConfig))
				.processor(new BatchValidationItemProcessor(this.hazelcastValidationConfig))
				.writer(new BatchValidationItemWriter(this.hazelcastValidationConfig))
				.build();
	}

	/**
	 * Listener.
	 *
	 * @return {@link JobExecutionListener}
	 */
	@Bean
	public JobExecutionListener listener() {
		return new JobValidationListener();
	}

	/**
	 * Transaction manager.
	 *
	 * @return {@link ResourcelessTransactionManager}
	 */
	@Bean
	public ResourcelessTransactionManager transactionManager() {
		return new ResourcelessTransactionManager();
	}
}
