package mx.isban.flujogd.batch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mx.isban.flujogd.batch.sftp.manager.SFTPManager;
import mx.isban.flujogd.batch.sftp.manager.SftpResponseBean;
import mx.isban.flujogd.batch.thread.BUCProcessThread;
import mx.isban.flujogd.batch.threadpool.ThreadPoolComponent;
import mx.isban.flujogd.batch.zip.manager.ZipManager;
import mx.isban.flujogd.common.bean.BatchParametroBean;
import mx.isban.flujogd.common.bean.LoteBean;
import mx.isban.flujogd.common.bean.SFTPBean;
import mx.isban.flujogd.common.bean.SFTPBeanDetalle;
import mx.isban.flujogd.common.util.ConfBatchEnum;
import mx.isban.flujogd.common.util.LoteExpedienteDetalleEnum;
import mx.isban.flujogd.common.util.LoteStatusEnum;
import mx.isban.flujogd.common.util.ParametroBatchEnum;
import mx.isban.flujogd.common.util.ParametroEnum;
import mx.isban.flujogd.persistence.PersistenceManagerBatch;
import mx.isban.flujogd.persistence.impl.PersistenceManagerBatchImpl;

public class SftpTC {

	private static final Logger LOG = LogManager.getLogger(SftpTC.class);

	public static void main(String[] args) {
		LOG.info("inicia...");

		testUpdateStatusLote();

		
	/*	  SFTPBean sftpBean = retrieveSftpBean(ConfBatchEnum.BATCH_CONF_SFTP.getName()); 
		  
		  SFTPManager sftpManager = new SFTPManager(sftpBean.getHost(), sftpBean.getPort(),
		  sftpBean.getUser(),
		  sftpBean.getPass(),sftpBean.getDetalle().getPathPrivateKey(),sftpBean.
		  getDetalle().getPassphrase()); 
		  
		  try (FileInputStream fileInputStream = new
		  FileInputStream( new File(
		  "C:\\planOPTIMUS\\procesos\\OPTIMUS\\2019-01-2\\1\\OptimusMasivaFolio_2019-01-2.zip")
		  )){ 
			 
			  SftpResponseBean sftpResponse = sftpManager.insert(fileInputStream,
		  "C:\\descargas\\stop1.txt","OptimusMasivaFolio_2019-01-2.zip"); if(sftpResponse.isOk())
		  LOG.info("El expediente se inserto con exito"); else
		  LOG.info("El expediente No se pudo insertar"); } catch (FileNotFoundException
		  e) { LOG.error(e.getMessage(), e);; } catch (IOException e) { LOG.error(e.getMessage(), e);; }*/
		 

	}

	private static SFTPBean retrieveSftpBean(String conf) {
		PersistenceManagerBatch persistenceManager = new PersistenceManagerBatchImpl();
		List<BatchParametroBean> parametros = persistenceManager.retrieveParamByConf(conf);
		SFTPBean sftpBean = null;
		if (parametros != null) {
			sftpBean = new SFTPBean();
			SFTPBeanDetalle detalle = new SFTPBeanDetalle();
			sftpBean.setDetalle(detalle);
			for (BatchParametroBean bean : parametros) {
				if (bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_SFTP_HOST.getName())) {
					sftpBean.setHost(bean.getValorParametro());
				} else if (bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_SFTP_PORT.getName())) {
					sftpBean.setPort(bean.getValorParametro());
				} else if (bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_SFTP_USER.getName())) {
					sftpBean.setUser(bean.getValorParametro());
				} else if (bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_SFTP_PASS.getName())) {
					sftpBean.setPass(bean.getValorParametro());
				} else if (bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_SFTP_SRC_PATH.getName())) {
					sftpBean.getDetalle().setSrcPath(bean.getValorParametro());
				} else if (bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_SFTP_OUTPUT_PATH.getName())) {
					sftpBean.getDetalle().setOutputPath(bean.getValorParametro());
				} else if (bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_SFTP_PATH_PRIVATE_KEY.getName())) {
					sftpBean.getDetalle().setPathPrivateKey(bean.getValorParametro());
				} else if (bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_SFTP_PASSPHRASE.getName())) {
					sftpBean.getDetalle().setPassphrase(bean.getValorParametro());
				}
			}
		}
		return sftpBean;
	}

	private static void testUpdateStatusLote() {
		// Se inicializan las variables necesarias para el metodo "execute"
		ThreadPoolComponent threadPool = new ThreadPoolComponent();

		boolean bucsErroneos = false;
		String estatusBuc = "";
		String estatusLote = "";

		PersistenceManagerBatch pm = new PersistenceManagerBatchImpl();
		List<LoteBean> lotes = pm.retrieveAllLotesByStatus(LoteStatusEnum.STATUS_SOLICITADO.getName());
		BatchParametroBean beanWP = pm.retrieveParam(ParametroBatchEnum.BATCH_PARAMETRO_WORKING_PATH.getName());
		List<Writer> writeEstatus = new ArrayList<>();
		// Se revisa cada uno de los lotes
		for (LoteBean lote : lotes) {

			// error en buc false
			bucsErroneos = false;
			
			
			String separator = Pattern.quote("\\");
			String[] filesPath = lote.getDetalle().getPath().split(separator);
			String appendPath = "";
			for (String path : filesPath) {
			//	appendPath = appendPath + path + File.separator;
				appendPath = appendPath + path;
			}
			appendPath = appendPath.substring(1, appendPath.length());

			// Asignamos un nombre al ZIP
			String nameZip = "OptimusMasivaFolio_" + lote.getDetalle().getPath().substring(1, 10);
			// Obtenemos la ruta del ZIP
			String path = beanWP.getValorParametro() + appendPath + File.separator + nameZip;
			
			Writer writeError = null;
			try {
				new File(path).mkdirs();
				writeError = new BufferedWriter(
						new OutputStreamWriter(new FileOutputStream(path + File.separator + "estatus.txt"), "Cp1252"));// Cp1252,UTF-8
			} catch (UnsupportedEncodingException | FileNotFoundException e) {
				LOG.error(e.getMessage(), e);;
			}
			writeEstatus.add(writeError);
			String[] bucsArray = lote.getDetalle().getBucs().split(",");
			for (int i = 0; i < bucsArray.length; i++) {

				BUCProcessThread bucProcessThread = new BUCProcessThread();
				bucProcessThread.config(bucsArray[i], path, writeError);
				threadPool.addthread(bucProcessThread);
				LOG.info("---bucProcessThread.isBucsErrors() " + bucProcessThread.isBucsErrors());
				// valida si no ubo errores en su procesamiendo error y procesado con error
				if (bucProcessThread.isBucsErrors()) {
					bucsErroneos = true;
					estatusBuc = LoteExpedienteDetalleEnum.ERROR.getName();
				} else {
					estatusBuc = LoteExpedienteDetalleEnum.ENCONTRDADO.getName();
				}
				// se inserta en la tabla de detalle
				pm.insertDetalleLote(lote.getIdLote(), estatusBuc, bucsArray[i], lote.getDetalle().getPath());
			}

		}

		// Ejecuta la orden de terminar el ExecutorService
		// Pero no lo termina inmediatamente
		// Espera a que terminen todos los hilos
		// Y ya no permite agregar mas
		threadPool.stop();
		// Retiene el hilo actual hasta que termine de procesarse el ExecutorService
		while (!threadPool.isTerminated()) {
			threadPool.awaitTermination(30, TimeUnit.SECONDS);
		}
		threadPool.stopNow();
		// Cerramos todos los writers
		// Si existen errores se agregan al log
		for (Writer writer : writeEstatus) {
			if (writer != null) {
				try {
					// Cerrar el log
					writer.close();
				} catch (IOException e) {
					// Mostrar el error en bitacora
					LOG.error(e.getMessage(), e);;
				} finally {
					try {
						// Cerrar el log
						writer.close();
					} catch (IOException e) {
						// Mostrar el error en bitacora
						LOG.error(e.getMessage(), e);;
					}
				}
			}
		}
		// Generamos los ZIP de cada lote
		// Se envian los ZIP por sftp
		for (LoteBean lote : lotes) {
			// Asignamos un nombre al ZIP
			String nameZip = "OptimusMasivaFolio_" + lote.getDetalle().getPath().substring(1, 10);
			// Obtenemos la ruta del ZIP
			String path = beanWP.getValorParametro() + lote.getDetalle().getPath().substring(1, lote.getDetalle().getPath().length()) + File.separator + nameZip;
			ZipManager zip = new ZipManager();
			LOG.info("path de zip: "+path);
			zip.zipPDF(path, path);
			// Se invoca el metodo "enviarSftp" con los detalles del ZIP
			enviarSftp(beanWP.getValorParametro() + lote.getDetalle().getPath(), nameZip + ".zip",
					lote.getDetalle().getPath().substring(1), lote.getIdLote());
		}

	}

	private static void enviarSftp(String path, String nameZip, String pathSftp, String idLote) {
		// Obtener el nombre del bean del sftp

		PersistenceManagerBatch pm = new PersistenceManagerBatchImpl();
		String estatusLote = "";
		SFTPBean sftpBean = retrieveSftpBean(ConfBatchEnum.BATCH_CONF_SFTP.getName());
		// Configurar los datos del sftp
		SFTPManager sftpManager = new SFTPManager(sftpBean.getHost(), sftpBean.getPort(), sftpBean.getUser(),
				sftpBean.getPass(), sftpBean.getDetalle().getPathPrivateKey(), sftpBean.getDetalle().getPassphrase());
		try (FileInputStream fileInputStream = new FileInputStream(new File(path + File.separator + nameZip))) {
			// Respuesta del sftp
			SftpResponseBean sftpResponse = sftpManager.insert(fileInputStream, pathSftp, nameZip,sftpBean.getDetalle().getOutputPath());
			if (sftpResponse.isOk()) {
				// Mostrar en bitacora si se copio el expediente
				LOG.info("SFTP: El expediente se copio con exito");
				// Se actualiza el estatus del lote a procesado
				// valida que no existan errores el procesar cad BUC
				estatusLote = LoteStatusEnum.STATUS_PROCESADO.getName();
			} else {
				// Mostrar en bitacora si no se copio el expediente
				LOG.info("SFTP: El expediente No se pudo copiar");
				estatusLote = LoteStatusEnum.STATUS_PROCESADO_CON_ERRORES.getName();
			}

			if (!pm.updateStatusLote(idLote, estatusLote)) {
				LOG.error("Problema para actualizar el estatus del blque: " + idLote);
			}

		} catch (FileNotFoundException e) {
			// Mostrar el error en bitacora
			LOG.error(e.getMessage(), e);;
		} catch (IOException e) {
			// Mostrar el error en bitacora
			LOG.error(e.getMessage(), e);;
		}
	}


}
