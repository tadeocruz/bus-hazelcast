package mx.isban.flujogd.batch;

import org.apache.logging.log4j.LogManager;

import org.apache.logging.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.scheduling.annotation.EnableAsync;

import mx.isban.flujogd.batch.component.BatchComponent;

/**
 * The Class FlujoGDBatchApplication.
 * Clase para ejecutar el flujo batch
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal FlujoGDBatchApplication
 */
@SpringBootApplication
@EnableAsync
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
public class FlujoGDBatchApplication implements CommandLineRunner{
	
	/** Inicializamos un log para la bitacora */
	private static final Logger LOG = LogManager.getLogger(FlujoGDBatchApplication.class);
	
	/**
	 * main
	 * Metodo inicial para ejecutar proceso batch.
	 *
	 * @param args Argumentos
	 */
	public static void main(String[] args) {
		// Inicializamos un spring application
		SpringApplication springApp = new SpringApplication(FlujoGDBatchApplication.class);
		// Correr el spring application
		springApp.run(args);
		System.exit(0);
	}

	/**
	 * Run.
	 * Metodo para lanzar proceso batch
	 *
	 * @param args the args
	 * @throws Exception the exception
	 */
	@Override
	public void run(String... args) throws Exception {
		LOG.info("Inicia BatchComponent");
		BatchComponent batchComponent = new BatchComponent();
		//Ejecutar el lote de componentes
		batchComponent.execute();
	}

}
