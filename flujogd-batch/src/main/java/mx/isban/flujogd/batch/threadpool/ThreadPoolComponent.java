/**
 * @author Alvaro
 */
package mx.isban.flujogd.batch.threadpool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mx.isban.flujogd.common.bean.ParametroBean;
import mx.isban.flujogd.common.util.ParametroBatchEnum;
import mx.isban.flujogd.persistence.PersistenceManagerBatch;
import mx.isban.flujogd.persistence.impl.PersistenceManagerBatchImpl;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal ThreadPoolComponent
 *
 */
public class ThreadPoolComponent {

	//Inicializamos un log para la bitacora
	private static final Logger LOG = LogManager.getLogger(ThreadPoolComponent.class);

	//Inicializamos un int para guardar en número de hilos
	private int numThreads = 0;

	//Inicializamos un executor
	private ExecutorService executor = null;
	
	//Inicializamos un contador
	private int cont = 0;

	/**
	 * Constructor
	 */
	public ThreadPoolComponent() {
		PersistenceManagerBatch pm = new PersistenceManagerBatchImpl();
		//Inicializamos un bean de los parametros
		ParametroBean bean = pm.retrieveParam(ParametroBatchEnum.BATCH_PARAMETRO_NUMBER_THREADS.getName());
		//Inicializamos un int para saber el numero de hilos
		int hilos = 0;
		if(bean==null) {
			//Mostrar en bitacora el mensaje
			LOG.info("No se ha configurado el valor de los numeros de hilos del pool, por default se generara con 1 hilo");
			//Agregar un hilo al numero de hilos
			hilos = 1;
		}else {
			try {
				hilos = new Integer(bean.getValorParametro());
			}catch(RuntimeException e){
				LOG.error("Error en la configuracion del numero de hilos del pool, por default se generara con 1 hilo");
				hilos = 1;
				throw e;
			}
		}
		numThreads = hilos;
		//Ejecutar el numero de hilos almacenados
		executor = Executors.newFixedThreadPool( numThreads );
		//Mostrar en bitacora el mensaje
		LOG.info("SE CREO UN THREADPOOL DE " + numThreads + " HILOS");
	}

	/**
	 * addthread
	 * @param runnable	Ejecutable
	 */
	public void addthread( Runnable runnable ){
		cont++;
		executor.execute( runnable );
	}

	/**
	 * stop
	 */
	public void stop(){
		LOG.info("THREADS AGREGADOS: " + cont );
		executor.shutdown();	
	}
	
	/**
	 * stopNow
	 */
	public void stopNow(){
		try {
			executor.shutdownNow();
			executor = null;
		}catch(RuntimeException e) {
			LOG.error(e);
		}finally {
			if(executor!=null) {
				executor.shutdownNow();
				executor = null;
			}
		}
		LOG.info("SE DETIENE EL THREAD POOL");
	}
	
	/**
	 * isTerminated
	 * @return
	 */
	public boolean isTerminated(){
		return executor.isTerminated();
	}
	
	/**
	 * awaitTermination
	 * @param timeout	Tiempo de espera
	 * @param unit	Unidad de tiempo
	 */
	public void awaitTermination( long timeout, TimeUnit unit ){
		try{
			executor.awaitTermination( timeout, unit );
		} catch (InterruptedException e) {
			LOG.error(e);
			Thread.currentThread().interrupt();
		}
	}

}
