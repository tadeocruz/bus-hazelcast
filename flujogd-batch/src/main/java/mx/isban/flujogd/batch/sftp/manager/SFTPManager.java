package mx.isban.flujogd.batch.sftp.manager;

import java.io.File;
import java.io.InputStream;
import java.util.StringTokenizer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.SftpException;
import com.jcraft.jsch.UserInfo;

/**
 * The Class SFTPManager.
 *
 * @author Alvaro Zamorano 
 * azamorano@serviciosexternos.isban.mx 
 * Clase principal SFTPManager
 */
public class SFTPManager {

	/** The Constant LOG. */
	// Inicializamos un log para la bitacora
	private static final Logger LOG = LogManager.getLogger(SFTPManager.class);

	/** The host. */
	private String host = null;
	
	/** The port. */
	private String port = null;
	
	/** The user. */
	private String user = null;
	
	/** The pass. */
	private String pass = null;
	
	/** The path private key. */
	private String pathPrivateKey = null;
	
	/** The passphrase. */
	private String passphrase = null;

	/**
	 * Constructor.
	 *
	 * @param host Ip del host
	 * @param port Puerto
	 * @param user Nombre de usuario
	 * @param pass Contrasena
	 */
	public SFTPManager(String host, String port, String user, String pass) {
		this.host = host;
		this.port = port;
		this.user = user;
		this.pass = pass;
	}

	/**
	 * Constructor.
	 *
	 * @param host           Ip del host
	 * @param port           Puerto
	 * @param user           Nombre de usuario
	 * @param pass           Contrasena
	 * @param pathPrivateKey Ruta para la llave privada de acceso
	 * @param passphrase     Frase de seguridad de la Contrasena
	 */
	public SFTPManager(String host, String port, String user, String pass, String pathPrivateKey, String passphrase) {
		this.host = host;
		this.port = port;
		this.user = user;
		this.pass = pass;
		this.pathPrivateKey = pathPrivateKey;
		this.passphrase = passphrase;
	}

	/**
	 * Creacion del repositorio con la estructura de aplicaciones de origen
	 * insert.
	 *
	 * @param is       Flujo de entrada
	 * @param filePath the file path
	 * @param fileName Nombre del archivo
	 * @param outputPath the output path
	 * @return Respuesta del sftp
	 */
	public SftpResponseBean insert(InputStream is, String filePath, String fileName, String outputPath) {
		JSch jsch = null;
		Session session = null;
		ChannelSftp channelSftp = null;
		SftpResponseBean response = new SftpResponseBean(true);
		try {
			jsch = new JSch();

			if (this.pathPrivateKey != null && !"".equals(this.pathPrivateKey)) {
				jsch.addIdentity(this.pathPrivateKey);
			}
			// Seteo de datos de login para sftp
			session = jsch.getSession(this.user, this.host, new Integer(this.port));
			UserInfo ui = new MyUserInfo(this.pass, this.passphrase);
			session.setUserInfo(ui);
			session.connect();

			// Conectar al servidor sftp
			Channel channel = session.openChannel("sftp");
			channel.connect();

			channelSftp = (ChannelSftp) channel;

			// Se divide el path para su creacion
			StringTokenizer tok = new StringTokenizer(filePath, File.separator);

			String pathFecha = "";
			String pathLote = "";

			// se obtiene el nombre de la carpeta por separado
			while (tok.hasMoreTokens()) {
				pathFecha = (String) tok.nextElement();
				pathLote = (String) tok.nextElement();

			}

			// se situa a la direccion del outputpath del sft para crear las carpets
			channelSftp.cd(outputPath);
			
			// Crear folder en sftp
			channelSftp = crearFolderSftp(channelSftp, outputPath, pathFecha, pathLote);

			channelSftp.put(is, fileName);
		} catch (JSchException e) {
			response.setOk(false);
			response.setErrorMessage(e.getMessage());
			LOG.error(e);
		} catch (SftpException e) {
			response.setOk(false);
			response.setErrorMessage(e.getMessage());
			LOG.error(e);
		} finally {
			// Cerrar todos los recursos
			try {
				if (channelSftp != null) {
					channelSftp.exit();
				}
			} catch (Exception ex) {
				LOG.error(ex);
			}
			try {
				if (session != null) {
					session.disconnect();
				}
			} catch (Exception ex) {
				LOG.error(ex);
			}
		}
		return response;
	}

	/**
	 * Crear folder sftp.
	 *
	 * @param channelSftpOrg the channel sftp org
	 * @param outputPath the output path
	 * @param pathFecha the path fecha
	 * @param pathLote the path lote
	 * @return the channel sftp
	 */
	private ChannelSftp crearFolderSftp(ChannelSftp channelSftpOrg, String outputPath, String pathFecha, String pathLote) {
		ChannelSftp channelSftp = channelSftpOrg;
		try {
		if (ckechExistFolder(channelSftp, outputPath + File.separator + pathFecha)) {
			// ingresa a la carpeta creada
			channelSftp.cd(pathFecha);
		} else {
			// se crea la carpeta fecha
			channelSftp.mkdir(pathFecha);
			// ingresa a la carpeta creada
			channelSftp.cd(pathFecha);
		}
		
		if (ckechExistFolder(channelSftp, outputPath + File.separator + pathFecha + File.separator + pathLote)) {
			// ingresa a la carpeta creada
			channelSftp.cd(pathLote);
		} else {
			// se crea la carpeta fecha
			channelSftp.mkdir(pathLote);
			// ingresa a la carpeta creada
			channelSftp.cd(pathLote);
		}
		}catch (SftpException e) {
			LOG.error(e);
		}
		return channelSftp;
	}

	/**
	 * The Class MyUserInfo.
	 *
	 * @author Alvaro Zamorano
	 */
	public static class MyUserInfo implements UserInfo {

		/** The pass. */
		private String pass;
		
		/** The passphrase. */
		private String passphrase;

		/**
		 * myuserinfo.
		 *
		 * @param pass       Contrasena
		 * @param passphrase Frase de seguridad de la Contrasena
		 */
		public MyUserInfo(String pass, String passphrase) {
			this.pass = pass;
			this.passphrase = passphrase;
		}

		/**
		 * Gets the password.
		 *
		 * @return the password
		 */
		public String getPassword() {
			return this.pass;
		}

		/**
		 * promptyesno.
		 *
		 * @param str Cadena a evaluar
		 * @return Verdadero
		 */
		public boolean promptYesNo(String str) {
			return true;
		}

		/**
		 * Gets the passphrase.
		 *
		 * @return the passphrase
		 */
		public String getPassphrase() {
			return this.passphrase;
		}

		/**
		 * promptpassphrase.
		 *
		 * @param message Mensaje a evaluar
		 * @return Verdadero
		 */
		public boolean promptPassphrase(String message) {
			return true;
		}

		/**
		 * promptpassword.
		 *
		 * @param message Mensaje a evaluar
		 * @return Verdadero
		 */
		public boolean promptPassword(String message) {
			return true;
		}

		/**
		 * showmessage.
		 *
		 * @param message Mensaje a enviar en la bitacora
		 */
		public void showMessage(String message) {
			LOG.info(message);
		}

	}

	/**
	 * Clase para revisar si el folder a crear ya existe en el repositorio del servidor de intercambio sftp
	 * Ckech exist folder.
	 *
	 * @param channelSftp the channel sftp
	 * @param path the path
	 * @return true, if successful
	 */
	private boolean ckechExistFolder(ChannelSftp channelSftp, String path) {
		SftpATTRS attrs = null;
		boolean exist = false;
		try {
			attrs = channelSftp.stat(path);
		} catch (SftpException e) {
			LOG.info("El Directorio " + path + " no existe"+ " Cause :: ", e);
		}

		if (attrs != null) {
			exist = true;
		} else {
			exist = false;
		}
		return exist;
	}

}
