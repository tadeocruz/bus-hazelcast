package mx.isban.flujogd.batch.component;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mx.isban.flujogd.batch.sftp.manager.SFTPManager;
import mx.isban.flujogd.batch.sftp.manager.SftpResponseBean;
import mx.isban.flujogd.batch.thread.BUCProcessThread;
import mx.isban.flujogd.batch.threadpool.ThreadPoolComponent;
import mx.isban.flujogd.batch.zip.manager.ZipManager;
import mx.isban.flujogd.common.bean.LoteBean;
import mx.isban.flujogd.common.bean.ParametroBean;
import mx.isban.flujogd.common.bean.SFTPBean;
import mx.isban.flujogd.common.bean.SFTPBeanDetalle;
import mx.isban.flujogd.common.generic.StartedGenericUtils;
import mx.isban.flujogd.common.util.ConfBatchEnum;
import mx.isban.flujogd.common.util.LoteExpedienteDetalleEnum;
import mx.isban.flujogd.common.util.LoteStatusEnum;
import mx.isban.flujogd.common.util.ParametroBatchEnum;
import mx.isban.flujogd.persistence.PersistenceManagerBatch;
import mx.isban.flujogd.persistence.impl.PersistenceManagerBatchImpl;

/**
 * 
 * @author Alvaro Zamorano 
 * azamorano@serviciosexternos.isban.mx 
 * Clase principal BatchComponent
 *
 */
public class BatchComponent {

	// Inicializamos un log para almacenar los errores
	private static final Logger LOG = LogManager.getLogger(BatchComponent.class);
	// Inicializamos un threadPool para administrar los hilos de conexion
	private ThreadPoolComponent threadPool = null;

	/**
	 * Metodo inicial para proceso batch
	 * execute
	 */
	public void execute() {
		// Se inicializan las variables necesarias para el metodo "execute"
		threadPool = new ThreadPoolComponent();
		PersistenceManagerBatch pm = new PersistenceManagerBatchImpl();
		List<LoteBean> lotes = pm.retrieveAllLotesByStatus(LoteStatusEnum.STATUS_SOLICITADO.getName());
		ParametroBean beanWP = pm.retrieveParam(ParametroBatchEnum.BATCH_PARAMETRO_WORKING_PATH.getName());
		List<Writer> writeEstatus = new ArrayList<>();
		// Se revisa cada uno de los lotes
		// Se revisa cada uno de los lotes
		for (LoteBean lote : lotes) {
			String separator = Pattern.quote("\\");
			String[] filesPath = lote.getDetalle().getPath().split(separator);
			StringBuilder appendPathTmp = new StringBuilder();
			for (String path : filesPath) {
				appendPathTmp.append(path);
			}
			String appendPath = appendPathTmp.substring(1, appendPathTmp.length());

			// Se divide el path para su creacion
			StringTokenizer tok = new StringTokenizer(lote.getDetalle().getPath(), File.separator);

			String pathFecha = "";

			// se obtiene el nombre de la carpeta por separado
			pathFecha = (String) tok.nextElement();
			

			// Asignamos un nombre al ZIP
			String nameZip = "OptimusMasivaFolio_" + pathFecha;
			// Obtenemos la ruta del ZIP
			String path = beanWP.getValorParametro() + appendPath + File.separator + nameZip;

			Writer writeError = null;
			try {
				new File(path).mkdirs();
				writeError = new BufferedWriter(
						new OutputStreamWriter(new FileOutputStream(path + File.separator + "estatus.txt"), "Cp1252"));// Cp1252,UTF-8
			} catch (UnsupportedEncodingException | FileNotFoundException e) {
				LOG.error(e);
			}
			writeEstatus.add(writeError);
			String[] bucsArray = lote.getDetalle().getBucs().split(",");
			// Insertar detalle de lote
			insertarDetalleLote(bucsArray, path, writeError, lote, pm);
		}
		// Ejecuta la orden de terminar el ExecutorService
		// Pero no lo termina inmediatamente
		// Espera a que terminen todos los hilos
		// Y ya no permite agregar mas
		threadPool.stop();
		// Retiene el hilo actual hasta que termine de procesarse el ExecutorService
		while (!threadPool.isTerminated()) {
			threadPool.awaitTermination(30, TimeUnit.SECONDS);
		}
		threadPool.stopNow();
		// Cerramos todos los writers
		// Escribir los errores en log
		escribirErroresLog(writeEstatus);
	
		// Generamos los ZIP de cada lote
		// Se envian los ZIP por sftp
		for (LoteBean lote : lotes) {

			// Se divide el path para su creacion
			StringTokenizer tok = new StringTokenizer(lote.getDetalle().getPath(), File.separator);

			String pathFecha = "";

			// se obtiene el nombre de la carpeta por separado
			pathFecha = (String) tok.nextElement();

			// Asignamos un nombre al ZIP
			String nameZip = "OptimusMasivaFolio_" + pathFecha;
			// Obtenemos la ruta del ZIP
			String path = beanWP.getValorParametro()
					+ lote.getDetalle().getPath().substring(1, lote.getDetalle().getPath().length()) + File.separator
					+ nameZip;
			ZipManager zip = new ZipManager();
			LOG.info("path de zip: " + path);
			zip.zipPDF(path, path);
			// Se invoca el metodo "enviarSftp" con los detalles del ZIP
			enviarSftp(beanWP.getValorParametro() + lote.getDetalle().getPath(), nameZip + ".zip",
					lote.getDetalle().getPath().substring(1), lote.getIdLote());
		}
	}

	/**
	 * Insertar detalle lote.
	 *
	 * @param bucsArray the bucs array
	 * @param path the path
	 * @param writeError the write error
	 * @param lote the lote
	 * @param pm the pm
	 */
	private void insertarDetalleLote(String[] bucsArray, String path, Writer writeError, LoteBean lote, PersistenceManagerBatch pm) {
		String estatusBuc = "";
		for (int i = 0; i < bucsArray.length; i++) {
			BUCProcessThread bucProcessThread = new BUCProcessThread();
			bucProcessThread.config(bucsArray[i], path, writeError);
			threadPool.addthread(bucProcessThread);
			LOG.info("---bucProcessThread.isBucsErrors() " + bucProcessThread.isBucsErrors());
			// valida si no ubo errores en su procesamiendo error y procesado con error
			if (bucProcessThread.isBucsErrors()) {
				estatusBuc = LoteExpedienteDetalleEnum.ERROR.getName();
			} else {
				estatusBuc = LoteExpedienteDetalleEnum.ENCONTRDADO.getName();
			}
			// se inserta en la tabla de detalle
			pm.insertDetalleLote(lote.getIdLote(), estatusBuc, bucsArray[i], lote.getDetalle().getPath());
		}
	}

	/**
	 * Escribir errores log.
	 *
	 * @param writeEstatus the write estatus
	 */
	private void escribirErroresLog(List<Writer> writeEstatus) {
		// Si existen errores se agregan al log
		for (Writer writer : writeEstatus) {
			if (writer != null) {
				try {
					// Cerrar el log
					writer.close();
				} catch (IOException e) {
					// Mostrar el error en bitacora
					LOG.error(e);
				} finally {
					try {
						// Cerrar el log
						writer.close();
					} catch (IOException e) {
						// Mostrar el error en bitacora
						LOG.error(e);
					}
				}
			}
		}
	}

	/**
	 * enviarSftp
	 * 
	 * @param path
	 * @param nameZip
	 */
	private void enviarSftp(String path, String nameZip, String pathSftp, String idLote) {
		// Obtener el nombre del bean del sftp

		PersistenceManagerBatch pm = new PersistenceManagerBatchImpl();
		String estatusLote = "";
		SFTPBean sftpBean = retrieveSftpBean(ConfBatchEnum.BATCH_CONF_SFTP.getName());
		// Configurar los datos del sftp
		SFTPManager sftpManager = new SFTPManager(sftpBean.getHost(), sftpBean.getPort(), sftpBean.getUser(),
				sftpBean.getPass(), sftpBean.getDetalle().getPathPrivateKey(), sftpBean.getDetalle().getPassphrase());
		try (FileInputStream fileInputStream = new FileInputStream(new File(path + File.separator + nameZip))) {
			// Respuesta del sftp
			SftpResponseBean sftpResponse = sftpManager.insert(fileInputStream, pathSftp, nameZip,
					sftpBean.getDetalle().getOutputPath());
			if (sftpResponse.isOk()) {
				// Mostrar en bitacora si se copio el expediente
				LOG.info("SFTP: El expediente se copio con exito");
				// Se actualiza el estatus del lote a procesado
				// valida que no existan errores el procesar cad BUC
				estatusLote = LoteStatusEnum.STATUS_PROCESADO.getName();
			} else {
				// Mostrar en bitacora si no se copio el expediente
				LOG.info("SFTP: El expediente No se pudo copiar");
				estatusLote = LoteStatusEnum.STATUS_PROCESADO_CON_ERRORES.getName();
			}

			if (!pm.updateStatusLote(idLote, estatusLote)) {
				LOG.error("Problema para actualizar el estatus del bloque: " + idLote);
			}

		} catch (FileNotFoundException e) {
			// Mostrar el error en bitacora
			LOG.error(e);
		} catch (IOException e) {
			// Mostrar el error en bitacora
			LOG.error(e);
		}
	}

	/**
	 * retrieveSftpBean
	 * 
	 * @param conf
	 * @return
	 */
	private SFTPBean retrieveSftpBean(String conf) {
		StartedGenericUtils utils = new StartedGenericUtils();
		PersistenceManagerBatch persistenceManager = new PersistenceManagerBatchImpl();
		// Lista de parametros
		List<ParametroBean> parametros = persistenceManager.retrieveParamByConf(conf);
		SFTPBean sftpBean = null;
		if (parametros != null) {
			sftpBean = new SFTPBean();
			SFTPBeanDetalle detalle = new SFTPBeanDetalle();
			// En caso de que existan parametros se agregan al detalle del sftp
			sftpBean.setDetalle(detalle);
			for (ParametroBean bean : parametros) {
				// Checar el tipo de parametro
				utils.getValue(bean, sftpBean);
			}
		}
		return sftpBean;
	}
}
