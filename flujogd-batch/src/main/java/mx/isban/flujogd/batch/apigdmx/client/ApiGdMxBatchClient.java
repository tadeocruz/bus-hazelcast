package mx.isban.flujogd.batch.apigdmx.client;

import java.security.KeyStoreException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import com.filenet.api.core.Document;
import com.filenet.api.core.ObjectStore;

import mx.isban.apigdmx.IConnection;
import mx.isban.apigdmx.commons.ConnectionCommonExtendedProperties;
import mx.isban.apigdmx.commons.ConnectionCommonProperties;
import mx.isban.apigdmx.connection.ConnectionFactoryImplFileNet;
import mx.isban.apigdmx.dg.PropertiesBean;
import mx.isban.apigdmx.exception.ApiGdBusinessException;
import mx.isban.apigdmx.exception.ApiGdInfraestructureException;
import mx.isban.apigdmx.session.SessionOnLineImplFileNet;
import mx.isban.flujogd.common.bean.DocumentoBean;
import mx.isban.flujogd.common.bean.DocumentoBeanDetalle;
import mx.isban.flujogd.common.bean.DocumentoBeanDetalle2;
import mx.isban.flujogd.common.bean.FilenetBean;
import mx.isban.idc.core.appserver.IsbKeyStorePublicOps;
import mx.isban.idc.core.appserver.IsbProvider;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal ApiGdMxBatchClient
 *
 */
public class ApiGdMxBatchClient {

	//Inicializamos un connection factory
	private ConnectionFactoryImplFileNet connectionFactory;
	//Inicializamos un iconnection
	private IConnection connection;
	//Inicializamos un string builder para almacenar los encabezados
	private StringBuilder encabezados;
	//Inicializamos un string builder para almacenar los atributos
	private StringBuilder atributos;

	/**
	 * apigdmxbatchclient
	 * @param bean	Vaina de la clase FilenetBean
	 * @throws KeyStoreException	Excepción generica del KeyStore
	 * @throws ApiGdInfraestructureException 	Excepcion de infraestructura
	 */
	public ApiGdMxBatchClient(FilenetBean bean) throws KeyStoreException, ApiGdInfraestructureException{
		//Inicializamos un string para almacenar la contrasena
		String password = null;
		//Se almacena la contrasena segun la condición
		if(bean.getDetalle().getPassword() !=null ) {
			password = bean.getDetalle().getPassword();
		}else {
			System.setProperty("java.naming.factory.initial","mx.isban.idc.jndi.provider.JndiProvider");
			System.setProperty("JNDI_F_PATH",bean.getPathIdc());
			IsbKeyStorePublicOps pub = null;
			pub = IsbProvider.getInstance();
			password = new String(pub.getSecretData(bean.getDetalle().getSecretData()));
		}

		//Crear una conexion
		connectionFactory = new ConnectionFactoryImplFileNet();
		ConnectionCommonProperties connectionCommonProperties = new ConnectionCommonProperties();
		ConnectionCommonExtendedProperties connectionCommonExtendedProperties = new ConnectionCommonExtendedProperties();

		//Configurar la conexion
		connectionCommonProperties.setPassword(password);
		connectionCommonProperties.setUser(bean.getUser());
		connectionCommonExtendedProperties.setUri(bean.getUri());
		connectionCommonExtendedProperties.setJaasName(bean.getJaas());
		connectionCommonExtendedProperties.setObjectStoreName(bean.getDetalle().getStore());

		connectionFactory.setConnectionCommonExtendedProperties(connectionCommonExtendedProperties);
		connectionFactory.setConnectionCommonProperties(connectionCommonProperties);

		//Iniciar la conexion
		connection = connectionFactory.getConnection();
	}

	/**
	 * retrievedocumentsbybuc
	 * @param buc	Id del expediente
	 * @return	Lista de documentos
	 * @throws ApiGdBusinessException	Excepcion de reglas del negocio
	 * @throws TimeoutException	Excepcion de falta de tiempo
	 */
	public List<DocumentoBean> retrieveDocumentsByBUC(String buc) throws ApiGdBusinessException, TimeoutException {
		//Crear una lista de documentos
		List<DocumentoBean> documentos = new ArrayList<>();
		encabezados = new StringBuilder();
		atributos = new StringBuilder();
		encabezados.append("");
		atributos.append("");
		SessionOnLineImplFileNet online = new SessionOnLineImplFileNet(connection);
		List<List<PropertiesBean>>metaDataList = online.searchSQLQuery( " * " , " DocumentoSantander " , " BUC = '"+buc+"' " );
		for( List<PropertiesBean> dataList:  metaDataList  ) {
			DocumentoBean documento = new DocumentoBean();
			for(PropertiesBean property:dataList) {
				if("Id".equals(property.getName().trim())) {
					Document docto =  online.find((ObjectStore) connection.getConnection(),(String)property.getValue());
					DocumentoBeanDetalle2 detalle2 = new DocumentoBeanDetalle2();
					//Se agregan los detalles al documento
					documento.setDetalle2(detalle2);
					documento.getDetalle2().setStream(docto.accessContentStream(0));
				} else if("NombreArchivo".equals(property.getName().trim())){
					DocumentoBeanDetalle detalle = new DocumentoBeanDetalle();
					//Se agregan los detalles al documento
					documento.setDetalle(detalle);
					documento.getDetalle().setNombreDocumento((String)property.getValue());
				} else if(isValidValue(property) ){
					//Agregar a los encabezados el nombre de las propiedades
					encabezados.append("|" + property.getName().trim());
					//Agregar a los atributos el valor de las propiedades
					atributos.append("|" + getValue(property));
				} 
			}
			documento.getDetalle2().setEncabezadosTxt(encabezados.toString());
			documento.getDetalle2().setAtributosTxt(atributos.toString());
			documentos.add(documento);
		}
		//Regresar el resultado
		return documentos;
	}

	private boolean isValidValue(PropertiesBean property) {
		//Inicializamos un boolean para saber si la propiedad es valida
		boolean isValid = true;
		String value = getValue(property);
		if("*".equals(value) || value.startsWith("Class=") ) {
			isValid = false;
		}
		//Regresar el resultado
		return isValid;
	}

	/**
	 * getValue
	 * @param property	Propiedad
	 * @return	Valor de la propiedad
	 */
	private String getValue(PropertiesBean property) {
		//Inicializamos un string para guardar el valor de la propiedad
		String value = "";
		if(property.getValue()!=null) {
			value = ((String)property.getValue()).trim();
		}
		//Regresar el resultado
		return value;
	}

}
