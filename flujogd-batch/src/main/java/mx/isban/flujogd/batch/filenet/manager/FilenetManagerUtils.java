package mx.isban.flujogd.batch.filenet.manager;

import java.io.File;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.bean.ParametroBean;
import mx.isban.flujogd.common.bean.XMLManagerResponse;
import mx.isban.flujogd.common.util.ParametroEnum;
import mx.isban.flujogd.common.util.QueueEnum;
import mx.isban.flujogd.common.util.XMLManager;
import mx.isban.flujogd.persistence.PersistenceManagerBloqueDetail;
import mx.isban.flujogd.persistence.PersistenceManagerSubscriptor;
import mx.isban.flujogd.persistence.impl.PersistenceManagerBloqueDetailImpl;
import mx.isban.flujogd.persistence.impl.PersistenceManagerSubscriptorImpl;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principalFilenetManagerUtils
 *
 */
public class FilenetManagerUtils {
	//Generar la ruta donde se extraeran los ZIP
	/**
	 * crearPath
	 * @param expedienteBean	Vaina del expediente
	 */
	public void crearPath(ExpedienteBean expedienteBean) {
		PersistenceManagerBloqueDetail persistenceManager = new PersistenceManagerBloqueDetailImpl();
		expedienteBean.setPathZIPExpediente(persistenceManager.retrieveParametroBloque(expedienteBean.getIdExpediente(),  QueueEnum.QUEUE_SFTP.getName(), ParametroEnum.PARAMETRO_SFTP_OUTPUT_PATH.getName()).get(0).getValorParametro());
		String pathUnzip = expedienteBean.getPathZIPExpediente()+File.separator+expedienteBean.getNombreExpediente();
		pathUnzip = pathUnzip.substring(0, pathUnzip.length()-4);
		expedienteBean.setPathUNZIPExpediente(pathUnzip);
	}
	
	/**
	 * recuperardocumentos
	 * @param expedienteBean	Vaina del expediente
	 * @return	Documento XML
	 */
	public XMLManagerResponse recuperarDocumentos(ExpedienteBean expedienteBean) {
		XMLManager xmlManager = new XMLManager();
		PersistenceManagerSubscriptor persistenceManager = new PersistenceManagerSubscriptorImpl();
		String nombreSubscriptor = persistenceManager.retrieveSubscriptorByIdExpediente(expedienteBean.getIdExpediente());
		ParametroBean hazelcastPathParam = persistenceManager.retrieveParam(nombreSubscriptor, ParametroEnum.PARAMETRO_GRAL_FILE_DESCRIPTOR.getName());
		return xmlManager.getDocumentos(expedienteBean.getPathUNZIPExpediente(),hazelcastPathParam.getValorParametro());
	}
	
	/**
	 * recuperarfolders
	 * @param expedienteBean	Vaina del expediente
	 * @return	Documento XML
	 */
	public XMLManagerResponse recuperarFolders(ExpedienteBean expedienteBean) {
		PersistenceManagerSubscriptor persistenceManager = new PersistenceManagerSubscriptorImpl();
		String nombreSubscriptor = persistenceManager.retrieveSubscriptorByIdExpediente(expedienteBean.getIdExpediente());
		ParametroBean hazelcastPathParam = persistenceManager.retrieveParam(nombreSubscriptor, ParametroEnum.PARAMETRO_GRAL_FILE_DESCRIPTOR.getName());
		XMLManager xmlManager = new XMLManager();
		return xmlManager.getFolders(expedienteBean.getPathUNZIPExpediente(), hazelcastPathParam.getValorParametro());
	}

}
