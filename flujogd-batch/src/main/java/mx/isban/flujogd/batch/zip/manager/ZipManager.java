package mx.isban.flujogd.batch.zip.manager;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal ZipManager
 *
 */
public class ZipManager {

	//Inicializamos un log para la bitacora
	private static final Logger LOG = LogManager.getLogger(ZipManager.class);

	/**
	 * zipPDF
	 * @param path	Ruta del ZIP
	 * @param nameZip	Nombre del ZIP
	 */
	public void zipPDF( String path, String nameZip) {
		//Inicializamos una lista para almacenar las rutas de los ZIP
		List<String> paths = new ArrayList<>();
		//Obtener las rutas
		getPaths(paths,path,"");
		ZipOutputStream zos = null;
		try (FileOutputStream fos = new FileOutputStream(new File( nameZip +".zip" ))){
			zos = new ZipOutputStream(fos);
			for( String file:paths ){
				ZipEntry ze= new ZipEntry(file);
				zos.putNextEntry(ze);
				
				zos.write( getByteFile(new File(path+File.separator+file)) );
			}
			zos.closeEntry();
			zos.close();
		} catch (FileNotFoundException e) {
			LOG.error(e);
		} catch (IOException e) {
			LOG.error(e);
		} 
	}

	/**
	 * getPaths
	 * @param paths	Lista de rutas
	 * @param path	Ruta del archivo
	 * @param innerPath	Ruta interna
	 */
	private void getPaths(List<String> paths,String path,String innerPath){
		//Inicializamos un File con la ruta del archivo a evaluar
		File file = new File(path+File.separator+innerPath);
		
		
		
		if (file.isFile()) {
			//Agregar la ruta del archivo si se cumple la condicion
			paths.add(innerPath);
		}else {
			//Obtener la ruta de los archivos dentro del directorio
			String[] subFiles = file.list();
			for (String filename: subFiles) {
				String innerPathNew = "";
				//Asignar la ruta de acuerdo a la condicional
				if(!"".equals(innerPath)) {
					innerPathNew = innerPath+File.separator+filename;
				}else {
					innerPathNew = filename;
				}
				getPaths(paths,path,innerPathNew);
			}
		}
	}

	/**
	 * getByteFile
	 * @param file	Archivo
	 * @return	Archivo convertido en un arreglo de bytes
	 */
	public static byte[] getByteFile( File file ){
		byte[] bFile = null;
		try {
			
			bFile = Files.readAllBytes(file.toPath());
		} catch (IOException e) {
			LOG.error(e);
		}
		return bFile;
	}

}
