package mx.isban.flujogd.batch.filenet.manager;

import java.util.List;

import mx.isban.flujogd.common.bean.FilenetBean;
import mx.isban.flujogd.common.bean.FilenetBeanDetalle;
import mx.isban.flujogd.common.bean.ParametroBean;
import mx.isban.flujogd.common.generic.FileNetUtils;
import mx.isban.flujogd.persistence.PersistenceManagerBatch;
import mx.isban.flujogd.persistence.impl.PersistenceManagerBatchImpl;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal FilenetPersistenceUtils
 *
 */
public class FilenetPersistenceUtils {
	
	/**
	 * retrieveFilenetBean
	 * @param conf	Configuracion de los parametros
	 * @return	Bean de la clase FilenetBean
	 */
	public FilenetBean retrieveFilenetBean(String conf){
		FileNetUtils utils = new FileNetUtils();
		PersistenceManagerBatch persistenceManager = new PersistenceManagerBatchImpl();
		//Inicializamos una lista de para almacenar los parametros
		List<ParametroBean> parametros = persistenceManager.retrieveParamByConf(conf);
		FilenetBean filenetBean = null;
		if(parametros != null){
			filenetBean = new FilenetBean();
			FilenetBeanDetalle detalle = new FilenetBeanDetalle();
			//Colocar el detalle del bean
			filenetBean.setDetalle(detalle);
			// Recorrer todos los parametros y enviarlos al bean de conexion para filenet
			for(ParametroBean bean:parametros){
				utils.setParametersFileNet(bean, filenetBean);
			}
		}
		return filenetBean;
	}

}
