package mx.isban.flujogd.batch.thread;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mx.isban.flujogd.batch.apigdmx.client.ApiGdMxBatchClient;
import mx.isban.flujogd.batch.filenet.manager.FilenetPersistenceUtils;
import mx.isban.flujogd.common.bean.DocumentoBean;
import mx.isban.flujogd.common.util.ConfBatchEnum;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal BUCProcessThread
 *
 */
public class BUCProcessThread implements Runnable{

	//Inicializamos un string para almacenar el buc del archivo
	private String buc = null;
	//Inicializamos un string para almacenar la ruta del archivo
	private String path = null;
	//Inicializamos un log para almacenar los errores
	private Writer writeErrors = null;
	
	private boolean bucsErrors = false;

	private static final Logger LOG = LogManager.getLogger(BUCProcessThread.class);

	/**
	 * run
	 */
	@Override
	public void run() {
		try {
			executeProcess();
		} catch (IOException e) {
			//Mostrar el error en bitacora
			LOG.error(e);
			//Mostrar mensaje de error del proceso
			LOG.error( "Ocurrio un error al realizar el proceso del Thread" );
		}
	}

	/**
	 * Ejecuta Logica de Thread
	 * @throws IOException 
	 * @throws Throwable
	 */
	private void executeProcess() throws IOException{
		//Mostrar el buc que se esta procesando
		LOG.info("Se procesa el BUC: "+this.buc);
		FilenetPersistenceUtils persistenceUtils = new FilenetPersistenceUtils();
		ApiGdMxBatchClient apiClient = null;
		try {
			apiClient = new ApiGdMxBatchClient(persistenceUtils.retrieveFilenetBean(ConfBatchEnum.BATCH_CONF_FILENET.getName()));
			//Crear una lista de documentos a partir del buc
			List<DocumentoBean> documentos = apiClient.retrieveDocumentsByBUC(this.buc);
			if(documentos.isEmpty()) {
				//Si no hay documentos se guarda el error en el log
				this.writeErrors.write(this.buc+"=ERROR\n");
				this.bucsErrors = true;
			}else {
				for(DocumentoBean bean:documentos) {
					//Almacenar los documentos
					salvarArchivos(this.path+File.separator+this.buc, bean);
				}
				this.writeErrors.write(this.buc+"=EXITO\n");
			}
		}catch(Exception e) {
			//Guardar en el log el buc que no se pudo guardar
			this.writeErrors.write(this.buc+"=ERROR\n");
			this.bucsErrors = true;
			//Mostrar el error en bitacora
			LOG.error(e);
			//Mostrar mensaje de error al realizar la conexion
			LOG.error("Error al realziar la conexion con ApiGdMxClient");
		}
	}

	/**
	 * config
	 * @param buc	Id del expediente
	 * @param path	Ruta del expediente
	 * @param writeErrors	Log de errores
	 */
	public void config(String buc,String path,Writer writeErrors){
		this.buc = buc;
		this.path = path;
		this.writeErrors = writeErrors;
	}

	/**
	 * salvarArchivos
	 * @param path	Ruta para guardar los archivos
	 * @param bean	Vaina de la clase DocumentoBean
	 */
	public void salvarArchivos( String path, DocumentoBean bean){
		File filePDF = null;
		try {
			File pathFile = new File( path );
			LOG.info("path path de salvarArchivos: "+path);
			if( !pathFile.exists() ) {
				//Crear directorio si no existe el archivo
				pathFile.mkdir();
			}
		}catch(RuntimeException e) {
			//Mostrar el error en bitacora
			LOG.error(e);
		}
		//Inicializamos un string para guardar la ruta del archivo pdf
		String nombreArchivoPdf = path+File.separator+bean.getDetalle().getNombreDocumento()+".pdf";
		LOG.info("nombreArchivoPdf: "+nombreArchivoPdf);
		//Inicializamos un string para guardar la ruta del archivo txt
		String nombreArchivoTxt = path+File.separator+bean.getDetalle().getNombreDocumento()+".txt";
		try (FileOutputStream osw = new FileOutputStream( nombreArchivoTxt )){
			osw.write(bean.getDetalle2().getEncabezadosTxt().getBytes());
			osw.write(bean.getDetalle2().getAtributosTxt().getBytes());
		} catch (FileNotFoundException e) {
			LOG.error(e);
		} catch (UnsupportedEncodingException e) {
			LOG.error(e);
		} catch (IOException e) {
			LOG.error(e);
		} 

		// Creacion del archivo pdf
		filePDF = new File( nombreArchivoPdf );
		try(FileOutputStream fileoutPDF = new FileOutputStream( filePDF )){
			fileoutPDF.write(toByteArray(bean.getDetalle2().getStream()));
			fileoutPDF.flush();
		} catch (IOException e) {
			LOG.error(e);
		}
	}

	/**
	 * toByteArray
	 * @param in	Conjunto de bytes
	 * @return	Arreglo de bytes
	 * @throws IOException	Excepción en caso de existir algun fallo en la ejecución del metodo
	 */
	public static byte[] toByteArray(InputStream in) throws IOException {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int len;

		while ((len = in.read(buffer)) != -1) {
			os.write(buffer, 0, len);
		}
		return os.toByteArray();
	}

	/**
	 * @return the bucsErrors
	 */
	public boolean isBucsErrors() {
		return bucsErrors;
	}

	/**
	 * @param bucsErrors the bucsErrors to set
	 */
	public void setBucsErrors(boolean bucsErrors) {
		this.bucsErrors = bucsErrors;
	}

}
