package mx.isban.flujogd.batch.sftp.manager;

import java.io.Serializable;

/**
 * Objecto que contendra la respuesta del servidor sftp
 * The Class SftpResponseBean.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase Bean
 * Clase utilizada para transporte de informacion
 */
public class SftpResponseBean implements Serializable{
	
	/** serialVersionUID. */
	private static final long serialVersionUID = -1602598294826813746L;

	/** The ok. - almacenara si existe la conexion del servidor*/
	private boolean ok;
	
	/** The error message.  - almacenara el mensaje de error del sftp*/
	private String errorMessage;
	
	/**
	 * Constructor.
	 *
	 * @param ok Verdadero o falso
	 */
	public SftpResponseBean(boolean ok) {
		this.ok = ok;
	}
	
	
	/**
	 * Checks if is ok.
	 *
	 * @return the ok
	 */
	public boolean isOk() {
		return ok;
	}
	
	/**
	 * Sets the ok.
	 *
	 * @param ok the ok to set
	 */
	public void setOk(boolean ok) {
		this.ok = ok;
	}
	
	/**
	 * Gets the error message.
	 *
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}
	
	/**
	 * Sets the error message.
	 *
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
