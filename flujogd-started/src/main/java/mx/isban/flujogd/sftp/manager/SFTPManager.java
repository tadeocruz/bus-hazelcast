package mx.isban.flujogd.sftp.manager;

import java.io.InputStream;

import mx.isban.flujogd.common.bean.SftpResponseBean;
import mx.isban.flujogd.common.generic.SFTPManagerUtils;

/**
 * The Class SFTPManager.
 */
public class SFTPManager {
	
	/** The pass. */
	private String pass = null;
	
	/** The path private key. */
	private String pathPrivateKey = null;

	/** The port. */
	private String port = null;
	
	/** The host. */
	private String host = null;
		
	/** The passphrase. */
	private String passphrase = null;
	
	/** The user. */
	private String user = null;
	
	
	/**
	 * Instantiates a new SFTP manager.
	 *
	 * @param host the host
	 * @param port the port
	 * @param user the user
	 * @param pass the pass
	 */
	public SFTPManager(String host, String port, String user, String pass) {
		this.port = port;
		this.host = host;
		this.pass = pass;
		this.user = user;
	}
	
	/**
	 * Instantiates a new SFTP manager.
	 *
	 * @param host the host
	 * @param port the port
	 * @param user the user
	 * @param pass the pass
	 * @param pathPrivateKey the path private key
	 * @param passphrase the passphrase
	 */
	public SFTPManager(String host, String port, String user, String pass, String pathPrivateKey, String passphrase) {
		this.port = port;
		this.host = host;
		this.pass = pass;
		this.user = user;
		this.passphrase = passphrase;
		this.pathPrivateKey = pathPrivateKey;
	}
	
	/**
	 * save.
	 *
	 * @param fileName the file name
	 * @param is the is
	 * @return the sftp response bean
	 */
	public SftpResponseBean save(String fileName, InputStream is) {
		SFTPManagerUtils utils = new SFTPManagerUtils();
		return utils.saveFileSFTP(fileName, is, this.pathPrivateKey, this.user, this.pass, this.host, this.port, this.passphrase);
	}

}
