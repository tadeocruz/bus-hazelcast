package mx.isban.flujogd.sftp.bean;

import java.io.Serializable;

import lombok.Data;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase Bean
 * Clase utilizada para transporte de informacion
 *
 */
@Data
public class SftpResponseBean implements Serializable{
	
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -6446625337975343334L;

	/**
	 * Constructor
	 * @param ok
	 */
	public SftpResponseBean(boolean ok) {
		this.ok = ok;
	}
	
	private boolean ok;
	private String errorMessage;
	private String errorCode;

}
