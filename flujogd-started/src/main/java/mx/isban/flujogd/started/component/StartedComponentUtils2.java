package mx.isban.flujogd.started.component;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IQueue;

import mx.isban.flujogd.common.bean.NetConfigBean;
import mx.isban.flujogd.common.bean.ParametroBean;
import mx.isban.flujogd.common.util.HazelcastConnectionException;
import mx.isban.flujogd.common.util.InetAddressUtil;
import mx.isban.flujogd.common.util.ParametroEnum;
import mx.isban.flujogd.common.util.QueueEnum;
import mx.isban.flujogd.persistence.PersistenceManagerSubscriptor;

/**
 * The Class StartedComponentUtils2.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase StartedComponent
 */
public class StartedComponentUtils2 {

	/** LOG. */
	private static final Logger LOG = LogManager.getLogger(StartedComponentUtils2.class);

	/**
	 * Metodo para realizar la baja de todo el cluster
	 * killProcess.
	 * @param persistenceManager the persistence manager
	 * @param nombreSubscriptor the nombre subscriptor
	 */
	public void killProcess(PersistenceManagerSubscriptor persistenceManager,String nombreSubscriptor) {
		
		LOG.info("Se ejecuta Shutdown");
		
		try {
			
			InetAddressUtil inetUtil = new InetAddressUtil();
			String ipNet = inetUtil.getLocalNet();
			
			if(!"".equals(ipNet)) {
				procesarBaja(persistenceManager, nombreSubscriptor, ipNet);
			}
			
		} catch (HazelcastConnectionException e) {
			LOG.error(e);
		} catch (InterruptedException e) {
			LOG.error(e);
		}
	}
	
	/**
	 * Metodo para ejecutar la baja de la instancia de hazelcast
	 * procesarBaja.
	 *
	 * @param persistenceManager the persistence manager
	 * @param nombreSubscriptor the nombre subscriptor
	 * @param ipNet the ip net
	 * @throws HazelcastConnectionException the hazelcast connection exception
	 * @throws InterruptedException the interrupted exception
	 */
	private void procesarBaja(PersistenceManagerSubscriptor persistenceManager,String nombreSubscriptor,String ipNet) throws HazelcastConnectionException, InterruptedException {
		
		// Configurar el bean con los datos del subscriptor
		NetConfigBean localNetConfigBean = persistenceManager.retrieveNetConfigByIp(nombreSubscriptor,ipNet);
		
		if(localNetConfigBean!=null){
			
			bajarQueue(localNetConfigBean.getIp(), localNetConfigBean.getPort());
			
		}else {
			
			ParametroBean configLocalNet = persistenceManager.retrieveParam(nombreSubscriptor, ParametroEnum.PARAMETRO_GRAL_CONFIG_LOCAL_NET.getName());
			localNetConfigBean = persistenceManager.retrieveNetConfigByIp(nombreSubscriptor,configLocalNet.getValorParametro());
			
			if(localNetConfigBean!=null){
				bajarQueue(localNetConfigBean.getIp(), localNetConfigBean.getPort());
			}else {
				LOG.info("Este servidor IP no esta habilitado para ejecutar un flujo hazelcast, por lo tanto no es posible ejecutar el killer");
			}
			
		}
	}
	
	/**
	 * Metodo para detener las queue de procesamientos
	 * bajarQueue.
	 *
	 * @param ip the ip
	 * @param port the port
	 * @throws HazelcastConnectionException the hazelcast connection exception
	 * @throws InterruptedException the interrupted exception
	 */
	private void bajarQueue(String ip,String port) throws HazelcastConnectionException, InterruptedException {
		
		StartedComponentUtils4 util = new StartedComponentUtils4();
		HazelcastInstance client = util.crearClienteHazelcast(ip, port, 0);
		
		if(client!=null) {
			IQueue<String> queueKiller = client.getQueue(QueueEnum.QUEUE_KILLER.getName());
			queueKiller.put("echo");
		}else {
			LOG.info("No fue posible establecer una conexion cliente con el servidor para ejecutar el killer");
		}
	}

	/**
	 * Validar si es horario valido para ejecutar los procesos
	 * isValidHourProcess.
	 *
	 * @param hour the hour
	 * @param hoursAlive the hours alive
	 * @return true, if is valid hour process
	 */
	public boolean isValidHourProcess(String hour, int hoursAlive) {
		
		DateFormat dateFormat = new SimpleDateFormat("HH:mm");
		Calendar startDate = Calendar.getInstance();
		Calendar endDate = Calendar.getInstance();
		Calendar cal = Calendar.getInstance();
		boolean killer = true;
		
		try {
			
			cal.setTime(dateFormat.parse(hour));
			startDate.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY));
			startDate.set(Calendar.MINUTE, cal.get(Calendar.MINUTE));
			endDate.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY));
			endDate.set(Calendar.MINUTE, cal.get(Calendar.MINUTE));
			endDate.add(Calendar.HOUR_OF_DAY, hoursAlive);
			LOG.info("startDate "+startDate.getTime());
			LOG.info("endDate "+endDate.getTime());
			killer = isValidHour(startDate, endDate);
			
		} catch (ParseException e) {
			LOG.error(e);
		}
		return killer;
	}

	/**
	 * isHourValid.
	 *
	 * @param startDate the start date
	 * @param endDate the end date
	 * @return true, if is valid hour
	 */
	private boolean isValidHour(Calendar startDate,Calendar endDate) {
		
		Calendar currentDate = Calendar.getInstance();
		boolean isValid = false;
		
		if(startDate.getTime().compareTo(currentDate.getTime())<=0 &&
				endDate.getTime().compareTo(currentDate.getTime())>=0) {
			
			isValid = true;
			
		}else {
			
			currentDate.add(Calendar.DAY_OF_MONTH, 1);
			
			if(startDate.getTime().compareTo(currentDate.getTime())<=0 &&
					endDate.getTime().compareTo(currentDate.getTime())>=0) {
				isValid = true;
			}
			
		}
		LOG.info("currentDate "+currentDate.getTime());
		return isValid;
	}

}
