/**
 * 
 */
package mx.isban.flujogd.started.component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.client.config.ClientNetworkConfig;
import com.hazelcast.core.HazelcastInstance;

import mx.isban.flujogd.common.bean.CifrasControlBean;
import mx.isban.flujogd.common.bean.ParametroBean;
import mx.isban.flujogd.common.bean.SFTPBean;
import mx.isban.flujogd.common.bean.SFTPBeanDetalle;
import mx.isban.flujogd.common.generic.HazelcastGenericUtils;
import mx.isban.flujogd.common.generic.StartedGenericUtils;
import mx.isban.flujogd.common.util.ConfEnum;
import mx.isban.flujogd.common.util.HazelcastConnectionException;
import mx.isban.flujogd.persistence.PersistenceManager;
import mx.isban.flujogd.persistence.PersistenceManagerCifrasControl;
import mx.isban.flujogd.persistence.impl.PersistenceManagerCifrasControlImpl;
import mx.isban.flujogd.persistence.impl.PersistenceManagerImpl;
import mx.isban.flujogd.sftp.manager.SFTPManager;

/**
 * Clase de utileria para componentes Started
 * Contiene la creacion de cliente de servicios hazelcast y SFTP
 * The Class StartedComponentUtils4.
 *
 * @author jccalderon
 */
public class StartedComponentUtils4 {

	/** LOG. */
	private static final Logger LOG = LogManager.getLogger(StartedComponentUtils.class);

	
	/** The sdf. */
	private static SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

	
	/**
	 * Crea una instancia hazelcast.
	 *
	 * @param ip the ip
	 * @param port the port
	 * @param timeSleep the time sleep
	 * @return the hazelcast instance
	 * @throws HazelcastConnectionException the hazelcast connection exception
	 */
	public HazelcastInstance crearClienteHazelcast(String ip,String port,long timeSleep) throws HazelcastConnectionException {
		
		HazelcastGenericUtils hazelcastUtils = new HazelcastGenericUtils();
		LOG.info("Inicia creacion cliente hazelcast");
		
		ClientConfig config = new ClientConfig();
		ClientNetworkConfig cnc = new ClientNetworkConfig();
		cnc.addAddress(ip+":"+port);
		config.setNetworkConfig(cnc);
		return hazelcastUtils.getClientHazelcast(timeSleep,config);
		
	}
	

	/**
	 * Metodo para notificar el estatus de los expedientes procesados
	 * notificarCifrasControl.
	 *
	 * @param subscriptor the subscriptor
	 */
	public void notificarCifrasControl(String subscriptor) {
		
		PersistenceManagerCifrasControl pmn = new PersistenceManagerCifrasControlImpl();
		CifrasControlBean ccBean = pmn.notificarCifrasCtrl(subscriptor);
		
		if(ccBean!= null && ccBean.getListIdsExpedientes()!=null && ccBean.getListIdsExpedientes().size() > 0 ) {
			
			SFTPBean sftpBean = retrieveSftpBean(ConfEnum.CONF_SFTP.getName());
			
			if(sftpBean!=null) {
				
				SFTPManager sftpManager = new SFTPManager(sftpBean.getHost(), sftpBean.getPort(), sftpBean.getUser(), sftpBean.getPass(),sftpBean.getDetalle().getPathPrivateKey(),sftpBean.getDetalle().getPassphrase());
				String nombre = sftpBean.getDetalle().getSrcPath()+"/cifras-control-"+subscriptor+"-"+sdf.format(new Date())+".txt";
				sftpManager.save(nombre, ccBean.getReporteInStre());
				LOG.info("Reporte {} generado.", nombre);
				pmn.updateExpedientes(ccBean.getListIdsExpedientes());
				
			} else {
				LOG.error("Falta configuracion de {}", ConfEnum.CONF_SFTP.getName());
			}
			
		} else {
			LOG.info("No existen registros a incluir");
		}
	}
	
	/**
	 * Metodo para obtener los parametros de sftp
	 * retrieveSftpBean.
	 *
	 * @param config the config
	 * @return the SFTP bean
	 */
	private SFTPBean retrieveSftpBean(String config){
		
		StartedGenericUtils utils = new StartedGenericUtils();
		PersistenceManager persistenceManager = new PersistenceManagerImpl();
		List<ParametroBean> parametros = persistenceManager.retrieveParamByConf(config);
		SFTPBean sftpBean = null;
		
		if(parametros != null){
			
			sftpBean = new SFTPBean();
			
			SFTPBeanDetalle detalle = new SFTPBeanDetalle();
			sftpBean.setDetalle(detalle);
			
			for(ParametroBean bean:parametros){
				// LLenar parametros de sftp
				utils.getValue(bean, sftpBean);
			}
		}
		
		return sftpBean;
	}
}
