package mx.isban.flujogd.started.component;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mx.isban.flujogd.common.bean.NetConfigBean;
import mx.isban.flujogd.started.bean.StartedJvmBean;

/**
 * The Class StartedComponentUtils.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase StartedComponentUtils
 */
public class StartedComponentUtils {

	/** LOG. */
	private static final Logger LOG = LogManager.getLogger(StartedComponentUtils.class);
	
	/**
	 * iniciaFlujo.
	 *
	 * @param componente the componente
	 * @param version the version
	 * @param ip the ip
	 * @param path the path
	 * @param port the port
	 * @param nombreSubscriptor the nombre subscriptor
	 * @param numeroInstancias the numero instancias
	 * @param javaPathCacerts the java path cacerts
	 * @param javaKeyStorePCommand the java key store P command
	 */
	public void iniciaFlujo(String componente,String version,String ip,String path,String port,String nombreSubscriptor,String numeroInstancias,String javaPathCacerts,String javaKeyStorePCommand) {
		
		StartedJvmBean bean = new StartedJvmBean();
		
		bean.setJavaPathCacertsValue(javaPathCacerts);
		bean.setJavaKeyStorePCommand(javaKeyStorePCommand);
		bean.setComponente(componente);
		bean.setVersion(version);
		bean.setIp(ip);
		bean.setNombreSubscriptor(nombreSubscriptor);
		bean.setPath(path);
		bean.setPort(port);
		
		LOG.info("EL BLOQUE "+componente+" REQUIERE "+numeroInstancias+" INSTANCIAS" + " :: SUBSC "+nombreSubscriptor);
		
		for(int i=0;i< Integer.parseInt(numeroInstancias);i++) {
			iniciaFlujoCmd(bean);
		}
	}

	/**
	 * iniciaFlujoCmd.
	 *
	 * @param bean the bean
	 */
	private void iniciaFlujoCmd(StartedJvmBean bean){
		
		String componenteJar = bean.getPath()+File.separator+bean.getComponente()+"-"+bean.getVersion()+".jar";
		
		String [] params = new String[]{
				
				bean.getCommandoJava(),
				bean.getCommandoSsl()+bean.getJavaPathCacertsValue(),
				bean.getCommandoSecurity(),
				bean.getCommandoHazelcast(),
				bean.getCommandoPkcs(),
				bean.getCommandoJks(),
				bean.getJavaKeyStorePCommand(),
				bean.getCommandoJar(),
				componenteJar,
				bean.getIp(),
				bean.getPort(),
				bean.getPath(),
				bean.getComponente(),
				bean.getNombreSubscriptor()};
		
		LOG.info(componenteJar);
		
		ProcessBuilder ejecucionT = new ProcessBuilder(params);
		
		try {
			ejecucionT.start();
		} catch (IOException e) {
			LOG.error(e);
		}
	}

	/**
	 * Concatena todas las ip donde viven los diferentes flujos de hazelcast.
	 *
	 * @param ip the ip
	 * @param lista the lista
	 * @return the string
	 */
	public String concatNetConfig( String ip, List<NetConfigBean> lista){
		
		StringBuilder ips = new StringBuilder();
		ips.append(ip);
		
		for(NetConfigBean netConfigBean:lista) {
			ips.append("," + netConfigBean.getIp());
		}
		
		return ips.toString();
	}
}
