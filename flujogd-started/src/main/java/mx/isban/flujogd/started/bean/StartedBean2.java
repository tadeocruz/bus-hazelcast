package mx.isban.flujogd.started.bean;

import java.io.Serializable;

/**
 * The Class StartedBean2.
 */
public class StartedBean2 implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4600017939156502364L;
	
	/** The ip. */
	private transient String ip;
	
	/** The path. */
	private transient String path;
	
	/** The port. */
	private transient String port;
	
	/** The started bean 3. */
	private StartedBean3 startedBean3;
	
	/**
	 * Gets the ip.
	 *
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}
	
	/**
	 * Sets the ip.
	 *
	 * @param ip the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}
	
	/**
	 * Gets the path.
	 *
	 * @return the path
	 */
	public String getPath() {
		return path;
	}
	
	/**
	 * Sets the path.
	 *
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}
	
	/**
	 * Gets the port.
	 *
	 * @return the port
	 */
	public String getPort() {
		return port;
	}
	
	/**
	 * Sets the port.
	 *
	 * @param port the port to set
	 */
	public void setPort(String port) {
		this.port = port;
	}
	
	/**
	 * Gets the started bean 3.
	 *
	 * @return the startedBean3
	 */
	public StartedBean3 getStartedBean3() {
		return startedBean3;
	}
	
	/**
	 * Sets the started bean 3.
	 *
	 * @param startedBean3 the startedBean3 to set
	 */
	public void setStartedBean3(StartedBean3 startedBean3) {
		this.startedBean3 = startedBean3;
	}

}
