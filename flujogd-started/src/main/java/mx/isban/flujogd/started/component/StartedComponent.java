package mx.isban.flujogd.started.component;

import java.util.List;
import java.util.ListIterator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hazelcast.core.HazelcastInstance;

import mx.isban.flujogd.common.bean.BloqueBean;
import mx.isban.flujogd.common.bean.NetConfigBean;
import mx.isban.flujogd.common.bean.ParametroBean;
import mx.isban.flujogd.common.bean.SubscriptorBean;
import mx.isban.flujogd.common.util.HazelcastConnectionException;
import mx.isban.flujogd.common.util.InetAddressUtil;
import mx.isban.flujogd.common.util.MallaStatusEnum;
import mx.isban.flujogd.common.util.ParametroEnum;
import mx.isban.flujogd.common.util.ParametroValueEnum;
import mx.isban.flujogd.common.util.QueueEnum;
import mx.isban.flujogd.persistence.PersistenceManagerBloqueDetail;
import mx.isban.flujogd.persistence.PersistenceManagerSubscriptor;
import mx.isban.flujogd.persistence.impl.PersistenceManagerBloqueDetailImpl;
import mx.isban.flujogd.persistence.impl.PersistenceManagerSubscriptorImpl;

/**
 * The Class StartedComponent.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase StartedComponent
 */
public class StartedComponent {

	/** LOG. */
	private static final Logger LOG = LogManager.getLogger(StartedComponent.class);

	/**
	 * Inicia el proceso de los flujos hazelcast.
	 */
	public void execute() {
		
		PersistenceManagerSubscriptor persistenceManager = new PersistenceManagerSubscriptorImpl();
		PersistenceManagerBloqueDetail persistenceManager2 = new PersistenceManagerBloqueDetailImpl();
		StartedComponentUtils2 utils2 = new StartedComponentUtils2();
		
		LOG.info("Inicia InitProcess");
		
		//Obtenemos todos los subscriptores
		List<SubscriptorBean> subscriptores = persistenceManager.retrieveAllSubscriptores();
		LOG.info("Subscriptores recuperados: " + subscriptores.size());
		
		for(SubscriptorBean subscriptor:subscriptores){
			
			//A que hora se debe de dar de baja el flujo?
			ParametroBean hazelcastHourShutdownParam = persistenceManager.retrieveParam(subscriptor.getNombreSubscriptor(), ParametroEnum.PARAMETRO_GRAL_HAZELCAST_HOUR_SHUTDOWN.getName());
			String[] params = hazelcastHourShutdownParam.getValorParametro().split(",");
			
			if(utils2.isValidHourProcess(params[0], new Integer(params[1]))) {
				
				LOG.info("{} : EL FLUJO ESTA EN HORARIO VALIDO: SE DEBE DE LEVANTAR O SI YA LO ESTA DEBE DE PERMANECER ASI", subscriptor.getNombreSubscriptor());
				procesarSubscriptor(persistenceManager, persistenceManager2, subscriptor);
			
			}else {
				
				LOG.info("{} : EL FLUJO ESTA FUERA DE HORARIO VALIDO: SE DEBE DE DAR DE BAJA O SI YA LO ESTA DEBE DE PERMANECER ASI", subscriptor.getNombreSubscriptor());
				utils2.killProcess(persistenceManager, subscriptor.getNombreSubscriptor());
				updateStatusMalla(persistenceManager, subscriptor.getIdSubscriptor(), MallaStatusEnum.MALLA_STATUS_INACTIVA.getName());
				persistenceManager.updateStausRed(subscriptor.getIdSubscriptor(), ParametroValueEnum.PARAMETRO_VALUE_INACTIVO.getName());
				
				//Cifras Control
				StartedComponentUtils4 utils = new StartedComponentUtils4();
				LOG.info("{} : Generando cifras control", subscriptor.getNombreSubscriptor());
				utils.notificarCifrasControl(subscriptor.getNombreSubscriptor());
			}
			
		}
		
		LOG.info("Termina InitProcess");
	}
	
	/**
	 * updateStatusMalla.
	 *
	 * @param persistenceManager the persistence manager
	 * @param idSubscriptor the id subscriptor
	 * @param status the status
	 */
	private void updateStatusMalla(PersistenceManagerSubscriptor persistenceManager, String idSubscriptor, String status) {
		
		LOG.info("Se cambia el estatus de la malla: "+status);
		persistenceManager.updateStausMallaSubscriptor(idSubscriptor, status);
		
	}
	
	/**
	 * procesarSubscriptor.
	 *
	 * @param persistenceManager the persistence manager
	 * @param persistenceManager2 the persistence manager 2
	 * @param subscriptor the subscriptor
	 */
	private void procesarSubscriptor(PersistenceManagerSubscriptor persistenceManager,PersistenceManagerBloqueDetail persistenceManager2,SubscriptorBean subscriptor) {
		
		LOG.info("Inicia revision flujo: " + subscriptor.getNombreSubscriptor());
		String nombreSubscriptor = subscriptor.getNombreSubscriptor();
		
		//Debe hazelcast estar activo?
		ParametroBean hazelcastActivoParam = persistenceManager.retrieveParam(nombreSubscriptor, ParametroEnum.PARAMETRO_GRAL_HAZELCAST_ACTIVO.getName());
		
		//Recuperamos el path donde viven los componentes hazelcast
		ParametroBean hazelcastPathParam = persistenceManager.retrieveParam(nombreSubscriptor, ParametroEnum.PARAMETRO_GRAL_HAZELCAST_PATH.getName());
		
		//Recuperamos el path donde vive el cacerts
		ParametroBean javaPathCacerts = persistenceManager.retrieveParam(nombreSubscriptor, ParametroEnum.PARAMETRO_GRAL_JAVA_PATH_CACERTS.getName());
		
		//Recuperamos el key store p
		ParametroBean javaKeyStoreP = persistenceManager.retrieveParam(nombreSubscriptor, ParametroEnum.PARAMETRO_GRAL_JAVA_KEY_STORE_P.getName());
		
		//Recuperamos al configuracion para la maquina en donde esta siendo ejecutado el flujogd-started
		InetAddressUtil inetUtil = new InetAddressUtil();
		
		String ipNet = inetUtil.getLocalNet();
		
		if(!"".equals(ipNet)) {
			
			//Configurar el bean con los datos del subscriptor
			NetConfigBean localNetConfigBean = persistenceManager.retrieveNetConfigByIp(nombreSubscriptor,ipNet);
			
			if(localNetConfigBean!=null){
				
				procesarLocalNetwork(persistenceManager, persistenceManager2, subscriptor, ipNet, hazelcastActivoParam, localNetConfigBean, hazelcastPathParam,javaPathCacerts,javaKeyStoreP);
			
			}else {
				
				ParametroBean configLocalNet = persistenceManager.retrieveParam(nombreSubscriptor, ParametroEnum.PARAMETRO_GRAL_CONFIG_LOCAL_NET.getName());
				localNetConfigBean = persistenceManager.retrieveNetConfigByIp(nombreSubscriptor,configLocalNet.getValorParametro());
				
				if(localNetConfigBean!=null){
					procesarLocalNetwork(persistenceManager, persistenceManager2, subscriptor, ipNet, hazelcastActivoParam, localNetConfigBean, hazelcastPathParam,javaPathCacerts,javaKeyStoreP);
				}else {
					LOG.info("Este servidor IP no esta habilitado para ejecutar un flujo hazelcast, si se requiere es necesario agregarlo a la parametria");
				}
				
			}
			
		}else {
			LOG.info("Ocurrio un problema al recuperar la IP de este servidor, sin ella no es posible ejecutar el flujo hazelcast");
		}
	}
	
	/**
	 * procesarLocalNetwork.
	 *
	 * @param persistenceManager the persistence manager
	 * @param persistenceManager2 the persistence manager 2
	 * @param subscriptor the subscriptor
	 * @param iAddress the i address
	 * @param hazelcastActivoParam the hazelcast activo param
	 * @param localNetConfigBean the local net config bean
	 * @param hazelcastPathParam the hazelcast path param
	 * @param javaPathCacerts the java path cacerts
	 * @param javaKeyStoreP the java key store P
	 */
	private void procesarLocalNetwork(PersistenceManagerSubscriptor persistenceManager,PersistenceManagerBloqueDetail persistenceManager2,SubscriptorBean subscriptor,String iAddress,ParametroBean hazelcastActivoParam,NetConfigBean localNetConfigBean,ParametroBean hazelcastPathParam,ParametroBean javaPathCacerts, ParametroBean javaKeyStoreP) {
		
		List<NetConfigBean> anothersNetConfig = persistenceManager.retrieveAllAnotherNetConfig(subscriptor.getNombreSubscriptor(),iAddress);
		StartedComponentUtils4 util = new StartedComponentUtils4();
		StartedComponentUtils3 utils3 = new StartedComponentUtils3();
		
		if(hazelcastActivoParam.getValorParametro().equals(ParametroValueEnum.PARAMETRO_VALUE_ACTIVO.getName())){
			
			LOG.info("Segun la parametria hazelcast para este subscriptor requiere estas activo");
			
			// Cuantos flujos(maquinas viruales) deben de estar arriba
			ParametroBean numeroFlujosParam = persistenceManager.retrieveParam(subscriptor.getNombreSubscriptor(), ParametroEnum.PARAMETRO_GRAL_RED_NUMERO_FLUJOS.getName());
			
			/* Si la configuracion del nodo esta activa solo se revisa que este bien */
			if(localNetConfigBean.getActiva().equals(ParametroValueEnum.PARAMETRO_VALUE_ACTIVO.getName())){
				
				utils3.revisarHazelcast(persistenceManager2, subscriptor.getNombreSubscriptor(), localNetConfigBean, anothersNetConfig, hazelcastPathParam,javaPathCacerts,javaKeyStoreP);
			
			}else{
				
				levantarHazelcast(persistenceManager, persistenceManager2, subscriptor, numeroFlujosParam, localNetConfigBean, anothersNetConfig, hazelcastPathParam,javaPathCacerts,javaKeyStoreP);
			
			}
			
		}else{
			
			LOG.info("Hazelcast ya no debe de estar activo");
			LOG.info("Revisamos si aun existen procesos ejecutandose en el flujo, de lo contrario se dan de baja los nodos");
			
			// Hazelcast para el susbscriptor especificado ya no debe de estar activo
			// Ya no se inyectan mas expedientes a la queue dispatcher
			// Se revisa si aun faltan expedientes por ser procesados en alguna de las queues de todos los bloques
			HazelcastInstance client = null;
			
			try {
				
				client = util.crearClienteHazelcast(localNetConfigBean.getIp(), localNetConfigBean.getPort(), 0);
				
				if(client!=null) {
					utils3.revisarExpedientesEnProcesos(persistenceManager, client, localNetConfigBean);
				}
				
			} catch (HazelcastConnectionException e) {
				LOG.error(e);
			}
		}
	}
	
	/**
	 * levantarHazelcast.
	 *
	 * @param persistenceManager the persistence manager
	 * @param persistenceManager2 the persistence manager 2
	 * @param subscriptor the subscriptor
	 * @param numeroFlujosParam the numero flujos param
	 * @param localNetConfigBean the local net config bean
	 * @param anothersNetConfig the anothers net config
	 * @param hazelcastPathParam the hazelcast path param
	 * @param javaPathCacerts the java path cacerts
	 * @param javaKeyStoreP the java key store P
	 */
	private void levantarHazelcast(PersistenceManagerSubscriptor persistenceManager,PersistenceManagerBloqueDetail persistenceManager2,SubscriptorBean subscriptor,ParametroBean numeroFlujosParam,NetConfigBean localNetConfigBean,List<NetConfigBean> anothersNetConfig,ParametroBean hazelcastPathParam,ParametroBean javaPathCacerts,ParametroBean javaKeyStoreP) {
		
		LOG.info("Los nodos del flujo para este servidor no estan activos actualmente");
		StartedComponentUtils utils = new StartedComponentUtils();
		
		// Si no esta activa, se revisa si es necesario levantar otro flujo
		Integer activos = persistenceManager.retrieveNetActiveNow(subscriptor.getNombreSubscriptor());
		
		if(new Integer(numeroFlujosParam.getValorParametro()).intValue()>activos){
			
			LOG.info("Segun la parametria, es necesario levantar los nodos del flujo en este servidor");
			List<BloqueBean> bloques = persistenceManager2.retrieveBloques(subscriptor.getNombreSubscriptor());
			
			// Agregamos el flujogd-dispatcher a los bloques en caso de que este abajo
			bloques.add(new BloqueBean(QueueEnum.QUEUE_DISPATCHER.getName(),bloques.get(0).getVersion(),"1",javaPathCacerts.getValorParametro(),javaKeyStoreP.getValorParametro()));
			ListIterator<BloqueBean> iter = bloques.listIterator(bloques.size());
			
			while(iter.hasPrevious()) {
				
				BloqueBean bean = iter.previous();
				
				utils.iniciaFlujo(bean.getNombre(), bean.getVersion(), utils.concatNetConfig(localNetConfigBean.getIp(), anothersNetConfig),hazelcastPathParam.getValorParametro(), localNetConfigBean.getPort(),subscriptor.getNombreSubscriptor(),bean.getBean2().getNumeroInstancias(),javaPathCacerts.getValorParametro(),javaKeyStoreP.getValorParametro());
			}
			
			// Levantamos flujogd-dispatcher
			persistenceManager.updateStausNetConfig(localNetConfigBean.getId(), ParametroValueEnum.PARAMETRO_VALUE_ACTIVO.getName());
			updateStatusMalla(persistenceManager, subscriptor.getIdSubscriptor(), MallaStatusEnum.MALLA_STATUS_ACTIVA.getName());
		
		}else {
			LOG.info("Segun la parametria, no es necesario levantar los nodos del flujo en este servidor");
		}
	}

}
