package mx.isban.flujogd.started;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.scheduling.annotation.EnableAsync;

import mx.isban.flujogd.started.component.StartedComponent;

/**
 * Clase principal para el .jar de started
 * The Class FlujoGDStartedApplication.
 * 
 */
@SpringBootApplication
@EnableAsync
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
public class FlujoGDStartedApplication implements CommandLineRunner{
	
	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(FlujoGDStartedApplication.class);
	
	/**
	 * Metodo principal para ejecutar el flujo started
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		SpringApplication sp = new SpringApplication(FlujoGDStartedApplication.class);
		sp.run(args);
		System.exit(0);
	}

	/**
	 * Lanzar ejecucion started
	 * Run.
	 *
	 * @param args the args
	 * @throws Exception the exception
	 */
	@Override
	public void run(String... args) throws Exception {
		LOG.info("Inicia StartedComponent");
		StartedComponent batchComponent = new StartedComponent();
		batchComponent.execute();
	}

}
