package mx.isban.flujogd.started.bean;

import java.io.Serializable;

/**
 * The Class StartedBean3.
 */
public class StartedBean3 implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4600017939156502364L;
	
	/** The nombre subscriptor. */
	private transient String nombreSubscriptor;
	
	/** The componente. */
	private transient String componente;
	
	/** The version. */
	private transient String version;
	
	/** The java path cacerts value. */
	private transient String javaPathCacertsValue;
	
	/** The java key store P command. */
	private transient String javaKeyStorePCommand;
	
	/**
	 * Gets the nombre subscriptor.
	 *
	 * @return the nombreSubscriptor
	 */
	public String getNombreSubscriptor() {
		return nombreSubscriptor;
	}
	
	/**
	 * Sets the nombre subscriptor.
	 *
	 * @param nombreSubscriptor the nombreSubscriptor to set
	 */
	public void setNombreSubscriptor(String nombreSubscriptor) {
		this.nombreSubscriptor = nombreSubscriptor;
	}
	
	/**
	 * Gets the componente.
	 *
	 * @return the componente
	 */
	public String getComponente() {
		return componente;
	}
	
	/**
	 * Sets the componente.
	 *
	 * @param componente the componente to set
	 */
	public void setComponente(String componente) {
		this.componente = componente;
	}
	
	/**
	 * Gets the version.
	 *
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}
	
	/**
	 * Sets the version.
	 *
	 * @param version the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}
	
	/**
	 * Gets the java path cacerts value.
	 *
	 * @return the javaPathCacertsValue
	 */
	public String getJavaPathCacertsValue() {
		return javaPathCacertsValue;
	}
	
	/**
	 * Sets the java path cacerts value.
	 *
	 * @param javaPathCacertsValue the javaPathCacertsValue to set
	 */
	public void setJavaPathCacertsValue(String javaPathCacertsValue) {
		this.javaPathCacertsValue = javaPathCacertsValue;
	}
	
	/**
	 * Gets the java key store P command.
	 *
	 * @return the javaKeyStorePCommand
	 */
	public String getJavaKeyStorePCommand() {
		return javaKeyStorePCommand;
	}
	
	/**
	 * Sets the java key store P command.
	 *
	 * @param javaKeyStorePCommand the javaKeyStorePCommand to set
	 */
	public void setJavaKeyStorePCommand(String javaKeyStorePCommand) {
		this.javaKeyStorePCommand = javaKeyStorePCommand;
	}
	

}
