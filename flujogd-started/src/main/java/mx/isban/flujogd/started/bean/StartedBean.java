package mx.isban.flujogd.started.bean;

import java.io.Serializable;

/**
 * Objeto que contendra comandos comunes
 * 
 * The Class StartedBean.
 */
public class StartedBean implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7700620531802270377L;
	
	/** The bean 2. */
	private StartedBean2 bean2;
	
	/** The command 1. */
	private static final transient String COMMAND1 = "java";
	
	/** The command 2. */
	private static final transient String COMMAND2 = "-Djavax.net.ssl.trustStore=";
	
	/** The command 3. */
	private static final transient String COMMAND3 = "-Djava.security.egd=file:/dev/./urandom";
	
	/** The command 4. */
	private static final transient String COMMAND4 = "-Dhazelcast.diagnostics.enabled=false";
	
	/** The command 5. */
	private static final transient String COMMAND5 = "-Djavax.net.ssl.keyStoreType=PKCS12";
	
	/** The command 6. */
	private static final transient String COMMAND6 = "-Djavax.net.ssl.trustStoreType=jks";
	
	/** The command 7. */
	private static final transient String COMMAND7 = "-jar";
	
	/**
	 * Gets the bean 2.
	 *
	 * @return the bean2
	 */
	public StartedBean2 getBean2() {
		return bean2;
	}
	
	/**
	 * Sets the bean 2.
	 *
	 * @param bean2 the bean2 to set
	 */
	public void setBean2(StartedBean2 bean2) {
		this.bean2 = bean2;
	}
	
	/**
	 * Gets the command 1.
	 *
	 * @return the command1
	 */
	public String getCommand1() {
		return COMMAND1;
	}
	
	/**
	 * Gets the command 2.
	 *
	 * @return the command2
	 */
	public String getCommand2() {
		return COMMAND2;
	}
	
	/**
	 * Gets the command 3.
	 *
	 * @return the command3
	 */
	public String getCommand3() {
		return COMMAND3;
	}
	
	/**
	 * Gets the command 4.
	 *
	 * @return the command4
	 */
	public String getCommand4() {
		return COMMAND4;
	}
	
	/**
	 * Gets the command 5.
	 *
	 * @return the command5
	 */
	public String getCommand5() {
		return COMMAND5;
	}
	
	/**
	 * Gets the command 6.
	 *
	 * @return the command6
	 */
	public String getCommand6() {
		return COMMAND6;
	}
	
	/**
	 * Gets the command 7.
	 *
	 * @return the command8
	 */
	public String getCommand7() {
		return COMMAND7;
	}
	
}
