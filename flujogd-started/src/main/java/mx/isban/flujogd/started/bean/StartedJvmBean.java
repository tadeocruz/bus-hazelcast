package mx.isban.flujogd.started.bean;

import java.io.Serializable;

import lombok.Data;

/**
 * Parametros para inicion de un nuevo compontnete o suscriptor
 * Los parametros consisten en las ips, nombre, path donde esta ubicado el jar de componente
 *  y comando java para incializar el jar
 * 
 */
@Data
public class StartedJvmBean implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7700620531802270377L;
	
	/** The command 1. */
	private static final transient String COMMANDO_JVM_JAVA = "java";
	
	/** The command 2. */
	private static final transient String COMMANDO_JVM_SSL = "-Djavax.net.ssl.trustStore=";
	
	/** The command 3. */
	private static final transient String COMMANDO_JVM_SECURITY = "-Djava.security.egd=file:/dev/./urandom";
	
	/** The command 4. */
	private static final transient String COMMANDO_JVM_HAZELCAST = "-Dhazelcast.diagnostics.enabled=false";
	
	/** The command 5. */
	private static final transient String COMMANDO_JVM_PKCS = "-Djavax.net.ssl.keyStoreType=PKCS12";
	
	/** The command 6. */
	private static final transient String COMMANDO_JVM_JKS = "-Djavax.net.ssl.trustStoreType=jks";
	
	/** The command 7. */
	private static final transient String COMMANDO_JVM_JAR = "-jar";
	
	/** The ip. */
	private transient String ip;
	
	/** The path. */
	private transient String path;
	
	/** The port. */
	private transient String port;
	
	/** The nombre subscriptor. */
	private transient String nombreSubscriptor;
	
	/** The componente. */
	private transient String componente;
	
	/** The version. */
	private transient String version;
	
	/** The java path cacerts value. */
	private transient String javaPathCacertsValue;
	
	/** The java key store P command. */
	private transient String javaKeyStorePCommand;
	
	/**
	 * Devuelve el comando java
	 *
	 * @return comando en string
	 */
	public String getCommandoJava() {
		return COMMANDO_JVM_JAVA;
	}
	
	/**
	 * Devuelve el comando -Djavax.net.ssl.trustStore=
	 *
	 * @return comando en string
	 */
	public String getCommandoSsl() {
		return COMMANDO_JVM_SSL;
	}
	
	/**
	 * Devuelve el comando -Djavax.net.ssl.trustStore="
	 *
	 * @return comando en string
	 */
	public String getCommandoSecurity() {
		return COMMANDO_JVM_SECURITY;
	}
	
	/**
	 * Devuelve el comando -Dhazelcast.diagnostics.enabled=false
	 *
	 * @return comando en string
	 */
	public String getCommandoHazelcast() {
		return COMMANDO_JVM_HAZELCAST;
	}
	
	/**
	 * Devuelve el comando -Djavax.net.ssl.keyStoreType=PKCS12
	 *
	 * @return comando en string
	 */
	public String getCommandoPkcs() {
		return COMMANDO_JVM_PKCS;
	}
	
	/**
	 * Devuelve el comando -Djavax.net.ssl.trustStoreType=jks
	 *
	 * @return comando en string
	 */
	public String getCommandoJks() {
		return COMMANDO_JVM_JKS;
	}
	
	/**
	 * Devuelve el comando -jar
	 *
	 * @return comando en string
	 */
	public String getCommandoJar() {
		return COMMANDO_JVM_JAR;
	}
	
}
