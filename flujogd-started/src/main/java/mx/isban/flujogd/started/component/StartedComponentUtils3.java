package mx.isban.flujogd.started.component;

import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hazelcast.core.DistributedObject;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IQueue;

import mx.isban.flujogd.common.bean.BloqueBean;
import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.bean.MemberBean;
import mx.isban.flujogd.common.bean.NetConfigBean;
import mx.isban.flujogd.common.bean.ParametroBean;
import mx.isban.flujogd.common.util.HazelcastConnectionException;
import mx.isban.flujogd.common.util.MapEnum;
import mx.isban.flujogd.common.util.ParametroValueEnum;
import mx.isban.flujogd.common.util.QueueEnum;
import mx.isban.flujogd.persistence.PersistenceManagerBloqueDetail;
import mx.isban.flujogd.persistence.PersistenceManagerSubscriptor;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase StartedComponent
 *
 */
public class StartedComponentUtils3 {

	/** LOG. */
	private static final Logger LOG = LogManager.getLogger(StartedComponentUtils3.class);
	
	/**
	 * revisarHazelcast.
	 *
	 * @param persistenceManager2 the persistence manager 2
	 * @param nombreSubscriptor the nombre subscriptor
	 * @param localNetConfigBean the local net config bean
	 * @param anothersNetConfig the anothers net config
	 * @param hazelcastPathParam the hazelcast path param
	 * @param javaPathCacerts the java path cacerts
	 * @param javaKeyStoreP the java key store P
	 */
	public void revisarHazelcast(PersistenceManagerBloqueDetail persistenceManager2,String nombreSubscriptor,NetConfigBean localNetConfigBean,List<NetConfigBean> anothersNetConfig,ParametroBean hazelcastPathParam,ParametroBean javaPathCacerts,ParametroBean javaKeyStoreP) {
		
		LOG.info("Los nodos del flujo para este servidor deberian de estar activos :: SUBSC :: ", nombreSubscriptor);
		LOG.info("Revisamos si todos los flujos estan activos, de lo contrario activamos el/los faltantes");
		
		StartedComponentUtils4 util = new StartedComponentUtils4();
		List<BloqueBean> bloques = persistenceManager2.retrieveBloques(nombreSubscriptor);
		
		if(!bloques.isEmpty()) {
			
			// Agregamos el flujogd-dispatcher a los bloques en caso de que este abajo
			bloques.add(new BloqueBean(QueueEnum.QUEUE_DISPATCHER.getName(),bloques.get(0).getVersion(),"1",javaPathCacerts.getValorParametro(),javaKeyStoreP.getValorParametro()));
			HazelcastInstance client = null;
			
			try {
				// Creacion de cliente de hazelcast
				client = util.crearClienteHazelcast(localNetConfigBean.getIp(), localNetConfigBean.getPort(),0);
			} catch (HazelcastConnectionException e) {
				LOG.error(e);
			}
			
			// Validar Hazelcast y levantar bloques de los flujos
			levantarBloques(client, bloques, localNetConfigBean, anothersNetConfig, hazelcastPathParam, nombreSubscriptor, javaPathCacerts, javaKeyStoreP);
			
		}else {
			LOG.info("No existen flujos en parametria para este subscriptor :: "+ nombreSubscriptor);
		}
	
	}
	
	/**
	 * Metodo para validar Hazelcast y levantar bloques de los flujos
	 * Levantar bloques.
	 *
	 * @param client the client
	 * @param bloques the bloques
	 * @param localNetConfigBean the local net config bean
	 * @param anothersNetConfig the anothers net config
	 * @param hazelcastPathParam the hazelcast path param
	 * @param nombreSubscriptor the nombre subscriptor
	 * @param javaPathCacerts the java path cacerts
	 * @param javaKeyStoreP the java key store P
	 */
	private void levantarBloques(HazelcastInstance client, List<BloqueBean> bloques, NetConfigBean localNetConfigBean, List<NetConfigBean> anothersNetConfig, ParametroBean hazelcastPathParam, String nombreSubscriptor, ParametroBean javaPathCacerts, ParametroBean javaKeyStoreP) {
		
		if(client!=null){
			
			// Hazelcast esta activo en esta maquina virtual, Se revisa que todos los bloques que conforman al flujo esten arriba
			Map<String, MemberBean> members = client.getMap(MapEnum.MAP_MEMBER_STATUS.getName());
			
			for (Map.Entry<String, MemberBean> entry : members.entrySet()){
				revisarMap(entry, bloques, localNetConfigBean, anothersNetConfig, hazelcastPathParam, nombreSubscriptor, members,javaPathCacerts,javaKeyStoreP);
			}
			
		}else{
			
			// Cuando hazelcast esta activo, se iniciara
			LOG.info("Hazelcast esta abajo, se levantan todos los bloques del flujo :: " +nombreSubscriptor);
			ListIterator<BloqueBean> iter = bloques.listIterator(bloques.size());
			StartedComponentUtils utils = new StartedComponentUtils();
			
			while(iter.hasPrevious()) {
				BloqueBean bean = iter.previous();
				utils.iniciaFlujo(bean.getNombre(), bean.getVersion(), utils.concatNetConfig(localNetConfigBean.getIp(), anothersNetConfig),hazelcastPathParam.getValorParametro(), localNetConfigBean.getPort(),nombreSubscriptor,bean.getBean2().getNumeroInstancias(),javaPathCacerts.getValorParametro(),javaKeyStoreP.getValorParametro());
			}
			
		}
		
	}

	/**
	 * revisarMap.
	 *
	 * @param entry the entry
	 * @param bloques the bloques
	 * @param localNetConfigBean the local net config bean
	 * @param anothersNetConfig the anothers net config
	 * @param hazelcastPathParam the hazelcast path param
	 * @param nombreSubscriptor the nombre subscriptor
	 * @param members the members
	 * @param javaPathCacerts the java path cacerts
	 * @param javaKeyStoreP the java key store P
	 */
	public void revisarMap(Map.Entry<String, MemberBean> entry,List<BloqueBean> bloques,NetConfigBean localNetConfigBean,List<NetConfigBean> anothersNetConfig,ParametroBean hazelcastPathParam,String nombreSubscriptor,Map<String, MemberBean> members,ParametroBean javaPathCacerts, ParametroBean javaKeyStoreP) {
		
		boolean allActive = true;
		StartedComponentUtils utils = new StartedComponentUtils();
		
		if(!entry.getValue().isActive()) {
			
			allActive = false;
			ListIterator<BloqueBean> iter = bloques.listIterator(bloques.size());
			
			while(iter.hasPrevious()) {
				
				BloqueBean bean = iter.previous();
				
				if(bean.getNombre().equals(entry.getValue().getName())){
					
					LOG.info("El flujo "+bean.getNombre()+" deberia de estar arriba y no lo esta, sera activado :: SUBSC :: "+nombreSubscriptor);
					utils.iniciaFlujo(bean.getNombre(), bean.getVersion(), utils.concatNetConfig(localNetConfigBean.getIp(), anothersNetConfig),hazelcastPathParam.getValorParametro(), localNetConfigBean.getPort(),nombreSubscriptor,bean.getBean2().getNumeroInstancias(),javaPathCacerts.getValorParametro(),javaKeyStoreP.getValorParametro());
					members.remove(entry.getKey(), entry.getValue());
					
				}
			}
		}
		
		if(allActive) {
			LOG.info("Todos los flujos estan activos :: " + nombreSubscriptor);
		}
		
	}
	
	/**
	 * revisarExpedientesEnProcesos.
	 *
	 * @param persistenceManager the persistence manager
	 * @param client the client
	 * @param localNetConfigBean the local net config bean
	 */
	public void revisarExpedientesEnProcesos(PersistenceManagerSubscriptor persistenceManager,HazelcastInstance client,NetConfigBean localNetConfigBean) {
		
		// Hazelcast esta activo en esta maquina virtual, Se revisa que no existan mas expedientes en las queues
		boolean isEmpty = true;
		List<DistributedObject> activeQueues = (List<DistributedObject>) client.getDistributedObjects();
		
		for(DistributedObject obj:activeQueues){
			
			IQueue<ExpedienteBean> queue = client.getQueue(obj.getName());
			
			if(queue.isEmpty()) {
				
				queue.destroy();
				
			}else {
				isEmpty = false;
			}
		}
		
		if(isEmpty){
			persistenceManager.updateStausNetConfig(localNetConfigBean.getId(), ParametroValueEnum.PARAMETRO_VALUE_INACTIVO.getName());
		}
	}

}
