package mx.isban.flujogd.batch.purge;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.scheduling.annotation.EnableAsync;

import mx.isban.flujogd.batch.purge.component.PurgeComponent;

/**
 * The Class FlujoGDBatchApplication.
 * Clase para procesar la limpia de archivos en el sftp
 */
@SpringBootApplication
@EnableAsync
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
public class FlujoGDBatchPurgeApplication implements CommandLineRunner{
	
	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(FlujoGDBatchPurgeApplication.class);
	
	/**
	 * The main method.
	 * Metodo que iniciara el flujo de purga
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		SpringApplication sp = new SpringApplication(FlujoGDBatchPurgeApplication.class);
		sp.run(args);
		System.exit(0);
	}

	/**
	 * Run.
	 * Metodo que lanzara la ejecucion de purga
	 * @param args Parameetro args de tipo String
	 * @throws Exception Manda una excepcion en caso de error
	 */
	@Override
	public void run(String... args) throws Exception {
		LOG.info("Inicia BatchPurgeComponent");
		PurgeComponent batchComponent = new PurgeComponent();
		batchComponent.execute();
	}

}
