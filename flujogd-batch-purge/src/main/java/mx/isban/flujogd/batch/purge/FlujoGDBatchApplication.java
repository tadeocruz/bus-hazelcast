package mx.isban.flujogd.batch.purge;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.scheduling.annotation.EnableAsync;

import mx.isban.flujogd.batch.purge.component.PurgeComponent;

@SpringBootApplication
@EnableAsync
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
public class FlujoGDBatchApplication implements CommandLineRunner{
	
	private static final Logger LOG = LogManager.getLogger(FlujoGDBatchApplication.class);
	
	public static void main(String[] args) {
		SpringApplication sp = new SpringApplication(FlujoGDBatchApplication.class);
		sp.run(args);
		System.exit(0);
	}

	@Override
	public void run(String... args) throws Exception {
		LOG.info("Inicia BatchPurgeComponent");
		PurgeComponent batchComponent = new PurgeComponent();
		batchComponent.execute();
	}

}
