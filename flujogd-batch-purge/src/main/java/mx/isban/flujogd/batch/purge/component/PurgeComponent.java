package mx.isban.flujogd.batch.purge.component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mx.isban.flujogd.common.bean.BatchParametroBean;
import mx.isban.flujogd.common.bean.LoteBean;
import mx.isban.flujogd.common.util.LoteStatusEnum;
import mx.isban.flujogd.common.util.ParametroEnum;
import mx.isban.flujogd.persistence.PersistenceManagerBatch;
import mx.isban.flujogd.persistence.impl.PersistenceManagerBatchImpl;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal PurgeComponent
 *
 */
public class PurgeComponent {

	private static final Logger LOG = LogManager.getLogger(PurgeComponent.class);

	/**
	 * execute
	 */
	public void execute() {
		PersistenceManagerBatch pm = new PersistenceManagerBatchImpl();
		List<LoteBean> lotes = pm.retrieveAllLotesByStatus(LoteStatusEnum.STATUS_PROCESADO.getName());
		LOG.info("Lotes recuperados: " + lotes.size());
		for(LoteBean lote:lotes) {
			BatchParametroBean beanDias = pm.retrieveParam(ParametroEnum.PARAMETRO_SFTP_DIAS_RESGUARDO.getName());
			LOG.info("Carpeta: "+lote.getDetalle().getPath());
			recorrerDirectorio(lote.getDetalle().getPath(),beanDias.getValorParametro());
		}
	}
	
	/**
	 * recorrerDirectorio
	 * @param path
	 */
	private void recorrerDirectorio(String path,String dias) {
		File folder = new File(path);
		File[] listOfFiles = folder.listFiles();
		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				eliminaArchivo(listOfFiles[i],dias);
			}else {
				recorrerDirectorio(listOfFiles[i].getAbsolutePath(),dias);
			}
		}
		if(folder.delete()) {
			LOG.info("El directorio se elimino con exito: "+path);
		}else {
			LOG.info("No se puedo eliminar el directorio: "+path);
		}
	}

	/**
	 * eliminaArchivo
	 * @param file
	 */
	private void eliminaArchivo(File file,String dias) {
		long ms = file.lastModified();
		Date fechaModificacion = new Date(ms);
		int diasTranscurridos = diferenciaEnDias(new Date(), fechaModificacion);
		if(diasTranscurridos>=new Integer(dias)) {
			try {
				Files.deleteIfExists(Paths.get(file.getAbsolutePath()));
				LOG.info("El archivo se elimino con exito: "+file.getAbsolutePath());
			} catch (IOException e) {
				LOG.error("Error al intentar borrar el archivo: "+file.getAbsolutePath());
			}
		}else {
			LOG.info("El archivo aun no esta en el rango de fecha para ser borrado: "+file.getAbsolutePath());
		}
	}

	/**
	 * diferenciaEnDias
	 * @param fechaMayor
	 * @param fechaMenor
	 * @return
	 */
	private int diferenciaEnDias(Date fechaMayor, Date fechaMenor) {
		long diferenciaEn_ms = fechaMayor.getTime() - fechaMenor.getTime();
		long dias = diferenciaEn_ms / (1000 * 60 * 60 * 24);
		return (int) dias;
	}

}
