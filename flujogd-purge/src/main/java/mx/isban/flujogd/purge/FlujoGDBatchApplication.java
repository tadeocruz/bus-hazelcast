package mx.isban.flujogd.purge;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.scheduling.annotation.EnableAsync;

import mx.isban.flujogd.purge.component.PurgeComponent;

/**
 * The Class FlujoGDBatchApplication.
 * Clase para procesar la limpia de archivos en el sftp
 */
@SpringBootApplication
@EnableAsync
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
public class FlujoGDBatchApplication implements CommandLineRunner{
	
	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(FlujoGDBatchApplication.class);
	
	/**
	 * The main method.
	 * Metodo que iniciara el flujo de purga
	 *
	 * @param args Parametro args con un arrayde tipo String
	 */
	public static void main(String[] args) {
		SpringApplication sp = new SpringApplication(FlujoGDBatchApplication.class);
		sp.run(args);
		System.exit(0);
	}

	/**
	 * Run.
	 * Metodo que lanzara la ejecucion de purga
	 * @param args the args Usa argumentos variables para la ejecucion
	 * @throws Exception Lanza un error de tipo Exception
	 */
	@Override
	public void run(String... args) throws Exception {
		LOG.info("Inicia PurgeComponent");
		PurgeComponent batchComponent = new PurgeComponent();
		batchComponent.execute();
	}

}
