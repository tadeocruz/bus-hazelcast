package mx.isban.flujogd.purge.component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.jcraft.jsch.UserInfo;

import mx.isban.flujogd.common.bean.ConexionSFTPBean;
import mx.isban.flujogd.common.bean.ExpedientePurgaBean;
import mx.isban.flujogd.common.bean.ParametroBean;
import mx.isban.flujogd.common.bean.SubscriptorBean;
import mx.isban.flujogd.common.generic.SFTPManagerUtils.MyUserInfo;
import mx.isban.flujogd.common.util.ParametroEnum;
import mx.isban.flujogd.persistence.PersistenceManagerExpediente;
import mx.isban.flujogd.persistence.PersistenceManagerSubscriptor;
import mx.isban.flujogd.persistence.impl.PersistenceManagerExpedienteImpl;
import mx.isban.flujogd.persistence.impl.PersistenceManagerSubscriptorImpl;

/**
 * The Class PurgeComponent.
 * Clase que ejecutara el proceso de purga
 *
 * @author Alvaro Zamorano
 *  azamorano@serviciosexternos.isban.mx 
 *  Clase PurgeComponent
 */
public class PurgeComponent {

	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(PurgeComponent.class);
	
	/** The Constant CAUSE. */
	private static final String CAUSE = " Cause :: ";

	/**
	 * execute.
	 * Metodo que ejecutara el proceso de purga
	 */
	public void execute() {
		PersistenceManagerSubscriptor persistenceManager = new PersistenceManagerSubscriptorImpl();
		List<SubscriptorBean> subscriptores = persistenceManager.retrieveAllSubscriptores();
		PersistenceManagerExpediente persistenceManagerExp = new PersistenceManagerExpedienteImpl();
		LOG.info("Subscriptores recuperados: " + subscriptores.size());
		for (SubscriptorBean subscriptor : subscriptores) {
			LOG.info("Depurando susciptor: {} | {}", subscriptor.getIdSubscriptor(), subscriptor.getNombreSubscriptor());
			ParametroBean beanDias = persistenceManager.retrieveParam(subscriptor.getNombreSubscriptor(),
					ParametroEnum.PARAMETRO_SFTP_DIAS_RESGUARDO.getName());
			ParametroBean zipTarget = persistenceManager.retrieveParam(subscriptor.getNombreSubscriptor(),
					ParametroEnum.PARAMETRO_ZIP_TARGET_PATH.getName());
            //recuperar id bloque segun suscriptor
			ConexionSFTPBean connSftp = persistenceManagerExp.retrieveDataServerFTP(subscriptor.getNombreSubscriptor());
			// Realizar purgado de sftp ORIGEN 
			if(beanDias.getValorParametro()!=null && !beanDias.getValorParametro().isEmpty()) {
				purgaSftp(subscriptor.getNombreSubscriptor(), beanDias.getValorParametro(), subscriptor.getIdSubscriptor(), connSftp);
			} else {
				beanDias.setValorParametro("7");
			}
			// Realizar purgado de salida para sftp con zip correcto
			purgaArchivos(beanDias.getValorParametro(), connSftp.getRutas().getPathStfpOut(), subscriptor.getIdSubscriptor(), 0);
			// Realizar purgado de unzip con filenet correcto
			purgaArchivos(beanDias.getValorParametro(), zipTarget.getValorParametro(), subscriptor.getIdSubscriptor(), 1);
		}
	}

	/**
	 * Purga directorio.
	 * Metodo para purgar el directorio de trabajo para la carga de expedientes
	 *
	 * Elimina archivos mayores a 7 dias de forma predeterminada,
	 *  si se configura el parametro sftp_dias_resguardo toma este valor para la depuracion
	 *  si no se configura el parametro no elimina los arcivos remotos de lo contrario intenta
	 *  eliminar dichos archivos 
	 * 
	 * @param estatus del proceso a validar
	 * @param ruta donde se realizara el purgado de expedientes
	 */
	private void purgaArchivos(String dias, String ruta, String idSubscriptor, int actualiza ) {
		PersistenceManagerExpediente persistenceManager = new PersistenceManagerExpedienteImpl();
		List<ExpedientePurgaBean> listaFiles = persistenceManager.retrieveExpedientesPurgaBySuscriptor(Integer.valueOf(dias), idSubscriptor);
	    listaFiles = removeDuplicates(listaFiles);
	    // Obtener el separador de sistema
	     String separator = File.separator;
			if("WIN".equals(System.getProperty("os.name").toUpperCase().substring(0, 3))) {
				separator = "/";
			}
		// Recorrer la lista de expedientes a purgar
		for (ExpedientePurgaBean expedientePurgaBean : listaFiles) {
			String folder = expedientePurgaBean.getNombre().substring(0, expedientePurgaBean.getNombre().length()-4);
			Path path = Paths.get(ruta+separator+folder);
			Path zipFile = Paths.get(ruta+separator+expedientePurgaBean.getNombre());
			LOG.info("Borrando: Exp: {} , Path:",expedientePurgaBean.getId(), path);
			borrarArchivos(path);
			borrarArchivos(zipFile);
			//Se actualiza el bloque a estatus 3 para evitar que se re-intente purgar nuevamente
			if(actualiza > 0) {
				LOG.info("Actualizando estatus del expediente {}",expedientePurgaBean.getId());
				persistenceManager.insertEstatusExpediente(""+expedientePurgaBean.getId(), "EXPEDIENTE_PURGADO", null);
			}
		}
	}

	
	/**
	 * Elimina los archvios de forma recursiva desde el path de un directorio.
	 * 
	 * @param path Ruta de partida para la depuracion
	 * @return True o False, segun el exito de la operacion
	 */
	private void borrarArchivos(Path path) {
		Stream<Path> tmp = null;
		try {
			if(Files.isDirectory(path, LinkOption.NOFOLLOW_LINKS)) {
				// Se recorre la carpeta en busca de archivos.
				tmp = Files.list(path);
				tmp.forEach(file -> 
					// Se eliminan los archivos de forma recursiva
					borrarArchivos(file)
				);
				// borra carpeta de trabajo
				Files.delete(path);
				LOG.debug("Carpera BORRADA: {}",path);
			} else {
				// borra archivo de trabajo
				Files.deleteIfExists(path);
				LOG.debug("Archivo BORRADO: {}",path);
			}
		} catch (IOException e) {
			LOG.error("No existe el archivo "+path,e);
		} finally {
			if(tmp!=null) {
				tmp.close();
			}
		}
	}
	
	
	/**
	 * Purga sftp.
	 * Metodo para realizar el purgado de expedientes en el servidor sftp
	 *
	 * @param subscriptor para los expedientes a purgar
	 * @param status del proceso a validar
	 */
	private void purgaSftp(String subscriptor, String dias, String idSubscriptor, ConexionSFTPBean connSftp) {
		PersistenceManagerExpediente persistenceManager = new PersistenceManagerExpedienteImpl();
		List<ExpedientePurgaBean> listaFiles = new ArrayList<ExpedientePurgaBean>();
		listaFiles = persistenceManager.retrieveExpedientesPurgaBySuscriptor(Integer.valueOf(dias), idSubscriptor);
		Session session = null;
		Channel channel = null;
		try {
			if(listaFiles!=null && !listaFiles.isEmpty()) {
				JSch ssh = new JSch();
		    	 // Validar si se tiene la lleve privada 
		    	 if(connSftp.getRutas().getPathPrivKey() != null && !"".equals(connSftp.getRutas().getPathPrivKey())) {
		    		 ssh.addIdentity(connSftp.getRutas().getPathPrivKey());
		    	 }
		    	 // Obtener session sftp
				session = ssh.getSession(connSftp.getUser(), connSftp.getServer(), Integer.parseInt(connSftp.getPuerto()));
				// Agregar informacion de usuario para session
				UserInfo ui=new MyUserInfo(connSftp.getPass(), connSftp.getRutas().getSftpPassPhrase());
				Properties config = new Properties();
			    config.put("StrictHostKeyChecking", "no");
			    session.setUserInfo(ui);
			    session.setConfig(config);
			     // Conectar a sftp
			    session.connect();
			    channel = session.openChannel("sftp");
			    channel.connect();
	
			    ChannelSftp sftp = (ChannelSftp) channel;	
			    listaFiles = removeDuplicates(listaFiles);
			    for (ExpedientePurgaBean expedientePurgaBean : listaFiles) {
			    	removeFiles(connSftp, sftp, expedientePurgaBean);
			    	LOG.info("Archivo: {} eliminado.", expedientePurgaBean.getNombre());
				}
			    channel.getExitStatus();
			} 
		} catch (JSchException e) {
			LOG.error("Error al generar conexion a sftp de suscriptor ::" + subscriptor + CAUSE, e);
		} finally {
			// Desconectar canal 
			if(channel!=null) {
			 channel.disconnect();
			}
			// Desconecta session
			if(session!=null) {
		     session.disconnect();
			}
		}
	}

	/**
	 * Removes the files.
	 * Metodo para realizar el purgado de los expedientes en sftp
	 *
	 * @param connSftp datos de configuracion de sftp
	 * @param sftp Canal de conexion con sftp
	 * @param expedientePurgaBean the expediente purga bean
	 */
	private void removeFiles(ConexionSFTPBean connSftp, ChannelSftp sftp, ExpedientePurgaBean expedientePurgaBean) {
		try {
			// Eliminar el expediente
			sftp.rm(connSftp.getRutas().getPathStfpSrc()+File.separator+expedientePurgaBean.getNombre());
			LOG.info("Eliminado: {}",connSftp.getRutas().getPathStfpSrc()+File.separator+expedientePurgaBean.getNombre());
		} catch (SftpException e) {
			LOG.error("Error al eliminar el expediente :: "+expedientePurgaBean.getNombre() + CAUSE,  e);
		}
	}
	
	 /**
 	 * Removes the duplicates.
 	 * Metodo para eliminar los expedientes duplicados en la lista obtenida
 	 *
 	 * @param list de expedientes a procesar
 	 * @return the list expedientes sin duplicados
 	 */
 	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<ExpedientePurgaBean> removeDuplicates(List<ExpedientePurgaBean> list) {
		    Set set = new TreeSet(new Comparator() {
				/* (non-Javadoc)
				 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
				 */
				@Override
				public int compare(Object o1, Object o2) {
					// Validacion de duplicados por nombre
					if(((ExpedientePurgaBean)o1).getNombre().equalsIgnoreCase(((ExpedientePurgaBean)o2).getNombre())) {
						return 0;
					}
					return 1;
				}
			});
		    // Agregar la lista sin duplicados
		    set.addAll(list);
		    // Retornar lista
		    return new ArrayList<ExpedientePurgaBean>(set);
		}
 	
}
