package mx.isban.flujogd.checksum.tc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mx.isban.flujogd.checksum.manager.ChecksumManager;

public class ChecksumTC {
	
	private static final Logger LOG = LogManager.getLogger(ChecksumTC.class);
	
	public static void main(String[] args) {
		ChecksumManager checksumManager = new ChecksumManager();
		String cs = checksumManager.getChecksum("C:/SFTP_Root/Vivere/00311758.zip");
		LOG.info(cs);
	}

}
