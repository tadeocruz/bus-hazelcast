package mx.isban.flujogd.checksum.manager;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal ChecksumManager
 *
 */
public class ChecksumManager {

	//Inicializamos un log para la bitacoria
	private static final Logger LOG = LogManager.getLogger(ChecksumManager.class);

	/**
	 * getchecksum
	 * @param srcFile	Archivo fuente
	 * @return	Checksum del archivo
	 */
	public String getChecksum(String srcFile) {
		//Crear un message digest
		MessageDigest md;
		//Inicializamos un string para almacenar el checksum
		String checksum = null;
		try {
			//Inicializamos el message digest con una instancia de codificacion "SHA-256"
			md = MessageDigest.getInstance("SHA-256");
			//Realizar el checksum del archivo origen
			//Almacenar el checksum en un arreglo de bytes
			byte[] hashInBytes = checksum(srcFile, md);
			//Convertir el arreglo a hexadecimal
			//Guardar el arreglo en el checksum
			checksum = bytesToHex(hashInBytes);
		} catch (NoSuchAlgorithmException e) {
			LOG.error(3);
			LOG.info(e);
		} catch (FileNotFoundException e) {
			LOG.error(3);
			LOG.info(e);
		} catch (IOException e) {
			LOG.error(3);
			LOG.info(e);
		}
		return checksum;
	}
	
	private byte[] checksum(String filepath, MessageDigest md) throws IOException {
		//Evaluar la ruta y la codificacion del message digest
        try (DigestInputStream dis = new DigestInputStream(new FileInputStream(filepath), md)) {
            while (dis.read() != -1) {	
                md = dis.getMessageDigest();
            }
        }
        //Regresar el message digest
        return md.digest();
    }

    private String bytesToHex(byte[] hashInBytes) {
    	//Inicializamos un string builder para almacenar los bytes del arreglo
        StringBuilder sb = new StringBuilder();
        for (byte b : hashInBytes) {
            sb.append(String.format("%02x", b));
        }
        //Regresar el string builder convertido en string
        return sb.toString();
    }

}
