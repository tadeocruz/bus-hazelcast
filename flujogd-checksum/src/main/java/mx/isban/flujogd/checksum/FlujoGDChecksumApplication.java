package mx.isban.flujogd.checksum;


import java.io.File;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.system.ApplicationPidFileWriter;
import org.springframework.scheduling.annotation.EnableAsync;

import mx.isban.flujogd.checksum.configuration.HazelcastChecksumConfig;
import mx.isban.flujogd.checksum.listener.ClusterMembershipListener;
import mx.isban.flujogd.common.generic.CommonsUtils;

/**
 * The Class FlujoGDChecksumApplication.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal FlujoGDChecksumApplication
 */
@SpringBootApplication
@EnableAsync
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
public class FlujoGDChecksumApplication implements CommandLineRunner {
	
	/** The Constant LOG. */
	//Inicializamos un log para la bitacora
	private static final Logger LOG = LogManager.getLogger(FlujoGDChecksumApplication.class);
	
	/**
	 * Metodo principal que inicia el flujo hazelcast.
	 *
	 * @param args Argumentos
	 */
    public static void main(String[] args) {
    	CommonsUtils utils = new CommonsUtils();
    	if(args.length<5) {
    		//Mostrar en bitacora el error
    		LOG.error("No es posible inicar el componente sin los parametros 'IPs','port','path','componente' y 'Nombre subscriptor'");
    		return;
    	}else{
    		// Obtener listado de las ips para servicio de hazelcast
			HazelcastChecksumConfig.setIps(utils.getListaIp(args));
			HazelcastChecksumConfig.setPort(args[1]);
			ClusterMembershipListener.setNombreSubscriptorChecksum(args[4]);
			ClusterMembershipListener.setComponenteChecksum(args[3]);
			// Instanciar flujo checksum como aplicacion spring
			SpringApplication sp = new SpringApplication(FlujoGDChecksumApplication.class);
			sp.addListeners(new ApplicationPidFileWriter(args[2]+File.separator+args[3]+".pid"));
			sp.run(args);
		}
    }

    /**
     * Metodo nativo hazelcast que dispara el proceso de arranque.
     *
     * @param args the args Parametro de entrada
     * @throws Exception En caso de error lanza una excepcion
     */
	@Override
	public void run(String... args) throws Exception {
		//Mostrar mensaje de inicio del checksum
		LOG.info("FlujoGDChecksumApplication - run");
	}

}
