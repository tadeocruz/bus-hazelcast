package mx.isban.flujogd.checksum.listener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hazelcast.core.ItemEvent;
import com.hazelcast.core.ItemListener;

import mx.isban.flujogd.checksum.trigger.ExecuteJobChecksumTrigger;
import mx.isban.flujogd.common.bean.ExpedienteBean;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal QueueChecksumListener
 *
 */
@Component
public class QueueChecksumListener implements ItemListener<ExpedienteBean> {
	
	//Inicializamos un log para la bitacora
	private static final Logger LOG = LogManager.getLogger(QueueChecksumListener.class);
	
    @Autowired
    private ExecuteJobChecksumTrigger jobBatch;

    /**
   	 * itemAdded
   	 * @param item
   	 */
    @Override
    public void itemAdded(ItemEvent<ExpedienteBean> item) {
    	LOG.info("Se detecta objeto agregado a la queue");
    	jobBatch.executeJobChecksum();
    }

    /**
	 * itemRemoved
	 * @param item
	 */
    @Override
    public void itemRemoved(ItemEvent<ExpedienteBean> item) {
    	LOG.info("Se detecta objeto removido de la queue");
    }

}