package mx.isban.flujogd.checksum.step;

import java.text.ParseException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.item.ItemReader;
import com.hazelcast.core.IQueue;

import mx.isban.flujogd.checksum.configuration.HazelcastChecksumConfig;
import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.util.QueueEnum;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase inicial BatchChecksumItemReader
 *
 */
public class BatchChecksumItemReader implements  ItemReader<ExpedienteBean>{

	//Inicializamos un log para la bitacora
	private static final Logger LOG = LogManager.getLogger(BatchChecksumItemReader.class);

	//Inicializamos un hazelcast checksum
	private HazelcastChecksumConfig hazelcastChecksumConfig; 
	
	//Inicializamos un expediente bean
	private ExpedienteBean expedienteBean;

	/**
	 * Constructor
	 * @param hazelcastChecksumConfig	Configuracion del checksum
	 */
	public BatchChecksumItemReader(HazelcastChecksumConfig hazelcastChecksumConfig){
		this.hazelcastChecksumConfig = hazelcastChecksumConfig;
	}


	/**
	 * Metodo que lee de la queue el siguiente dato a ser procesado
	 */
	@Override
	public ExpedienteBean read() throws InterruptedException, ParseException {
		//Inicializamos un iqueue para almacenar la cola del hazelcast checksum
		IQueue<ExpedienteBean> queueChecksum = hazelcastChecksumConfig.getQueue(QueueEnum.QUEUE_CHECKSUM.getName());
		try {
			//Tomar un expediente de la cola
			expedienteBean = queueChecksum.take();
			if (expedienteBean != null) {
				//Mostrar en la bitacora el expediente recuperado
				LOG.info("=== SE RECUPERA EXPEDIENTE: "+expedienteBean.getIdExpediente() + " - "+ expedienteBean.getNombreExpediente() + " - " + expedienteBean.getDetalle().getDetalle2().getReferenciaExterna());
				return expedienteBean;
			}else{
				return null;
			}
		} catch (InterruptedException e) {
			LOG.error(e);
			Thread.currentThread().interrupt();
		}
		return null;
	}
}
