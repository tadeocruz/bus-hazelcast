package mx.isban.flujogd.checksum.listener;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hazelcast.core.MemberAttributeEvent;
import com.hazelcast.core.MembershipEvent;
import com.hazelcast.core.MembershipListener;

import mx.isban.flujogd.checksum.configuration.HazelcastChecksumConfig;
import mx.isban.flujogd.common.bean.MemberBean;
import mx.isban.flujogd.common.bean.NetConfigBean;
import mx.isban.flujogd.common.util.InetAddressUtil;
import mx.isban.flujogd.common.util.MapEnum;
import mx.isban.flujogd.persistence.PersistenceManagerSubscriptor;
import mx.isban.flujogd.persistence.impl.PersistenceManagerSubscriptorImpl;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal ClusterMembershipListener
 *
 */
public class ClusterMembershipListener implements MembershipListener{
	
	//Inicializamos un log para la bitacora
	private static final Logger LOG = LogManager.getLogger(ClusterMembershipListener.class);
	
	/**
	 * nombreSubscriptor
	 */
	//Inicializamos un string para almacenar el nombre del subscriptor
	private static String nombreSubscriptor;
	
	/**
	 * componente
	 */
	//Inicializamos un string para almacenar el componente
	private static String componente;
	
	/**
	 * hazelcastConfig
	 */
	//Inicializamos un hazelcast checksum
	private static HazelcastChecksumConfig hazelcastConfig;

	/**
	 * memberAdded
	 * @param membershipEvent
	 */
	@Override
	public void memberAdded(MembershipEvent membershipEvent) {
		//Obtener el uuid del miembro
		String uuid = membershipEvent.getMember().getUuid();
		//Obtener el uuid del miebro local del cluster
		String uuidLocal = membershipEvent.getCluster().getLocalMember().getUuid();
		//Mostrar en bitacora el miembro agregado
		LOG.info("Se grego un nuevo miembro: "+uuid);
		if(uuid.equals(uuidLocal)) {
			//Mostrar el checksum del miembro
			LOG.info("flujogd-checksum: "+uuid);
			//Notificar el miembro agregado
			notifyMemberAddChecksum(uuid);
		}
	}

	/**
	 * memberRemoved
	 * @param membershipEvent
	 */
	@Override
	public void memberRemoved(MembershipEvent membershipEvent) {
		//Mostar el miembro eliminado
		LOG.info("Se elimino el miembro: "+membershipEvent.getMember().getUuid());
		//Notificar el miembro eliminado
		notifyMemberRemovedChecksum(membershipEvent.getMember().getUuid());
	}
	
	/**
	 * Notifica al cluster que flujo fue agregado
	 * @param uuid
	 */
	private void notifyMemberAddChecksum(String uuid) {
		LOG.info("Notificamos al cluster que flujo fue agregado");
		PersistenceManagerSubscriptor persistenceManagerChecksum = new PersistenceManagerSubscriptorImpl();
		InetAddressUtil inetUtilChecksum = new InetAddressUtil();
		String ipNetChecksum = inetUtilChecksum.getLocalNet();
		if(!"".equals(ipNetChecksum)) {
			//Configurar el bean con los datos del subscriptor
			NetConfigBean netConfigBean = persistenceManagerChecksum.retrieveNetConfigByIp(nombreSubscriptor,ipNetChecksum);
			if(netConfigBean!=null){
				Map<String, MemberBean> members = hazelcastConfig.getMapMembers(MapEnum.MAP_MEMBER_STATUS.getName());
				MemberBean memberChecksum = new MemberBean();
				memberChecksum.setActive(true);
				memberChecksum.setIp(netConfigBean.getIp());
				memberChecksum.setPort(netConfigBean.getPort());
				memberChecksum.setName(componente);
				memberChecksum.setUuid(uuid);
				members.put(uuid, memberChecksum);
				LOG.info("La notificacion se realizo con exito");
			}
		}else {
			LOG.info("Ocurrio un problema al recuperar la IP de este servidor, sin ella no es posible notificar la baja del nodo");
		}
	}

	/**
	 * Notifica al cluster que flujo se dio de baja
	 * @param uuid
	 */
	private void notifyMemberRemovedChecksum(String uuid) {
		LOG.info("Notificamos al cluster que flujo se dio de baja");
		PersistenceManagerSubscriptor persistenceManagerChecksum = new PersistenceManagerSubscriptorImpl();
		InetAddressUtil inetUtilChecksum = new InetAddressUtil();
		String ipNetChecksum = inetUtilChecksum.getLocalNet();
		if(!"".equals(ipNetChecksum)) {
			//Configurar el bean con los datos del subscriptor
			NetConfigBean netConfigBeanChecksum = persistenceManagerChecksum.retrieveNetConfigByIp(nombreSubscriptor,ipNetChecksum);
			if(netConfigBeanChecksum!=null){
				Map<String, MemberBean> members = hazelcastConfig.getMapMembers(MapEnum.MAP_MEMBER_STATUS.getName());
				MemberBean memberChecksum = members.get(uuid);
				if(memberChecksum!=null) {
					memberChecksum.setActive(false);
					members.replace(uuid, memberChecksum);
				}
				LOG.info("La notificacion se realizo con exito");
			}
		}else {
			LOG.info("Ocurrio un problema al recuperar la IP de este servidor, sin ella no es posible notificar la baja del nodo");
		}
	}

	/**
	 * memberAttributeChanged
	 * @param memberAttributeEvent
	 */
	@Override
	public void memberAttributeChanged(MemberAttributeEvent memberAttributeEvent) {
		LOG.info("Cambio un atributo de uno de los miembros");
	}

	/**
	 * @param nombreSubscriptor the nombreSubscriptor to set
	 */
	public static void setNombreSubscriptorChecksum(String nombreSubscriptor) {
		ClusterMembershipListener.nombreSubscriptor = nombreSubscriptor;
	}

	/**
	 * @param componente the componente to set
	 */
	public static void setComponenteChecksum(String componente) {
		ClusterMembershipListener.componente = componente;
	}

	/**
	 * @param hazelcastConfig the hazelcastConfig to set
	 */
	public static void setHazelcastConfig(HazelcastChecksumConfig hazelcastConfig) {
		ClusterMembershipListener.hazelcastConfig = hazelcastConfig;
	}

}
