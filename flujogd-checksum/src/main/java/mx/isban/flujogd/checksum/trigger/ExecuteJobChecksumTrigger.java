package mx.isban.flujogd.checksum.trigger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal ExecuteJobChecksumTrigger
 *
 */
@Component
public class ExecuteJobChecksumTrigger {
	
	//Inicializamos un log para la bitacora
	private static final Logger LOG = LogManager.getLogger(ExecuteJobChecksumTrigger.class);
	
	@Autowired
	//Inicializamos un job launcher
	private JobLauncher jobLauncherChecksum;
	@Autowired
	//Inicializamos un job
	private Job processJobChecksum;

	/**
	 * Ejecuta el job batch
	 */
	public void executeJobChecksum() {
		try {
			//Crear un job parameters para asignar parametros al job
			JobParameters jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis()).toJobParameters();
			//Ejecutar el job launcher con los parametros
			jobLauncherChecksum.run(processJobChecksum, jobParameters);
		} catch (JobParametersInvalidException | JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException e) {
			LOG.error(e);
		}
	}

}
