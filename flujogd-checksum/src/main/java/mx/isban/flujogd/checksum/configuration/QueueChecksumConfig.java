package mx.isban.flujogd.checksum.configuration;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import mx.isban.flujogd.checksum.listener.QueueChecksumListener;
import mx.isban.flujogd.common.util.QueueEnum;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal QueueChecksumConfig
 *
 */
@Configuration
public class QueueChecksumConfig {
	
	//Inicializamos un log para la bitacora
	private static final Logger LOG = LogManager.getLogger(QueueChecksumConfig.class);
	
	@Autowired
	//Inicializamos un hazel checksum
	private HazelcastChecksumConfig hazelcastConfig;
	
	@Autowired
	//Inicializamos un queue checksum
	private QueueChecksumListener queueChecksumListener;

	/**
	 * postConstruct
	 */
	@PostConstruct
	public void postConstruct() {
		hazelcastConfig.getQueue( QueueEnum.QUEUE_CHECKSUM.getName() ).addItemListener( queueChecksumListener, true );
		LOG.info( "FlujoGD-Checksum  - Listener Start" );
	}
}
