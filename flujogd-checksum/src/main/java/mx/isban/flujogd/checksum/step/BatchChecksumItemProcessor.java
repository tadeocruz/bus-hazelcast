package mx.isban.flujogd.checksum.step;

import java.io.File;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.item.ItemProcessor;

import com.hazelcast.core.IExecutorService;

import mx.isban.flujogd.checksum.configuration.HazelcastChecksumConfig;
import mx.isban.flujogd.checksum.manager.ChecksumManager;
import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.bean.NotificationBean;
import mx.isban.flujogd.common.bean.NotificationBean2;
import mx.isban.flujogd.common.bean.NotificationBean3;
import mx.isban.flujogd.common.util.ExpedienteErrorEnum;
import mx.isban.flujogd.common.util.ExpedienteStatusEnum;
import mx.isban.flujogd.common.util.ExpedienteUtil;
import mx.isban.flujogd.common.util.NotificationManager;
import mx.isban.flujogd.common.util.ParametroEnum;
import mx.isban.flujogd.common.util.QueueEnum;
import mx.isban.flujogd.common.util.QueueStatusEnum;
import mx.isban.flujogd.persistence.PersistenceManagerBloqueDetail;
import mx.isban.flujogd.persistence.PersistenceManagerExpediente;
import mx.isban.flujogd.persistence.callable.BloquePersistenceTask;
import mx.isban.flujogd.persistence.callable.ExpedientePersistenceTask;
import mx.isban.flujogd.persistence.callable.NotificacionPersistenceTask;
import mx.isban.flujogd.persistence.impl.PersistenceManagerBloqueDetailImpl;
import mx.isban.flujogd.persistence.impl.PersistenceManagerExpedienteImpl;

/**
 * Clase que realizara las validaciones de checksum para los expedientes
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal BatchChecksumItemProcessor
 *
 */
public class BatchChecksumItemProcessor implements ItemProcessor<ExpedienteBean, ExpedienteBean>{

	//Inicializamos un log para la bitacora
	private static final Logger LOG = LogManager.getLogger(BatchChecksumItemProcessor.class);
	//Inicializamos un string para almacenar el tipo de servicio
	private static String EXECUTOR_SERVICE = "executorService";
	
	//Inicializamos un hazelcast checksum
	private HazelcastChecksumConfig hazelcastChecksumConfig;

	/**
	 * Constructor
	 * @param hazelcastChecksumConfig	Configuracion del checksum
	 */
	public BatchChecksumItemProcessor(HazelcastChecksumConfig hazelcastChecksumConfig){
		this.hazelcastChecksumConfig = hazelcastChecksumConfig;
	}
	

	/**
	 * Metodo que procesara los expedientes pendientes
	 * process.
	 *
	 * @param expedienteBean the expediente bean
	 * @return the expediente bean
	 * @throws Exception the exception
	 */
	@Override
	public synchronized ExpedienteBean process(ExpedienteBean expedienteBean) throws Exception {
		if(expedienteBean.getPathZIPExpediente()==null) {
			PersistenceManagerBloqueDetail persistenceManager = new PersistenceManagerBloqueDetailImpl();
			//Configurar el bean del expediente
			expedienteBean.setPathZIPExpediente(persistenceManager.retrieveParametroBloque(expedienteBean.getIdExpediente(),  QueueEnum.QUEUE_SFTP.getName(), ParametroEnum.PARAMETRO_SFTP_OUTPUT_PATH.getName()).get(0).getValorParametro());
		}
		//Crear un checksum manager para administrar el checksum
		ChecksumManager checksumManager = new ChecksumManager();
		//Crear un string para guardar el resultado del checksum manager
		String checksum = checksumManager.getChecksum(expedienteBean.getPathZIPExpediente()+File.separator+expedienteBean.getNombreExpediente());
		if(checksum.equalsIgnoreCase(expedienteBean.getDetalle().getDetalle2().getChecksum())) {
			if(updateStausBloqueExpediente(expedienteBean, QueueStatusEnum.STATUS_OK.getName(), QueueEnum.QUEUE_CHECKSUM.getName(), null)) {
				ExpedienteUtil expedienteUtil = new ExpedienteUtil();
				//Camibar el estado de la cola
				expedienteUtil.cambiaEstausQueue(expedienteBean);
				//Determinar si el expediente es el siguiente en la cola
				expedienteUtil.determinaSiguienteQueue(expedienteBean);
				return expedienteBean;
			}
			expedienteBean.getDetalle().setSiguienteQueue("");
			return expedienteBean;
		}else {
			//Mostrar el error si no se encuentra el checksum evaluado
			return error(expedienteBean, ExpedienteErrorEnum.ERROR_CHECKSUM.getName());
		}
	}
	
	/**
	 * Metodo para setear los valores en caso de error
	 * error.
	 *
	 * @param expedienteBean the expediente bean
	 * @param nombreError the nombre error
	 * @return the expediente bean
	 */
	private ExpedienteBean error(ExpedienteBean expedienteBean,String nombreError){
		//Colocar el siguiente expediente en la cola
		expedienteBean.getDetalle().setSiguienteQueue("");
		LOG.error(nombreError);
		updateStausBloqueExpediente(expedienteBean, QueueStatusEnum.STATUS_ERROR.getName(), QueueEnum.QUEUE_CHECKSUM.getName(), nombreError);
		return expedienteBean;
	}
	
	/**
	 * Metodo para actualizar el estatus de los bloques para los expedientes
	 * updateStausBloqueExpediente.
	 *
	 * @param expedienteBean the expediente bean
	 * @param statusBloque the status bloque
	 * @param queueName the queue name
	 * @param nombreError the nombre error
	 * @return true, if successful
	 */
	private boolean updateStausBloqueExpediente(ExpedienteBean expedienteBean, String statusBloque, String queueName, String nombreError) {
		PersistenceManagerExpediente persistenceChecksum = new PersistenceManagerExpedienteImpl();
		IExecutorService executorServiceChecksum = this.hazelcastChecksumConfig.getInstanceChecksum().getExecutorService( EXECUTOR_SERVICE );
		Future<Object> future = executorServiceChecksum.submit( new BloquePersistenceTask(expedienteBean.getIdExpediente(), statusBloque, queueName,nombreError) );
		boolean okChecksum = false;
		try {
			okChecksum = (Boolean)future.get(); 
			if(okChecksum){
				String estatusExpediente = ExpedienteStatusEnum.STATUS_ARCHIVO_VALIDO.getName();
				if(nombreError!=null) {
					//Determinar el estatus del expediente
					estatusExpediente = ocurrioUnError(persistenceChecksum, expedienteBean, queueName, nombreError);
				}
				okChecksum = updateStausExpediente(expedienteBean.getIdExpediente(), estatusExpediente, nombreError);
			}
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(e);
		}
		return okChecksum;
	}
	
	/**
	 * Metodo que actualizara el estatus del expediente en BD
	 * updateStausExpediente.
	 *
	 * @param idExpediente the id expediente
	 * @param status the status
	 * @param idError the id error
	 * @return true, if successful
	 */
	private boolean updateStausExpediente(String idExpediente, String status, String idError) {
		IExecutorService executorServiceChecksum = this.hazelcastChecksumConfig.getInstanceChecksum().getExecutorService( EXECUTOR_SERVICE );
		Future<Object> future = executorServiceChecksum.submit( new ExpedientePersistenceTask(idExpediente, status, idError) );
		boolean okChecksum = false;
		try {
			okChecksum = (Boolean)future.get();
		} catch (InterruptedException e) {
			LOG.error(e);
			//Interrumpir la ejecucion del hijo actual
			Thread.currentThread().interrupt();
		} catch (ExecutionException e) {
			LOG.error(e);
		}
		return okChecksum;
	}
	
	/**
	 * Metodo que insertara la queue siguiente para la notificacion
	 * updateStausExpediente.
	 *
	 * @param expedienteBean the expediente bean
	 * @param nombreError the nombre error
	 * @return true, if successful
	 */
	private boolean insertNotificacion(ExpedienteBean expedienteBeanChecksum, String nombreError) {
		IExecutorService executorServiceChecksum = this.hazelcastChecksumConfig.getInstanceChecksum().getExecutorService( EXECUTOR_SERVICE );
		NotificationBean bean = new NotificationBean();
		NotificationBean2 bean2 = new NotificationBean2();
		NotificationBean3 bean3 = new NotificationBean3();
		bean2.setBean3(bean3);
		bean.setBean2(bean2);
		bean.setIdExpediente(expedienteBeanChecksum.getIdExpediente());
		bean.setRequest(null);
		bean.setResponseCode(null);
		bean.setResponseMsg(null);
		bean2.setRefExterna(null);
		bean2.setReqId(null);
		bean2.setErrorId(nombreError);
		bean2.setErrorMsg(nombreError);
		bean3.setBuc(null);
		bean3.setPathZip(expedienteBeanChecksum.getPathZIPExpediente());
		bean3.setFileName(expedienteBeanChecksum.getNombreExpediente());
		bean3.setPathUnzip(expedienteBeanChecksum.getPathUNZIPExpediente());
		Future<Object> future = executorServiceChecksum.submit( new NotificacionPersistenceTask(bean) );
		boolean okChecksum = false;
		try {
			okChecksum = (Boolean)future.get();
		} catch (InterruptedException e) {
			LOG.error(e);
			Thread.currentThread().interrupt();
		} catch (ExecutionException e) {
			LOG.error(e);
		}
		return okChecksum;
	}
	
	/**
	 * Metodo para setear los valores de error en el checksum
	 * ocurrioUnError.
	 *
	 * @param persistence Parametro de tipo PersistenceManagerExpediente
	 * @param expedienteBean Parametro expedienteBean de tipo ExpedienteBean
	 * @param queueName Parametro queueName de tipo String
	 * @param nombreError Parametro de entrada de nombreError tipo String
	 * @return the string Regresa el valor  de estatusExpediente
	 */
	private String ocurrioUnError(PersistenceManagerExpediente persistence,ExpedienteBean expedienteBean,String queueName,String nombreError) {
		String estatusExpedienteChecksum = ExpedienteStatusEnum.STATUS_ERROR_VALIDACION.getName();
		//Nueva validacion para numero de reintentos y envio de notificaciones
		Integer intentosChecksum = persistence.retrieveIntentosTipoError(expedienteBean.getIdExpediente(), queueName, nombreError);
		Integer reintentosPermitidos = persistence.retrieveReintentosByNombreError(nombreError);
		if(intentosChecksum>=reintentosPermitidos) {
			estatusExpedienteChecksum = ExpedienteStatusEnum.STATUS_PROCESO_CANCELADO.getName();
			NotificationManager notificationChecksum = new NotificationManager();
			notificationChecksum.preparaEnvioNotificacion(expedienteBean, nombreError, nombreError,expedienteBean.getNombreExpediente().substring(0, expedienteBean.getNombreExpediente().length()-4));
			insertNotificacion(expedienteBean,nombreError);
		}
		return estatusExpedienteChecksum;
	}
}
