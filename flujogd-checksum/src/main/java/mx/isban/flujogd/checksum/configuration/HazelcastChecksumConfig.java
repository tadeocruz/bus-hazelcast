package mx.isban.flujogd.checksum.configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.hazelcast.config.Config;
import com.hazelcast.config.ListenerConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IQueue;

import mx.isban.flujogd.checksum.listener.ClusterMembershipListener;
import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.bean.MemberBean;
import mx.isban.flujogd.common.generic.HazelcastGenericUtils;


/**
 * Clase para la configuracion de los servidores hazelcast, contendra los puertos y las ips
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal HazelcastChecksumConfig
 *
 */
@Configuration
public class HazelcastChecksumConfig {
	/**
	 * ips las direcciones ip de los servidores de hazelcast
	 */
	private static List<String> ips;
	
	/**
	 * port los puertos de los servidores de hazelcast
	 */
	private static String port;
	/**
	 * Constructor
	 */
	public HazelcastChecksumConfig() {
		ClusterMembershipListener.setHazelcastConfig(this);
	}
	
	
	/**
	 * 
	 * @return
	 */
	@Bean
	public HazelcastInstance getInstanceChecksum() {
		return Hazelcast.newHazelcastInstance(getHazelcastChecksumConfig());
	}

	/**
	 * getqueue
	 * @param name	Nombre de la cola
	 * @return	Instancia de la cola
	 */
	public IQueue<ExpedienteBean> getQueue(String name) {
		return getInstanceChecksum().getQueue(name);
	}
	
	/**
	 * getmapmembers
	 * @param name	Nombre del mapa de miembros
	 * @return	Instancia del mapa
	 */
	public Map<String, MemberBean> getMapMembers(String name) {
		return getInstanceChecksum().getMap(name);
	}
	
	/**
	 * getmapexpedientes
	 * @param name	Nombre del mapa de expedientes
	 * @return	Instancia del mapa
	 */
	public Map<String, Boolean> getMapExpedientes(String name) {
		return getInstanceChecksum().getMap(name);
	}
	
	/**
	 * getmapnotificaciones
	 * @param name	Nombre del mapa de notificaciones
	 * @return	Instancia del mapa
	 */
	public Map<String, Boolean> getMapNotificaciones(String name) {
		return getInstanceChecksum().getMap(name);
	}

	/**
	 * Config cluster network and discovery mechanism
	 * @return Config	Configuracion del cluster
	 */
	@Bean
	public Config getHazelcastChecksumConfig() {
		HazelcastGenericUtils util = new HazelcastGenericUtils();
		Config config = new Config();
		config.addListenerConfig(new ListenerConfig("mx.isban.flujogd.checksum.listener.ClusterMembershipListener"));
		config.setInstanceName("FlujoGD-Checksum");
		return util.setConfigHazelcast(port, ips, config);
	}

	/**
	 * @param ips the ips to set
	 */
	public static void setIps(List<String> ips) {
		List<String> copia = new ArrayList<>();
		copia.addAll(ips);
		HazelcastChecksumConfig.ips = copia;
	}

	/**
	 * @param port the port to set
	 */
	public static void setPort(String port) {
		HazelcastChecksumConfig.port = port;
	}

}
