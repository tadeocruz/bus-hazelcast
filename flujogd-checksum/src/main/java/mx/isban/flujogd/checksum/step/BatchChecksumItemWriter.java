package mx.isban.flujogd.checksum.step;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.item.ItemWriter;

import com.hazelcast.core.IQueue;

import mx.isban.flujogd.checksum.configuration.HazelcastChecksumConfig;
import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.util.MapEnum;
import mx.isban.flujogd.common.util.QueueEnum;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal BatchChecksumItemWriter
 *
 */
public class BatchChecksumItemWriter implements ItemWriter<ExpedienteBean>{
	
	//Inicializamos un log para la bitacora
	private static final Logger LOG = LogManager.getLogger(BatchChecksumItemWriter.class);
	
	//Inicializamos un hazelcast checksum
	private HazelcastChecksumConfig hazelcastChecksumConfig;

	/**
	 * Constructor
	 * @param hazelcastChecksumConfig	Configuracion del checksum
	 */
	public BatchChecksumItemWriter(HazelcastChecksumConfig hazelcastChecksumConfig){
		this.hazelcastChecksumConfig = hazelcastChecksumConfig;
	}
	

	/**
	 * Metodo para notificar la siguiente queue a procesar
	 * write.
	 *
	 * @param items the items Parametro item para almacenar los valores
	 * @throws Exception the exception Arroja un error de tipo Exception
	 */
	@Override
	public void write(List<? extends ExpedienteBean> items) throws Exception {
		//Crear un string para almacenar el expediente que sigue en la cola
		String nextQueue = items.get(0).getDetalle().getSiguienteQueue();
		if(!"".equals(nextQueue)) {
			LOG.info("Siguiente queue: " + nextQueue + " - "+ items.get(0).getNombreExpediente() + " - " + items.get(0).getDetalle().getDetalle2().getReferenciaExterna());
			if(QueueEnum.QUEUE_NOTIFICATION.getName().equals(nextQueue)) {
				//Si se va a enviar a la queue de notificaciones se debe de eliminar el mapa de expedientes
				eliminarExpedienteDelMapa(items.get(0));
				Map<String, Boolean> notificacionesMap = this.hazelcastChecksumConfig.getMapNotificaciones(MapEnum.MAP_NOTIFICACIONES_IN_PROCESS.getName());
				if(notificacionesMap.get(items.get(0).getIdExpediente())==null) {
					IQueue<ExpedienteBean> queue = hazelcastChecksumConfig.getQueue(nextQueue);
					queue.put(items.get(0));
					//Colocar en el mapa el id del expediente
					notificacionesMap.put(items.get(0).getIdExpediente(), true);
				}
			}else {
				IQueue<ExpedienteBean> queue = hazelcastChecksumConfig.getQueue(nextQueue);
				queue.put(items.get(0));
			}
		}else {
			//Si ocurrio un error en el proceso se elimina el expediente del mapa
			eliminarExpedienteDelMapa(items.get(0));
			LOG.info("No hay otra queue a procesar");
		}
	}
	
	/**
	 * Metodo para eliminar el expediente que ya fue procesado por la queue
	 * eliminarExpedienteDelMapa.
	 *
	 * @param expedienteBean the expediente bean
	 */
	private void eliminarExpedienteDelMapa(ExpedienteBean expedienteBean) {
		Map<String, Boolean> expedientesMap = this.hazelcastChecksumConfig.getMapExpedientes(MapEnum.MAP_EXPEDIENTES_IN_PROCESS.getName());
		if(expedientesMap.remove(expedienteBean.getDetalle().getDetalle2().getDetalle3().getKeyMap(), true)) {
			LOG.info("BatchChecksumItemWriter - SE ELIMINO EXPEDIENTE DEL MAPA CON EXITO - "+expedienteBean.getIdExpediente() + " - "+ expedienteBean.getNombreExpediente());
		}else {
			LOG.error("BatchChecksumItemWriter - NO SE PUDO ELIMINAR EXPEDIENTE DEL MAPA - "+expedienteBean.getIdExpediente() + " - "+ expedienteBean.getNombreExpediente());
		}
	}
	
}
