package mx.isban.flujogd.checksum.configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.support.transaction.ResourcelessTransactionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import mx.isban.flujogd.checksum.listener.JobChecksumListener;
import mx.isban.flujogd.checksum.step.BatchChecksumItemProcessor;
import mx.isban.flujogd.checksum.step.BatchChecksumItemReader;
import mx.isban.flujogd.checksum.step.BatchChecksumItemWriter;
import mx.isban.flujogd.common.bean.ExpedienteBean;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal BatchChecksumConfig
 *
 */
@EnableBatchProcessing(modular = true)
@Configuration
public class BatchChecksumConfig {

	//Inicializamos un log para la bitacora
	private static final Logger LOG = LogManager.getLogger(BatchChecksumConfig.class);

	//Inicializamos un job builder
	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	//Inicializamos un step builder
	@Autowired
	private StepBuilderFactory stepBuilderFactory;
	
	//Inicializamos un hazelcast checksum
	@Autowired
	private HazelcastChecksumConfig hazelcastChecksumConfig; 

	/**
	 * @return Job
	 */
	@Bean
	//Proceso que sigue el job
	public Job processJob() {
		//Mostrar mensaje de inicio del job
		LOG.info("execute job");
		//Regresar el resultado
		return jobBuilderFactory.get("jobBuilderFactory")
				.incrementer(new RunIdIncrementer())
				.listener(listener())
				.flow(orderStep())
				.end()
				.build();
	}

	/**
	 * To create a step, reader, processor and writer has been passed serially
	 * 
	 * @return	Step del job
	 */
	@Bean
	//Ordenar los steps
	public Step orderStep() {
		//Mostar mensaje del step del job
		LOG.info("step de job");
		//Regresar el resultado
		return stepBuilderFactory.get("orderStep1")
				.<ExpedienteBean, ExpedienteBean> chunk(1)
				.reader(new BatchChecksumItemReader(this.hazelcastChecksumConfig))
				.processor(new BatchChecksumItemProcessor(this.hazelcastChecksumConfig))
				.writer(new BatchChecksumItemWriter(this.hazelcastChecksumConfig))
				.build();
	}

	/**
	 * @return {@link JobExecutionListener}
	 */
	@Bean
	//Listener del checksum
	public JobExecutionListener listener() {
		return new JobChecksumListener();
	}

	/**
	 * @return {@link ResourcelessTransactionManager}
	 */
	@Bean
	//Administrador de transacciones
	public ResourcelessTransactionManager transactionManager() {
		return new ResourcelessTransactionManager();
	}
}
