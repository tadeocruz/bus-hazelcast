package mx.isban.flujogd.checksum.listener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal JobChecksumListener
 *
 */
public class JobChecksumListener extends JobExecutionListenerSupport{
	
	private static final Logger LOG = LogManager.getLogger(JobChecksumListener.class);
	
	/**
	 * afterJob
	 * @param jobExecution
	 */
	@Override
	public void afterJob(JobExecution jobExecution) {
		if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
			LOG.info("is done");
		}
	}
}
