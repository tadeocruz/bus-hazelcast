package mx.isban.flujogd.persistence.callable;

import java.io.Serializable;
import java.util.concurrent.Callable;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.HazelcastInstanceAware;

import mx.isban.flujogd.persistence.PersistenceManagerBloque;
import mx.isban.flujogd.persistence.impl.PersistenceManagerBloqueImpl;

/**
 * Clase para persistencia de hazelcast para los componentes de los flujos
 * The Class BloquePersistenceTask.
 *
 * @author Alvaro
 */
public class BloquePersistenceTask implements Callable<Object>, Serializable, HazelcastInstanceAware{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1845527693555859790L;

	/** The hazelcast instance. */
	@SuppressWarnings("unused")
	private transient HazelcastInstance hazelcastInstance;
	
	/** The id expediente. */
	private String idExpediente;
	
	/** The status. */
	private String status;
	
	/** The nombre bloque. */
	private String nombreBloque;
	
	/** The id error. */
	private String idError;
	
	/**
	 * Instantiates a new bloque persistence task.
	 *
	 * @param idExpediente the id expediente
	 * @param status the status
	 * @param nombreBloque the nombre bloque
	 * @param idError the id error
	 */
	public BloquePersistenceTask(String idExpediente, String status, String nombreBloque, String idError) {
		this.idExpediente = idExpediente;
		this.status = status;
		this.nombreBloque = nombreBloque;
		this.idError = idError;
	}

	/**
	 * Sets the hazelcast instance.
	 *
	 * @param hazelcastInstance the new hazelcast instance
	 */
	@Override
	public void setHazelcastInstance(HazelcastInstance hazelcastInstance) {
		this.hazelcastInstance = hazelcastInstance;
	}

	/**
	 * Call.
	 *
	 * @return the object La respuesta de la actualizacion del estatus
	 * @throws Exception Error de tipo exception
	 */
	@Override
	public Object call() throws Exception {
		PersistenceManagerBloque persistenceManager = new PersistenceManagerBloqueImpl();
		return persistenceManager.updateStatusBloqueExp(this.idExpediente, this.status, this.nombreBloque, this.idError);
		
	}

}
