package mx.isban.flujogd.persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mx.isban.flujogd.common.bean.LoteBean;
import mx.isban.flujogd.common.bean.LoteBeanDetalle;
import mx.isban.flujogd.common.bean.ParametroBean;
import mx.isban.flujogd.common.util.CatalogoEnum;
import mx.isban.flujogd.common.util.LoteExpedienteDetalleEnum;
import mx.isban.flujogd.persistence.OracleManagerBatch;
import mx.isban.flujogd.persistence.PersistenceManagerBatch;

/**
 * Clase que permitira la persistencia de informacion entre los distintos procesos
 * The Class PersistenceManagerBatchImpl.
 *
 * @author Alvaro Zamorano azamorano@serviciosexternos.isban.mx Clase
 *         PersistenceManagerBatchImpl
 */
public class PersistenceManagerBatchImpl implements PersistenceManagerBatch {

	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(PersistenceManagerBatchImpl.class);
	
	// Inicializamos un string con el atributo ID_LOTE_EST_CAT_PK
	private static final String ID_LOTE_EST_CAT_PK = "ID_LOTE_EST_CAT_PK"; 
	
	// Inicializamos un string con el atributo ID_LOTE_DET_CAT_PK
	private static final String ID_LOTE_DET_CAT_PK = "ID_LOTE_DET_CAT_PK"; 
	
	/**
	 * Metodo para obtener el listado de los lotes por estatus
	 * retrieveAllLotesByStatus.
	 *
	 * @param nomEstatus the nom estatus
	 * @return the list
	 */
	public List<LoteBean> retrieveAllLotesByStatus(String nomEstatus) {
		OracleManagerBatch om = new OracleManagerBatch();
		List<LoteBean> lotes = null;
		StringBuilder sb = null;
		lotes = new ArrayList<LoteBean>();
		sb = new StringBuilder();
		// Agregar el query a ejecutar para la consulta
		sb.append("select ID_LOTE_PK,TXT_BUCS,TXT_PATH from OPT_MX_MAE_LOTE_DRO where ID_LOTE_EST_CAT_FK = ?");
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement(sb.toString())){
			// Agregar parametros de consulta
			ps.setInt(1,
					(retrieveIdCatalogoByName(
							getQuery(CatalogoEnum.CAT_ESTATUS_LOTE.getName(), ID_LOTE_EST_CAT_PK), nomEstatus,
							ID_LOTE_EST_CAT_PK)));
			try (ResultSet rs = ps.executeQuery()){
				// Obtener el listado de lotes
				while (rs.next()) {
					LoteBean lote = new LoteBean();
					LoteBeanDetalle detalle = new LoteBeanDetalle();
					lote.setDetalle(detalle);
					lote.setIdLote(rs.getString("ID_LOTE_PK"));
					lote.getDetalle().setBucs(rs.getString("TXT_BUCS"));
					lote.getDetalle().setPath(rs.getString("TXT_PATH"));
					lotes.add(lote);
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		}
		return lotes;
	}

	/**
	 * Metodo para obtener los parametos de los lotes
	 * retrieveParam.
	 *
	 * @param nombreParam the nombre param
	 * @return the batch parametro bean
	 */
	public ParametroBean retrieveParam(String nombreParam) {
		OracleManagerBatch om = new OracleManagerBatch();
		ParametroBean parametroBean = null;
		StringBuilder sb = null;
		// Agregar el query a ejecutar para la consulta
		sb = new StringBuilder();
		sb.append("select TXT_NOMBR,TXT_VALOR,FLG_ENCRI from OPT_MX_MAE_LOTE_PARAM where TXT_NOMBR = ?");
		try (Connection conn = om.getConnectionByWallet(); 
			PreparedStatement ps = conn.prepareStatement(sb.toString())){
				// Agregar parametros de consulta
				ps.setString(1, nombreParam);
				try(ResultSet rs = ps.executeQuery()){				
					if (rs.next()) {
						parametroBean = new ParametroBean();
						parametroBean.setNombreParametro(rs.getString("TXT_NOMBR"));
						parametroBean.setValorParametro(rs.getString("TXT_VALOR"));
						parametroBean.setEstaEncriptado(rs.getString("FLG_ENCRI"));
					}
			}
		} catch (SQLException e) {
			LOG.error(e);
		}
		return parametroBean;
	}

	/**
	 * Metodo para obtener los parametros por configuracion
	 * Retrieve param by conf.
	 *
	 * @param conf the conf
	 * @return the list
	 */
	public List<ParametroBean> retrieveParamByConf(String conf) {
		List<ParametroBean> lista = new ArrayList<ParametroBean>();
		OracleManagerBatch om = new OracleManagerBatch();
		ParametroBean parametroBean = null;
		StringBuilder sb = new StringBuilder();
		// Agregar el query a ejecutar para la consulta
		sb.append(
				"select p.TXT_NOMBR,p.TXT_VALOR,p.FLG_ENCRI from OPT_MX_MAE_LOTE_PARAM p, OPT_MX_MAE_LOTE_CONF c ");
		sb.append("where c.ID_CONF_PK = p.ID_CONF_FK and c.TXT_NOMBR = ?");
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement(sb.toString())){
			// Agregar parametros de consulta
			ps.setString(1, conf);
			try (ResultSet rs = ps.executeQuery()){				
				while (rs.next()) {
					parametroBean = new ParametroBean();
					parametroBean.setNombreParametro(rs.getString("TXT_NOMBR"));
					parametroBean.setValorParametro(rs.getString("TXT_VALOR"));
					parametroBean.setEstaEncriptado(rs.getString("FLG_ENCRI"));
					lista.add(parametroBean);
				}
			}
			
		} catch (SQLException e) {
			LOG.error(e);
		}
		return lista;
	}

	/**
	 * Parametro para actualizar el estatus de los lotes
	 * Update status lote.
	 *
	 * @param idLote the id lote
	 * @param nomEstatus the nom estatus
	 * @return true, if successful
	 */
	public boolean updateStatusLote(String idLote, String nomEstatus) {
		boolean ok = false;
		OracleManagerBatch om = new OracleManagerBatch();
		StringBuilder sb = new StringBuilder();
		// Agregar el query a ejecutar para la actualizacion
		sb.append("update OPT_MX_MAE_LOTE_DRO set ID_LOTE_EST_CAT_FK = ? where ID_LOTE_PK = ?");
		try (Connection conn = om.getConnectionByWallet(); 
				PreparedStatement ps = conn.prepareStatement(sb.toString())){
			// Agregar parametros de actualizacion
			ps.setInt(1,
					(retrieveIdCatalogoByName(
							getQuery(CatalogoEnum.CAT_ESTATUS_LOTE.getName(), ID_LOTE_EST_CAT_PK), nomEstatus,
							ID_LOTE_EST_CAT_PK)));
			ps.setInt(2, new Integer(idLote));
			if (ps.executeUpdate() > 0) {
				ok = true;
			}
		} catch (SQLException e) {
			LOG.error(e);
		}
		return ok;
	}

	/**
	 * Metodo para insertar detalle del lote
	 * Insert detalle lote.
	 *
	 * @param idLoteFK the id lote FK
	 * @param nomDetalleStatus the nom detalle status
	 * @param buc the buc
	 * @param path the path
	 * @return true, if successful
	 */
	public boolean insertDetalleLote(String idLoteFK, String nomDetalleStatus, String buc, String path) {
		boolean ok = false;
		OracleManagerBatch om = new OracleManagerBatch();
		StringBuilder sb = new StringBuilder();
		// Agregar el query a ejecutar para la insercion de detalles
		sb.append("insert into OPT_MX_REL_LOTE_DET_DRO(ID_LOTE_DET_PK,ID_LOTE_FK,ID_LOTE_DET_CAT_FK,TXT_BUC,FCH_CRE_REG,FCH_ULT_ACTU_REG,TXT_PATH)values(S_OPT_MX_LOTE_DET.NEXTVAL,?,?,?,sysdate,sysdate,?)");
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement(sb.toString())){
			// Agregar parametros de insercion
			ps.setInt(1, new Integer(idLoteFK));
			ps.setInt(2,
					(retrieveIdCatalogoByName(
							getQuery(CatalogoEnum.CAT_ESTATUS_DET_LOTE.getName(), ID_LOTE_DET_CAT_PK),
							nomDetalleStatus, ID_LOTE_DET_CAT_PK)));
			ps.setString(3, buc);
			ps.setString(4, path);

			if (ps.executeUpdate() > 0) {
				ok = true;
			}
		} catch (SQLException e) {
			LOG.error(e);
		} 
		return ok;
	}

	/**
	 * getQuery.
	 *
	 * @param catalogName the catalog name
	 * @param idCampo the id campo
	 * @return the query
	 */
	private String getQuery(String catalogName, String idCampo) {
		return "select " + idCampo + " from " + catalogName + " WHERE TXT_NOMBR = ?";
	}

	/**
	 * Metodo para obtener el Id del catalogo por nombre
	 * retrieveIdCatalogoByName.
	 *
	 * @param query Parametro con el query para obtener el id
	 * @param name the name Parametro par obtener el nombre
	 * @param idCampo the id campo Parametro para obtener el campo
	 * @return the integer Regresae el id con el nombre y campo del catalogo
	 */
	private Integer retrieveIdCatalogoByName(String query, String name, String idCampo) {
		OracleManagerBatch om = new OracleManagerBatch();
		Integer id = 0;
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement(query)){
			ps.setString(1, name);
			try(ResultSet rs = ps.executeQuery()){				
				if (rs.next()) {
					id = rs.getInt(idCampo);
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		}

		return id;
	}


	/**
	 * Metodo para actualizar el detalle de los lotes
	 * Update detalle lote.
	 *
	 * @param idLote the id lote Parametro para el id del lote
	 * @param nomEstatus the nom estatus Parametro para el estatus
	 * @return true Regresa el valor de true si es exitoso
	 */
	public boolean updateDetalleLote(String idLote, String nomEstatus) {
		boolean ok = false;
		// Instancia de utileria de Oracle
		OracleManagerBatch om = new OracleManagerBatch();
		StringBuilder sb = new StringBuilder();
		// Agregar el query a ejecutar para la actualizacion de detalles
		sb.append("update OPT_MX_REL_LOTE_DET_DRO set ID_LOTE_DET_CAT_FK = ? where ID_LOTE_FK = ?");
		// Recuperar la conexion y Preparar query a ejecutar
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement(sb.toString())){
			// Agregar parametros de actualizacion
			ps.setInt(1,
					(retrieveIdCatalogoByName(
							getQuery(CatalogoEnum.CAT_ESTATUS_DET_LOTE.getName(), ID_LOTE_DET_CAT_PK),
							LoteExpedienteDetalleEnum.ERROR.getName(), ID_LOTE_DET_CAT_PK)));
			ps.setInt(2, new Integer(idLote));
			// Validar ejecucion del query
			if (ps.executeUpdate() > 0) {
				ok = true;
			}
		} catch (SQLException e) {
			LOG.error(e);
		} 
		return ok;
	}
}
