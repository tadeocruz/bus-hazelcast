package mx.isban.flujogd.persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mx.isban.flujogd.common.util.CatalogoEnum;
import mx.isban.flujogd.common.util.QueueStatusEnum;
import mx.isban.flujogd.persistence.OracleManager;
import mx.isban.flujogd.persistence.PersistenceManagerBloque;

/**
 * The Class PersistenceManagerBloqueImpl.
 * Clase que nos permitira la insercion o actualizacion de los estatus 
 * para los bloques de los expedientes
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase PersistenceManagerBloqueImpl
 */
public class PersistenceManagerBloqueImpl implements PersistenceManagerBloque{

	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(PersistenceManagerBloqueImpl.class);

	/**
	 * updateStatusBloqueExp.
	 * Metodo para insertar o actulizar el estatus de los expedientes
	 * 
	 * @param idExpediente the id expediente
	 * @param status the status
	 * @param nombreBloque the nombre bloque
	 * @param nombreError the nombre error
	 * @return true, if successful
	 */
	public boolean updateStatusBloqueExp(String idExpediente, String status, String nombreBloque, String nombreError) {
		Integer id = retrieveIdEstatusBloque(idExpediente, nombreBloque);
		Integer idTipoError = 0;
		boolean ok = false;
		if(id==0) {
			// Si el expediente es nuevo, se inserta el estatus
			ok = insertStatusBloqueExp(idExpediente, status, nombreBloque);
			id = retrieveIdEstatusBloque(idExpediente, nombreBloque);
			if(QueueStatusEnum.STATUS_ERROR.getName().equals(status)) {
				insertTipoError(id, nombreError, 1);
				idTipoError = retrieveTipoError(id, nombreError);
				insertHistoricoError(idTipoError);
			}
		}else {
			// Si el expediente existe, se actualiza el estatus
			ok = updateStatusBloqueExp(id, status);
			if(QueueStatusEnum.STATUS_ERROR.getName().equals(status)) {
				idTipoError = retrieveTipoError(id, nombreError);
				updateTipoError(idTipoError);
				insertHistoricoError(idTipoError);
			}
		}
		return ok;
	}

	/**
	 * updateStatusBloqueExp.
	 * Metodo para actualizar el estatus de los bloques
	 *
	 * @param idEstatusBloque the id estatus bloque
	 * @param status the status
	 * @return true, if successful
	 */
	private boolean updateStatusBloqueExp(Integer idEstatusBloque, String status) {
		boolean ok = false;
		OracleManager om = new OracleManager();
		// Preparar la conexion y el query para actualizar el estatus
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement("update OPT_MX_MAE_ESTAT_BLOQ set ID_CAT_ESTAT_BLOQ_FK = ?, FCH_ACTU = sysdate where ID_ESTAT_BLOQ_PK = ?")){
			// Agregar los parametros de actualizaion para estatus
			ps.setInt(1, retrieveIdCatalogoByName(getQuery(CatalogoEnum.CAT_ESTATUS_BLOQUE.getName()), status));
			ps.setInt(2, idEstatusBloque);
			if(ps.executeUpdate() > 0) {
				ok = true;
			}
		} catch (SQLException e) {
			LOG.error(e);
		}
		return ok;
	}

	/**
	 * updateTipoError.
	 * Metodo para actualizar el tipo de error
	 * 
	 * @param idTipoError the id tipo error
	 * @return true, if successful
	 */
	private boolean updateTipoError(Integer idTipoError) {
		boolean ok = false;
		OracleManager om = new OracleManager();
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement("update OPT_MX_MAE_TIPO_ERROR set NUM_CONT = NUM_CONT+1 where ID_TIPO_ERROR_PK = ?")){
			// Agregar el parametro de actualizacion del tipo de parametro
			ps.setInt(1, idTipoError);
			if(ps.executeUpdate() > 0) {
				ok = true;
			}
		} catch (SQLException e) {
			LOG.error(e);
		}
		return ok;
	}

	/**
	 * insertStatusBloqueExp.
	 * Insertar el estatus para los bloques de los expedientes
	 *
	 * @param idExpediente the id expediente
	 * @param status the status
	 * @param nombreBloque the nombre bloque
	 * @return true, if successful
	 */
	private boolean insertStatusBloqueExp(String idExpediente, String status, String nombreBloque) {
		boolean ok = false;
		OracleManager om = new OracleManager();
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement("insert into OPT_MX_MAE_ESTAT_BLOQ(ID_EXP_FK,ID_CAT_ESTAT_BLOQ_FK,ID_CAT_BLOQ_FK,FCH_CREA)values(?,?,?,sysdate)")){
			// Agregar los parametros de insercion de estatus
			ps.setInt(1, new Integer(idExpediente));
			ps.setInt(2, retrieveIdCatalogoByName(getQuery(CatalogoEnum.CAT_ESTATUS_BLOQUE.getName()), status));
			ps.setInt(3, retrieveIdCatalogoByName(getQuery(CatalogoEnum.CAT_BLOQUE.getName()), nombreBloque));
			if(ps.executeUpdate() > 0) {
				ok = true;
			}
		} catch (SQLException e) {
			LOG.error(e);
		}
		return ok;
	}

	/**
	 * insertTipoError.
	 * Metodo para insertar los tipos de errores
	 *
	 * @param idStatusBloque the id status bloque
	 * @param nombreError the nombre error
	 * @param intentos the intentos
	 * @return true, if successful
	 */
	private boolean insertTipoError(Integer idStatusBloque, String nombreError, Integer intentos) {
		boolean ok = false;
		OracleManager om = new OracleManager();
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement("insert into OPT_MX_MAE_TIPO_ERROR (ID_ESTAT_BLOQ_FK,ID_CAT_ERROR_FK,NUM_CONT)values(?,?,?)")){
			// Agregar los parametros de insercion de tipo de error
			ps.setInt(1, idStatusBloque);
			ps.setInt(2, retrieveIdCatalogoByName(getQuery(CatalogoEnum.CAT_ERROR.getName()), nombreError));
			ps.setInt(3, intentos);
			if(ps.executeUpdate() > 0) {
				ok = true;
			}
		} catch (SQLException e) {
			LOG.error(e);
		}
		return ok;
	}

	/**
	 * insertHistoricoError.
	 * Metodo para insertar el historico de los tipos de error
	 * 
	 * @param idTipoError the id tipo error
	 * @return true, if successful
	 */
	private boolean insertHistoricoError(Integer idTipoError) {
		boolean ok = false;
		OracleManager om = new OracleManager();
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement("insert into OPT_MX_MAE_HIST_ERROR (ID_TIPO_ERROR_FK,FCH_CREA)values(?,sysdate)")){
			// Agregar parametro para insertar historico de errores
			ps.setInt(1, idTipoError);
			if(ps.executeUpdate() > 0) {
				ok = true;
			}
		} catch (SQLException e) {
			LOG.error(e);
		}
		return ok;
	}

	/**
	 * retrieveIdEstatusBloque.
	 * Metodo para obtener los id de los estatus del bloque
	 * 
	 * @param idExpediente the id expediente
	 * @param nombreBloque the nombre bloque
	 * @return the integer
	 */
	private Integer retrieveIdEstatusBloque(String idExpediente,String nombreBloque) {
		OracleManager om = new OracleManager();
		Integer id = 0;
		StringBuilder sb = null;
		sb = new StringBuilder();
		sb.append("select ID_ESTAT_BLOQ_PK from OPT_MX_MAE_ESTAT_BLOQ where ID_EXP_FK = ? and ID_CAT_BLOQ_FK = ?");
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement(sb.toString())){
			ps.setInt(1, new Integer(idExpediente));
			ps.setInt(2, retrieveIdCatalogoByName(getQuery(CatalogoEnum.CAT_BLOQUE.getName()), nombreBloque));
			try(ResultSet rs = ps.executeQuery()){				
				if(rs.next()){
					id = rs.getInt("ID_ESTAT_BLOQ_PK");
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		} 
		return id;
	}

	/**
	 * retrieveTipoError.
	 * Metodo para consultar el tipo de error 
	 * 
	 * @param idEstatusBloque the id estatus bloque
	 * @param nombreError the nombre error
	 * @return the integer
	 */
	private Integer retrieveTipoError(Integer idEstatusBloque,String nombreError) {
		OracleManager om = new OracleManager();
		Integer id = 0;
		StringBuilder sb = new StringBuilder();
		sb.append("select ID_TIPO_ERROR_PK from OPT_MX_MAE_TIPO_ERROR where ID_ESTAT_BLOQ_FK = ? and ID_CAT_ERROR_FK = ?");
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement(sb.toString())){
			// Agregar parametros para consultar el tipo de erro
			ps.setInt(1, idEstatusBloque);
			ps.setInt(2, retrieveIdCatalogoByName(getQuery(CatalogoEnum.CAT_ERROR.getName()), nombreError));
			try(ResultSet rs = ps.executeQuery()){				
				if(rs.next()){
					id = rs.getInt("ID_TIPO_ERROR_PK");
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		}
		return id;
	}

	/**
	 * retrieveIdCatalogoByName.
	 * Metodo para obtener el id del catalogo por nombre
	 *
	 * @param query the query
	 * @param name the name
	 * @return the integer
	 */
	private Integer retrieveIdCatalogoByName(String query, String name) {
		OracleManager omCatalogo = new OracleManager();
		Integer id = 0;
		// Preparar conexion
		try (Connection connCatalogo = omCatalogo.getConnectionByWallet();
				PreparedStatement ps = connCatalogo.prepareStatement(query)){
			ps.setString(1, name);
			// Ejecutar la consulta
			try (ResultSet rs = ps.executeQuery()){				
				if(rs.next()){
					id = rs.getInt("ID_CAT_PK");
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		}

		return id;
	}
	
	/**
	 * getQuery.
	 *
	 * @param catalogName the catalog name
	 * @return the query
	 */
	private String getQuery(String catalogName) {
		return "select ID_CAT_PK from "+catalogName+" WHERE TXT_NOMBR = ?";
	}

}
