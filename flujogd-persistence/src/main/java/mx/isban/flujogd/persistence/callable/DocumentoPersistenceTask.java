package mx.isban.flujogd.persistence.callable;

import java.io.Serializable;
import java.util.concurrent.Callable;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.HazelcastInstanceAware;

import mx.isban.flujogd.persistence.PersistenceManagerDocumento;
import mx.isban.flujogd.persistence.impl.PersistenceManagerDocumentoImpl;

/**
 * The Class DocumentoPersistenceTask.
 *
 * @author Alvaro
 */
public class DocumentoPersistenceTask implements Callable<Object>, Serializable, HazelcastInstanceAware{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5103347697272848803L;

	/** The hazelcast instance. */
	@SuppressWarnings("unused")
	private transient HazelcastInstance hazelcastInstance;
	
	/** The id expediente. */
	private String idExpediente;
	
	/** The tipo documento. */
	private String tipoDocumento;
	
	/** The nombre. */
	private String nombre;
	
	/** The checksum. */
	private String checksum;
	
	/** The estatus. */
	private String estatus;
	
	/**
	 * Instanciar nuevo documento con persistencia de datos
	 * Instantiates a new documento persistence task.
	 *
	 * @param idExpediente the id expediente
	 * @param tipoDocumento the tipo documento
	 * @param nombre the nombre
	 * @param checksum the checksum
	 * @param estatus the estatus
	 */
	public DocumentoPersistenceTask(String idExpediente, String tipoDocumento, String nombre, String checksum, String estatus) {
		this.idExpediente = idExpediente;
		this.tipoDocumento = tipoDocumento;
		this.nombre = nombre;
		this.checksum = checksum;
		this.estatus = estatus;
	}

	/**
	 * Sets the hazelcast instance.
	 *
	 * @param hazelcastInstance the new hazelcast instance
	 */
	@Override
	public void setHazelcastInstance(HazelcastInstance hazelcastInstance) {
		this.hazelcastInstance = hazelcastInstance;
	}

	/**
	 * Metodo para insertar documentos a filenet
	 * Call.
	 *
	 * @return the object
	 * @throws Exception the exception
	 */
	@Override
	public Object call() throws Exception {
		PersistenceManagerDocumento persistenceManager = new PersistenceManagerDocumentoImpl();
		return persistenceManager.insertDocumento(this.idExpediente, this.tipoDocumento, this.nombre, this.checksum, this.estatus);
	}

}