/*
 * 
 */
package mx.isban.flujogd.persistence;

import java.util.List;

import mx.isban.flujogd.common.bean.ConexionSFTPBean;
import mx.isban.flujogd.common.bean.ExpedientePurgaBean;

/**
 * The Interface PersistenceManagerExpediente.
 *
 * @author Alvaro
 */
public interface PersistenceManagerExpediente {
	
	//Insertar el estatus del expediente
	/**
	 * insertestatusexpediente.
	 *
	 * @param idExpediente Id del expediente
	 * @param status Estatus del expediente
	 * @param idError Id de error
	 * @return Estatus de la insercion del expediente
	 */
	boolean insertEstatusExpediente(String idExpediente, String status, String idError);

	//Traer numero de intentos de acuerdo al tipo de error
	/**
	 * retrieveintentostipoerro.
	 *
	 * @param idExpediente Id del expediente
	 * @param idBloque Id del bloque
	 * @param idCatError Id de la categoria del error
	 * @return Numero de intentos
	 */
	Integer retrieveIntentosTipoError(String idExpediente, String idBloque, String idCatError);

	//Traer el numero de intentos de acuerdo al nombre del error
	/**
	 * retrievereintentosbynombreerror.
	 *
	 * @param idError Id del error
	 * @return Numero de intentos
	 */
	Integer retrieveReintentosByNombreError(String idError);
	
	/**
	 * insertServidorNacimiento.
	 *
	 * @param idExpediente the id expediente
	 * @param ipServer the ip server
	 * @return true, if successful
	 */
	boolean insertServidorNacimiento(String idExpediente, String ipServer);

	/**
	 * Retrieve expedientes purga by suscriptor
	 * Metodo para obtener los expedientes a purgar por suscriptor
	 *
	 * @param estatus the estatus
	 * @param idSubscriptor identificador del suscriptor
	 * @return the list
	 */
	List<ExpedientePurgaBean> retrieveExpedientesPurgaBySuscriptor(int dias, String idSubscriptor);
	
	/**
	 * Retrieve data server FTP.
	 * Metodo para obtener los datos de conexion al servidor sftp
	 *
	 * @param subscriptor the subscriptor
	 * @return the conexion SFTP bean
	 */
	ConexionSFTPBean retrieveDataServerFTP(String subscriptor);
	
	/**
	 * Validar expediente.
	 *
	 * @param nombre the nombre
	 * @return lista de expedientes
	 */
	List<String> validarExpediente(String nombre);

}
