package mx.isban.flujogd.persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mx.isban.flujogd.common.bean.BloqueBean;
import mx.isban.flujogd.common.bean.BloqueBean2;
import mx.isban.flujogd.common.bean.ParametroBean;
import mx.isban.flujogd.common.bean.ReglaBean;
import mx.isban.flujogd.common.bean.ReglaBeanDetalle;
import mx.isban.flujogd.common.generic.CommonsUtils;
import mx.isban.flujogd.persistence.OracleManager;
import mx.isban.flujogd.persistence.PersistenceManagerBloqueDetail;

/**
 * The Class PersistenceManagerBloqueDetailImpl.
 * Metodo para consultar, insertar y actualizar detalles de los bloques de expedientes
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase PersistenceManagerBloqueDetailImpl
 */
public class PersistenceManagerBloqueDetailImpl implements PersistenceManagerBloqueDetail{
	
	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(PersistenceManagerBloqueDetailImpl.class);
	
	/** The Constant TXT_NOMBRE. */
	private static final String TXT_NOMBRE = "TXT_NOMBR";

	/**
	 * retrieveBloques.
	 * Metodo para consultar los bloques de expedientes por subscriptor y id de expediente
	 *
	 * @param subscriptorName the subscriptor name
	 * @param idExpediente the id expediente
	 * @return the list
	 */
	public List<BloqueBean> retrieveBloques(String subscriptorName, Integer idExpediente) {
		Integer id = retrieveIdFlujo(subscriptorName);
		return retrieveBloquesList(id,idExpediente);
	}
	
	/**
	 * retrieveBloques.
	 * Metodo para consultar los bloques de expedientes por subscriptor
	 * @param subscriptorName the subscriptor name
	 * @return the list
	 */
	public List<BloqueBean> retrieveBloques(String subscriptorName) {
		Integer id = retrieveIdFlujo(subscriptorName);
		return retrieveBloquesList(id);
	}
	
	/**
	 * retrieveIdFlujo.
	 *
	 * @param subscriptorName Parametro subscriptorName de tipo String
	 * @return the integer Retorna el id del flujo
	 */
	private Integer retrieveIdFlujo(String subscriptorName) {
		OracleManager om = new OracleManager();
		Integer id = 0;
		StringBuilder sb = new StringBuilder();
		// Preparar query de consulta de id de flujo
		sb.append("select omf.ID_FLUJO_PK from OPT_MX_MAE_SUBSC os, OPT_MX_REL_SUBSC_FLUJO ors, OPT_MX_MAE_FLUJO omf ");
		sb.append("where os.TXT_NOMBR = ? ");
		sb.append("and os.ID_SUBSC_PK = ors.ID_SUBSC_FK ");
		sb.append("and ors.ID_FLUJO_FK = omf.ID_FLUJO_PK");
		// Preparar conexion a BD
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement(sb.toString())){
			// Agregar parametros para consulta de id Flujo
			ps.setString(1, subscriptorName);
			// ejecutar consulta de id flujo
			try(ResultSet rs = ps.executeQuery()){				
				if(rs.next()){
					id = rs.getInt("ID_FLUJO_PK");
				}
			}
		} catch (SQLException e) {
			// En caso de error, mostrar excepcion
			LOG.error(e);
		} 	
		return id;
	}
	
	/**
	 * retrieveBloquesList.
	 * Metodo para obtener un listado de los bloques de expedientes a procesar
	 *
	 * @param idFlujo the id flujo
	 * @return the list
	 */
	private List<BloqueBean> retrieveBloquesList(Integer idFlujo){
		OracleManager omBloques = new OracleManager();
		Integer idRegla = null;
		ReglaBean reglaBean = null;
		List<BloqueBean> listaBloques = new ArrayList<BloqueBean>();
		// Preparar query de consulta de bloques 
		try (Connection connBloques = omBloques.getConnectionByWallet();
				PreparedStatement ps = connBloques.prepareStatement("select f.ID_REGLA_INIT_FK from OPT_MX_MAE_FLUJO f where f.ID_FLUJO_PK = ?")){
			// Agregar parametros de consulta
			ps.setInt(1, idFlujo);
			// Ejecutar consulta
			try (ResultSet rs = ps.executeQuery()){				
				if(rs.next()){
					idRegla = rs.getInt("ID_REGLA_INIT_FK");
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		}
		
		do{
			reglaBean = retrieveRegla(idRegla);
			idRegla = reglaBean.getIdSiguienteRegla();
			if(reglaBean.getEsFlujo()==0) {				
				listaBloques.add(reglaBean.getBloqueBean());
			} else {				
				listaBloques.addAll(retrieveBloquesList(reglaBean.getId()));
			}
		}while(idRegla != 0);
		
		return listaBloques;
	}
	
	/**
	 * Metodo para obtener la lista de bloques pendientes
	 * retrieveBloquesList.
	 *
	 * @param idFlujo the id flujo
	 * @param idExpediente the id expediente
	 * @return the list
	 */
	private List<BloqueBean> retrieveBloquesList(Integer idFlujo,Integer idExpediente){
		OracleManager om = new OracleManager();
		Integer idRegla = null;
		ReglaBean reglaBean = null;
		List<BloqueBean> lista = new ArrayList<BloqueBean>();
		// Preparar conexion a BD
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement("select f.ID_REGLA_INIT_FK from OPT_MX_MAE_FLUJO f where f.ID_FLUJO_PK = ?")){
			// Agregar parametros de consulta
			ps.setInt(1, idFlujo);
			// Ejecutar consulta de bloques
			try(ResultSet rs = ps.executeQuery()){				
				if(rs.next()){
					idRegla = rs.getInt("ID_REGLA_INIT_FK");
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		}
		// Obtener las reglas y estatus
		do{
			reglaBean = retrieveReglaAndStatus(idRegla,idExpediente);
			idRegla = reglaBean.getIdSiguienteRegla();
			if(reglaBean.getEsFlujo()==0) {				
				lista.add(reglaBean.getBloqueBean());
			} else {				
				lista.addAll(retrieveBloquesList(reglaBean.getId(),idExpediente));
			}
		}while(idRegla != 0);
		
		return lista;
	}
	
	/**
	 * retrieveRegla.
	 * Metodo para obtener las reglas a ejecutar para los expedientes
	 *
	 * @param idRegla the id regla
	 * @return the regla bean
	 */
	private ReglaBean retrieveRegla(Integer idRegla){
		OracleManager om = new OracleManager();
		StringBuilder sb = new StringBuilder();
		ReglaBean reglaBean = new ReglaBean();
		// Preparar el query para consultar las reglas
		sb.append("select regla.FLG_ES_FLUJO, regla.ID_FLUJO_BLOQUE, regla.ID_REGLA_SIG_FK, regla.NUM_INSTA ");
		sb.append("from OPT_MX_MAE_REGLA regla ");
		sb.append("where regla.ID_REGLA_PK = ?");
		// Preparar la conexion a BD
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement(sb.toString())){
			// Agregar parametros de consulta de reglas
			ps.setInt(1, idRegla);
			// Ejecutar la consulta de reglas
			try(ResultSet rs = ps.executeQuery()){				
				if(rs.next()){
					reglaBean.setEsFlujo(rs.getInt("FLG_ES_FLUJO"));
					reglaBean.setId(rs.getInt("ID_FLUJO_BLOQUE"));
					reglaBean.setIdSiguienteRegla(rs.getInt("ID_REGLA_SIG_FK"));
					ReglaBeanDetalle detalle = new ReglaBeanDetalle();
					detalle.setNumeroInstancias(rs.getInt("NUM_INSTA"));
					reglaBean.setDetalle(detalle);
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		} 
		// Validar el flujo de la regla
		if(reglaBean.getEsFlujo()==0) {			
			reglaBean.setBloqueBean(retrieveQueueById(reglaBean.getId(),reglaBean.getDetalle().getNumeroInstancias()));
		}
		return reglaBean;
	}
	
	/**
	 * retrieveReglaAndStatus
	 * En el query el primer case determina en que estatus se encuentra el bloque
	 * El el query el segundo case determina el numero de intentos para ese bloque .
	 *
	 * @param idRegla the id regla
	 * @param idExpediente the id expediente
	 * @return the regla bean
	 */
	private ReglaBean retrieveReglaAndStatus(Integer idRegla,Integer idExpediente){
		OracleManager omRegla = new OracleManager();
		StringBuilder sbRegla = new StringBuilder();
		ReglaBean reglaBean = new ReglaBean();
		ReglaBeanDetalle detalle = new ReglaBeanDetalle();
		CommonsUtils utils = new CommonsUtils();
		reglaBean.setDetalle(detalle);
		// Preparar query para consulta de reglas y estatus
		sbRegla.append("select ");
		sbRegla.append("case ");
		sbRegla.append("when regla.FLG_ES_FLUJO = 0 then ( ");
		sbRegla.append("select min(ID_CAT_ESTAT_BLOQ_FK)  from OPT_MX_MAE_ESTAT_BLOQ  where ID_EXP_FK = ? and ID_CAT_BLOQ_FK = regla.ID_FLUJO_BLOQUE ");
		sbRegla.append(")else 0 ");
		sbRegla.append("end as ESTATUS, ");
		sbRegla.append("regla.FLG_ES_FLUJO, regla.ID_FLUJO_BLOQUE, regla.ID_REGLA_SIG_FK, regla.NUM_INSTA ");
		sbRegla.append("from OPT_MX_MAE_REGLA regla ");
		sbRegla.append("where regla.ID_REGLA_PK = ?");
		// Preparar conexion a BD
		try (Connection conn = omRegla.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement(sbRegla.toString())){
			// Agregar parametro de consulta para reglas
			ps.setInt(1, idExpediente);
			ps.setInt(2, idRegla);
			// Ejecutar la consulta
			try(ResultSet rsRegla = ps.executeQuery()){				
				if(rsRegla.next()){
					String estatus = rsRegla.getString("ESTATUS");
					estatus = utils.validarNullTo0(estatus);
					reglaBean.getDetalle().setEstatus(new Integer(estatus));
					reglaBean.setEsFlujo(rsRegla.getInt("FLG_ES_FLUJO"));
					reglaBean.setId(rsRegla.getInt("ID_FLUJO_BLOQUE"));
					reglaBean.setIdSiguienteRegla(rsRegla.getInt("ID_REGLA_SIG_FK"));
					reglaBean.getDetalle().setNumeroInstancias(rsRegla.getInt("NUM_INSTA"));
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		}
		// Validar el flujo de la regla
		if(reglaBean.getEsFlujo()==0) {			
			reglaBean.setBloqueBean(retrieveQueueById(reglaBean.getId(),reglaBean.getDetalle().getNumeroInstancias()));
		}
		return reglaBean;
	}
	
	/**
	 * retrieveQueueById.
	 * Metodo para obtener las queues por id
	 *
	 * @param idBloque the id bloque
	 * @param numeroInstancias the numero instancias
	 * @return the bloque bean
	 */
	private BloqueBean retrieveQueueById(Integer idBloque,Integer numeroInstancias){
		OracleManager om = new OracleManager();
		BloqueBean bloque = new BloqueBean();
		// Preparar la conexion a la BD y query para obtener las queues
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement("select ID_CAT_PK,TXT_NOMBR,TXT_VERS from OPT_MX_MAE_CAT_BLOQ where ID_CAT_PK = ?")){
			// Agregar parametro de consulta para queues
			ps.setInt(1, idBloque);
			// Ejecutar la consulta de queues
			try(ResultSet rs = ps.executeQuery()){				
				if(rs.next()){
					bloque.setId(rs.getString("ID_CAT_PK"));
					bloque.setNombre(rs.getString(TXT_NOMBRE));
					bloque.setVersion(rs.getString("TXT_VERS"));
					BloqueBean2 bean2 = new BloqueBean2();
					bloque.setBean2(bean2);
					bean2.setActivo("0");
					// Setear el numero de instancia que se deben de levantar
					bean2.setNumeroInstancias(numeroInstancias.toString());
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		} 		
		return bloque;
	}
	
	/** Metodo para obtener los bloques por nombre de bloque
	 * retrieveBloqueByName.
	 *
	 * @param nombreBloque the nombre bloque
	 * @return the bloque bean
	 */
	public BloqueBean retrieveBloqueByName(String nombreBloque) {
		OracleManager om = new OracleManager();
		BloqueBean bean = null;
		StringBuilder sb = new StringBuilder();
		// Preparar el query de consulta de bloques por nombre
		sb.append("select ID_CAT_PK, TXT_NOMBR, TXT_VERS from OPT_MX_MAE_CAT_BLOQ ");
		sb.append("where TXT_NOMBR = ?");
		// Preparar conexion a BD
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement(sb.toString())){
			// Agregar parametro de consulta			
			ps.setString(1, nombreBloque);
			// Ejecutar la consulta
			try(ResultSet rs = ps.executeQuery()){				
				if(rs.next()){
					bean = new BloqueBean();
					bean.setId(rs.getString("ID_CAT_PK"));
					bean.setNombre(rs.getString(TXT_NOMBRE));
					bean.setVersion(rs.getString("TXT_VERS"));
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		} 		
		return bean;
	}
	
	/**
	 * Metodo para obtener el listado de parametros para los bloques a procesar
	 * retrieveParametroBloque.
	 *
	 * @param idExpediente the id expediente
	 * @param nombreBloque the nombre bloque
	 * @param parametro the parametro
	 * @return the list lista de bloques
	 */
	public List<ParametroBean> retrieveParametroBloque(String idExpediente, String nombreBloque,String parametro){
		OracleManager om = new OracleManager();
		StringBuilder sb = new StringBuilder();
		List<ParametroBean> lista = new ArrayList<ParametroBean>();
		// Preparar la consulta de parametros por bloques
		sb.append("select param.TXT_NOMBR, param.TXT_VALOR ");
		sb.append("from OPT_MX_MAE_EXP exp, OPT_MX_MAE_NOTI noti, OPT_MX_MAE_SUBSC sub, OPT_MX_MAE_CONF conf, OPT_MX_MAE_CAT_BLOQ bloq, OPT_MX_MAE_PARAM param ");
		sb.append("where exp.ID_NOTI_FK = noti.ID_NOTI_PK ");
		sb.append("and noti.ID_SUBSC_FK = sub.ID_SUBSC_PK ");
		sb.append("and sub.ID_SUBSC_PK = conf.ID_SUBSC_FK ");
		sb.append("and conf.ID_CAT_BLOQ_FK = bloq.ID_CAT_PK ");
		sb.append("and conf.ID_CONF_PK = param.ID_CONF_FK ");
		sb.append("and exp.ID_EXP_PK = ? ");
		sb.append("and bloq.TXT_NOMBR = ? ");
		sb.append("and param.TXT_NOMBR = ?");
		// preparar la conexion a BD
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// Agregar parametros de consulta
			ps.setLong(1, new Long(idExpediente));
			ps.setString(2, nombreBloque);
			ps.setString(3, parametro);
			// Ejecutar consulta
			try(ResultSet rs = ps.executeQuery()){				
				if(rs.next()){
					ParametroBean bean = new ParametroBean();
					bean.setNombreParametro(rs.getString(TXT_NOMBRE));
					bean.setValorParametro(rs.getString("TXT_VALOR"));
					lista.add(bean);
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		}		
		return lista;
	}

}
