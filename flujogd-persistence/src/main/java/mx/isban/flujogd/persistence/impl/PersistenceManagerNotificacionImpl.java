package mx.isban.flujogd.persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mx.isban.flujogd.common.bean.AccentureDocumentoDetalleRequest;
import mx.isban.flujogd.common.bean.AccentureDocumentoRequest;
import mx.isban.flujogd.common.bean.AccentureNotificationRequest;
import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.bean.ExpedienteBeanDetalle;
import mx.isban.flujogd.common.bean.ExpedienteBeanDetalle2;
import mx.isban.flujogd.common.bean.ExpedienteBeanDetalle3;
import mx.isban.flujogd.common.bean.NotificationBean;
import mx.isban.flujogd.persistence.OracleManager;
import mx.isban.flujogd.persistence.PersistenceManagerNotificacion;

/**
 * The Class PersistenceManagerNotificacionImpl.
 * Clase que permite insertar y actualiar notificaciones
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase PersistenceManagerNotificacionImpl
 */
public class PersistenceManagerNotificacionImpl implements PersistenceManagerNotificacion{
	
	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(PersistenceManagerNotificacionImpl.class);
	
	/** The sdf. */
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	/**
	 * Insert or update notification.
	 * Metodo para insertar o actualziar notificaciones
	 *
	 * @param bean the bean
	 * @return true, if successful
	 */
	public boolean insertOrUpdateNotification(NotificationBean bean) {
		// Instanciar utileria de persistencia para notificacion
		PersistenceManagerNotificacionImplUtils utils = new PersistenceManagerNotificacionImplUtils();
		// Obtener las notificaciones por id de expediente
		Integer idNotificacion = utils.retrieveIdNotificacionByIdExpediente(bean.getIdExpediente());
		boolean ok = false;
		// Validar si existe notificacion
		if(idNotificacion==0) {
			// Insertar notificacion
			ok = utils.insertNotification(bean);
		}else {
			// Actualizar notificacion
			ok = utils.updateNotification(idNotificacion, bean);
		}
		return ok;
	}

	/**
	 * RetrieveAllNotificacionesBySubscriptor.
	 * Metodo para validar las reglas de flujo
	 *
	 * @param nombreSubscriptor Parametro nombreSubscriptor para el nombre del subscriptor
	 * @return the list Regresa la lista con las notificaciones del expediente
	 */
	public List<ExpedienteBean> retrieveAllNotificacionesBySubscriptor(String nombreSubscriptor) {
		// Inicializar lista para los expedientes
		List<ExpedienteBean> notificaciones = new ArrayList<ExpedienteBean>();
		OracleManager om = new OracleManager();
		// Preparar el query de consulta
		StringBuilder sb = new StringBuilder();
		sb.append("select en.TXT_REF_EXT, en.TXT_REQID, en.TXT_ERROR_ID, en.TXT_ERROR_MSG,en.TXT_BUC, en.ID_EXP_FK  from OPT_MX_MAE_ESTAT_NOTI en, OPT_MX_MAE_EXP exp, OPT_MX_MAE_NOTI noti, OPT_MX_MAE_SUBSC subs ");
		sb.append("where en.ID_CAT_ESTAT_NOTI_FK = 1 ");
		sb.append("and en.ID_EXP_FK = exp.ID_EXP_PK ");
		sb.append("and exp.ID_NOTI_FK = noti.ID_NOTI_PK ");
		sb.append("and noti.ID_SUBSC_FK = subs.ID_SUBSC_PK ");
		sb.append("and subs.TXT_NOMBR = ?");
		// Realizar conexion y preparar el query a ejecutar
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement(sb.toString())){
			// Agregar parametro de consulta
			ps.setString(1, nombreSubscriptor);
			try (ResultSet rs = ps.executeQuery()){
				// Obtener los expedientes
				while(rs.next()) {
					// Setear parametros al bean de expediente
					ExpedienteBean expedienteBean = new ExpedienteBean();
					ExpedienteBeanDetalle detalle = new ExpedienteBeanDetalle();
					ExpedienteBeanDetalle2 detalle2 = new ExpedienteBeanDetalle2();
					ExpedienteBeanDetalle3 detalle3 = new ExpedienteBeanDetalle3();
					detalle3.setOrigen(nombreSubscriptor);
					expedienteBean.setDetalle(detalle);
					detalle2.setDetalle3(detalle3);
					expedienteBean.getDetalle().setDetalle2(detalle2);
					expedienteBean.setIdExpediente(rs.getString("ID_EXP_FK"));
					AccentureNotificationRequest request = new AccentureNotificationRequest();
					request.setBuc(rs.getString("TXT_BUC"));
					request.setFechaProceso(sdf.format(new Date()));
					request.setReferenciaExterna(rs.getString("TXT_REF_EXT"));
					request.setRequestId(rs.getString("TXT_REQID"));
					AccentureDocumentoRequest documento = new AccentureDocumentoRequest();
					AccentureDocumentoDetalleRequest detalleAcc = new AccentureDocumentoDetalleRequest();
					detalleAcc.setErrorId(rs.getString("TXT_ERROR_ID"));
					detalleAcc.setError(rs.getString("TXT_ERROR_MSG"));
					documento.setDetalle(detalleAcc);
					// Inicializar una lista para los documentos
					List<AccentureDocumentoRequest> docs= new ArrayList<AccentureDocumentoRequest>();
					docs.add(documento);
					request.setArchivos(docs);
					expedienteBean.getDetalle().getDetalle2().setAccenturRequest(request);
					// Agregar el expedientes a la lsita
					notificaciones.add(expedienteBean);
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		}
		return notificaciones;
	}

}
