package mx.isban.flujogd.persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mx.isban.flujogd.common.bean.ErroresCifrasControlAuxBean;
import mx.isban.flujogd.common.bean.ErroresCifrasControlBean;
import mx.isban.flujogd.common.bean.NotificationBean;
import mx.isban.flujogd.common.util.CatalogoEnum;
import mx.isban.flujogd.common.util.NotificacionStatusEnum;
import mx.isban.flujogd.persistence.OracleManager;

/**
 * The Class PersistenceManagerNotificacionImplUtils.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase PersistenceManagerNotificacionImpl
 */
public class PersistenceManagerNotificacionImplUtils{
	
	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(PersistenceManagerNotificacionImplUtils.class);
	
	/** Inicializamos un string con el atributo ID_ESTATUS */
	private static final String ID_ESTATUS = "ID_ESTATUS"; 
	
	/** Inicializamos un string con el atributo NO_EXPEDIENTES */
	private static final String NO_EXPEDIENTES = "NO_EXPEDIENTES"; 
	
	/**
	 * insertNotification.
	 * Metodo para insertar las notificaciones de expedientes
	 *
	 * @param bean the bean
	 * @return true, if successful
	 */
	public boolean insertNotification(NotificationBean bean) {
		boolean ok = false;
		// Instanciar oracle manager para conexiones a BD
		OracleManager om = new OracleManager();
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement("insert into OPT_MX_MAE_ESTAT_NOTI (ID_CAT_ESTAT_NOTI_FK, ID_EXP_FK, FCH_CREA, TXT_REF_EXT,TXT_REQID,TXT_ERROR_ID,TXT_ERROR_MSG,TXT_BUC,TXT_PATH_ZIP,TXT_FILE_NAME,TXT_PATH_UNZIP,TXT_REQ,TXT_RES_CODE,TXT_RES_MSG)values(?,?,sysdate,?,?,?,?,?,?,?,?,?,?,?)")){
			// Agregar los parametros para la insercion de la notificacion
			if(null==bean.getRequest()) {
				ps.setInt(1, retrieveIdCatalogoByName(getQuery(CatalogoEnum.CAT_ESTATUS_NOTIFICACION.getName()), NotificacionStatusEnum.NOTIFICACION_STATUS_NO_ENVIADA.getName()));
			}else {
				ps.setInt(1, retrieveIdCatalogoByName(getQuery(CatalogoEnum.CAT_ESTATUS_NOTIFICACION.getName()), NotificacionStatusEnum.NOTIFICACION_STATUS_ENVIADA.getName()));
			}
			ps.setInt(2, new Integer(bean.getIdExpediente()));
			ps.setString(3, bean.getBean2().getRefExterna());
			ps.setString(4, bean.getBean2().getReqId());
			ps.setString(5, bean.getBean2().getErrorId());
			ps.setString(6, bean.getBean2().getErrorMsg());
			ps.setString(7, bean.getBean2().getBean3().getBuc());
			ps.setString(8, bean.getBean2().getBean3().getPathZip());
			ps.setString(9, bean.getBean2().getBean3().getFileName());
			ps.setString(10, bean.getBean2().getBean3().getPathUnzip());
			
			ps.setString(11, bean.getRequest());
			ps.setString(12, bean.getResponseCode());
			ps.setString(13, bean.getResponseMsg());
			// Validar respuesta del insert de notificacion
			if(ps.executeUpdate() > 0) {				
				ok = true;
			}
		} catch (SQLException e) {
			LOG.error(e);
		}
		return ok;
	}
	
	/**
	 * retrieveIdNotificacionByIdExpediente.
	 * Metodo para obtener el id de las notificaciones por id de expediente
	 *
	 * @param idExpediente Parametro para el id del expediente
	 * @return the integer Regresa el id de las notificaciones
	 */
	public Integer retrieveIdNotificacionByIdExpediente(String idExpediente) {
		// Instanciar oracle manager para conexiones a BD
		OracleManager om = new OracleManager();
		Integer id = 0;
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement("select ID_ESTAT_NOTI_PK from OPT_MX_MAE_ESTAT_NOTI WHERE ID_EXP_FK = ?")){
			// Agregar el parametro de consulta por id de expediente
			ps.setInt(1, new Integer(idExpediente));
			// Obtener los registros
			try(ResultSet rs = ps.executeQuery()){				
				if(rs.next()){
					id = rs.getInt("ID_ESTAT_NOTI_PK");
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		}		
		return id;
	}
	
	/**
	 * updateNotification.
	 * Metodo para actualizar las notificaciones
	 * 
	 * @param idNotificacion the id notificacion
	 * @param bean the bean
	 * @return true, if successful
	 */
	public boolean updateNotification(Integer idNotificacion,NotificationBean bean) {
		boolean ok = false;
		// Instanciar oracle manager para conexiones a BD
		OracleManager om = new OracleManager();
		// Preparar la conexion a BD
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement("update OPT_MX_MAE_ESTAT_NOTI set ID_CAT_ESTAT_NOTI_FK = ?, FCH_ACTU =sysdate, TXT_REQ = ?, TXT_RES_CODE = ?, TXT_RES_MSG = ? where ID_ESTAT_NOTI_PK = ?")){
			// Agregar parametros de actualizacion de notificaciones
			ps.setInt(1, retrieveIdCatalogoByName(getQuery(CatalogoEnum.CAT_ESTATUS_NOTIFICACION.getName()), NotificacionStatusEnum.NOTIFICACION_STATUS_ENVIADA.getName()));
			ps.setString(2, bean.getRequest());
			ps.setString(3, bean.getResponseCode());
			ps.setString(4, bean.getResponseMsg());
			ps.setInt(5, idNotificacion);
			// Validar respuesta de la actualizacion
			if(ps.executeUpdate() > 0) {				
				ok = true;
			}
		} catch (SQLException e) {
			LOG.error(e);
		}
		return ok;
	}
	
	/**
	 * getQuery.
	 * Metodo para obtener un query simple
	 * 
	 * @param catalogName the catalog name
	 * @return the query
	 */
	private String getQuery(String catalogName) {
		return "select ID_CAT_PK from "+catalogName+" WHERE TXT_NOMBR = ?";
	}

	/**
	 * getRegistros.
	 * Metodo para obtener los registros por subscriptor
	 *
	 * @param nombreSubscriptor the nombre subscriptor
	 * @return the registros
	 */
	public String getRegistros(String nombreSubscriptor) {
		StringBuilder sqlSb = new StringBuilder();
		String res = "";
		// Instanciar oracle manager para conexiones a BD
		OracleManager om = new OracleManager();
		// Preparar el query de la consulta para los registros
		sqlSb.append("SELECT esta.ID_CAT_PK ID_ESTATUS,esta.TXT_NOMBR ESTATUS,count(esta.ID_CAT_PK) NO_EXPEDIENTES "); 
		sqlSb.append("FROM OPT_MX_MAE_CAT_ESTAT_EXP esta, OPT_MX_MAE_EXP es,OPT_MX_MAE_SUBSC subsc,OPT_MX_MAE_NOTI noti "); 
		sqlSb.append("where esta.ID_CAT_PK = (SELECT max(esta.ID_CAT_ESTAT_EXP_FK) "); 
		sqlSb.append("FROM OPT_MX_MAE_EXP exp, OPT_MX_MAE_ESTAT_EXP esta "); 
		sqlSb.append("WHERE exp.ID_EXP_PK = esta.ID_EXP_FK) ");  
		sqlSb.append("and esta.ID_CAT_PK in (3,5,6,7,10) "); 
		sqlSb.append("and subsc.TXT_NOMBR = ? "); 
		sqlSb.append("and subsc.ID_SUBSC_PK = noti.ID_SUBSC_FK ");  
		sqlSb.append("and noti.id_noti_pk = es.ID_NOTI_FK ");
		sqlSb.append("group by esta.ID_CAT_PK,esta.TXT_NOMBR ");
		// Preparar conexion y consulta de registros
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement(sqlSb.toString())){
			// Agregar el susbscriptor como parametro de consulta
			ps.setString(1, nombreSubscriptor);
			// Obtener los registros
			res = obtenerNumeroRegistros(ps);
			
		} catch (SQLException e) {
			LOG.error(e);
		}	
		// Devolver las cifras del control
		return res;
		
	}
	
	/** 
	 * Obtener numero registros.
	 * Metodo para mostrar los errores para las cifras de control
	 * 
	 * @param ps the ps
	 * @return the string
	 */
	private String obtenerNumeroRegistros(PreparedStatement ps) {
		// Cadena que se mostrara en las cifras de control
		String res = "";
		res += "REGISTROS|numRegistros|";
		res += "ERROR_RECEPCION|numErrorRecepcion|";
		res += "ERROR_VALIDACION|numErrorValidacion|";
		res += "ARCHIVO_IN_CM|numArchivoINCM|";
		res += "ERROR_ARCHIVO_IN_CM|numErrorArchivoINCM|";
		res += "PROCESO_CANCELADO|numProcesoCancelado\n";
		// Inicializar el numero de errores
		ErroresCifrasControlBean errores = new ErroresCifrasControlBean();
		errores.setNumRegistros(0);
		errores.setNumErrorRecepcion(0);
		errores.setNumErrorValidacion(0);
		errores.setNumArchivoINCM(0);
		errores.setAux(new ErroresCifrasControlAuxBean());
		errores.getAux().setNumErrorArchivoINCM(0);
		errores.getAux().setNumProcesoCancelado(0);
		
		try (ResultSet rs = ps.executeQuery()){				
			// Obtener el numero de errores en los expedientes
			while(rs.next()) {
				obtenerErrores(rs, errores);
			}
			errores.setNumRegistros(errores.getNumErrorRecepcion() + errores.getNumErrorValidacion() + errores.getNumArchivoINCM() + errores.getAux().getNumErrorArchivoINCM() + errores.getAux().getNumProcesoCancelado());
			// Mostrar las cifras de los errores
			res = res.replaceAll("numRegistros", String.valueOf(errores.getNumRegistros()));
			res = res.replaceAll("numErrorRecepcion", String.valueOf(errores.getNumErrorRecepcion()));
			res = res.replaceAll("numErrorValidacion", String.valueOf(errores.getNumErrorValidacion()));
			res = res.replaceAll("numArchivoINCM", String.valueOf(errores.getNumArchivoINCM()));
			res = res.replaceAll("numErrorArchivoINCM", String.valueOf(errores.getAux().getNumErrorArchivoINCM()));
			res = res.replaceAll("numProcesoCancelado", String.valueOf(errores.getAux().getNumProcesoCancelado()));
			
		} catch (SQLException e) {
			LOG.error(e);
		}
		return res;
	}

	/**
	 * Obtener errores.
	 * Metodo para obtener los errores para las cifras de control
	 *
	 * @param rs the rs
	 * @param errores the errores
	 */
	private void obtenerErrores(ResultSet rs, ErroresCifrasControlBean errores) {
		try {
			if(rs.getInt(ID_ESTATUS) == 3) {
				errores.setNumErrorRecepcion(rs.getInt(NO_EXPEDIENTES));
			}
			if(rs.getInt(ID_ESTATUS) == 5) {					
				errores.setNumErrorValidacion(rs.getInt(NO_EXPEDIENTES));
			}
			
			if(rs.getInt(ID_ESTATUS) == 6) {					
				errores.setNumArchivoINCM(rs.getInt(NO_EXPEDIENTES));
			}
			
			if(rs.getInt(ID_ESTATUS) == 7) {					
				errores.getAux().setNumErrorArchivoINCM(rs.getInt(NO_EXPEDIENTES));
			}
			
			if(rs.getInt(ID_ESTATUS) == 10) {					
				errores.getAux().setNumProcesoCancelado(rs.getInt(NO_EXPEDIENTES));
			}
		} catch (SQLException e) {
			LOG.error(e);
		}
	}

	/**
	 * retrieveIdCatalogoByName.
	 * Metodo para obtener el id de catalogo por nombre
	 *
	 * @param query the query
	 * @param name the name
	 * @return the integer
	 */
	private Integer retrieveIdCatalogoByName(String query, String name) {
		OracleManager oracle = new OracleManager();
		Integer id = 0;
		// Preparar conexion
		try (Connection con = oracle.getConnectionByWallet();
				PreparedStatement ps = con.prepareStatement(query)){
			ps.setString(1, name);
			// Obtener los registros
			try (ResultSet result = ps.executeQuery()){				
				if(result.next()){
					id = result.getInt("ID_CAT_PK");
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		}		
		return id;
	}

}
