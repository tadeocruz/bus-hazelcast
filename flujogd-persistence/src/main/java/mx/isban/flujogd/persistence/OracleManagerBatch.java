package mx.isban.flujogd.persistence;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Provider;
import java.security.Security;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import oracle.jdbc.pool.OracleDataSource;
import oracle.security.pki.OraclePKIProvider;

/**
 * Metodo que realizara la conexion by wallet o jdbc
 * The Class OracleManagerBatch.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase OracleManagerBatch
 */
public class OracleManagerBatch {

	/** The Constant LOG. */
	//Inicializamos un log para guardar la bitacora
	private static final Logger LOG = LogManager.getLogger(OracleManagerBatch.class);

	/** The ip batch. */
	//Crear un string para guardar la ip
	private static String IP_BATCH;
	
	/** The user batch. */
	//Crear un string para guardar el nombre de usuario
	private static String USER_BATCH;
	
	/** The pass batch. */
	//Crear un string para guardar la contrasena
	private static String PASS_BATCH;
	
	/** The port batch. */
	//Crear un string para guardar el puerto
	private static String PORT_BATCH;
	
	/** The instance batch. */
	//Crear un string para guardar la instancia
	private static String INSTANCE_BATCH;
	
	/** The path wallet batch. */
	//Crear un string para guardar la ruta del wallet
	private static String PATH_WALLET_BATCH;
	
	/** The url db batch. */
	//Crear un string para guardar la url de la base de datos
	private static String URL_DB_BATCH;

	static{
		Properties properties = null;
		InputStream is = null;
		try {
			properties = new Properties();
			//Obtener la ruta del flujo
			String pathFlujogd = System.getenv().get("PATH_FLUJOGD");
			is = new FileInputStream(pathFlujogd+"/dbBatch.properties");
			properties.load( is );
			//Configurar las propiedades
			IP_BATCH = properties.getProperty( "ip" );
			USER_BATCH = properties.getProperty( "user" );
			PASS_BATCH = properties.getProperty( "pass" );
			PORT_BATCH = properties.getProperty( "port" );
			INSTANCE_BATCH = properties.getProperty( "instance" );
			PATH_WALLET_BATCH = properties.getProperty( "pathWallet" );
			URL_DB_BATCH = properties.getProperty( "urlDB" );
			//Mostrar en bitacora el tipo de conexion
			if("".equals(PATH_WALLET_BATCH)) {
				LOG.info("LA CONEXION SERA POR DRIVER MANAGER");
			}else{
				LOG.info("LA CONEXION SERA POR WALLET");
			}
		} catch (IOException ex) {
			LOG.error(ex);
		}
	}

	/**
	 * OracleManagerBatch.
	 */
	public OracleManagerBatch() {
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (ClassNotFoundException e) {
			LOG.error(e);
		}
	}

	/**
	 * getConnection
	 * Metodo para obtener la conexion by jdbc.
	 *
	 * @return the connection
	 */
	public Connection getConnection() {
		Connection conn = null;
		try {
			//Configurar la conexion
			conn = DriverManager.getConnection("jdbc:oracle:thin:@"+IP_BATCH+":"+PORT_BATCH+":"+INSTANCE_BATCH,USER_BATCH,PASS_BATCH);
		} catch (SQLException e) {
			LOG.error(e);
		}
		//Regresar la conexion
		return conn;
	}

	/**
	 * getConnectionByWallet
	 * Metodo para obtener la conexion por wallet.
	 *
	 * @return connection Regresa la conexion
	 * @throws SQLException de tipo SQL Exception
	 */
	public Connection getConnectionByWallet() throws SQLException {
		if("".equals(PATH_WALLET_BATCH)) {
			return getConnection();
		}
		//Crear el data source
		OracleDataSource dataSource = null;
		//Crear las propiedades
		Properties props = null;
		Provider p = null;
		dataSource = new OracleDataSource();
		props = new Properties();
		//Configurar las propiedades
		props.put("oracle.net.wallet_location", "(source=(method=file)(method_data=(directory="+PATH_WALLET_BATCH+")))");
		props.put("oracle.net.tns_admin",PATH_WALLET_BATCH);
		//Colocar los datos del data source
		dataSource.setConnectionProperties(props);
		dataSource.setURL(URL_DB_BATCH);
		dataSource.setImplicitCachingEnabled(true);
		dataSource.setFastConnectionFailoverEnabled(true);
		p = new OraclePKIProvider();
		//Insertar el proveedor
		Security.insertProviderAt(p, 3);
		//Regresar la conexion al data source
		return dataSource.getConnection();
	}

}
