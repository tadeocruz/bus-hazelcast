package mx.isban.flujogd.persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mx.isban.flujogd.common.bean.ConexionSFTPBean;
import mx.isban.flujogd.persistence.OracleManager;

/**
 * Contiene metodos de utilerias de generacion de consultas
 * @author tcruz
 *
 */
public class PersistenceManagerExpedienteUtils{
	
	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(PersistenceManagerExpedienteUtils.class);
	
	/** The Constant TXT_VALOR. */
	private static final String TXT_VALOR = "TXT_VALOR";
	
	/**
	 * retrieveIdCatalogoByName.
	 * Metodo para obtener el id del catalogo por nombre
	 *
	 * @param query Parametro para el query del catalogo
	 * @param name Parametro para el nombre del catalogo
	 * @return the integer Regresa el id de tipo Integer
	 */
	protected Integer retrieveIdCatalogoByName(String query, String name) {
		OracleManager omExpediente = new OracleManager();
		Integer id = 0;
		
		LOG.debug("retrieveIdCatalogoByName :: Query : {}, Tabla: ", query, name);
		
		// Preparar la conexion
		try (Connection conn = omExpediente.getConnectionByWallet();
				PreparedStatement prepared = conn.prepareStatement(query)){
			// Agregrar el nombre del catalogo
			prepared.setString(1, name);
			// Ejecutar la consulta del id del catalogo
			try (ResultSet rs = prepared.executeQuery()){
				if(rs.next()){
					id = rs.getInt("ID_CAT_PK");
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		}		
		return id;
	}
	
	/**
	 * getQuery.
	 * Metodo para obtener una consulta simple por nombre
	 *
	 * @param catalogName the catalog name
	 * @return the query
	 */
	protected String getQuery(String catalogName) {
		return "select ID_CAT_PK from "+catalogName+" WHERE TXT_NOMBR = ?";
	}

	/**
	 * Gets the params.
	 * Metodo para obtener los parametros para purgado
	 *
	 * @param sftp objeto para los parametros
	 * @param rs resultado de la consulta de parametros
	 * @return the params parametros de purgado
	 * @throws SQLException the SQL exception
	 */
	protected void getParams(ConexionSFTPBean sftp, ResultSet rs) throws SQLException {
		switch (rs.getString("TXT_NOMBR")) {
		case "sftp_user":
			sftp.setUser(rs.getString(TXT_VALOR));
			break;
		case "sftp_pass":
			sftp.setPass(rs.getString(TXT_VALOR));
			break;
		case "sftp_host":
			sftp.setServer(rs.getString(TXT_VALOR));
			break;
		case "sftp_port":
			sftp.setPuerto(rs.getString(TXT_VALOR));
			break;
		case "sftp_src_path":
			sftp.getRutas().setPathStfpSrc(rs.getString(TXT_VALOR));
			break;
		case "sftp_output_path":
			sftp.getRutas().setPathStfpOut(rs.getString(TXT_VALOR));
			break;
		case "zip_target_path":
			sftp.getRutas().setPathUnzip(rs.getString(TXT_VALOR));
			break;
		case "sftp_path_priv_key":
			sftp.getRutas().setPathPrivKey(rs.getString(TXT_VALOR));
			break;
		case "sftp_passphrase":
			sftp.getRutas().setSftpPassPhrase(rs.getString(TXT_VALOR));
			break;
		default:
			break;
		}
	}

}
