package mx.isban.flujogd.persistence;

import java.util.Map;

import mx.isban.flujogd.common.bean.DocumentoBean;

/**
 * The Interface PersistenceManagerDocumento.
 *
 * @author Alvaro
 */
public interface PersistenceManagerDocumento {
	
	//Insertar un documento
	/**
	 * insertdocumento.
	 *
	 * @param idExpediente Id del expediente
	 * @param tipoDocumento Tipo de documento
	 * @param nombre Nombre del documento
	 * @param checksum Checksum del documento
	 * @param estatus Estatus del documento
	 * @return Estatus de la insercion del documento
	 */
	boolean insertDocumento(String idExpediente, String tipoDocumento, String nombre, String checksum, String estatus);
	
	//Traer documento
	/**
	 * retrieve documento.
	 *
	 * @param referenciaExterna Referencia externa del documento
	 * @param tipoDocumento Tipo de documento
	 * @param estatusDocumento Estatus del documento
	 * @return Documento
	 */
	DocumentoBean retrieveDocumento(String referenciaExterna,String tipoDocumento,String estatusDocumento);
	
	//Actualizar el estatus del documento
	/**
	 * updatestatusdocumento.
	 *
	 * @param idDocumento Id del documento
	 * @param status Estatus del documento
	 * @return Estatus de la actualizacion del documento
	 */
	boolean updateStatusDocumento(String idDocumento, String status);
	
	//Traer los documentos de acuerdo al expediente
	/**
	 * retrievedocumentosbyexpediente.
	 *
	 * @param idExpediente Id del expediente
	 * @param estatusDocumento Estatus del documento
	 * @return Mapa de documentos
	 */
	Map<String,DocumentoBean> retrieveDocumentosByExpediente(String idExpediente,String estatusDocumento);
	
	/**
	 * Retrieve documentos by expediente.
	 *
	 * @param idExpediente Parametro de tipo String
	 * @return the map Regresa el mapa de documentos
	 */
	Map<String,DocumentoBean> retrieveDocumentosByExpediente(String idExpediente);

}
