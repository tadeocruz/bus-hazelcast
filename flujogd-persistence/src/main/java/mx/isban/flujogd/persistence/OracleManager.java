package mx.isban.flujogd.persistence;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Provider;
import java.security.Security;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import oracle.jdbc.pool.OracleDataSource;
import oracle.security.pki.OraclePKIProvider;

/**
 * The Class OracleManager.
 * Clase que realiazara las conexiones by wallet o jdbc
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase OracleManager
 */
public class OracleManager {

	/** Inicializamos un log para guardar la bitacora */
	private static final Logger LOG = LogManager.getLogger(OracleManager.class);

	/** Crear un string para guardar la ip */
	private static String IP;
	
	/** Crear un string para guardar el nombre de usuario */
	private static String USER;
	
	/** Crear un string para guardar la contrasena */
	private static String PASS;
	
	/** Crear un string para guardar el puerto */
	private static String PORT;
	
	/** Crear un string para guardar la instancia */
	private static String INSTANCE;
	
	/** Crear un string para guardar la cartera de rutas */
	private static String PATH_WALLET;
	
	/** Crear un string para guardar la url a la base de datos */
	private static String URL_DB;

	static{
		Properties properties = null;
		properties = new Properties();
		//Obtener la ruta del flujo
		String pathFlujogd = System.getenv().get("PATH_FLUJOGD");
		try (InputStream is = new FileInputStream(pathFlujogd+"/db.properties")){
			//Cargar las propiedades de la base de datos
			properties.load( is );
			//Colocar las propiedades
			IP = properties.getProperty( "ip" );
			USER = properties.getProperty( "user" );
			PASS = properties.getProperty( "pass" );
			PORT = properties.getProperty( "port" );
			INSTANCE = properties.getProperty( "instance" );
			PATH_WALLET = properties.getProperty( "pathWallet" );
			URL_DB = properties.getProperty( "urlDB" );
			//Evaluar la cartera de rutas
			//Mostrar en bitacora el tipo de conexion
			if("".equals(PATH_WALLET)) {
				LOG.info("LA CONEXION SERA POR DRIVER MANAGER");
			}else{
				LOG.info("LA CONEXION SERA POR WALLET");
			}

		} catch (IOException ex) {
			LOG.error(ex);
		}
	}

	/**
	 * getConnection.
	 *
	 * @return the connection
	 */
	public Connection getConnection() {
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (ClassNotFoundException e) {
			LOG.error(e);
		}
		Connection conn = null;
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			LOG.error("Error en la espera de conexion : Cause ", e);
		}
		try {
			//Evaluar la conexion con la base
			conn = DriverManager.getConnection("jdbc:oracle:thin:@"+IP+":"+PORT+":"+INSTANCE,USER,PASS);
		} catch (SQLException e) {
			LOG.error(e);
		}
		return conn;
	}

	/**
	 * getConnectionByWallet.
	 *
	 * @return Conexion con el data source
	 * @throws SQLException Excepcion de SQL
	 */
	public Connection getConnectionByWallet() throws SQLException {
		//Evaluar la ruta
		if("".equals(PATH_WALLET)) {
			return getConnection();
		}
		//Crear el data source
		OracleDataSource dataSource = null;
		Properties props = null;
		Provider p = null;
		dataSource = new OracleDataSource();
		props = new Properties();
		//Colocar las propiedades de oracle
		props.put("oracle.net.wallet_location", "(source=(method=file)(method_data=(directory="+PATH_WALLET+")))");
		props.put("oracle.net.tns_admin",PATH_WALLET);
		//Configurar las propiedades de conexion del data source
		dataSource.setConnectionProperties(props);
		//Colocar la url a la base de datos
		dataSource.setURL(URL_DB);
		dataSource.setImplicitCachingEnabled(true);
		dataSource.setFastConnectionFailoverEnabled(true);
		p = new OraclePKIProvider();
		Security.insertProviderAt(p, 3);
		//Regresar la conexion
		return dataSource.getConnection();
	}

}
