package mx.isban.flujogd.persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mx.isban.flujogd.common.bean.NetConfigBean;
import mx.isban.flujogd.common.bean.ParametroBean;
import mx.isban.flujogd.common.bean.SubscriptorBean;
import mx.isban.flujogd.common.util.NetConfigStatusEnum;
import mx.isban.flujogd.persistence.OracleManager;
import mx.isban.flujogd.persistence.PersistenceManagerSubscriptor;

/**
 * The Class PersistenceManagerSubscriptorImpl.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase PersistenceManagerSubscriptorImpl
 */
public class PersistenceManagerSubscriptorImpl implements PersistenceManagerSubscriptor{
	
	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(PersistenceManagerSubscriptorImpl.class);
	
	/** The Constant SENTENCE_1. */
	private static final String SENTENCE_1 = "from OPT_MX_MAE_RED opr, OPT_MX_MAE_SUBSC ops ";
	
	/** The Constant SENTENCE_2. */
	private static final String SENTENCE_2 = "where opr.ID_SUBSC_FK =  ops.ID_SUBSC_PK ";
	
	/** The Constant SENTENCE_3. */
	private static final String SENTENCE_3 = "and ops.TXT_NOMBR = ? ";
	
	/** The Constant PUERTO_TXT. */
	private static final String PUERTO_TXT = "TXT_PUERT";
	
	/** The Constant ACTIVA_TXT. */
	private static final String ACTIVA_TXT = "FLG_ACTIV";
	
	/** The Constant NOMBR_TXT. */
	private static final String NOMBR_TXT = "TXT_NOMBR";
	
	/** The Constant IP_TXT. */
	private static final String IP_TXT = "TXT_IP";

	/**
	 * retrieveNetConfig.
	 * Metodo para obtener la configuracion de net
	 *
	 * @param subscriptorName Parametro para el nombre de subscriptor de tipo String
	 * @param esActiva Parametro esActiva de tipo String
	 * @return the list Regresa la lista que obtiene la configuracion
	 */
	public List<NetConfigBean> retrieveNetConfig(String subscriptorName,String esActiva) {
		OracleManager om = new OracleManager();
		List<NetConfigBean> list = new ArrayList<NetConfigBean>();
		StringBuilder sb = new StringBuilder();
		// Preparar el query para consulta de configuracion de net
		sb.append("select opr.TXT_IP, opr.TXT_PUERT, opr.FLG_ACTIV ");
		sb.append(SENTENCE_1);
		sb.append(SENTENCE_2);
		sb.append(SENTENCE_3);
		if(!esActiva.equals(NetConfigStatusEnum.NET_CONFIG_TODAS.getName())) {				
			sb.append("and opr.FLG_ACTIV = ?");
		}
		// Preparar la conexion
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement(sb.toString())){
			// Agregar parametros de consulta
			ps.setString(1, subscriptorName);
			if(!esActiva.equals(NetConfigStatusEnum.NET_CONFIG_TODAS.getName())) {				
				ps.setInt(2, new Integer(esActiva));
			}
			// Ejecutar la consulta
			try(ResultSet rs = ps.executeQuery()){				
				while(rs.next()){
					NetConfigBean bean = new NetConfigBean();
					bean.setIp(rs.getString(IP_TXT));
					bean.setPort(rs.getString(PUERTO_TXT));
					bean.setActiva(rs.getString(ACTIVA_TXT));
					list.add(bean);
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		}		
		return list;
	}
	
	/**
	 * retrieveNetConfigByIp.
	 * Metodo para obtener la confuguracion de net por ip
	 *
	 * @param subscriptorName the subscriptor name
	 * @param ip the ip
	 * @return the net config bean
	 */
	public NetConfigBean retrieveNetConfigByIp(String subscriptorName, String ip) {
		OracleManager om = new OracleManager();
		NetConfigBean netConfigBean = null;
		StringBuilder sb = new StringBuilder();
		// Preparar la consulta por ip
		sb.append("select opr.ID_RED_PK, opr.TXT_IP, opr.TXT_PUERT, opr.FLG_ACTIV ");
		sb.append(SENTENCE_1);
		sb.append(SENTENCE_2);
		sb.append(SENTENCE_3);
		sb.append("and opr.TXT_IP = ? ");
		// Preparar la conexion de BD
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement(sb.toString())){
			// Agregar parametros de consulta
			ps.setString(1, subscriptorName);
			ps.setString(2, ip);
			// Ejecutar la consulta
			try(ResultSet rs = ps.executeQuery()){
				if(rs.next()){
					netConfigBean = new NetConfigBean();
					netConfigBean.setId(rs.getString("ID_RED_PK"));
					netConfigBean.setIp(rs.getString(IP_TXT));
					netConfigBean.setPort(rs.getString(PUERTO_TXT));
					netConfigBean.setActiva(rs.getString(ACTIVA_TXT));
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		}
		return netConfigBean;
	}
	
	/**
	 * retrieveParam.
	 * Metodo para obtener los parametros del subscriptor
	 *
	 * @param subscriptorName the subscriptor name
	 * @param nombreParam the nombre param
	 * @return the parametro bean
	 */ 
	public ParametroBean retrieveParam(String subscriptorName, String nombreParam) {
		OracleManager om = new OracleManager();
		ParametroBean parametroBean = null;
		StringBuilder sb = new StringBuilder();
		// Preparar la consulta de parametros
		sb.append("select opp.TXT_NOMBR,opp.TXT_VALOR,opp.FLG_ENCRI from OPT_MX_MAE_SUBSC ops, OPT_MX_MAE_CONF opc, OPT_MX_MAE_PARAM opp ");
		sb.append("where ops.ID_SUBSC_PK = opc.ID_SUBSC_FK ");
		sb.append("and opc.ID_CONF_PK = opp.ID_CONF_FK ");
		sb.append(SENTENCE_3);
		sb.append("and opp.TXT_NOMBR = ? ");
		// Preparar la conexion a BD
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement(sb.toString())){
			// Agregar parametros de consulta
			ps.setString(1, subscriptorName);
			ps.setString(2, nombreParam);
			// Ejecutar la consulta
			try(ResultSet rs = ps.executeQuery()){				
				if(rs.next()){
					parametroBean = new ParametroBean();
					parametroBean.setNombreParametro(rs.getString(NOMBR_TXT));
					parametroBean.setValorParametro(rs.getString("TXT_VALOR"));
					parametroBean.setEstaEncriptado(rs.getString("FLG_ENCRI"));
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		} 		
		return parametroBean;
	}
	
	/**
	 * retrieveAllSubscriptores.
	 * Metodo para obtener todos los subscriptores
	 * 
	 * @return the list Regresa la lista con los datos del subscriptor
	 */
	public List<SubscriptorBean> retrieveAllSubscriptores() {
		OracleManager om = new OracleManager();
		List<SubscriptorBean> lista = new ArrayList<SubscriptorBean>();
		StringBuilder sb = new StringBuilder();
		// Preparar el query para consulta de subscriptores
		sb.append("select ID_SUBSC_PK, TXT_NOMBR, FLG_MALLA_ACTIV from OPT_MX_MAE_SUBSC");
		// Preparar conexion a BD
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// Ejecutar consulta de subscriptores
			try (ResultSet rs = ps.executeQuery()){				
				while(rs.next()){
					SubscriptorBean subscriptorBean = new SubscriptorBean();
					subscriptorBean.setIdSubscriptor(rs.getString("ID_SUBSC_PK"));
					subscriptorBean.setNombreSubscriptor(rs.getString(NOMBR_TXT));
					subscriptorBean.setMallaActiva(String.valueOf(rs.getInt("FLG_MALLA_ACTIV")).toString());
					lista.add(subscriptorBean);
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		}
		return lista;
	}
	
	/**
	 * retrieveNetActiveNow.
	 * Metodo para obtener la configuracion de net
	 *
	 * @param subscriptorName Parametro para guarda el valor de nombre del subscriptor
	 * @return the integer Regresa la configuracion
	 */
	public Integer retrieveNetActiveNow(String subscriptorName) {
		OracleManager omNet = new OracleManager();
		Integer activos = null;
		StringBuilder sbNet = new StringBuilder();
		// Preparar el query para consulta
		sbNet.append("select count(opr.ID_RED_PK) as TOT from OPT_MX_MAE_SUBSC ops, OPT_MX_MAE_RED opr ");
		sbNet.append("where ops.ID_SUBSC_PK = opr.ID_SUBSC_FK ");
		sbNet.append("and opr.FLG_ACTIV = 1 ");
		sbNet.append("and ops.TXT_NOMBR = ?");
		// Preparar conexion a BD
		try (Connection conn = omNet.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement(sbNet.toString())){
			// Agregar parametros de consulta			
			ps.setString(1, subscriptorName);
			// Ejecutar consulta
			try (ResultSet rs = ps.executeQuery()){				
				if(rs.next()){
					activos = rs.getInt("TOT");
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		}		
		return activos;
	}

	/**
	 * retrieveAllAnotherNetConfig.
	 * Metodo para obtener otras configuraciones de net
	 *
	 * @param subscriptorName Parametro de nombre del subscriptor  de tipo String
	 * @param ip  Parametro de ip de tipo String
	 * @return the list Regresa la lista con los valores de net
	 */
	public List<NetConfigBean> retrieveAllAnotherNetConfig(String subscriptorName, String ip) {
		OracleManager omNet = new OracleManager();
		List<NetConfigBean> listaNet = new ArrayList<NetConfigBean>();
		StringBuilder sbNet = new StringBuilder();
		// Preparar el query para consulta de configuraciones net
		sbNet.append("select opr.ID_RED_PK, opr.TXT_IP, opr.TXT_PUERT, opr.FLG_ACTIV ");
		sbNet.append(SENTENCE_1);
		sbNet.append(SENTENCE_2);
		sbNet.append(SENTENCE_3);
		sbNet.append("and opr.TXT_IP != ? ");
		// Preparar conexion a BD
		try (Connection conn = omNet.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement(sbNet.toString())){
			// Agregar parametros de consulta
			ps.setString(1, subscriptorName);
			ps.setString(2, ip);
			// Ejecutar consulta
			try(ResultSet rs = ps.executeQuery()){				
				while(rs.next()){
					NetConfigBean netConfigBean = new NetConfigBean();
					netConfigBean.setId(rs.getString("ID_RED_PK"));
					netConfigBean.setIp(rs.getString(IP_TXT));
					netConfigBean.setPort(rs.getString(PUERTO_TXT));
					netConfigBean.setActiva(rs.getString(ACTIVA_TXT));
					listaNet.add(netConfigBean);
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		}
		return listaNet;
	}
	
	/**
	 * retrieveSubscriptorByIdExpediente.
	 * Metodo para obtener los subscriptores por id de expediente
	 *
	 * @param idExpediente Parametro para el id del expediente
	 * @return the string Regresa el valor del nombre del subscriptor
	 */
	public String retrieveSubscriptorByIdExpediente(String idExpediente) {
		OracleManager om = new OracleManager();
		String nombreSubscriptor = null;
		StringBuilder sb = new StringBuilder();
		// Preparar el query para consulta de subscriptores
		sb.append("select ops.TXT_NOMBR from OPT_MX_MAE_EXP ope, OPT_MX_MAE_NOTI opn, OPT_MX_MAE_SUBSC ops ");
		sb.append("where ope.ID_NOTI_FK = opn.ID_NOTI_PK ");
		sb.append("and opn.ID_SUBSC_FK = ops.ID_SUBSC_PK ");
		sb.append("and ope.ID_EXP_PK =  ?");
		// Preparar conexion a BD
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement(sb.toString())){
			// Agregar parametros de consulta
			ps.setString(1, idExpediente);
			// Ejecutar consulta de subscriptores
			try (ResultSet rs = ps.executeQuery()){				
				if(rs.next()){
					nombreSubscriptor = rs.getString(NOMBR_TXT);
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		}
		return nombreSubscriptor;
	}
	
	/**
	 * updateStausNetConfig.
	 * Metodo para actualizar estatus de configuracion net
	 *
	 * @param idRed the id red
	 * @param status the status
	 * @return true, if successful
	 */
	public boolean updateStausNetConfig(String idRed, String status) {
		boolean ok = false;
		OracleManager om = new OracleManager();
		// Preparar query de actualizacion de configuracion net
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement("update OPT_MX_MAE_RED set FLG_ACTIV = ? where ID_RED_PK = ?")){
			// Agregar parametros de actualizacion
			ps.setInt(1, new Integer(status));
			ps.setInt(2, new Integer(idRed));
			// Validar ejecucion de la actualizacion
			if(ps.executeUpdate() > 0) {				
				ok = true;
			}
		} catch (SQLException e) {
			LOG.error(e);
		}
		return ok;
	}
	
	/**
	 * updateStausNetConfig.
	 * Metodo para actualizar el estatus de la malla de net
	 *
	 * @param idSubscriptor the id subscriptor
	 * @param status the status
	 * @return true, if successful
	 */
	public boolean updateStausMallaSubscriptor(String idSubscriptor, String status) {
		boolean ok = false;
		OracleManager omMalla = new OracleManager();
		// Preparar conexion a BD
		try (Connection conn = omMalla.getConnectionByWallet();
				PreparedStatement psMalla = conn.prepareStatement("update OPT_MX_MAE_SUBSC set FLG_MALLA_ACTIV = ? where ID_SUBSC_PK = ?")){
			// Agregar parametros de actualizacion
			psMalla.setInt(1, new Integer(status));
			psMalla.setInt(2, new Integer(idSubscriptor));
			// Validar la ejecucion de actualizacion de la malla
			if(psMalla.executeUpdate() > 0) {				
				ok = true;
			}
		} catch (SQLException e) {
			LOG.error(e);
		} 
		return ok;
	}

	/**
	 * Update staus red.
	 * Metodo para actualizar el estatus de la red
	 *
	 * @param idSubscriptor the id subscriptor
	 * @param status the status
	 * @return true, if successful
	 */
	@Override
	public boolean updateStausRed(String idSubscriptor, String status) {
		boolean ok = false;
		OracleManager om = new OracleManager();
		// Preparar conexion a BD y query de actualizacion
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement("update OPT_MX_MAE_RED set FLG_ACTIV = ? where ID_SUBSC_FK = ?")){
			// Agregar parametros de actualizacion			
			ps.setInt(1, new Integer(status));
			ps.setInt(2, new Integer(idSubscriptor));
			// Validar ejecucion de la actualizacion
			if(ps.executeUpdate() > 0) {				
				ok = true;
			}
		} catch (SQLException e) {
			LOG.error(e);
		}
		return ok;
	}

}
