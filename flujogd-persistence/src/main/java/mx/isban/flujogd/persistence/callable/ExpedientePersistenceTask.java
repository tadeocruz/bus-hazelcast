package mx.isban.flujogd.persistence.callable;

import java.io.Serializable;
import java.util.concurrent.Callable;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.HazelcastInstanceAware;

import mx.isban.flujogd.persistence.PersistenceManagerExpediente;
import mx.isban.flujogd.persistence.impl.PersistenceManagerExpedienteImpl;

/**
 * Clase para instanciar con persistencia los expedientes
 * The Class ExpedientePersistenceTask.
 *
 * @author Alvaro
 */
public class ExpedientePersistenceTask implements Callable<Object>, Serializable, HazelcastInstanceAware{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5103347697272848803L;

	/** The hazelcast instance. */
	@SuppressWarnings("unused")
	private transient HazelcastInstance hazelcastInstance;
	
	/** The id expediente. */
	private String idExpediente;
	
	/** The status. */
	private String status;
	
	/** The id error. */
	private String idError;
	
	/**
	 * Instantiates a new expediente persistence task.
	 *
	 * @param idExpediente the id expediente
	 * @param status the status
	 * @param idError the id error
	 */
	public ExpedientePersistenceTask(String idExpediente, String status, String idError) {
		this.idExpediente = idExpediente;
		this.status = status;
		this.idError = idError;
	}

	/**
	 * Sets the hazelcast instance.
	 *
	 * @param hazelcastInstance the new hazelcast instance
	 */
	@Override
	public void setHazelcastInstance(HazelcastInstance hazelcastInstance) {
		this.hazelcastInstance = hazelcastInstance;
	}

	/**
	 * Metodo para insertar nuevo expediente
	 * Call.
	 *
	 * @return the object
	 * @throws Exception the exception
	 */
	@Override
	public Object call() throws Exception {
		PersistenceManagerExpediente persistenceManager = new PersistenceManagerExpedienteImpl();
		return persistenceManager.insertEstatusExpediente(this.idExpediente, this.status, this.idError);
	}

}
