package mx.isban.flujogd.persistence;

import java.util.List;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.bean.NotificationBean;

/**
 * The Interface PersistenceManagerNotificacion.
 *
 * @author Alvaro
 */
public interface PersistenceManagerNotificacion {

	//Insertar o actualizar la notificacion
	/**
	 * insertorupdatenotification.
	 *
	 * @param bean Bean de la notificacion
	 * @return Estatus de la insercion o actualizacion
	 */
	boolean insertOrUpdateNotification(NotificationBean bean);
	
	//Traer las notificacions de acuerdo al suscriptor
	/**
	 * retrieveallnotificacionesbysubscriptor.
	 *
	 * @param nombreSubscriptor Nombre del suscriptor
	 * @return Lista de notificaciones
	 */
	List<ExpedienteBean> retrieveAllNotificacionesBySubscriptor(String nombreSubscriptor);
	
}
