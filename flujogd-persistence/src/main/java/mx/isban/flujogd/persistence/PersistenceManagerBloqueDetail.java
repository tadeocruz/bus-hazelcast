package mx.isban.flujogd.persistence;

import java.util.List;

import mx.isban.flujogd.common.bean.BloqueBean;
import mx.isban.flujogd.common.bean.ParametroBean;

/**
 * The Interface PersistenceManagerBloqueDetail.
 *
 * @author Alvaro
 */
public interface PersistenceManagerBloqueDetail {
	
	//Traer la lista de bloques
	/**
	 * retrievebloques.
	 *
	 * @param subscriptorName Nombre del suscriptor
	 * @param idExpediente Id del expediente
	 * @return Lista de bloques
	 */
	List<BloqueBean> retrieveBloques(String subscriptorName,Integer idExpediente);
	
	//Traer la lista de bloques
	/**
	 * retrievebloqus.
	 *
	 * @param subscriptorName Nombre del suscriptor
	 * @return Lista de bloques
	 */
	List<BloqueBean> retrieveBloques(String subscriptorName);
	
	//Traer el bloque de acuerdo a su nombre
	/**
	 * retrievebloquebyname.
	 *
	 * @param nombreBloque Nombre del bloque
	 * @return Bloque
	 */
	BloqueBean retrieveBloqueByName(String nombreBloque);

	//Traer la lista de parametros del bloque
	/**
	 * retrieveparametrobloque.
	 *
	 * @param idExpediente Id del expediente
	 * @param nombreBloque Nombre del bloque
	 * @param parametro Parametro
	 * @return Lista de parametros
	 */
	List<ParametroBean> retrieveParametroBloque(String idExpediente,String nombreBloque,String parametro);

}
