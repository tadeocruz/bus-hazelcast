package mx.isban.flujogd.persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mx.isban.flujogd.common.bean.BloqueBean;
import mx.isban.flujogd.common.bean.BloqueBean2;
import mx.isban.flujogd.common.bean.ReglaBean;
import mx.isban.flujogd.common.bean.ReglaBeanDetalle;
import mx.isban.flujogd.common.generic.CommonsUtils;
import mx.isban.flujogd.persistence.OracleManager;

/**
 * The Class PersistenceManagerImplUtils.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase PersistenceManagerImpl
 */
public class PersistenceManagerImplUtils{
	
	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(PersistenceManagerImplUtils.class);

	/**
	 * retrieveFlujo.
	 * Metodo para obtener el flujo que debe de seguir el proceso
	 *
	 * @param idFlujo Parametro para el id del flujo
	 * @param idExpediente Parametro para el id del expediente
	 * @return the string Regresa el idExpediente y idRegla
	 */
	public String retrieveFlujo(Integer idFlujo,Integer idExpediente){
		OracleManager om = new OracleManager();
		Integer idRegla = null;
		// Preparar conexion y consulta
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement("select f.ID_REGLA_INIT_FK from OPT_MX_MAE_FLUJO f where f.ID_FLUJO_PK = ?")){
			// Agregar el parametro de id de flujo para la consulta
			ps.setInt(1, idFlujo);
			// Ejecutar la consulta dl flujo
			try(ResultSet rs = ps.executeQuery()){				
				if(rs.next()){
					idRegla = rs.getInt("ID_REGLA_INIT_FK");
				}
			}
			
		} catch (SQLException e) {
			LOG.error(e);
		}		
		return validateReglaBean(idExpediente,idRegla);
	}
	
	/**
	 * validateReglaBean.
	 * Metodo para validar las reglas del flujo
	 * 
	 * @param idExpediente Parametro para el idExpediente de tipo int
	 * @param idRegla Parametro para el idRegla de tipo int
	 * @return si es true regresa el bloque
	 */
	private String validateReglaBean(Integer idExpediente,Integer idRegla) {
		StringBuilder flujo = new StringBuilder("");
		ReglaBean reglaBean = null;
		do{
			reglaBean = retrieveReglaAndStatus(idRegla, idExpediente);
			idRegla = reglaBean.getIdSiguienteRegla();
			//Se verifica que sea un bloque y que aun no haya sido procesado
			//Si ya fue procesado se omite
			//Si aun no ha sido procesado y tiene menos de 3 intentos se agrega al flujo
			if(reglaBean.getEsFlujo()==0) {
				if(reglaBean.getDetalle().getEstatus()!=1) {
						flujo.append(reglaBean.getBloqueBean().getNombre() + ",");
				}
			}else{
				//Es un flujo
				flujo.append(retrieveFlujo(reglaBean.getId(),idExpediente) + ",");
			}
		}while(idRegla != 0);
		return flujo.toString();
	}
	
	/**
	 * retrieveReglaAndStatus
	 * En el query el primer case determina en que estatus se encuentra el bloque
	 * El el query el segundo case determina el numero de intentos para ese bloque .
	 *
	 * @param idRegla the id regla
	 * @param idExpediente the id expediente
	 * @return the regla bean
	 */
	private ReglaBean retrieveReglaAndStatus(Integer idRegla,Integer idExpediente){
		CommonsUtils utils = new CommonsUtils();
		OracleManager om = new OracleManager();
		StringBuilder sb =  new StringBuilder();
		ReglaBean reglaBean = new ReglaBean();
		ReglaBeanDetalle detalle = new ReglaBeanDetalle();
		reglaBean.setDetalle(detalle);
		// Preparar el select a ajecutar
		sb.append("select ");
		sb.append("case ");
		sb.append("when regla.FLG_ES_FLUJO = 0 then ( ");
		sb.append("select min(ID_CAT_ESTAT_BLOQ_FK)  from OPT_MX_MAE_ESTAT_BLOQ  where ID_EXP_FK = ? and ID_CAT_BLOQ_FK = regla.ID_FLUJO_BLOQUE ");
		sb.append(")else 0 ");
		sb.append("end as ESTATUS, ");
		sb.append("regla.FLG_ES_FLUJO, regla.ID_FLUJO_BLOQUE, regla.ID_REGLA_SIG_FK ");
		sb.append("from OPT_MX_MAE_REGLA regla ");
		sb.append("where regla.ID_REGLA_PK = ?");
		// Preparar la conexion de BD
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement(sb.toString())){
			// Agregar los parametros para la consulta de las reglas
			ps.setInt(1, idExpediente);
			ps.setInt(2, idRegla);
			// ejecutar consulta de reglas
			try( ResultSet rs = ps.executeQuery()){				
				if(rs.next()){
					String estatus = rs.getString("ESTATUS");
					estatus = utils.validarNullTo0(estatus);
					reglaBean.getDetalle().setEstatus(new Integer(estatus));
					reglaBean.setEsFlujo(rs.getInt("FLG_ES_FLUJO"));
					reglaBean.setId(rs.getInt("ID_FLUJO_BLOQUE"));
					reglaBean.setIdSiguienteRegla(rs.getInt("ID_REGLA_SIG_FK"));
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		}
		if(reglaBean.getEsFlujo()==0) {
			reglaBean.setBloqueBean(retrieveQueueById(reglaBean.getId()));
		}
		return reglaBean;
	}
	
	/**
	 * retrieveQueueById.
	 * Metodo para obtener la queue por id
	 *
	 * @param idBloque the id bloque
	 * @return the bloque bean
	 */
	private BloqueBean retrieveQueueById(Integer idBloque){
		OracleManager om = new OracleManager();
		BloqueBean bloque = new BloqueBean();
		// Preparar la conexion a BD
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement("select ID_CAT_PK,TXT_NOMBR,TXT_VERS from OPT_MX_MAE_CAT_BLOQ where ID_CAT_PK = ?")){
			// Agregar id del bloque para obtener la queue
			ps.setInt(1, idBloque);
			// Ejecutar la consulta de las queue
			try(ResultSet rs = ps.executeQuery()){
				if(rs.next()){
					bloque.setId(rs.getString("ID_CAT_PK"));
					bloque.setNombre(rs.getString("TXT_NOMBR"));
					bloque.setVersion(rs.getString("TXT_VERS"));
					BloqueBean2 bean2 = new BloqueBean2();
					bloque.setBean2(bean2);
					bean2.setActivo("0");
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		} 		
		return bloque;
	}

}
