package mx.isban.flujogd.persistence;

import java.util.List;

import mx.isban.flujogd.common.bean.LoteBean;
import mx.isban.flujogd.common.bean.ParametroBean;

/**
 * The Interface PersistenceManagerBatch.
 *
 * @author Alvaro
 */
public interface PersistenceManagerBatch {
	
	//Traer la lista de lotes de acuerdo al estatus
	/**
	 * retrievealllotesbystatus.
	 *
	 * @param idStatus Id del estatus
	 * @return Lista de los lotes
	 */
	List<LoteBean> retrieveAllLotesByStatus(String idStatus);
	
	//Traer los parametros del lote 
	/**
	 * retrieveparam.
	 *
	 * @param nombreParam Nombre de los parametros
	 * @return Parametro del lote
	 */
	ParametroBean retrieveParam(String nombreParam);
	
	//Traer los parametros del lote de acuerdo al la configuracion
	/**
	 * retrieveparambyconf.
	 *
	 * @param conf Configuracion
	 * @return Lista de parametros del lote
	 */
	List<ParametroBean> retrieveParamByConf(String conf);
	
	/**
	 * Update status lote.
	 *
	 * @param idLote the id lote
	 * @param idStatus the id status
	 * @return true, if successful
	 */
	boolean updateStatusLote(String idLote, String idStatus);
	
	/**
	 * Insert detalle lote.
	 *
	 * @param idLoteFK the id lote FK
	 * @param idLoteDetCatFk the id lote det cat fk
	 * @param buc the buc
	 * @param path the path
	 * @return true, if successful
	 */
	boolean insertDetalleLote(String idLoteFK,String idLoteDetCatFk,String buc,String path);
	
	/**
	 * Update detalle lote.
	 *
	 * @param idLoteFK the id lote FK
	 * @param idLoteDetCatFk the id lote det cat fk
	 * @return true, if successful
	 */
	boolean updateDetalleLote(String idLoteFK,String idLoteDetCatFk);



}
