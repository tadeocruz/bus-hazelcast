package mx.isban.flujogd.persistence;

import java.util.List;

import mx.isban.flujogd.common.bean.NetConfigBean;
import mx.isban.flujogd.common.bean.ParametroBean;
import mx.isban.flujogd.common.bean.SubscriptorBean;

/**
 * The Interface PersistenceManagerSubscriptor.
 *
 * @author Alvaro
 */
public interface PersistenceManagerSubscriptor {
	
	//Traer la configuracion de red
	/**
	 * retrievenetconfig.
	 *
	 * @param subscriptorName Nombre del suscriptor
	 * @param esActiva Tipo de red
	 * @return Lista de configuraciones
	 */
	List<NetConfigBean> retrieveNetConfig(String subscriptorName,String esActiva);

	//Traer la configuracion de red de acuerdo a la ip
	/**
	 * retrievenetconfigip.
	 *
	 * @param subscriptorName Nombre del suscriptor
	 * @param ip Direccion ip
	 * @return Configuracion de red
	 */
	NetConfigBean retrieveNetConfigByIp(String subscriptorName,String ip);

	//Traer el parametro
	/**
	 * retrieveparam.
	 *
	 * @param subscriptorName Nombre del suscriptor
	 * @param nombreParam Nombre del parametro
	 * @return Parametro
	 */
	ParametroBean retrieveParam(String subscriptorName,String nombreParam);

	//Traer todos los suscriptores
	/**
	 * retrieveallsubscriptores.
	 *
	 * @return Lista de suscriptores
	 */
	List<SubscriptorBean> retrieveAllSubscriptores();

	//Traer la red activa
	/**
	 * retrievenetactivenow.
	 *
	 * @param subscriptorName Nombre del suscriptor
	 * @return Numero de la red activa
	 */
	Integer retrieveNetActiveNow(String subscriptorName);

	//Traer todas la configuracions de otras redes
	/**
	 * retrieveallanothernetconfig.
	 *
	 * @param subscriptorName Nombre del suscriptor
	 * @param ip Direccion ip
	 * @return Lista de configuraciones
	 */
	List<NetConfigBean> retrieveAllAnotherNetConfig(String subscriptorName,String ip);
	
	//Traer el suscriptor de acuerdo al id del expediente
	/**
	 * retrievesubscriptorbyidexpediente.
	 *
	 * @param idExpediente Id del expediente
	 * @return Nombre del suscriptor
	 */
	String retrieveSubscriptorByIdExpediente(String idExpediente);
	
	//Actualizar el estatus de la configuracion de red
	/**
	 * updatestatusnetconfig.
	 *
	 * @param idRed Id de la red
	 * @param status Estatus de la red
	 * @return Estatus de la actualizacion
	 */
	boolean updateStausNetConfig(String idRed, String status);
	
	//Actualizar el estatus de la malla del suscriptor
	/**
	 * updatestatusmallasubscriptor.
	 *
	 * @param idSubscriptor Id del suscriptor
	 * @param status Estatus del suscriptor
	 * @return Estatus de la actualizacion
	 */
	boolean updateStausMallaSubscriptor(String idSubscriptor, String status);
	
	//Actualizar el estatus de la red
	/**
	 * updatestatusred.
	 *
	 * @param idSubscriptor Id del suscriptor
	 * @param status Estatus del suscriptor
	 * @return Estatus de la actualizacion
	 */
	boolean updateStausRed(String idSubscriptor, String status);

}
