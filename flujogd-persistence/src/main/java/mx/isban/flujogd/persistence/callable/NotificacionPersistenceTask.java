package mx.isban.flujogd.persistence.callable;

import java.io.Serializable;
import java.util.concurrent.Callable;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.HazelcastInstanceAware;

import mx.isban.flujogd.common.bean.NotificationBean;
import mx.isban.flujogd.persistence.PersistenceManagerNotificacion;
import mx.isban.flujogd.persistence.impl.PersistenceManagerNotificacionImpl;

/**
 * Clase para instanciar la notificacion con la persistencia
 * The Class NotificacionPersistenceTask.
 *
 * @author Alvaro
 */
public class NotificacionPersistenceTask implements Callable<Object>, Serializable, HazelcastInstanceAware{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5103347697272848803L;

	/** The hazelcast instance. */
	@SuppressWarnings("unused")
	private transient HazelcastInstance hazelcastInstance;
	
	/** The bean. */
	private NotificationBean bean;
	
	/**
	 * Instantiates a new notificacion persistence task.
	 *
	 * @param bean the bean
	 */
	public NotificacionPersistenceTask(NotificationBean bean) {
		this.bean = bean;
	}

	/**
	 * Sets the hazelcast instance.
	 *
	 * @param hazelcastInstance the new hazelcast instance
	 */
	@Override
	public void setHazelcastInstance(HazelcastInstance hazelcastInstance) {
		this.hazelcastInstance = hazelcastInstance;
	}

	/**
	 * Metodo para insertar o actualizar las notificaciones
	 * Call.
	 *
	 * @return the object
	 * @throws Exception the exception
	 */
	@Override
	public Object call() throws Exception {
		PersistenceManagerNotificacion persistenceManager = new PersistenceManagerNotificacionImpl();
		return persistenceManager.insertOrUpdateNotification(this.bean);
	}

}
