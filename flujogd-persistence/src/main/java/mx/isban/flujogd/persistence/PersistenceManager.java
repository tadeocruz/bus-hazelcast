package mx.isban.flujogd.persistence;

import java.util.List;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.bean.ParametroBean;

/**
 * Interfaz de persistencia sobre los expedientes 
 * @author Alvaro
 *
 */
public interface PersistenceManager {

	/**
	 * retrieveallexpedientes
	 * @return	Lista de los expedientes
	 */
	List<ExpedienteBean> retrieveAllExpedientes();

	/**
	 * retrieveallexpedientesbysubscriptor
	 * @param subscriptorName	Nombre del suscriptor
	 * @return	Lista de los expedientes
	 */
	List<ExpedienteBean> retrieveAllExpedientesBySubscriptor(String subscriptorName);
	
	/**
	 * retrieveparametrosbloque
	 * @param idExpediente	Id del expediente
	 * @param nombreBloque	Nombre del bloque
	 * @return	Lista de los parametros
	 */
	List<ParametroBean> retrieveParametrosBloque(String idExpediente,String nombreBloque);
	
	/**
	 * retrieveparambyconf
	 * @param conf	Configuracion
	 * @return	Lista de los parametros
	 */
	List<ParametroBean> retrieveParamByConf(String conf);

	/**
	 * Llena el flujo del expediente
	 * @param exp expediente a procesar
	 */
	void llenarFlujo(ExpedienteBean exp);

}
