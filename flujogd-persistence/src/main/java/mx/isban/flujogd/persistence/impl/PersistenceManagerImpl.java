package mx.isban.flujogd.persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.bean.ExpedienteBeanDetalle;
import mx.isban.flujogd.common.bean.ExpedienteBeanDetalle2;
import mx.isban.flujogd.common.bean.ExpedienteBeanDetalle3;
import mx.isban.flujogd.common.bean.FlujoBean;
import mx.isban.flujogd.common.bean.ParametroBean;
import mx.isban.flujogd.persistence.OracleManager;
import mx.isban.flujogd.persistence.PersistenceManager;

/**
 * The Class PersistenceManagerImpl.
 * Clase que nos permitira obteber los expedientes y sus flujos que se deben de ejecutar
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase PersistenceManagerImpl
 */
public class PersistenceManagerImpl implements PersistenceManager{
	
	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(PersistenceManagerImpl.class);
	
	/** The Constant SENTENCE_1. */
	private static final String SENTENCE_1 = "where exp.ID_NOTI_FK = noti.ID_NOTI_PK ";
	
	/** The Constant SENTENCE_2. */
	private static final String SENTENCE_2 = "and rel.ID_SUBSC_FK = subsc.ID_SUBSC_PK ";
	
	/** Inicializamos un string con el atributo TXT_NOMBR */
	private static final String TXT_NOMBR = "TXT_NOMBR"; 
	
	/**
	 * retrieveAllExpedientes.
	 * Metodo para obtener todos los expedientes
	 *
	 * @return the list
	 */
	public List<ExpedienteBean> retrieveAllExpedientes() {
		List<ExpedienteBean> expedientesAll = new ArrayList<ExpedienteBean>();
		OracleManager om = new OracleManager();
		StringBuilder queryAll = new StringBuilder();
		// Preparar el query para obtener todos los expedientes
		queryAll.append("select exp.ID_EXP_PK, exp.TXT_NOM, exp.TXT_CHSUM, rel.ID_FLUJO_FK, exp.TXT_REF_EXT, exp.TXT_REQID ");
		queryAll.append("from OPT_MX_MAE_EXP exp, OPT_MX_MAE_NOTI noti, OPT_MX_MAE_SUBSC subsc, OPT_MX_REL_SUBSC_FLUJO rel ");
		// Agregar clausula de consulta
		queryAll.append(SENTENCE_1);
		queryAll.append("and noti.ID_SUBSC_FK = subsc.ID_SUBSC_PK "); 
		queryAll.append("and (select max(ID_CAT_ESTAT_EXP_FK) from OPT_MX_MAE_ESTAT_EXP where ID_EXP_FK = exp.ID_EXP_PK group by ID_EXP_FK) in (1,2,3,4,5,7) ");
		// Agregar clausula de consulta
		queryAll.append(SENTENCE_2);
		queryAll.append("and rel.FLG_ACTIV = 1");
		// Preparar conexion a BD y ejecutar la consutla de los expedientes
		try (Connection conn = om.getConnectionByWallet();
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(queryAll.toString())){
			// Obteber los expedientes obtendidos
			while(rs.next()) {
				// setear los detalles del expediente
				ExpedienteBean bean = new ExpedienteBean();
				bean.setIdExpediente(rs.getString("ID_EXP_PK"));
				bean.setNombreExpediente(rs.getString("TXT_NOM"));
				ExpedienteBeanDetalle detalle = new ExpedienteBeanDetalle();
				ExpedienteBeanDetalle2 detalle2 = new ExpedienteBeanDetalle2();
				detalle.setDetalle2(detalle2);
				bean.setDetalle(detalle);
				bean.getDetalle().getDetalle2().setChecksum(rs.getString("TXT_CHSUM"));
				bean.getDetalle().getDetalle2().setReferenciaExterna(rs.getString("TXT_REF_EXT"));
				bean.getDetalle().getDetalle2().setRequestId(rs.getString("TXT_REQID"));
				bean.getDetalle().setDocumentos(new ArrayList<>());
				bean.getDetalle().setIdFlujo(rs.getInt("ID_FLUJO_FK"));
				// Agregar el expediente a la lista
				expedientesAll.add(bean);
			}
		} catch (SQLException e) {
			LOG.error(e);
		}
		
		// Recorrer los expedientes para obtener el flujo que deben de seguir
		for(ExpedienteBean exp: expedientesAll){
			llenarFlujo(exp);
		}
		
		return expedientesAll;
	}
	
	/**
	 * retrieveAllExpedientesBySubscriptor.
	 * Metodo para obtener todos los expedientes por subscriptor
	 * 
	 * @param subscriptorName the subscriptor name
	 * @return the list
	 */
	public List<ExpedienteBean> retrieveAllExpedientesBySubscriptor(String subscriptorName) {
		List<ExpedienteBean> expedientes = new ArrayList<ExpedienteBean>();
		OracleManager om = new OracleManager();
		StringBuilder sb = new StringBuilder();
		// Preparar el query para la consulta de expedientes
		sb.append("select exp.ID_EXP_PK, exp.TXT_NOM, exp.TXT_CHSUM, rel.ID_FLUJO_FK, exp.TXT_REF_EXT, exp.TXT_REQID, tipoExp.TXT_NOMBR, exp.TXT_IP ");
		sb.append("from OPT_MX_MAE_EXP exp, OPT_MX_MAE_NOTI noti, OPT_MX_MAE_SUBSC subsc, OPT_MX_REL_SUBSC_FLUJO rel, OPT_MX_MAE_CAT_TIPO_PROCE_EXP tipoExp ");
		// Agregar clausula de consulta
		sb.append(SENTENCE_1);
		sb.append("and noti.ID_SUBSC_FK = subsc.ID_SUBSC_PK ");
		sb.append("and exp.ID_CAT_TIPO_PROCE_FK = tipoExp.ID_CAT_PK "); 
		sb.append("and (select max(ID_CAT_ESTAT_EXP_FK) from OPT_MX_MAE_ESTAT_EXP where ID_EXP_FK = exp.ID_EXP_PK group by ID_EXP_FK) in (1,2,3,4,5,7) ");
		// Agregar clausula de consulta
		sb.append(SENTENCE_2);
		sb.append("and rel.FLG_ACTIV = 1 ");
		sb.append("and subsc.TXT_NOMBR = ?");
		// Preparar la conexion a BD
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement(sb.toString())){
			// Agregar parametros para la consulta por subscriptor
			ps.setString(1, subscriptorName);
			// Ejecutar la consulta
			try(ResultSet rs = ps.executeQuery()){		
				// Recorrer los expedientes obtenidos
				while(rs.next()) {
					ExpedienteBean bean = new ExpedienteBean();
					bean.setIdExpediente(rs.getString("ID_EXP_PK"));
					bean.setNombreExpediente(rs.getString("TXT_NOM"));
					ExpedienteBeanDetalle detalle = new ExpedienteBeanDetalle();
					ExpedienteBeanDetalle2 detalle2 = new ExpedienteBeanDetalle2();
					ExpedienteBeanDetalle3 detalle3 = new ExpedienteBeanDetalle3();
					detalle3.setTipoProceso(rs.getString(TXT_NOMBR));
					detalle3.setServidorNacimiento(rs.getString("TXT_IP"));
					detalle3.setOrigen(subscriptorName);
					detalle2.setDetalle3(detalle3);
					detalle.setDetalle2(detalle2);
					bean.setDetalle(detalle);
					bean.getDetalle().getDetalle2().setChecksum(rs.getString("TXT_CHSUM"));
					bean.getDetalle().getDetalle2().setReferenciaExterna(rs.getString("TXT_REF_EXT"));
					bean.getDetalle().getDetalle2().setRequestId(rs.getString("TXT_REQID"));
					bean.getDetalle().setDocumentos(new ArrayList<>());
					bean.getDetalle().setIdFlujo(rs.getInt("ID_FLUJO_FK"));
					LOG.info( "PERSISTENCIA - SE AGREGA EXPEDIENTE - "+bean.getIdExpediente() + " - " + bean.getNombreExpediente() + " - " + bean.getDetalle().getDetalle2().getReferenciaExterna() + " - tipo expediente - "  + bean.getDetalle().getDetalle2().getDetalle3().getTipoProceso() );
					// Se agrega el expediente a la lista que se procesara
					expedientes.add(bean);
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		}
		
		return expedientes;
	}


	/**
	 * Llena el flujo del expediente
	 * 
	 * @param exp expediente a procesar
	 */
	public void llenarFlujo(ExpedienteBean exp) {
		Map<Integer, FlujoBean> mapa = new TreeMap<Integer, FlujoBean>();
		PersistenceManagerImplUtils utils = new PersistenceManagerImplUtils();
		// Obteber el flujo que debe de ejecutarse para el exoediente
		String flujo = utils.retrieveFlujo(exp.getDetalle().getIdFlujo(),new Integer(exp.getIdExpediente()));
		String[] flujosStr = flujo.split(",");
		for(int i = 1; i <= flujosStr.length; i++) {
			FlujoBean flujoBean = new FlujoBean(flujosStr[i-1]);
			//Determinar si el flujo ya ha sido ejecutado, si ya fue ejecutado con exito ya no se hace nada
			//Si ya fue ejecutado y fallo, se determina cuantas veces se intento realizar el proceso
			//Si han sido menos de 3, se vuelve a intentar, de lo contrario ya no se intenta
			
			mapa.put(i,flujoBean);
		}
		//establece los flujos
		exp.getDetalle().setFlujos(mapa);
	}

	/**
	 * retrieveParametrosBloque.
	 * Metodo para obtener los parametros del bloque
	 * 
	 * @param idExpediente the id expediente
	 * @param nombreBloque the nombre bloque
	 * @return the list
	 */
	public List<ParametroBean> retrieveParametrosBloque(String idExpediente, String nombreBloque) {
		OracleManager om = new OracleManager();
		StringBuilder sb = new StringBuilder();
		List<ParametroBean> lista = new ArrayList<ParametroBean>();
		// Preparar la consulta de parametros de los bloques
		sb.append("select param.TXT_NOMBR, param.TXT_VALOR ");
		sb.append("from OPT_MX_MAE_EXP exp, OPT_MX_MAE_NOTI noti, OPT_MX_MAE_SUBSC sub, OPT_MX_MAE_CONF conf, OPT_MX_MAE_CAT_BLOQ bloq, OPT_MX_MAE_PARAM param ");
		sb.append(SENTENCE_1);
		sb.append("and noti.ID_SUBSC_FK = sub.ID_SUBSC_PK ");
		sb.append("and sub.ID_SUBSC_PK = conf.ID_SUBSC_FK ");
		sb.append("and conf.ID_CAT_BLOQ_FK = bloq.ID_CAT_PK ");
		sb.append("and conf.ID_CONF_PK = param.ID_CONF_FK ");
		sb.append("and exp.ID_EXP_PK = ? ");
		sb.append("and bloq.TXT_NOMBR = ?");
		// Preparar la conexion
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// Agregar los parametros de consulta
			ps.setLong(1, new Long(idExpediente));
			ps.setString(2, nombreBloque);
			// Ejecutar la consulta de parametros
			try (ResultSet rs = ps.executeQuery()){		
				// Recorrer los parametros del blqoue
				while(rs.next()){
					ParametroBean bean = new ParametroBean();
					bean.setNombreParametro(rs.getString(TXT_NOMBR));
					bean.setValorParametro(rs.getString("TXT_VALOR"));
					lista.add(bean);
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		} 		
		return lista;
	}
	
	/**
	 * retrieveParamByConf.
	 * Metodo para obtener los parametros por configuracion
	 *
	 * @param conf Parametro para obtener los parametros por configuracion
	 * @return the list Regresa la lista con los valores del parametro
	 */
	public List<ParametroBean> retrieveParamByConf(String conf) {
		List<ParametroBean> lista = new ArrayList<ParametroBean>();
		OracleManager om = new OracleManager();
		ParametroBean parametroBean = null;
		StringBuilder sb = new StringBuilder();
		// Preparar la consulta de parametros por configuraciopn
		sb.append("select p.TXT_NOMBR,p.TXT_VALOR,p.FLG_ENCRI from OPT_MX_MAE_PARAM p, OPT_MX_MAE_CONF c ");
		sb.append("where c.ID_CONF_PK = p.ID_CONF_FK and c.TXT_NOMBR = ?");
		// Preparar la conexion
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement(sb.toString())){
			// Agregar el parametro de consulta
			ps.setString(1, conf);
			// Ejecutar la consulta de parametros
			try(ResultSet rs = ps.executeQuery()){	
				// Recorrer los parametros del bloque
				while(rs.next()){
					parametroBean = new ParametroBean();
					parametroBean.setNombreParametro(rs.getString(TXT_NOMBR));
					parametroBean.setValorParametro(rs.getString("TXT_VALOR"));
					parametroBean.setEstaEncriptado(rs.getString("FLG_ENCRI"));
					// Se agrega el parametro a la lista
					lista.add(parametroBean);
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		} 		
		return lista;
	}

}
