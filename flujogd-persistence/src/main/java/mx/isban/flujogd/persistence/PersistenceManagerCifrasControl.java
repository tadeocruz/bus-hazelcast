package mx.isban.flujogd.persistence;

import java.util.List;

import mx.isban.flujogd.common.bean.CifrasControlBean;

/**
 * Exponer los metodos para cifras de control
 * The Interface PersistenceManagerCifrasControl.
 *
 * @author Alvaro
 */
public interface PersistenceManagerCifrasControl {

	//Notificar las cifras de control
	/**
	 * notificarcifrasctrl.
	 *
	 * @param nombreSubscriptor Nombre del suscriptor
	 * @return Cifras de control
	 */
	CifrasControlBean notificarCifrasCtrl(String nombreSubscriptor);
	
	//Actualizar los expedientes
	/**
	 * updateexpediente.
	 *
	 * @param idsExpedientes Lista de ids de los expedientes
	 * @return Estatus de la actualizacion
	 */
	boolean updateExpedientes(List<Integer> idsExpedientes);
	
}
