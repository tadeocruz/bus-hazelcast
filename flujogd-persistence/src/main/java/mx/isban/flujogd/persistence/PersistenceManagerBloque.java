package mx.isban.flujogd.persistence;

/**
 * Interfaz para trabajar con el bloque del flujo
 * 
 * @author Alvaro
 *
 */
public interface PersistenceManagerBloque {
	
	/**
	 * updatestatusbloqueexp.
	 *
	 * @param idExpediente Id del expediente
	 * @param status Estatus del expediente
	 * @param nombreBloque Nombre del bloque
	 * @param idError Id de error
	 * @return Estatus del bloque
	 */
	boolean updateStatusBloqueExp(String idExpediente, String status, String nombreBloque, String idError);

}
