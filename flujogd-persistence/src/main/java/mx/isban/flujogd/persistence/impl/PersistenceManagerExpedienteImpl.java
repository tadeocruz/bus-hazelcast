package mx.isban.flujogd.persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mx.isban.flujogd.common.bean.ConexionSFTPBean;
import mx.isban.flujogd.common.bean.ExpedientePurgaBean;
import mx.isban.flujogd.common.bean.RutaServerBean;
import mx.isban.flujogd.common.util.CatalogoEnum;
import mx.isban.flujogd.persistence.OracleManager;
import mx.isban.flujogd.persistence.PersistenceManagerExpediente;

/**
 * The Class PersistenceManagerExpedienteImpl.
 * Clase para la persistencia de los expedientes
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase PersistenceManagerExpedienteImpl
 */
public class PersistenceManagerExpedienteImpl implements PersistenceManagerExpediente{
	
	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(PersistenceManagerExpedienteImpl.class);
	
	/** The Constant TXT_VALOR. */
	private static final String TXT_VALOR = "TXT_VALOR";

	/** The Constant TXT_NOM. */
	private static final String TXT_NOM = "TXT_NOM";

	/**
	 * updateStatusExpediente.
	 * Metodo para insertar el estatus del expediente
	 *
	 * @param idExpediente Parametro para el id del expediente
	 * @param status Parametro para el estatus del expediente
	 * @param errorName Parametro errorName en caso de que el nombre sea incorrecto
	 * @return true si inserta el estatus exitoso
	 */
	public boolean insertEstatusExpediente(String idExpediente, String status, String errorName) {
		boolean ok = false;
		OracleManager om = new OracleManager();
		// Preparar la conexion a BD
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement("insert into OPT_MX_MAE_ESTAT_EXP(ID_ESTAT_EXP_PK,ID_EXP_FK,ID_CAT_ESTAT_EXP_FK,ID_CAT_ERROR_FK,FCH_CREA)VALUES(S_OPT_MX_MAE_ESTAT_EXP.NEXTVAL,?,?,?,sysdate)")){
			// Agregar los parametros para insertar el estatus del expediente 
			ps.setInt(1, new Integer(idExpediente));
			ps.setInt(2, retrieveIdCatalogoByName(getQuery(CatalogoEnum.CAT_ESTATUS_EXPEDIENTE.getName()),status));
			int errorId = retrieveIdCatalogoByName(getQuery(CatalogoEnum.CAT_ERROR.getName()),errorName);
			if(errorId==0) {
				ps.setString(3,null);
			}else {
				ps.setInt(3,errorId);
			}
			
			if(ps.executeUpdate() > 0) {
				ok = true;
			}
		} catch (SQLException e) {
			LOG.error(e);
		}
		return ok;
	}
	
	/**
	 * retrieveIdCatalogoByName.
	 * Metodo para obtener el id del catalogo por nombre
	 *
	 * @param query Parametro para el query del catalogo
	 * @param name Parametro para el nombre del catalogo
	 * @return the integer Regresa el id de tipo Integer
	 */
	public Integer retrieveIdCatalogoByName(String query, String name) {
		OracleManager omExpediente = new OracleManager();
		Integer id = 0;
		// Preparar la conexion
		try (Connection conn = omExpediente.getConnectionByWallet();
				PreparedStatement prepared = conn.prepareStatement(query)){
			// Agregrar el nombre del catalogo
			prepared.setString(1, name);
			// Ejecutar la consulta del id del catalogo
			try (ResultSet rs = prepared.executeQuery()){				
				if(rs.next()){
					id = rs.getInt("ID_CAT_PK");
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		}		
		return id;
	}
	
	/**
	 * retrieveIntentosTipoError.
	 * Metodo para obtener los intentos que se ejecutarn por tipo de error
	 *
	 * @param idExpediente the id expediente
	 * @param nombreBloque the nombre bloque
	 * @param nombreError the nombre error
	 * @return the integer
	 */
	public Integer retrieveIntentosTipoError(String idExpediente, String nombreBloque, String nombreError) {
		OracleManager om = new OracleManager();
		Integer cont = 0;
		StringBuilder sb = new StringBuilder();
		// Preparar la consulta de intentos por error
		sb.append("select e.NUM_CONT from OPT_MX_MAE_ESTAT_BLOQ b, OPT_MX_MAE_TIPO_ERROR e, OPT_MX_MAE_CAT_ERROR ce ");
		sb.append("where b.ID_ESTAT_BLOQ_PK = e.ID_ESTAT_BLOQ_FK ");
		sb.append("and e.ID_CAT_ERROR_FK = ce.ID_CAT_PK ");
		sb.append("and ce.ID_CAT_PK = ? ");
		sb.append("and b.ID_EXP_FK = ? ");
		sb.append("and b.ID_CAT_BLOQ_FK = ?");
		// Preparar la conexion
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement(sb.toString())){
			// Agregar los parametros de consulta
			ps.setInt(1, retrieveIdCatalogoByName(getQuery(CatalogoEnum.CAT_ERROR.getName()), nombreError));
			ps.setInt(2, new Integer(idExpediente));
			ps.setInt(3, retrieveIdCatalogoByName(getQuery(CatalogoEnum.CAT_BLOQUE.getName()), nombreBloque));
			// Ejecutar la consulta de intentos por error
			try (ResultSet rs = ps.executeQuery()){				
				if(rs.next()){
					cont = rs.getInt("NUM_CONT");
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		} 
		return cont;
	}
	
	/**
	 * retrieveReintentosByNombreError.
	 * Metodo para obtener los reintentos por nombre de error
	 *
	 * @param nombreError Parametro nombreError para almacenar el nombre del error
	 * @return the integer Retorna los reintentos
	 */
	public Integer retrieveReintentosByNombreError(String nombreError) {
		OracleManager om = new OracleManager();
		Integer reintentos = 0;
		StringBuilder sb = new StringBuilder();
		sb.append("select NUM_REINT from OPT_MX_MAE_CAT_ERROR where ID_CAT_PK = ?");
		// Preparar la conexion
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement(sb.toString())){
			// Agregar parametros para consulta
			ps.setInt(1, retrieveIdCatalogoByName(getQuery(CatalogoEnum.CAT_ERROR.getName()), nombreError));
			// Ejecutar la consulta
			try(ResultSet rs = ps.executeQuery()){				
				if(rs.next()){
					reintentos = rs.getInt("NUM_REINT");
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		}
		return reintentos;
	}
	
	/**
	 * getQuery.
	 * Metodo para obtener una consulta simple por nombre
	 *
	 * @param catalogName the catalog name
	 * @return the query
	 */
	private String getQuery(String catalogName) {
		return "select ID_CAT_PK from "+catalogName+" WHERE TXT_NOMBR = ?";
	}

	/**
	 * insertServidorNacimiento.
	 * Metodo para insertar la fecha del servidor
	 *
	 * @param idExpediente the id expediente
	 * @param ipServer the ip server
	 * @return true, if successful
	 */
	public boolean insertServidorNacimiento(String idExpediente, String ipServer) {
		boolean ok = false;
		OracleManager om = new OracleManager();
		// Preparar la conexion a BD
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement("update OPT_MX_MAE_EXP set TXT_IP = ? where ID_EXP_PK = ?")){
			// Agregar parametros para la actualizacion
			ps.setString(1,ipServer);
			ps.setInt(2, new Integer(idExpediente));
			// Validar la respuesta de la actualizacion
			if(ps.executeUpdate() > 0) {
				ok = true;
			}
		} catch (SQLException e) {
			LOG.error(e);
		}
		return ok;
	}

	/**
	 * Retrieve expedientes purga.
	 * Obtener todos los expedientes en un periodo de 7 dias atras 
	 *
	 * @param estatus the estatus 
	 * @return lista de expedientes
	 */
	/* (non-Javadoc)
	 * @see mx.isban.flujogd.persistence.PersistenceManagerExpediente#retrieveExpedientesPurgaBySuscriptor(java.lang.String, java.lang.String)
	 */
	@Override
	public List<ExpedientePurgaBean> retrieveExpedientesPurgaBySuscriptor(int dias, String idSubscriptor) {
		ExpedientePurgaBean expediente = new ExpedientePurgaBean();
		List<ExpedientePurgaBean> listExpedientes = new ArrayList<ExpedientePurgaBean>();
		OracleManager om = new OracleManager();
		StringBuilder sb= new StringBuilder();		
			sb
			.append("SELECT expestat.id_cat_estat_exp_fk as ID_ESTAT, exp.txt_nom, exp.id_exp_pk ")
			.append("FROM opt_mx_mae_estat_exp expestat, opt_mx_mae_exp exp, opt_mx_mae_subsc subsc, ")
			.append(" opt_mx_mae_noti noti WHERE expestat.fch_crea <= (sysdate - ?) ")
			.append(" AND exp.id_exp_pk = expestat.id_exp_fk and expestat.id_estat_exp_pk = ( ")
			.append("  select DISTINCT FIRST_VALUE(estat.id_estat_exp_pk)  ")
			.append("  OVER (ORDER BY estat.id_estat_exp_pk DESC, estat.fch_crea ASC) ")
			.append("  from opt_mx_mae_estat_exp estat where estat.id_exp_fk = exp.id_exp_pk) ")
			.append(" AND exp.id_noti_fk = noti.id_noti_pk AND noti.id_subsc_fk = subsc.id_subsc_pk ")
			.append(" AND subsc.id_subsc_pk = ? and expestat.id_cat_estat_exp_fk <> 13");
		// Preparar la conexion a BD
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement(sb.toString())){
			// Agregar los parametros para consultar el estatus del expediente
				ps.setInt(1, dias);
				ps.setString(2, idSubscriptor);
			// Ejecutar la consulta
			try(ResultSet rs = ps.executeQuery()){
				while(rs.next()){
					expediente = new ExpedientePurgaBean();
					expediente.setId(rs.getInt("ID_EXP_PK"));
					expediente.setNombre(rs.getString(TXT_NOM));
					expediente.setIdBloque(rs.getString("ID_ESTAT"));
					listExpedientes.add(expediente);
				}
			}
		} catch (SQLException e) {
			LOG.error("Error al obtener los expedientes a purgar :: ", e);
		}
		return listExpedientes;
	}
	
	/**
	 * Retrieve data server FTP.
	 * Metodo para obtener los parametros requeridos para la purga
	 *
	 * @param subscriptor the subscriptor
	 * @return the conexion SFTP bean
	 */
	@Override
	public ConexionSFTPBean retrieveDataServerFTP(String subscriptor) {
		OracleManager om = new OracleManager();
		ConexionSFTPBean sftp = new ConexionSFTPBean();
		sftp.setRutas(new RutaServerBean());
		StringBuilder sb = new StringBuilder();
		// Preparar la consulta de parametros
		sb.append("SELECT opp.TXT_NOMBR,opp.TXT_VALOR,opp.FLG_ENCRI FROM OPT_MX_MAE_SUBSC ops, OPT_MX_MAE_CONF opc, OPT_MX_MAE_PARAM opp ");
		sb.append("WHERE ops.ID_SUBSC_PK = opc.ID_SUBSC_FK ");
		sb.append("AND opc.ID_CONF_PK = opp.ID_CONF_FK ");
		sb.append("AND ops.TXT_NOMBR = ? ");
		// Preparar la conexion a BD
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement(sb.toString())){
			// Agregar parametros de consulta
			ps.setString(1, subscriptor);
			// Ejecutar la consulta
			try(ResultSet rs = ps.executeQuery()){				
				while(rs.next()){
					getParams(sftp, rs);
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		} 
		return sftp;
	}

	/**
	 * Gets the params.
	 * Metodo para obtener los parametros para purgado
	 *
	 * @param sftp objeto para los parametros
	 * @param rs resultado de la consulta de parametros
	 * @return the params parametros de purgado
	 * @throws SQLException the SQL exception
	 */
	private void getParams(ConexionSFTPBean sftp, ResultSet rs) throws SQLException {
		switch (rs.getString("TXT_NOMBR")) {
		case "sftp_user":
			sftp.setUser(rs.getString(TXT_VALOR));
			break;
		case "sftp_pass":
			sftp.setPass(rs.getString(TXT_VALOR));
			break;
		case "sftp_host":
			sftp.setServer(rs.getString(TXT_VALOR));
			break;
		case "sftp_port":
			sftp.setPuerto(rs.getString(TXT_VALOR));
			break;
		case "sftp_src_path":
			sftp.getRutas().setPathStfpSrc(rs.getString(TXT_VALOR));
			break;
		case "sftp_output_path":
			sftp.getRutas().setPathStfpOut(rs.getString(TXT_VALOR));
			break;
		case "zip_target_path":
			sftp.getRutas().setPathUnzip(rs.getString(TXT_VALOR));
			break;
		case "sftp_path_priv_key":
			sftp.getRutas().setPathPrivKey(rs.getString(TXT_VALOR));
			break;
		case "sftp_passphrase":
			sftp.getRutas().setSftpPassPhrase(rs.getString(TXT_VALOR));
			break;
		default:
			break;
		}
	}

	/**
	 * Validar expediente.
	 * Metodo para validar si existen expedientes procesados 
	 *
	 * @param nombre del expediente
	 * @return the list - lista de nombres de expedientes encontrados
	 */
	@Override
	public List<String> validarExpediente(String nombre) {
		List<String> listExp = new ArrayList<String>();
		OracleManager om = new OracleManager();
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT E.TXT_NOM, B.ID_ESTAT_BLOQ_PK, B.ID_CAT_BLOQ_FK ")
		.append("FROM OPT_MX_MAE_EXP E ")
		.append("INNER JOIN OPT_MX_MAE_ESTAT_BLOQ B ON E.ID_EXP_PK = B.ID_EXP_FK WHERE ")
		.append("B.ID_CAT_BLOQ_FK = 5 AND E.TXT_NOM = ? AND B.ID_CAT_ESTAT_BLOQ_FK = 1");
		// Preparar la conexion a BD
		try (Connection conn = om.getConnectionByWallet();
			PreparedStatement ps = conn.prepareStatement(sb.toString())){
			// Agregar los parametros para consultar el estatus del expediente 
			ps.setString(1, nombre);	
			try(ResultSet rs = ps.executeQuery()){				
				while(rs.next()){
					LOG.error(rs.getString(TXT_NOM));
					listExp.add(rs.getString(TXT_NOM));
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		}
		return listExp;
	}

}
