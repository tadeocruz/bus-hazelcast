package mx.isban.flujogd.persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mx.isban.flujogd.common.bean.DocumentoBean;
import mx.isban.flujogd.common.bean.DocumentoBeanDetalle;
import mx.isban.flujogd.common.bean.DocumentoBeanDetalle2;
import mx.isban.flujogd.common.util.CatalogoEnum;
import mx.isban.flujogd.persistence.OracleManager;
import mx.isban.flujogd.persistence.PersistenceManagerDocumento;

/**
 * The Class PersistenceManagerDocumentoImpl.
 * Clase que permitira la consulta del documeto
 * Actualziar estatus del documento
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase PersistenceManagerExpedienteImpl
 */
public class PersistenceManagerDocumentoImpl implements PersistenceManagerDocumento{
	
	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(PersistenceManagerDocumentoImpl.class);
	
	// Inicializamos un string con el atributo ID_DOC_PK
	private static final String ID_DOC_PK = "ID_DOC_PK"; 

	/**
	 * insertDocumento.
	 * Metodo para insertar el documento en BD
	 *
	 * @param idExpediente Parametro de tipo String para el expediente
	 * @param tipoDocumento Parametro para el tipo de documento
	 * @param nombre Parametro para el nombre del documento
	 * @param checksum Parametro de tipo String para checksum
	 * @param estatus Parametro para el estatus del documento
	 * @return boolean si es true inserta el documento
	 */
	public boolean insertDocumento(String idExpediente, String tipoDocumento, String nombre, String checksum, String estatus) {
		boolean ok = false;
		OracleManager om = new OracleManager();
		// Preparar la conexion
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement("insert into OPT_MX_MAE_DOC (ID_CAT_DOC_FK,ID_CAT_ESTAT_DOC_FK,ID_EXP_FK,TXT_NOMBR,TXT_CHSUM,FCH_ACTU_ESTAT)VALUES(?,?,?,?,?,sysdate)")){
			LOG.info("Insertar documento :: " +nombre);
			// Agregar los parametros para insertar el documento
			ps.setInt(1, retrieveIdCatalogoByName(getQuery(CatalogoEnum.CAT_DOCUMENTO.getName()), tipoDocumento));
			ps.setInt(2,retrieveIdCatalogoByName(getQuery(CatalogoEnum.CAT_ESTATUS_DOCUMENTO.getName()),estatus));
			ps.setInt(3, new Integer(idExpediente));
			ps.setString(4, nombre);
			ps.setString(5, checksum);
			// Validar el resultado del insert del documento
			if(ps.executeUpdate() > 0) {				
				ok = true;
			}
		} catch (SQLException e) {
			LOG.error("Error al insertar el documento :: "+ nombre +" en BD :: " + e);
		}
		return ok;
	}
	
	/**
	 * retrieveIdCatalogoByName.
	 * Metodo para obtener el id del catalogo por nombre
	 *
	 * @param query the query
	 * @param name the name
	 * @return the integer
	 */
	public Integer retrieveIdCatalogoByName(String query, String name) {
		OracleManager om = new OracleManager();
		Integer id = 0;
		// Preparar la conexion
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement(query)){
			// Agregar el parameto name para la consulta
			ps.setString(1, name);
			// Ejecutar la consulta
			try(ResultSet rs = ps.executeQuery()){				
				if(rs.next()){
					id = rs.getInt("ID_CAT_PK");
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		}		
		return id;
	}
	
	/**
	 * getQuery.
	 * Metodo para realizar la consulta del catalogo por nombre
	 *
	 * @param catalogName the catalog name
	 * @return the query
	 */
	private String getQuery(String catalogName) {
		return "select ID_CAT_PK from "+catalogName+" WHERE TXT_NOMBR = ?";
	}

	/**
	 * retrieveDocumento.
	 * Metodo para obtener el documento
	 *
	 * @param referenciaExterna the referencia externa
	 * @param tipoDocumento the tipo documento
	 * @param estatusDocumento the estatus documento
	 * @return the documento bean
	 */
	public DocumentoBean retrieveDocumento(String referenciaExterna,String tipoDocumento,String estatusDocumento) {
		LOG.info("Referencia externa :: "+ referenciaExterna);
		OracleManager om = new OracleManager();
		DocumentoBean documento = null;
		StringBuilder sb = new StringBuilder();
		// Preparar la consulta del documento
		sb.append("select doc.ID_DOC_PK from OPT_MX_MAE_DOC doc, OPT_MX_MAE_EXP exp ");
		sb.append("where doc.ID_EXP_FK = exp.ID_EXP_PK ");
		sb.append("and exp.TXT_REF_EXT = ? ");
		sb.append("and doc.ID_CAT_DOC_FK = ? ");
		sb.append("and doc.ID_CAT_ESTAT_DOC_FK = ?");
		// Preparar la conexion
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement(sb.toString())){
			LOG.info("Conexion creada ::"+conn.getClientInfo());
			// Agregar los parametros de consulta
			ps.setString(1,referenciaExterna);
			ps.setInt(2, retrieveIdCatalogoByName(getQuery(CatalogoEnum.CAT_DOCUMENTO.getName()), tipoDocumento));
			ps.setInt(3, retrieveIdCatalogoByName(getQuery(CatalogoEnum.CAT_ESTATUS_DOCUMENTO.getName()), estatusDocumento));
			// Ejecutar la consulta del documento
			try(ResultSet rs = ps.executeQuery()){				
				LOG.info("Query ejecutado");
				if(rs.next()){
					documento = new DocumentoBean();
					documento.setIdDocumento(String.valueOf(rs.getInt(ID_DOC_PK)));
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		}
		return documento;
	}

	/**
	 * updateStatusDocumento.
	 * Metodo para actualizar el estatus del documento
	 *
	 * @param idDocumento the id documento
	 * @param status the status
	 * @return true, if successful
	 */
	public boolean updateStatusDocumento(String idDocumento, String status) {
		boolean ok = false;
		OracleManager om = new OracleManager();
		// Preparar la conexion
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement("update OPT_MX_MAE_DOC set ID_CAT_ESTAT_DOC_FK = ?, FCH_ACTU_ESTAT = sysdate where ID_DOC_PK = ?")){
			// Agregar parametros para la actualizacion del estatus
			ps.setInt(1, retrieveIdCatalogoByName(getQuery(CatalogoEnum.CAT_ESTATUS_DOCUMENTO.getName()),status));
			ps.setInt(2, new Integer(idDocumento));
			// Validar actualizacion
			if(ps.executeUpdate() > 0) {
				ok = true;
			}
		} catch (SQLException e) {
			LOG.error(e);
		} 
		return ok;
	}

	/**
	 * Retrieve documentos by expediente.
	 * Metodo para obteber los documentos por estatus
	 *
	 * @param idExpediente the id expediente
	 * @param estatusDocumento the estatus documento
	 * @return the map
	 */
	public Map<String,DocumentoBean> retrieveDocumentosByExpediente(String idExpediente, String estatusDocumento) {
		OracleManager om = new OracleManager();
		StringBuilder sb = new StringBuilder();
		Map<String,DocumentoBean> documentos = new HashMap<>();
		// Preparar la consulta del documento por estatus
		sb.append("select ID_DOC_PK,ID_CAT_DOC_FK,TXT_NOMBR,TXT_CHSUM from OPT_MX_MAE_DOC where ID_EXP_FK = ? and ID_CAT_ESTAT_DOC_FK = ?");
		// Preparar la conexion
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement(sb.toString())){
			// Agregar parametro para consulta
			ps.setInt(1, new Integer(idExpediente));
			ps.setInt(2, retrieveIdCatalogoByName(getQuery(CatalogoEnum.CAT_ESTATUS_DOCUMENTO.getName()), estatusDocumento));
			// Ejecutar la consulta
			try(ResultSet rs = ps.executeQuery()){	
				// Recorrer los expedientes
				while(rs.next()){
					DocumentoBean documentoBean = new DocumentoBean();
					documentoBean.setIdDocumento(String.valueOf(rs.getInt(ID_DOC_PK)).toString());
					documentoBean.setTipoDocumento(rs.getString("ID_CAT_DOC_FK"));
					DocumentoBeanDetalle detalle = new DocumentoBeanDetalle();
					detalle.setNombreDocumento(rs.getString("TXT_NOMBR"));
					DocumentoBeanDetalle2 detalle2 = new DocumentoBeanDetalle2();
					detalle2.setChecksum(rs.getString("TXT_CHSUM"));
					documentoBean.setDetalle2(detalle2);
					documentoBean.setDetalle(detalle);
					documentos.put(documentoBean.getDetalle().getNombreDocumento(),documentoBean);
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		} 
		return documentos;
	}
	
	/**
	 * Retrieve documentos by expediente.
	 * Metodo para obtener los documentos por expediente
	 *
	 * @param idExpediente the id expediente
	 * @return the map
	 */
	public Map<String,DocumentoBean> retrieveDocumentosByExpediente(String idExpediente) {
		OracleManager omDocumentos = new OracleManager();
		StringBuilder sb = new StringBuilder();
		Map<String,DocumentoBean> documentos = new HashMap<>();
		// Preparar la consulta por expediente
		sb.append("select ID_DOC_PK,ID_CAT_DOC_FK,TXT_NOMBR,TXT_CHSUM from OPT_MX_MAE_DOC where ID_EXP_FK = ?");
		// Preparar la conexion
		try (Connection conn = omDocumentos.getConnectionByWallet();
				PreparedStatement psDocumentos = conn.prepareStatement(sb.toString())){
			// Agregar parametro de consulta
			psDocumentos.setInt(1, new Integer(idExpediente));
			// Ejecutar consulta
			try(ResultSet rs = psDocumentos.executeQuery()){				
				while(rs.next()){
					DocumentoBean documento = new DocumentoBean();
					documento.setIdDocumento(String.valueOf(rs.getInt(ID_DOC_PK)).toString());
					documento.setTipoDocumento(rs.getString("ID_CAT_DOC_FK"));
					DocumentoBeanDetalle detalle = new DocumentoBeanDetalle();
					detalle.setNombreDocumento(rs.getString("TXT_NOMBR"));
					DocumentoBeanDetalle2 detalle2 = new DocumentoBeanDetalle2();
					detalle2.setChecksum(rs.getString("TXT_CHSUM"));
					documento.setDetalle2(detalle2);
					documento.setDetalle(detalle);
					documentos.put(documento.getDetalle().getNombreDocumento(),documento);
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		}
		return documentos;
	}

}
