package mx.isban.flujogd.persistence.impl;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mx.isban.flujogd.common.bean.CifrasControlBean;
import mx.isban.flujogd.persistence.OracleManager;
import mx.isban.flujogd.persistence.PersistenceManagerCifrasControl;

/**
 * Clase que permite la persistencia de datos para las cifras de control
 * The Class PersistenceManagerCifrasControlImpl.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase PersistenceManagerNotificacionImpl
 */
public class PersistenceManagerCifrasControlImpl implements PersistenceManagerCifrasControl{

	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(PersistenceManagerCifrasControlImpl.class);

	/**
	 * Metodo para notificar los resultados de los procesos
	 * notificarCifrasCtrl.
	 *
	 * @param nombreSubscriptor the nombre subscriptor
	 * @return the cifras control bean
	 */
	public CifrasControlBean notificarCifrasCtrl(String nombreSubscriptor) {
		PersistenceManagerNotificacionImplUtils utils = new PersistenceManagerNotificacionImplUtils();
		List<Integer> listIdsExpedientes = new ArrayList<Integer>();
		StringBuilder sqlSb = new StringBuilder();
		StringBuilder resSb = new StringBuilder();
		InputStream streamFile = null;
		// Instancia para oracle
		OracleManager om = new OracleManager();
		// se añade cabecera
		resSb.append(utils.getRegistros(nombreSubscriptor));
		// Preparar la consulta para obtener las cifras de control
		sqlSb.append("SELECT es.ID_EXP_PK ID_EXPEDIENTE, es.TXT_NOM EXPEDIENTE,esta.ID_CAT_PK ID_ESTATUS,esta.TXT_NOMBR ESTATUS, es.TXT_REF_EXT REFERENCIA "); 
		sqlSb.append("FROM OPT_MX_MAE_CAT_ESTAT_EXP esta, OPT_MX_MAE_EXP es,OPT_MX_MAE_SUBSC subsc,OPT_MX_MAE_NOTI noti "); 
		sqlSb.append("where esta.ID_CAT_PK = (SELECT max(esta.ID_CAT_ESTAT_EXP_FK) "); 
		sqlSb.append("FROM OPT_MX_MAE_EXP exp, OPT_MX_MAE_ESTAT_EXP esta "); 
		sqlSb.append("WHERE exp.ID_EXP_PK = esta.ID_EXP_FK) ");  
		sqlSb.append("and esta.ID_CAT_PK in (3,5,6,7,10) "); 
		sqlSb.append("and subsc.TXT_NOMBR = ? "); 
		sqlSb.append("and es.FLG_CIFRA <> 1 and subsc.ID_SUBSC_PK = noti.ID_SUBSC_FK ");  
		sqlSb.append("and noti.id_noti_pk = es.ID_NOTI_FK ");
		// Realizar conexion y preparar query a ejecutar
		try (Connection conn = om.getConnectionByWallet();
				PreparedStatement ps = conn.prepareStatement(sqlSb.toString())){
			// Agregar parametros para la consulta
			ps.setString(1, nombreSubscriptor);
			try (ResultSet rs = ps.executeQuery()){		
				// Obtener los expedientes
				while(rs.next()) {
					listIdsExpedientes.add(rs.getInt("ID_EXPEDIENTE"));
					// Se genera el registro 
					resSb.append(rs.getString("EXPEDIENTE").toString().replaceAll(".zip", "") + "|" + rs.getString("ESTATUS") +"|" + rs.getString("REFERENCIA") + "\n");
				}
			}
		} catch (SQLException e) {
			LOG.error(e);
		}
		streamFile = new ByteArrayInputStream(resSb.toString().getBytes(StandardCharsets.UTF_8));

		return new CifrasControlBean(streamFile, listIdsExpedientes);
	}

	/**
	 * Actualizar los expedientes procesados
	 * updateExpedientes.
	 *
	 * @param idsExpedientes the ids expedientes
	 * @return true, if successful
	 */
	public boolean updateExpedientes(List<Integer> idsExpedientes) {
		boolean ok = false;
		OracleManager om = new OracleManager();
		StringBuilder idsExpedientesStrTmp = new StringBuilder("");
		// Recorrer los expedientes 
		for(Integer idExpediente : idsExpedientes) {
			idsExpedientesStrTmp.append(idExpediente +",");
		}
		String idsExpedientesStr = idsExpedientesStrTmp.toString();
		if(idsExpedientesStr == null || idsExpedientesStr.isEmpty()) {
			return ok;
		}
		//eliminamos la ultima coma de los ids 0,idsEpedientes.toString().length()-1
		idsExpedientesStr = idsExpedientesStr.substring(0,idsExpedientesStr.length()-1);
		// Preparar la conexion y el query que se ejecutara
		try (Connection conn = om.getConnectionByWallet();
			// Se actualiza el registro en la DB
			PreparedStatement ps = conn.prepareStatement("update OPT_MX_MAE_EXP set FLG_CIFRA = 1 where ID_EXP_PK in ("+idsExpedientesStr+")")){
			// Validacion de ejecucion
			if(ps.executeUpdate() > 0) {				
				ok = true;
			}
		} catch (SQLException e) {
			LOG.error(e);
		}
		return ok;
	}

}
