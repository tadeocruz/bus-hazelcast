package mx.isban.flujogd.persistence.tc;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.bean.NotificationBean;
import mx.isban.flujogd.common.bean.NotificationBean2;
import mx.isban.flujogd.common.bean.NotificationBean3;
import mx.isban.flujogd.persistence.PersistenceManager;
import mx.isban.flujogd.persistence.PersistenceManagerNotificacion;
import mx.isban.flujogd.persistence.impl.PersistenceManagerImpl;
import mx.isban.flujogd.persistence.impl.PersistenceManagerNotificacionImpl;

public class PersistenceTC {
	
	private static final Logger LOG = LogManager.getLogger(PersistenceTC.class);
	
	public static void main(String[] args) {
//		PersistenceManagerNotificacion pm = new PersistenceManagerNotificacionImpl();
		PersistenceManager pm = new PersistenceManagerImpl();
		List<ExpedienteBean> lista = pm.retrieveAllExpedientesBySubscriptor("VIVERE");
//		Integer id = pm.retrieveIdCatalogoByName(CatalogoEnum.CAT_ERROR.getName(), null);
//		LOG.info(id);
//		pm.updateStatusExpediente("1", "2");
//		List<ExpedienteBean> expedientes = pm.retrieveAllExpedientes();
//		for(ExpedienteBean bean:expedientes) {
//			LOG.info(bean.toString());
//			Map<Integer, FlujoBean> flujos = bean.getDetalle().getFlujos();
//			Iterator<Integer> iter = flujos.keySet().iterator();
//			while(iter.hasNext()) {
//				Integer key = iter.next();
//				FlujoBean flujo = flujos.get(key);
//				LOG.info(flujo.getQueue());
//			}
//		}
//		NotificationBean bean = new NotificationBean();
//		NotificationBean2 bean2 = new NotificationBean2();
//		NotificationBean3 bean3 = new NotificationBean3();
//		bean2.setBean3(bean3);
//		bean.setBean2(bean2);
//		bean.setIdExpediente("");
//		bean.setRequest("");
//		bean.setResponseCode("");
//		bean.setResponseMsg("");
//		bean2.setErrorId("");
//		bean2.setErrorMsg("");
//		bean2.setRefExterna("");
//		bean2.setReqId("");
//		bean3.setBuc("");
//		bean3.setFileName("");
//		bean3.setPathUnzip("");
//		bean3.setPathZip("");
//		pm.insertOrUpdateNotification(bean);
		LOG.info("done...");
	}

}
