package mx.isban.flujogd.persistence.tc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mx.isban.flujogd.common.bean.BatchParametroBean;
import mx.isban.flujogd.common.util.ParametroBatchEnum;
import mx.isban.flujogd.persistence.PersistenceManagerBatch;
import mx.isban.flujogd.persistence.impl.PersistenceManagerBatchImpl;

public class PersistenceBatchTC {
	
	private static final Logger LOG = LogManager.getLogger(PersistenceBatchTC.class);
	
	public static void main(String[] args) {
		PersistenceManagerBatch pm = new PersistenceManagerBatchImpl();
		BatchParametroBean bean = pm.retrieveParam(ParametroBatchEnum.BATCH_PARAMETRO_NUMBER_THREADS.getName());
		LOG.info(bean);
		LOG.info("done...");
	}

}
