/**
 * 
 */
package mx.isban.flujogd.common.generic;

import java.io.File;
import java.io.InputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.jcraft.jsch.UserInfo;

import mx.isban.flujogd.common.bean.SftpResponseBean;
import mx.isban.flujogd.common.util.ExpedienteErrorEnum;

/**
 * The Class SFTPManagerUtils.
 * Clase de utileria para la conexion a sftp
 *
 * @author jccalderon
 * Stefanini
 * 
 */
public class SFTPManagerUtils {
	
	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(SFTPManagerUtils.class);
	/**
	 * The Class MyUserInfo.
	 */
	public static class MyUserInfo implements UserInfo{
		/** The passphrase. */
		private String passphrase;
		
		/** The pass. */
		private String pass;
		
		
		/**
		 * Instantiates a new my user info.
		 *
		 * @param pass the pass
		 * @param passphrase the passphrase
		 */
		public MyUserInfo(String pass, String passphrase) {
			this.pass = pass;
			this.passphrase = passphrase;
			}

		/**
		 * Prompt yes no.
		 *
		 * @param str the str
		 * @return true, if successful
		 */
		public boolean promptYesNo(String str){
			return true;
		}
		
		/**
		 * Gets the passphrase.
		 *
		 * @return the passphrase
		 */
		public String getPassphrase(){ 
			return this.passphrase; 
		}
		
	    /**
    	 * Gets the password.
    	 *
    	 * @return the password
    	 */
    	public String getPassword(){ 
    		return this.pass; 
    		}


	    /**
    	 * Prompt passphrase.
    	 *
    	 * @param message the message
    	 * @return true, if successful
    	 */
    	public boolean promptPassphrase(String message){ 
    		return true; 
    		}

    	/**
    	 * Show message.
    	 *
    	 * @param message the message
    	 */
    	public void showMessage(String message){
    		LOG.info(message);
    	}	   
    	
	    /**
    	 * Prompt password.
    	 *
    	 * @param message the message
    	 * @return true, if successful
    	 */
    	public boolean promptPassword(String message){
    		return true;
    		}


	  }
	
	/**
	 * Save file SFTP.
	 * Metodo para guardar el archivo en el sftp
	 *
	 * @param fileName the file name
	 * @param is the is
	 * @param pathPrivateKey the path private key
	 * @param user the user
	 * @param pass the pass
	 * @param host the host
	 * @param port the port
	 * @param passphrase the passphrase
	 * @return the sftp response bean
	 */
	public SftpResponseBean saveFileSFTP(String fileName, InputStream is, String pathPrivateKey, String user, String pass, String host, String port, String passphrase) {
		JSch jsch = null;
		Session session = null;
		ChannelSftp channelSftpSave = null;
		SftpResponseBean response = new SftpResponseBean(true);
		try {
			jsch = new JSch();
			if(pathPrivateKey!=null && !"".equals(pathPrivateKey)) {				
				jsch.addIdentity(pathPrivateKey);
			}
			session = jsch.getSession(user, host, new Integer(port));
			UserInfo ui=new MyUserInfo(pass, passphrase);
			session.setUserInfo(ui);
			session.connect();
			
			Channel channelSave = session.openChannel("sftp");
			channelSave.connect();
			
			channelSftpSave = (ChannelSftp)channelSave;
			channelSftpSave.put(is, fileName);
		} catch (JSchException e) {
			response.setOk(false);//No se puede conectar
			response.setErrorMessage(e.getMessage());
			response.setErrorCode(ExpedienteErrorEnum.ERROR_SFTP_CONEXION.getName());
			LOG.error("Error al conectar al sftp. Cause :: ", e);
		} catch (SftpException e) {
			response.setOk(false);//No se puede recuperar el archivo
			response.setErrorMessage(e.getMessage());
			response.setErrorCode(ExpedienteErrorEnum.ERROR_SFTP_ARCH_NO_EXISTE.getName());
			LOG.error("Error al obtener el archivo " + fileName +" Cause :: ", e);
		} finally {
				if(channelSftpSave!=null) {					
					channelSftpSave.exit();
				}
				if(session!=null) {					
					session.disconnect();
				}
		}
		return response;
	}

	/**
	 * Gets the file SFTP.
	 * Metodo para obtener el archivo del sftp
	 *
	 * @param fileName the file name
	 * @param srcPath the src path
	 * @param outputPath the output path
	 * @param pathPrivateKey the path private key
	 * @param user the user
	 * @param pass the pass
	 * @param host the host
	 * @param port the port
	 * @param passphrase the passphrase
	 * @return the file SFTP
	 */
	public SftpResponseBean getFileSFTP(String fileName, String srcPath, String outputPath, String pathPrivateKey,
			String user, String pass, String host, String port, String passphrase) {
		JSch jsch = null;
		Session session = null;
		ChannelSftp channelSftp = null;
		SftpResponseBean response = new SftpResponseBean(true);
		try {
			jsch = new JSch();
			if(pathPrivateKey!=null && !"".equals(pathPrivateKey)) {
				jsch.addIdentity(pathPrivateKey);
			}
			session = jsch.getSession(user, host, new Integer(port));
			UserInfo ui=new MyUserInfo(pass,passphrase);
			session.setUserInfo(ui);
			session.connect();
			
			Channel channel = session.openChannel("sftp");
			channel.connect();
			
			channelSftp = (ChannelSftp)channel;
			
			
			String separator = File.separator;
			if("WIN".equals(System.getProperty("os.name").toUpperCase().substring(0, 3))) {
				separator = "/";
			}
            // Validar si existe repositorio 
            File pathOut = new File(outputPath);
            if (!pathOut.exists()) {
                LOG.info("Path no existe :: " + pathOut);
                // Crear el repositorio en caso de no existir
                pathOut.mkdir();
                LOG.info("Path creado :: " + pathOut);
            }
            
			channelSftp.get(srcPath + separator + fileName, outputPath+ separator + fileName);
			
		} catch (JSchException e) {
			LOG.error(e);
			response.setOk(false);//No se puede conectar
			response.setErrorMessage(e.getMessage());
			response.setErrorCode(ExpedienteErrorEnum.ERROR_SFTP_CONEXION.getName());
		} catch (SftpException e) {
            LOG.error("Error al realizar la transferencia SFTP :: ", e);		
            response.setOk(false);//No se puede recuperar el archivo
			response.setErrorMessage(e.getMessage());
			response.setErrorCode(ExpedienteErrorEnum.ERROR_SFTP_ARCH_NO_EXISTE.getName());
		} finally {
				if(channelSftp!=null) {					
					channelSftp.exit();
				}
				if(session!=null) {					
					session.disconnect();
				}
		}
		return response;
	}
}
