package mx.isban.flujogd.common.bean;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * Bean para transporte de datos Flujo hazelcast con funciones especificas pero
 * independiente de los subscriptores
 * 
 * @author Alvaro Zamorano azamorano@serviciosexternos.isban.mx
 *
 */
@Getter
@Setter
public class DocumentoBean implements Serializable {

	private static final long serialVersionUID = -8255627825342078777L;

	/**
	 * idDocumento
	 */
	private String idDocumento;

	/**
	 * idExpediente
	 */
	private String idExpediente;

	/**
	 * tipoDocumento
	 */
	private String tipoDocumento;

	/**
	 * detalle
	 */
	private DocumentoBeanDetalle detalle;

	/**
	 * detalle2
	 */
	private DocumentoBeanDetalle2 detalle2;
	
	/**
	 * Indica si el documento se asocia al folder
	 */
	private boolean asociarFolder;
}
