package mx.isban.flujogd.common.bean;

import java.io.Serializable;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal NetConfigBean
 *
 */
public class NetConfigBean implements Serializable{
	
	/**
	 * serialVersionUID
	 */
	//Inicializamos un long para el serial
	private static final long serialVersionUID = -5174661239774980617L;
	//Inicializamos un string para guardar el id
	private String id;
	//Inicializamos un string para guardar la ip
	private String ip;
	//Inicializamos un string para guardar el puerto
	private String port;
	//Inicializamos un string para guardar el status
	private String activa;
	
	/**
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}
	/**
	 * @param ip the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}
	/**
	 * @return the port
	 */
	public String getPort() {
		return port;
	}
	/**
	 * @param port the port to set
	 */
	public void setPort(String port) {
		this.port = port;
	}
	/**
	 * @return the activa
	 */
	public String getActiva() {
		return activa;
	}
	/**
	 * @param activa the activa to set
	 */
	public void setActiva(String activa) {
		this.activa = activa;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

}
