package mx.isban.flujogd.common.exceptions;

/**
 * Clase HazelcastConnectionException
 * 
 * @author Alvaro Zamorano azamorano@serviciosexternos.isban.mx
 *
 */
public class HazelcastConnectionException extends Exception {

	/**
	 * serialVersionUID
	 */
	// Inicializamos un long para guardar el serial
	private static final long serialVersionUID = 1398697226818360231L;

	/**
	 * hazelcastconnectionexception.
	 *
	 * @param message Mensaje a notificar
	 * @param e       excepcion hazelcast conexion
	 */
	public HazelcastConnectionException(String message, RuntimeException e) {
		// Mensaje de excepcion
		super(message);
	}

}
