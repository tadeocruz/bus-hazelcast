package mx.isban.flujogd.common.bean;

import java.io.Serializable;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal AccentureResponseToken
 *
 */
public class AccentureTokenResponse implements Serializable{
	
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 401273054043976602L;
	private String accessToken;
	private String tokenType;
	private String expiresIn;

	/**
	 * @return the access_token
	 */
	public String getAccessToken() {
		return accessToken;
	}

	/**
	 * @param accessToken the access_token to set
	 */
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	/**
	 * @return the token_type
	 */
	public String getTokenType() {
		return tokenType;
	}

	/**
	 * @param tokenType the token_type to set
	 */
	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

	/**
	 * @return the expires_in
	 */
	public String getExpiresIn() {
		return expiresIn;
	}

	/**
	 * @param expiresIn the expires_in to set
	 */
	public void setExpiresIn(String expiresIn) {
		this.expiresIn = expiresIn;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AccentureTokenResponse [access_token=" + accessToken + ", token_type=" + tokenType + ", expires_in="
				+ expiresIn + "]";
	}
	
}
