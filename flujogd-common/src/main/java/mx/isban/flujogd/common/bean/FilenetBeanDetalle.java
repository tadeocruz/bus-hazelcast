package mx.isban.flujogd.common.bean;

import java.io.Serializable;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal FilenetBeanDetalle
 *
 */
public class FilenetBeanDetalle implements Serializable{

	
	/**
	 * serialVersionUID
	 */
	//Inicializamos un long para el serial
	private static final long serialVersionUID = -6052519168222673355L;
	
	//Inicializamos un string para el tipo de almacenamiento
	private String store;
	//Inicializamos un string para guardar la contrasena
	private String password;
	
	private String secretData;
	
	/**
	 * @return the store
	 */
	public String getStore() {
		return store;
	}
	/**
	 * @param store the store to set
	 */
	public void setStore(String store) {
		this.store = store;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the secretData
	 */
	public String getSecretData() {
		return secretData;
	}
	/**
	 * @param secretData the secretData to set
	 */
	public void setSecretData(String secretData) {
		this.secretData = secretData;
	}
	
}
