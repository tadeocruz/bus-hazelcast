package mx.isban.flujogd.common.bean;

import java.io.Serializable;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal BloqueBean
 *
 */ 
public class BloqueBean2 implements Serializable{
	
	//Inicializamos un long para el serial
	private static final long serialVersionUID = -6491114716492387766L;
	
	//Inicializamos un string para guardar si el bean se encuentra activo o no
	private String activo;
	private String numeroInstancias;
	//Inicializamos un string para guardar el javaPathCacerts
	private String javaPathCacerts;
	private String javaKeyStoreP;

	/**
	 * Metodo get que obtiene el valor activo
	 * @return the activo
	 */
	public String getActivo() {
		return activo;
	}

	/**
	 * Setea el valor de activo
	 * @param activo the activo to set
	 */
	public void setActivo(String activo) {
		this.activo = activo;
	}

	/**
	 * Metodo get que ontiene el numeroIntancias
	 * @return the numeroInstancias
	 */
	public String getNumeroInstancias() {
		return numeroInstancias;
	}

	/**
	 * Setea el valor de numeroInstancias
	 * @param numeroInstancias the numeroInstancias to set
	 */
	public void setNumeroInstancias(String numeroInstancias) {
		this.numeroInstancias = numeroInstancias;
	}

	/**
	 * Obtiene el valor JavaPathCacerts
	 * @return the javaPathCacerts
	 */
	public String getJavaPathCacerts() {
		return javaPathCacerts;
	}

	/**
	 * Setea el valor javaPathCacerts
	 * @param javaPathCacerts the javaPathCacerts to set
	 */
	public void setJavaPathCacerts(String javaPathCacerts) {
		this.javaPathCacerts = javaPathCacerts;
	}

	/**
	 * Obtiene el valor javaKeyStoreP
	 * @return the javaKeyStoreP
	 */
	public String getJavaKeyStoreP() {
		return javaKeyStoreP;
	}

	/**
	 * Setea el valor javaKeyStoreP
	 * @param javaKeyStoreP the javaKeyStoreP to set
	 */
	public void setJavaKeyStoreP(String javaKeyStoreP) {
		this.javaKeyStoreP = javaKeyStoreP;
	}

}
