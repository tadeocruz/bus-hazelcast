package mx.isban.flujogd.common.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Bean para transporte de datos
 * Flujo hazelcast con funciones especificas pero independiente de los subscriptores
 *
 */
public class DocumentoBeanDetalle implements Serializable{
	
	private static final long serialVersionUID = -8255627825342078777L;
	
	private String nombreDocumento;
	private String metadato;
	private String path;
	private String extension;
	private List<AtributoBean> atributos = new ArrayList<>();
	
	/**
	 * @return the nombreDocumento
	 */
	public String getNombreDocumento() {
		return nombreDocumento;
	}
	/**
	 * @param nombreDocumento the nombreDocumento to set
	 */
	public void setNombreDocumento(String nombreDocumento) {
		this.nombreDocumento = nombreDocumento;
	}
	/**
	 * @return the metadato
	 */
	public String getMetadato() {
		return metadato;
	}
	/**
	 * @param metadato the metadato to set
	 */
	public void setMetadato(String metadato) {
		this.metadato = metadato;
	}
	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}
	/**
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}
	/**
	 * @return the extension
	 */
	public String getExtension() {
		return extension;
	}
	/**
	 * @param extension the extension to set
	 */
	public void setExtension(String extension) {
		this.extension = extension;
	}
	/**
	 * @return the atributos
	 */
	public List<AtributoBean> getAtributos() {
		return new ArrayList<>(this.atributos);
	}
	/**
	 * @param atributos the atributos to set
	 */
	public void setAtributos(List<AtributoBean> atributos) {
		this.atributos = new ArrayList<>(atributos);
	}

}
