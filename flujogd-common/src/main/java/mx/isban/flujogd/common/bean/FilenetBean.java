package mx.isban.flujogd.common.bean;

import java.io.Serializable;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal FilenetBean
 *
 */
public class FilenetBean implements Serializable{

	
	/**
	 * serialVersionUID
	 */
	//Inicializamos un long para el serial
	private static final long serialVersionUID = -6052519168222673355L;
	
	//Inicializamos un string para almacenar la ruta del id
	private String pathIdc;
	//Inicializamos un string para almacenar el nombre de usuario
	private String user;
	//Inicializamos un string para almacenar la uri
	private String uri;
	//Inicializamos un string para almacenar el jaas
	private String jaas;
	private FilenetBeanDetalle detalle;
	
	/**
	 * @return the pathIdc
	 */
	public String getPathIdc() {
		return pathIdc;
	}
	/**
	 * @param pathIdc the pathIdc to set
	 */
	public void setPathIdc(String pathIdc) {
		this.pathIdc = pathIdc;
	}
	/**
	 * @return the user
	 */
	public String getUser() {
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}
	/**
	 * @return the uri
	 */
	public String getUri() {
		return uri;
	}
	/**
	 * @param uri the uri to set
	 */
	public void setUri(String uri) {
		this.uri = uri;
	}
	/**
	 * @return the jaas
	 */
	public String getJaas() {
		return jaas;
	}
	/**
	 * @param jaas the jaas to set
	 */
	public void setJaas(String jaas) {
		this.jaas = jaas;
	}
	/**
	 * @return the detalle
	 */
	public FilenetBeanDetalle getDetalle() {
		return detalle;
	}
	/**
	 * @param detalle the detalle to set
	 */
	public void setDetalle(FilenetBeanDetalle detalle) {
		this.detalle = detalle;
	}

}
