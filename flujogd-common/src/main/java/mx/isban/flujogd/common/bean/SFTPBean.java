package mx.isban.flujogd.common.bean;

import java.io.Serializable;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal SFTPBean
 *
 */
public class SFTPBean implements Serializable{
	
	/**
	 * 
	 */
	//Inicializamos un long para el serial
	private static final long serialVersionUID = -7769331062460279510L;
	
	//Inicializamos un string para guardar el host
	private String host;
	//Inicializamos un string para guardar el puerto
	private String port;
	//Inicializamos un string para guardar el nombre de usuario
	private String user;
	//Inicializamos un string para guardar la contrasena
	private String pass;
	private SFTPBeanDetalle detalle;
	
	/**
	 * @return the host
	 */
	public String getHost() {
		return host;
	}
	/**
	 * @param host the host to set
	 */
	public void setHost(String host) {
		this.host = host;
	}
	/**
	 * @return the port
	 */
	public String getPort() {
		return port;
	}
	/**
	 * @param port the port to set
	 */
	public void setPort(String port) {
		this.port = port;
	}
	/**
	 * @return the user
	 */
	public String getUser() {
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}
	/**
	 * @return the pass
	 */
	public String getPass() {
		return pass;
	}
	/**
	 * @param pass the pass to set
	 */
	public void setPass(String pass) {
		this.pass = pass;
	}
	/**
	 * @return the detalle
	 */
	public SFTPBeanDetalle getDetalle() {
		return detalle;
	}
	/**
	 * @param detalle the detalle to set
	 */
	public void setDetalle(SFTPBeanDetalle detalle) {
		this.detalle = detalle;
	}

}
