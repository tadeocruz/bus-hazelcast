package mx.isban.flujogd.common.bean;

import java.io.InputStream;
import java.io.Serializable;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Bean para transporte de datos
 * Flujo hazelcast con funciones especificas pero independiente de los subscriptores
 *
 */
public class DocumentoBeanDetalle2 implements Serializable{
	
	private static final long serialVersionUID = -8255627825342078777L;
	
	private String encabezadosTxt;
	private String atributosTxt;
	private transient InputStream stream;
	private String errorMsg;
	private String checksum;
	
	/**
	 * @return the encabezadosTxt
	 */
	public String getEncabezadosTxt() {
		return encabezadosTxt;
	}
	/**
	 * @param encabezadosTxt the encabezadosTxt to set
	 */
	public void setEncabezadosTxt(String encabezadosTxt) {
		this.encabezadosTxt = encabezadosTxt;
	}
	/**
	 * @return the atributosTxt
	 */
	public String getAtributosTxt() {
		return atributosTxt;
	}
	/**
	 * @param atributosTxt the atributosTxt to set
	 */
	public void setAtributosTxt(String atributosTxt) {
		this.atributosTxt = atributosTxt;
	}
	/**
	 * @return the stream
	 */
	public InputStream getStream() {
		return stream;
	}
	/**
	 * @param stream the stream to set
	 */
	public void setStream(InputStream stream) {
		this.stream = stream;
	}
	/**
	 * @return the errorMsg
	 */
	public String getErrorMsg() {
		return errorMsg;
	}
	/**
	 * @param errorMsg the errorMsg to set
	 */
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	/**
	 * @return the checksum
	 */
	public String getChecksum() {
		return checksum;
	}
	/**
	 * @param checksum the checksum to set
	 */
	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}

}
