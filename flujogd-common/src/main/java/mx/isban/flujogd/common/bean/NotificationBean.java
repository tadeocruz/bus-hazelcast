package mx.isban.flujogd.common.bean;

import java.io.Serializable;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal NotificationBean
 *
 */
public class NotificationBean implements Serializable{
	
	/**
	 * 
	 */
	//Inicializamos un long para el serial
	private static final long serialVersionUID = 2788415472988041419L;
	//Inicializamos un string para guardar el id del expediente
	private String idExpediente;
	//Inicializamos un string para guardar la peticion
	private String request;
	//Inicializamos un string para guardar el codigo de respuesta
	private String responseCode;
	//Inicializamos un string para guardar el mensaje de respuesta
	private String responseMsg;
	private NotificationBean2 bean2;
	/**
	 * @return the idExpediente
	 */
	public String getIdExpediente() {
		return idExpediente;
	}
	/**
	 * @param idExpediente the idExpediente to set
	 */
	public void setIdExpediente(String idExpediente) {
		this.idExpediente = idExpediente;
	}
	/**
	 * @return the request
	 */
	public String getRequest() {
		return request;
	}
	/**
	 * @param request the request to set
	 */
	public void setRequest(String request) {
		this.request = request;
	}
	/**
	 * @return the responseCode
	 */
	public String getResponseCode() {
		return responseCode;
	}
	/**
	 * @param responseCode the responseCode to set
	 */
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	/**
	 * @return the responseMsg
	 */
	public String getResponseMsg() {
		return responseMsg;
	}
	/**
	 * @param responseMsg the responseMsg to set
	 */
	public void setResponseMsg(String responseMsg) {
		this.responseMsg = responseMsg;
	}
	/**
	 * @return the bean2
	 */
	public NotificationBean2 getBean2() {
		return bean2;
	}
	/**
	 * @param bean2 the bean2 to set
	 */
	public void setBean2(NotificationBean2 bean2) {
		this.bean2 = bean2;
	}

}
