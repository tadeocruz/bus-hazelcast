package mx.isban.flujogd.common.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.isban.flujogd.common.bean.AccentureDocumentoDetalleRequest;
import mx.isban.flujogd.common.bean.AccentureDocumentoRequest;
import mx.isban.flujogd.common.bean.AccentureNotificationRequest;
import mx.isban.flujogd.common.bean.DocumentoBean;
import mx.isban.flujogd.common.bean.ExpedienteBean;

/**
 * The Class NotificationManager.
 *
 * @author Alvaro
 */
public class NotificationManager {
	
	/** The sdf. */
	//Inicializamos un simpre date format para dar formato a la fecha
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	/** The Constant FALSE. */
	private static final String FALSE = "FALSE"; 
	
	/**
	 * preparaenvionotificacion.
	 *
	 * @param expedienteBean Vaina del expediente
	 * @param errorId Id del error
	 * @param msgError Mensaje de error
	 * @param buc Id del expediente
	 */
	public void preparaEnvioNotificacion(ExpedienteBean expedienteBean,String errorId,String msgError,String buc) {
		if(buc==null) {
			buc = "";
		}
		//Notificar a vivere que no se pudo realizar el copiado
		expedienteBean.getDetalle().setSiguienteQueue(QueueEnum.QUEUE_NOTIFICATION.getName());
		AccentureNotificationRequest request = new AccentureNotificationRequest();
		//Asignar el buc
		request.setBuc(buc);
		//Asignar la fecha de proceso
		request.setFechaProceso(sdf.format(new Date()));
		//Asignar la referencia externa
		request.setReferenciaExterna(expedienteBean.getDetalle().getDetalle2().getReferenciaExterna());
		//Asignar el id de la peticion
		request.setRequestId(expedienteBean.getDetalle().getDetalle2().getRequestId());
		
		//Crear una lista para las peticiones
		List<AccentureDocumentoRequest> docs= new ArrayList<>();
		//Crear una lista para los documentos XML
		List<DocumentoBean> documentos = expedienteBean.getDetalle().getDocumentos();
		if(documentos.isEmpty()) {
			AccentureDocumentoRequest documento = new AccentureDocumentoRequest();
			documento.setDocId(buc);
			documento.setDocNombre(expedienteBean.getNombreExpediente());
			documento.setChecksum(expedienteBean.getDetalle().getDetalle2().getChecksum());
			AccentureDocumentoDetalleRequest detalle = new AccentureDocumentoDetalleRequest();
			documento.setDetalle(detalle);
			if("".equals(msgError)) {
				//Mostar el mensaje de exito
				documento.setEstado("PROCESADO_CON_EXITO");
				documento.getDetalle().setReenvio(FALSE);
			}else {
				documento.setEstado("ERROR");
				//Asignar el id del error
				detalle.setErrorId(errorId);
				//Asignar el mensaje de error
				detalle.setError(msgError);
				documento.getDetalle().setReenvio("TRUE");
			}
			docs.add(documento);
		}else {
			// Recorrer todos los documentos
			for(DocumentoBean docBean:documentos) {
				// Obtener datos documento
				AccentureDocumentoRequest documento = prepararDocumento(docBean);
				docs.add(documento);
			}
		}
		
		//Asignar los archivos
		request.setArchivos(docs);
		//Obtener el detalle del expediente
		expedienteBean.getDetalle().getDetalle2().setAccenturRequest(request);
	}
	
	/**
	 * Preparar documento.
	 * Metodo para preparar datos del documento
	 *
	 * @param docBean the doc bean
	 * @return the accenture documento request
	 */
	private AccentureDocumentoRequest prepararDocumento(DocumentoBean docBean) {
		AccentureDocumentoRequest documento = new AccentureDocumentoRequest();
		AccentureDocumentoDetalleRequest detalle = new AccentureDocumentoDetalleRequest();
		documento.setDetalle(detalle);
		documento.setDocId(docBean.getIdDocumento());
		documento.setDocNombre(docBean.getDetalle().getNombreDocumento());
		documento.setChecksum("0");
		if("".equals(docBean.getDetalle2().getErrorMsg())) {
			documento.setEstado("PROCESADO_CON_EXITO");
			documento.getDetalle().setReenvio(FALSE);
		}else {
			documento.setEstado("ERROR");
			detalle.setError(docBean.getDetalle2().getErrorMsg());
			documento.getDetalle().setReenvio(getReenvio(detalle.getError()));
		}
		return documento;
	}

	/**
	 * getReenvio.
	 *
	 * @param error the error
	 * @return the reenvio
	 */
	private String getReenvio(String error) {
		String reenvio = "TRUE";
		if(error.equals(DocumentoStatusEnum.STATUS_DUPLICATE.getName())) {
			reenvio = FALSE;
		}
		return reenvio;
	}

}
