package mx.isban.flujogd.common.bean;

import java.io.Serializable;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase ReglaBean
 *
 */
public class ReglaBean implements Serializable{
	
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -6715018212202415139L;
	private Integer esFlujo;
	private Integer id;
	private Integer idSiguienteRegla;
	private BloqueBean bloqueBean;
	private ReglaBeanDetalle detalle;
	/**
	 * @return the esFlujo
	 */
	public Integer getEsFlujo() {
		return esFlujo;
	}
	/**
	 * @param esFlujo the esFlujo to set
	 */
	public void setEsFlujo(Integer esFlujo) {
		this.esFlujo = esFlujo;
	}
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the idSiguienteRegla
	 */
	public Integer getIdSiguienteRegla() {
		return idSiguienteRegla;
	}
	/**
	 * @param idSiguienteRegla the idSiguienteRegla to set
	 */
	public void setIdSiguienteRegla(Integer idSiguienteRegla) {
		this.idSiguienteRegla = idSiguienteRegla;
	}
	/**
	 * @return the bloqueBean
	 */
	public BloqueBean getBloqueBean() {
		return bloqueBean;
	}
	/**
	 * @param bloqueBean the bloqueBean to set
	 */
	public void setBloqueBean(BloqueBean bloqueBean) {
		this.bloqueBean = bloqueBean;
	}
	/**
	 * @return the detalle
	 */
	public ReglaBeanDetalle getDetalle() {
		return detalle;
	}
	/**
	 * @param detalle the detalle to set
	 */
	public void setDetalle(ReglaBeanDetalle detalle) {
		this.detalle = detalle;
	}

}
