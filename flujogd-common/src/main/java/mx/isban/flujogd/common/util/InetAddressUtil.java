package mx.isban.flujogd.common.util;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Utileria para inicializar servidores con direccion local
 * 
 * The Class InetAddressUtil.
 */
public class InetAddressUtil {
	
	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(InetAddressUtil.class);
	
	/**
	 * Obtener datos locales del servidor
	 * getLocalNet.
	 *
	 * @return the local net
	 */
	public String getLocalNet() {
		InetAddress iAddress = null;
		String ipNet = "";
		try {
			// Recuperar ip
			iAddress = InetAddress.getLocalHost();
			ipNet = iAddress.getHostAddress();
			LOG.info("IP: "+ipNet);
		} catch (UnknownHostException e) {
			LOG.error(e);
		}
		return ipNet.trim();
	}
	
}
