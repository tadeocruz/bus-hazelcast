package mx.isban.flujogd.common.bean;

import java.io.Serializable;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal AccentureDocumentoRequest
 *
 */
public class AccentureDocumentoRequest implements Serializable{
	
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 636416435729948645L;
	private String docId;
	private String docNombre;
	private String checksum;
	private String estado;
	private AccentureDocumentoDetalleRequest detalle;
	/**
	 * @return the docId
	 */
	public String getDocId() {
		return docId;
	}
	/**
	 * @param docId the docId to set
	 */
	public void setDocId(String docId) {
		this.docId = docId;
	}
	/**
	 * @return the docNombre
	 */
	public String getDocNombre() {
		return docNombre;
	}
	/**
	 * @param docNombre the docNombre to set
	 */
	public void setDocNombre(String docNombre) {
		this.docNombre = docNombre;
	}
	/**
	 * @return the checksum
	 */
	public String getChecksum() {
		return checksum;
	}
	/**
	 * @param checksum the checksum to set
	 */
	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}
	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}
	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}
	/**
	 * @return the detalle
	 */
	public AccentureDocumentoDetalleRequest getDetalle() {
		return detalle;
	}
	/**
	 * @param detalle the detalle to set
	 */
	public void setDetalle(AccentureDocumentoDetalleRequest detalle) {
		this.detalle = detalle;
	}
	
}
