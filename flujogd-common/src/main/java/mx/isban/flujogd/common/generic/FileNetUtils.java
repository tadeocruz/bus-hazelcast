/**
 * 
 */
package mx.isban.flujogd.common.generic;

import mx.isban.flujogd.common.bean.FilenetBean;
import mx.isban.flujogd.common.bean.ParametroBean;
import mx.isban.flujogd.common.util.ParametroEnum;

/**
 * Clase de utileria para parametros y conexiones de filenet.
 *
 * @author jccalderon
 * Stefanini
 * 30/07/2019
 */
public class FileNetUtils {
	
	/**
	 * Metodo para enviar los parametros de conexion para fileNet
	 * Sets the parameters file net.
	 *
	 * @param bean the bean
	 * @param filenetBean the filenet bean
	 */
	public void setParametersFileNet(ParametroBean bean, FilenetBean filenetBean) {
		if(bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_FILENET_PATH_IDC.getName())) {
			//Colocar el parametro de ruta de filenete
			filenetBean.setPathIdc(bean.getValorParametro());
		}else if(bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_FILENET_USER.getName())) {
			//Colocar el parametro de usuario de filenet
			filenetBean.setUser(bean.getValorParametro());
		}else if(bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_FILENET_URI.getName())) {
			//Colocar el parametro de uri de filenet
			filenetBean.setUri(bean.getValorParametro());
		}else if(bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_FILENET_JAAS.getName())) {
			//Colocar el parametro de jaas de filenet
			filenetBean.setJaas(bean.getValorParametro());
		}else if(bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_FILENET_STORE.getName())) {
			//Colocar el parametro de guardado de filenet
			filenetBean.getDetalle().setStore(bean.getValorParametro());
		}else if(bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_FILENET_PASSWORD.getName())) {
			//Colocar el parametro de contrasena de filenet
			filenetBean.getDetalle().setPassword(bean.getValorParametro());
		}else if(bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_FILENET_SECRET_DATA.getName())) {
			//Secret Data
			filenetBean.getDetalle().setSecretData(bean.getValorParametro());
		}
	}

}
