package mx.isban.flujogd.common.bean;

import java.io.Serializable;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal BloqueBean
 *
 */
public class BloqueBean implements Serializable{
	
	//Inicializamos un long para el serial
	private static final long serialVersionUID = -6491114716492387766L;
	
	//Inicializamos un string para guardar el id
	private String id;
	//Inicializamos un string para guardar el nombre
	private String nombre;
	//Inicializamos un string para guardar la version
	private String version;
	
	private BloqueBean2 bean2;
	
	public BloqueBean(){}
	
	/**
	 * bloquebean.
	 *
	 * @param nombre Nombre del bloque
	 * @param version Version del bloque
	 * @param numeroInstancias the numero instancias
	 * @param javaPathCacerts the java path cacerts
	 * @param javaKeyStoreP the java key store P
	 */
	public BloqueBean(String nombre,String version,String numeroInstancias,String javaPathCacerts,String javaKeyStoreP){
		this.nombre = nombre;
		this.version = version;
		this.bean2 = new BloqueBean2();
		this.bean2.setNumeroInstancias(numeroInstancias);
		this.bean2.setJavaPathCacerts(javaPathCacerts);
		this.bean2.setJavaKeyStoreP(javaKeyStoreP);
	}
	
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}
	/**
	 * @param version the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the bean2
	 */
	public BloqueBean2 getBean2() {
		return bean2;
	}

	/**
	 * @param bean2 the bean2 to set
	 */
	public void setBean2(BloqueBean2 bean2) {
		this.bean2 = bean2;
	}

}
