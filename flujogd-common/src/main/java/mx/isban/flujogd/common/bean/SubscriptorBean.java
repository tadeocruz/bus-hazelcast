package mx.isban.flujogd.common.bean;

import java.io.Serializable;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal SubscriptorBean
 *
 */
public class SubscriptorBean implements Serializable{
	
	/**
	 * serialVersionUID
	 */
	//Inicializamos un long para el serial
	private static final long serialVersionUID = -507391848237655508L;
	//Inicializamos un string para guardar el id del subscriptor
	private String idSubscriptor;
	//Inicializamos un string para guardar el nombre del subscriptor
	private String nombreSubscriptor;
	//Inicializamos un string para guardar el status de la malla activa
	private String mallaActiva;
	
	/**
	 * @return the idSubscriptor
	 */
	public String getIdSubscriptor() {
		return idSubscriptor;
	}
	/**
	 * @param idSubscriptor the idSubscriptor to set
	 */
	public void setIdSubscriptor(String idSubscriptor) {
		this.idSubscriptor = idSubscriptor;
	}
	/**
	 * @return the nombreSubscriptor
	 */
	public String getNombreSubscriptor() {
		return nombreSubscriptor;
	}
	/**
	 * @param nombreSubscriptor the nombreSubscriptor to set
	 */
	public void setNombreSubscriptor(String nombreSubscriptor) {
		this.nombreSubscriptor = nombreSubscriptor;
	}
	/**
	 * @return the mallaActiva
	 */
	public String getMallaActiva() {
		return mallaActiva;
	}
	/**
	 * @param mallaActiva the mallaActiva to set
	 */
	public void setMallaActiva(String mallaActiva) {
		this.mallaActiva = mallaActiva;
	}

}
