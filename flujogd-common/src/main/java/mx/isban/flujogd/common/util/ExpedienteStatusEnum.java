package mx.isban.flujogd.common.util;

/**
 * 
 * @author Alvaro
 *
 */
public enum ExpedienteStatusEnum {
	
	//Tipo de estatus - sin procesar
	STATUS_SIN_PROCESAR("SIN_PROCESAR"),
	//Tipo de estatus - recibido
	STATUS_RECIBIDO("RECIBIDO"),
	//Tipo de estatus - error en la recepcion
	STATUS_ERROR_RECEPCION("ERROR_RECEPCION"),
	//Tipo de estatus - archivo valido
	STATUS_ARCHIVO_VALIDO("ARCHIVO_VALIDO"),
	//Tipo de estatus - error en la validacion
	STATUS_ERROR_VALIDACION("ERROR_VALIDACION"),
	//Tipo de estatus - archivo en cm
	STATUS_ARCHIVO_IN_CM("ARCHIVO_IN_CM"),
	//Tipo de estatus - error en el archivo en cm
	STATUS_ERROR_ARCHIVO_IN_CM("ERROR_ARCHIVO_IN_CM"),
	//Tipo de estatus - proceso finalizado
	STATUS_PROCESO_FINALIZADO("PROCESO_FINALIZADO"),
	//Tipo de estatus - expediente validado
	STATUS_EXPEDIENTE_VALIDADO("EXPEDIENTE_VALIDADO"),
	//Tipo de estatus - proceso cancelado
	STATUS_PROCESO_CANCELADO("PROCESO_CANCELADO");

	//Inicializamos un string para el nombre
	private String name;

	ExpedienteStatusEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
}
