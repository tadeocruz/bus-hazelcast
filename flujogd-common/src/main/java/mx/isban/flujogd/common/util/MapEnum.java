package mx.isban.flujogd.common.util;

/**
 * 
 * @author Alvaro
 *
 */
public enum MapEnum {
	
	//Tipo de mapa - estatus del miembro
	MAP_MEMBER_STATUS("memberStatus"),
	//Tipo de mapa - expedientes en proceso
	MAP_EXPEDIENTES_IN_PROCESS("expedientesInProcess"),
	//Tipo de mapa - notificaciones en proceso
	MAP_NOTIFICACIONES_IN_PROCESS("notificacionesInProcess"),
	//Tipo de mapa - token
	MAP_TOKEN("token");
	
	//Inicializamos un string para guardar el nombre
	private String name;

	MapEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
}
