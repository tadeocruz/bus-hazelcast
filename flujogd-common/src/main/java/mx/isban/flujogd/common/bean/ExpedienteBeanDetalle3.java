package mx.isban.flujogd.common.bean;


import java.io.Serializable;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal ExpedienteBeanDetalle2
 *
 */
public class ExpedienteBeanDetalle3 implements Serializable {

	//Inicializamos un long para el serial
	private static final long serialVersionUID = 7093317025232792358L;

	//Inicializamos un string para almacenar el tipo de proceso
	private String tipoProceso;
	
	private String keyMap;
	
	private String servidorNacimiento;
	
	/** The origen. */
	private String origen;
	
	/**
	 * Obtiene el valor de tipoProceso
	 * @return the tipoProceso
	 */
	public String getTipoProceso() {
		return tipoProceso;
	}

	/**
	 * Setea el valor de tipoProceso
	 * @param tipoProceso the tipoProceso to set
	 */
	public void setTipoProceso(String tipoProceso) {
		this.tipoProceso = tipoProceso;
	}

	/**
	 * Metodo que obtiene el valor keyMap
	 * @return the keyMap
	 */
	public String getKeyMap() {
		return keyMap;
	}

	/**
	 * Setea el valor keyMap
	 * @param keyMap the keyMap to set
	 */
	public void setKeyMap(String keyMap) {
		this.keyMap = keyMap;
	}

	/**
	 * Obtiene el valor de servidorNacimiento
	 * @return the servidorNacimiento
	 */
	public String getServidorNacimiento() {
		return servidorNacimiento;
	}

	/**
	 * Setea el valor de servidorNacimiento
	 * @param servidorNacimiento the servidorNacimiento to set
	 */
	public void setServidorNacimiento(String servidorNacimiento) {
		this.servidorNacimiento = servidorNacimiento;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}
	

}