package mx.isban.flujogd.common.bean;

import java.io.Serializable;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal SFTPBeanDetalle
 *
 */
public class SFTPBeanDetalle implements Serializable{
	
	/**
	 * 
	 */
	//Inicializamos un long para el serial
	private static final long serialVersionUID = -7769331062460279510L;
	
	//Inicializamos un string para guardar la ruta fuente
	private String srcPath;
	//Inicializamos un string para guardar la ruta de salida
	private String outputPath;
	//Inicializamos un string para guardar la ruta de la llave privada
	private String pathPrivateKey;
	//Inicializamos un string para guardar la frase de segurida de la contrasena
	private String passphrase;
	
	/**
	 * @return the srcPath
	 */
	public String getSrcPath() {
		return srcPath;
	}
	/**
	 * @param srcPath the srcPath to set
	 */
	public void setSrcPath(String srcPath) {
		this.srcPath = srcPath;
	}
	/**
	 * @return the outputPath
	 */
	public String getOutputPath() {
		return outputPath;
	}
	/**
	 * @param outputPath the outputPath to set
	 */
	public void setOutputPath(String outputPath) {
		this.outputPath = outputPath;
	}
	/**
	 * @return the pathPrivateKey
	 */
	public String getPathPrivateKey() {
		return pathPrivateKey;
	}
	/**
	 * @param pathPrivateKey the pathPrivateKey to set
	 */
	public void setPathPrivateKey(String pathPrivateKey) {
		this.pathPrivateKey = pathPrivateKey;
	}
	/**
	 * @return the passphrase
	 */
	public String getPassphrase() {
		return passphrase;
	}
	/**
	 * @param passphrase the passphrase to set
	 */
	public void setPassphrase(String passphrase) {
		this.passphrase = passphrase;
	}
	
}
