/**
 * 
 */
package mx.isban.flujogd.common.generic;

import mx.isban.flujogd.common.bean.ParametroBean;
import mx.isban.flujogd.common.bean.SFTPBean;
import mx.isban.flujogd.common.util.ParametroEnum;

/**
 * Utileria para componenes Started
 * The Class StartedGenericUtils.
 *
 * @author jccalderon
 * Stefanini
 */
public class StartedGenericUtils {

	/**
	 * Obtener parametros de configuracion SFTP
	 * getValue.
	 *
	 * @param bean the bean
	 * @param sftpBean the sftp bean
	 * @return the value
	 */
	public void getValue(ParametroBean bean, SFTPBean sftpBean) {
		if(bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_SFTP_HOST.getName())) {
			sftpBean.setHost(bean.getValorParametro());
		}else if(bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_SFTP_PORT.getName())) {
			sftpBean.setPort(bean.getValorParametro());
		}else if(bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_SFTP_USER.getName())) {
			sftpBean.setUser(bean.getValorParametro());
		}else if(bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_SFTP_PASS.getName())) {
			sftpBean.setPass(bean.getValorParametro());
		}else if(bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_SFTP_SRC_PATH.getName())) {
			sftpBean.getDetalle().setSrcPath(bean.getValorParametro());
		}else if(bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_SFTP_OUTPUT_PATH.getName())) {
			sftpBean.getDetalle().setOutputPath(bean.getValorParametro());
		}else if(bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_SFTP_PATH_PRIVATE_KEY.getName())) {
			sftpBean.getDetalle().setPathPrivateKey(bean.getValorParametro());
		}else if(bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_SFTP_PASSPHRASE.getName())) {
			sftpBean.getDetalle().setPassphrase(bean.getValorParametro());
		}
	}
}
