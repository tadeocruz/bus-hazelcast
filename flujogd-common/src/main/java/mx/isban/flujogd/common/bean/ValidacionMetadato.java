package mx.isban.flujogd.common.bean;

import lombok.Getter;
import lombok.Setter;

/**
 * Clase ValidacionMetadato
 * @author DELL
 *
 */
@Getter
@Setter
public class ValidacionMetadato {
	/**
	 * Tipo de dato del que debe ser el metadato
	 */
	private String tipo;
	/**
	 * Expresión regular con la que debe de cumplir el metadato
	 */
	private String validacion;
	/**
	 * Indica si es requerido(1) o no (0)
	 */
	private boolean requerido;
	/**
	 * Nombre del metadato
	 */
	private String symbolicname;
	
	/**
	 * Default constructor
	 */
	public ValidacionMetadato() {
		
	}
	
	/**
	 * Constructor con todos los parámetros
	 * 
	 * @param tipo Tipo de dato del que debe ser el metadato
	 * @param validacion Expresión regular con la que debe de cumplir el metadato
	 * @param requerido Indica si es requerido(1) o no (0)
	 * @param symbolicname Nombre del metadato
	 */
	public ValidacionMetadato(String tipo, String validacion, boolean requerido, String symbolicname) {
		super();
		this.tipo = tipo;
		this.validacion = validacion;
		this.requerido = requerido;
		this.symbolicname = symbolicname;
	}
	
	
}
