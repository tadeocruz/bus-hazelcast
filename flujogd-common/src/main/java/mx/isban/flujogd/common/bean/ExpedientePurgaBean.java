/**
 * 
 */
package mx.isban.flujogd.common.bean;

import java.io.Serializable;

/**
 * The Class ExpedientePurgaBean.
 * Bean para los datos del expediente para la purga
 *
 * @author jccalderon - Julio Cesar Sanchez Calderon
 * Stefanini
 * 06/09/2019
 */
public class ExpedientePurgaBean implements Serializable{
 
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7340727890758255409L;
	
	/** The nombre. */
	private Integer id;
	
	/** The nombre. */
	private String nombre;
	
	/** The id bloque. */
	private String idBloque;
	
	/** The estatus bloque. */
	private String estatusBloque;
	
	/** The estatus proceso. */
	private String estatusProceso;
	
	
	/**
	 * Gets the nombre.
	 *
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	
	/**
	 * Sets the nombre.
	 *
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	/**
	 * Gets the id bloque.
	 *
	 * @return the idBloque
	 */
	public String getIdBloque() {
		return idBloque;
	}
	
	/**
	 * Sets the id bloque.
	 *
	 * @param idBloque the idBloque to set
	 */
	public void setIdBloque(String idBloque) {
		this.idBloque = idBloque;
	}
	
	/**
	 * Gets the estatus bloque.
	 *
	 * @return the estatusBloque
	 */
	public String getEstatusBloque() {
		return estatusBloque;
	}
	
	/**
	 * Sets the estatus bloque.
	 *
	 * @param estatusBloque the estatusBloque to set
	 */
	public void setEstatusBloque(String estatusBloque) {
		this.estatusBloque = estatusBloque;
	}

	/**
	 * @return the estatusProceso
	 */
	public String getEstatusProceso() {
		return estatusProceso;
	}

	/**
	 * @param estatusProceso the estatusProceso to set
	 */
	public void setEstatusProceso(String estatusProceso) {
		this.estatusProceso = estatusProceso;
	}

	/**
	 * @return Primary key del expedietne
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id Primary key del expedietne
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	
}
