package mx.isban.flujogd.common.bean;


import java.io.Serializable;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal ExpedienteBeanDetalle2
 *
 */
public class ExpedienteBeanDetalle2 implements Serializable {

	//Inicializamos un long para el serial
	private static final long serialVersionUID = 7093317025232792358L;

	//Inicializamos un string para almacenar el checksum
	private String checksum;
	private AccentureNotificationRequest accenturRequest;
	//Inicializamos un string para almacenar la referencia externa
	private String referenciaExterna;
	//Inicializamos un string para almacenar el request id
	private String requestId;
	//Inicializamos un string para almacenar el tipo de proceso
	private ExpedienteBeanDetalle3 detalle3;
	
	/**
	 * @return the checksum
	 */
	public String getChecksum() {
		return checksum;
	}
	/**
	 * @param checksum the checksum to set
	 */
	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}
	/**
	 * @return the accenturRequest
	 */
	public AccentureNotificationRequest getAccenturRequest() {
		return accenturRequest;
	}
	/**
	 * @param accenturRequest the accenturRequest to set
	 */
	public void setAccenturRequest(AccentureNotificationRequest accenturRequest) {
		this.accenturRequest = accenturRequest;
	}
	/**
	 * @return the referenciaExterna
	 */
	public String getReferenciaExterna() {
		return referenciaExterna;
	}
	/**
	 * @param referenciaExterna the referenciaExterna to set
	 */
	public void setReferenciaExterna(String referenciaExterna) {
		this.referenciaExterna = referenciaExterna;
	}
	/**
	 * @return the requestId
	 */
	public String getRequestId() {
		return requestId;
	}
	/**
	 * @param requestId the requestId to set
	 */
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	/**
	 * @return the detalle3
	 */
	public ExpedienteBeanDetalle3 getDetalle3() {
		return detalle3;
	}
	/**
	 * @param detalle3 the detalle3 to set
	 */
	public void setDetalle3(ExpedienteBeanDetalle3 detalle3) {
		this.detalle3 = detalle3;
	}

}