/**
 * 
 */
package mx.isban.flujogd.common.bean;

import java.io.Serializable;

/**
 * The Class ConexionSFTPBean.
 * Objeto para los datos de conexion al sftp
 *
 * @author jccalderon - Julio Cesar Sanchez Calderon
 * Stefanini
 * 06/09/2019
 */
public class ConexionSFTPBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2557891885930973997L;

	/** The server. */
	private String server;
	
	/** The puerto. */
	private String puerto;
	
	/** The user. */
	private String user;
	
	/** The pass. */
	private String pass;
	
	/** The rutas. */
	private RutaServerBean rutas;
	
	/**
	 * Gets the server.
	 *
	 * @return the server
	 */
	public String getServer() {
		return server;
	}
	
	/**
	 * Sets the server.
	 *
	 * @param server the server to set
	 */
	public void setServer(String server) {
		this.server = server;
	}
	
	/**
	 * Gets the puerto.
	 *
	 * @return the puerto
	 */
	public String getPuerto() {
		return puerto;
	}
	
	/**
	 * Sets the puerto.
	 *
	 * @param puerto the puerto to set
	 */
	public void setPuerto(String puerto) {
		this.puerto = puerto;
	}
	
	/**
	 * Gets the user.
	 *
	 * @return the user
	 */
	public String getUser() {
		return user;
	}
	
	/**
	 * Sets the user.
	 *
	 * @param user the user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}
	
	/**
	 * Gets the pass.
	 *
	 * @return the pass
	 */
	public String getPass() {
		return pass;
	}
	
	/**
	 * Sets the pass.
	 *
	 * @param pass the pass to set
	 */
	public void setPass(String pass) {
		this.pass = pass;
	}
	
	/**
	 * Gets the rutas.
	 *
	 * @return the rutas
	 */
	public RutaServerBean getRutas() {
		return rutas;
	}
	
	/**
	 * Sets the rutas.
	 *
	 * @param rutas the rutas to set
	 */
	public void setRutas(RutaServerBean rutas) {
		this.rutas = rutas;
	}
	
	
}
