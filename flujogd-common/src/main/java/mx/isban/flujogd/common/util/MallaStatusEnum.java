package mx.isban.flujogd.common.util;

/**
 * 
 * @author Alvaro
 *
 */
public enum MallaStatusEnum {
	
	//Tipo de estatus de la malla - inactiva
	MALLA_STATUS_INACTIVA("0"),
	//Tipo de estatus de la malla - activa
	MALLA_STATUS_ACTIVA("1");
	
	//Inicializamos un string para guardar el nombre
	private String name;

	MallaStatusEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
}
