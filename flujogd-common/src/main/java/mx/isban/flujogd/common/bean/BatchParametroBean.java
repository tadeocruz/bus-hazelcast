package mx.isban.flujogd.common.bean;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * Clase principal BatchParametroBean
 * 
 * @author Alvaro Zamorano azamorano@serviciosexternos.isban.mx
 * 
 */
@Getter
@Setter
public class BatchParametroBean implements Serializable {

	/**
	 * Inicializamos un long para guardar el serial
	 */
	private static final long serialVersionUID = 8198653481401885462L;

	/**
	 * Inicializamos un string para guardar el nombre del parametro
	 */
	private String nombreParametro;

	/**
	 * Inicializamos un string para guardar el valor del parametro
	 */
	private String valorParametro;

	/**
	 * Inicializamos un string para guardar si el parametro esta encriptado o no
	 */
	private String estaEncriptado;
	
	/**
	 * Método toString()
	 */
	@Override
	public String toString() {
		return "BatchParametroBean [nombreParametro=" + nombreParametro + ", valorParametro=" + valorParametro + ", estaEncriptado=" + estaEncriptado + "]";
	}

}
