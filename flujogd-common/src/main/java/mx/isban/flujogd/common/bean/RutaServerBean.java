/**
 * 
 */
package mx.isban.flujogd.common.bean;

import java.io.Serializable;

/**
 * The Class RutaServerBean.
 * Objeto para las rutas de trabajo del servidor
 *
 * @author jccalderon - Julio Cesar Sanchez Calderon
 * Stefanini
 * 06/09/2019
 */
public class RutaServerBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7882664430710295539L;
	
	/** The path stfp src. */
	private String pathStfpSrc;
	
	/** The path stfp out. */
	private String pathStfpOut;
	
	/** The path unzip. */
	private String pathUnzip;
	
	/** The path priv key. */
	private String pathPrivKey;
	
	/** The sftp pass phrase. */
	private String sftpPassPhrase;
	
	
	/**
	 * Gets the path stfp src.
	 *
	 * @return the pathStfpSrc
	 */
	public String getPathStfpSrc() {
		return pathStfpSrc;
	}
	
	/**
	 * Sets the path stfp src.
	 *
	 * @param pathStfpSrc the pathStfpSrc to set
	 */
	public void setPathStfpSrc(String pathStfpSrc) {
		this.pathStfpSrc = pathStfpSrc;
	}
	
	/**
	 * Gets the path unzip.
	 *
	 * @return the pathUnzip
	 */
	public String getPathUnzip() {
		return pathUnzip;
	}
	
	/**
	 * Sets the path unzip.
	 *
	 * @param pathUnzip the pathUnzip to set
	 */
	public void setPathUnzip(String pathUnzip) {
		this.pathUnzip = pathUnzip;
	}

	/**
	 * @return the pathStfpOut
	 */
	public String getPathStfpOut() {
		return pathStfpOut;
	}

	/**
	 * @param pathStfpOut the pathStfpOut to set
	 */
	public void setPathStfpOut(String pathStfpOut) {
		this.pathStfpOut = pathStfpOut;
	}

	/**
	 * @return the pathPrivKey
	 */
	public String getPathPrivKey() {
		return pathPrivKey;
	}

	/**
	 * @param pathPrivKey the pathPrivKey to set
	 */
	public void setPathPrivKey(String pathPrivKey) {
		this.pathPrivKey = pathPrivKey;
	}

	/**
	 * @return the sftpPassPhrase
	 */
	public String getSftpPassPhrase() {
		return sftpPassPhrase;
	}

	/**
	 * @param sftpPassPhrase the sftpPassPhrase to set
	 */
	public void setSftpPassPhrase(String sftpPassPhrase) {
		this.sftpPassPhrase = sftpPassPhrase;
	}
	
	
}
