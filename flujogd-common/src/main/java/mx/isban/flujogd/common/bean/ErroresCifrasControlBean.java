/**
 * 
 */
package mx.isban.flujogd.common.bean;

import java.io.Serializable;

/**
 * The Class CifrasControlTask.
 * Objeto que contendra el numero de errores para las cifras de control
 *
 * @author jccalderon
 * Stefanini
 * 01/08/2019
 */
public class ErroresCifrasControlBean implements Serializable{
	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The num registros. */
	private int numRegistros;
	
	/** The num error recepcion. */
	private int numErrorRecepcion;
	
	/** The num error validacion. */
	private int numErrorValidacion;
	
	/** The num archivo INCM. */
	private int numArchivoINCM;
	
	/** The aux. */
	private ErroresCifrasControlAuxBean aux;
	/**
	 * Gets the num registros.
	 *
	 * @return the numRegistros
	 */
	public int getNumRegistros() {
		return numRegistros;
	}
	
	/**
	 * Sets the num registros.
	 *
	 * @param numRegistros the numRegistros to set
	 */
	public void setNumRegistros(int numRegistros) {
		this.numRegistros = numRegistros;
	}
	
	/**
	 * Gets the num error recepcion.
	 *
	 * @return the numErrorRecepcion
	 */
	public int getNumErrorRecepcion() {
		return numErrorRecepcion;
	}
	
	/**
	 * Sets the num error recepcion.
	 *
	 * @param numErrorRecepcion the numErrorRecepcion to set
	 */
	public void setNumErrorRecepcion(int numErrorRecepcion) {
		this.numErrorRecepcion = numErrorRecepcion;
	}
	
	/**
	 * Gets the num error validacion.
	 *
	 * @return the numErrorValidacion
	 */
	public int getNumErrorValidacion() {
		return numErrorValidacion;
	}
	
	/**
	 * Sets the num error validacion.
	 *
	 * @param numErrorValidacion the numErrorValidacion to set
	 */
	public void setNumErrorValidacion(int numErrorValidacion) {
		this.numErrorValidacion = numErrorValidacion;
	}
	
	/**
	 * Gets the num archivo INCM.
	 *
	 * @return the numArchivoINCM
	 */
	public int getNumArchivoINCM() {
		return numArchivoINCM;
	}
	
	/**
	 * Sets the num archivo INCM.
	 *
	 * @param numArchivoINCM the numArchivoINCM to set
	 */
	public void setNumArchivoINCM(int numArchivoINCM) {
		this.numArchivoINCM = numArchivoINCM;
	}

	/**
	 * @return the aux
	 */
	public ErroresCifrasControlAuxBean getAux() {
		return aux;
	}

	/**
	 * @param aux the aux to set
	 */
	public void setAux(ErroresCifrasControlAuxBean aux) {
		this.aux = aux;
	}	
	
}
