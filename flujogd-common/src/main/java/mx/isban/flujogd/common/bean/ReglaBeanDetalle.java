package mx.isban.flujogd.common.bean;

import java.io.Serializable;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase ReglaBeanDetalle
 *
 */
public class ReglaBeanDetalle implements Serializable{
	
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -6715018212202415139L;

	private Integer estatus;
	private Integer intentos;
	private Integer numeroInstancias;
	/**
	 * @return the estatus
	 */
	public Integer getEstatus() {
		return estatus;
	}
	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}
	/**
	 * @return the intentos
	 */
	public Integer getIntentos() {
		return intentos;
	}
	/**
	 * @param intentos the intentos to set
	 */
	public void setIntentos(Integer intentos) {
		this.intentos = intentos;
	}
	/**
	 * @return the numeroInstancias
	 */
	public Integer getNumeroInstancias() {
		return numeroInstancias;
	}
	/**
	 * @param numeroInstancias the numeroInstancias to set
	 */
	public void setNumeroInstancias(Integer numeroInstancias) {
		this.numeroInstancias = numeroInstancias;
	}
	
}
