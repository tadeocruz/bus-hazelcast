package mx.isban.flujogd.common.util;

/**
 * 
 * @author Alvaro
 *
 */
public enum ExpedienteErrorEnum {
	
	//Tipo de error - conexion con el sftp
	ERROR_SFTP_CONEXION("SFTP_CONEXION"),
	//Tipo de error - archivo no exite en el sftp
	ERROR_SFTP_ARCH_NO_EXISTE("SFTP_ARCH_NO_EXISTE"),
	//Tipo de error - checksum
	ERROR_CHECKSUM("ERROR EN CHECKSUM"),
	//Tipo de error - descomprimido
	ERROR_UNZIP("UNZIP"),
	//Tipo de error - permisos insuficientes
	ERROR_PERMISOS_INSU("PERMISOS_INSU"),
	//Tipo de error - archivo corrupto
	ERROR_ARCHIVO_CORRUPTO("ARCHIVO_CORRUPTO"),
	//Tipo de error - paridad del archivo
	ERROR_ARCHIVO_PARIDAD("ARCHIVO_PARIDAD"),
	//Tipo de error - formato invalido
	ERROR_FORMATO_INVALIDO("FORMATO_INVALIDO"),
	//Tipo de error - xml del expediente no existe
	ERROR_XML_EXP_NO_EXISTE("XML_EXP_NO_EXISTE"),
	//Tipo de error - xml del documento no existe
	ERROR_XML_DOC_NO_EXISTE("XML_DOC_NO_EXISTE"),
	//Tipo de error - guardar la gdocumental
	ERROR_GDOCUMENTAL_GUARDAR("GDOCUMENTAL_GUARDAR"),
	//Tipo de error - conexion con la gdocumental
	ERROR_GDOCUMENTAL_CONEXION("GDOCUMENTAL_CONEXION"),
	//Tipo de error - error en el parseo del xml
	ERROR_XML_ERROR_PARSEO("XML_ERROR_PARSEO"),
	//Tipo de error - expediente duplicado
	ERROR_EXPEDIENTE_DUPLICADO("EXPEDIENTE_DUPLICADO"),
	ERROR_EXPEDIENTE_TAMANO_MAX("EXPEDIENTE_TAM_MAX"),
	ERROR_EXPEDIENTE_DB_CONEXION("EXPEDIENTE_DB_CONEXION");
	
	//Inicializamos un string para guardar el nombre
	private String name;

	ExpedienteErrorEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
}
