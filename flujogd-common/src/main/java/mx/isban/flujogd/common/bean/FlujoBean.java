package mx.isban.flujogd.common.bean;

import java.io.Serializable;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal FlujoBean
 *
 */
public class FlujoBean implements Serializable{
	
	/**
	 * serialVersionUID
	 */
	//Inicializamos un long para el serial
	private static final long serialVersionUID = 9136408065087071518L;

	//Inicializamos un string para guardar la cola
	private String queue;
	//Inicializamos un boolean para guardar el status de la cola
	private boolean procesada;
	//Inicializamos un string para guardar la version
	private String version;

	/**
	 * Instantiates a new flujo bean.
	 */
	public FlujoBean() {
	}
	
	/**
	 * flujobean
	 * @param queue	Cola
	 */
	public FlujoBean(String queue) {
		this.queue = queue;
	}
	
	
	/**
	 * @return the queue
	 */
	public String getQueue() {
		return queue;
	}
	/**
	 * @param queue the queue to set
	 */
	public void setQueue(String queue) {
		this.queue = queue;
	}
	/**
	 * @return the procesada
	 */
	public boolean isProcesada() {
		return procesada;
	}
	/**
	 * @param procesada the procesada to set
	 */
	public void setProcesada(boolean procesada) {
		this.procesada = procesada;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FlujoBean [queue=" + queue + ", procesada=" + procesada + "]";
	}

	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}

}
