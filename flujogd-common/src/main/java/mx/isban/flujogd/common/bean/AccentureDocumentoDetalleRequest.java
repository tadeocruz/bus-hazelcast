package mx.isban.flujogd.common.bean;

import java.io.Serializable;

/**
 * The Class AccentureDocumentoDetalleRequest.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal AccentureDocumentoDetalleRequest
 */
public class AccentureDocumentoDetalleRequest implements Serializable{
	
	/** serialVersionUID. */
	private static final long serialVersionUID = 636416435729948645L;
	
	/** The error id. */
	private String errorId;
	
	/** The error. */
	private String error;
	
	/** The reenvio. */
	private String reenvio;
	
	/**
	 * Gets the error id.
	 *
	 * @return the errorId
	 */
	public String getErrorId() {
		return errorId;
	}
	
	/**
	 * Sets the error id.
	 *
	 * @param errorId the errorId to set
	 */
	public void setErrorId(String errorId) {
		this.errorId = errorId;
	}
	
	/**
	 * Gets the error.
	 *
	 * @return the error
	 */
	public String getError() {
		return error;
	}
	
	/**
	 * Sets the error.
	 *
	 * @param error the error to set
	 */
	public void setError(String error) {
		this.error = error;
	}
	
	/**
	 * Gets the reenvio.
	 *
	 * @return the reenvio
	 */
	public String getReenvio() {
		return reenvio;
	}
	
	/**
	 * Sets the reenvio.
	 *
	 * @param reenvio the reenvio to set
	 */
	public void setReenvio(String reenvio) {
		this.reenvio = reenvio;
	}
	
}
