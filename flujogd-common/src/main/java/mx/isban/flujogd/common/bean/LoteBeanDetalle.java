package mx.isban.flujogd.common.bean;

import java.io.Serializable;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal LoteBeanDetalle
 *
 */
public class LoteBeanDetalle implements Serializable{
	
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 8524154215191695542L;
	
	private String fechaCreacion;
	private String fechaActualizacion;
	private String bucs;
	private String path;
	/**
	 * @return the fechaCreacion
	 */
	public String getFechaCreacion() {
		return fechaCreacion;
	}
	/**
	 * @param fechaCreacion the fechaCreacion to set
	 */
	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	/**
	 * @return the fechaActualizacion
	 */
	public String getFechaActualizacion() {
		return fechaActualizacion;
	}
	/**
	 * @param fechaActualizacion the fechaActualizacion to set
	 */
	public void setFechaActualizacion(String fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}
	/**
	 * @return the bucs
	 */
	public String getBucs() {
		return bucs;
	}
	/**
	 * @param bucs the bucs to set
	 */
	public void setBucs(String bucs) {
		this.bucs = bucs;
	}
	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}
	/**
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}
	
}
