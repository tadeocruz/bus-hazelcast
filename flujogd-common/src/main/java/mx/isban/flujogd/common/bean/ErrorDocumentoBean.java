package mx.isban.flujogd.common.bean;

import java.io.Serializable;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

/**
 * ErrorDocumentoBean es una clase que tiene como finalidad almacenar los
 * valores necesarios para informar un error en la validación de documentos en
 * caso de que lo exista
 * 
 * @author DELL
 *
 */
@Getter
@Setter
public class ErrorDocumentoBean implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 4751912756896050091L;

	/**
	 * nombre del documento al que está relacionado el error
	 */
	private String documento;
	/**
	 * error relacionado al documento
	 */
	private String error;

	/**
	 * Mapa con los errores relacionados con los metadatos de un documento, teniendo
	 * como valor nombre metadato, error correspondiente
	 */
	private Map<String, String> metadatosErrores;

	/**
	 * Constructor con parametros para errores relacionados con los documentos
	 * únicamente
	 * 
	 * @param documento  nombre del documento al que está relacionado el error
	 * @param error      error relacionado al documento
	 */
	public ErrorDocumentoBean(String documento, String error) {
		super();
		this.documento = documento;
		this.error = error;
	}

	/**
	 * Constructor con todas las propiedades
	 * 
	 * @param documento        nombre del documento al que está relacionado el error
	 * @param error            error relacionado al documento
	 * @param metadatosErrores errores de metadatos correspondientes al documento
	 */
	public ErrorDocumentoBean(String documento, String error, Map<String, String> metadatosErrores) {
		super();
		this.documento = documento;
		this.error = error;
		this.metadatosErrores = metadatosErrores;
	}

}
