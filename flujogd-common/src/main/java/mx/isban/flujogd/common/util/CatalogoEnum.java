package mx.isban.flujogd.common.util;

/**
 * 
 * @author Alvaro
 *
 */
public enum CatalogoEnum {
	//Tipo de catalogo - aplicativo
	CAT_APLICATIVO("OPT_MX_MAE_CAT_APP"),
	//Tipo de catalogo - bloque
	CAT_BLOQUE("OPT_MX_MAE_CAT_BLOQ"),
	//Tipo de catalogo - documento
	CAT_DOCUMENTO("OPT_MX_MAE_CAT_DOC"),
	//Tipo de catalogo - error
	CAT_ERROR("OPT_MX_MAE_CAT_ERROR"),
	//Tipo de catalogo - bloque
	CAT_ESTATUS_BLOQUE("OPT_MX_MAE_CAT_ESTAT_BLOQ"),
	//Tipo de catalogo - estatus del documento
	CAT_ESTATUS_DOCUMENTO("OPT_MX_MAE_CAT_ESTAT_DOC"),
	//Tipo de catalogo - estatus del expediente
	CAT_ESTATUS_EXPEDIENTE("OPT_MX_MAE_CAT_ESTAT_EXP"),
	//Tipo de catalogo - estatus de la notificacion
	CAT_ESTATUS_NOTIFICACION("OPT_MX_MAE_CAT_ESTAT_NOTI"),
	//Tipo de catalogo - protocolo del destino
	CAT_PROTOCOLO_DESTINO("OPT_MX_MAE_CAT_PROTO_DEST"),
	//Tipo de catalogo - protocolo del origen
	CAT_PROTOCOLO_ORIGEN("OPT_MX_MAE_CAT_PROTO_ORIG"),
	//Tipo de catalogo - tipo de proceso del expediente
	CAT_TIPO_PROCESO_EXPEDIENTE("OPT_MX_MAE_CAT_TIPO_PROCE_EXP"),
	
	CAT_ESTATUS_LOTE("OPT_MX_MAE_LOTE_EST_CAT"),
	
	CAT_ESTATUS_DET_LOTE("OPT_MX_MAE_LOTE_DET_CAT");
	
	//Inicializamos un string para guardar el nombre
	private String name;

	CatalogoEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
}
