package mx.isban.flujogd.common.bean;

import java.io.Serializable;

/**
 * The Class SftpResponseBean.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase Bean
 * Clase utilizada para transporte de informacion
 */
public class SftpResponseBean implements Serializable{
	
	/** serialVersionUID. */
	private static final long serialVersionUID = -6446625337975343334L;

	/** The error message. */
	private String errorMessage;

	/** The ok. */
	private boolean ok;
	
	
	/** The error code. */
	private String errorCode;
	
	/**
	 * Constructor.
	 *
	 * @param ok the ok
	 */
	public SftpResponseBean(boolean ok) {
		this.ok = ok;
	}
	
	/**
	 * Checks if is ok.
	 *
	 * @return the ok
	 */
	public boolean isOk() {
		return ok;
	}
	
	
	/**
	 * Gets the error message.
	 *
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}
	
	/**
	 * Sets the ok.
	 *
	 * @param ok the ok to set
	 */
	public void setOk(boolean ok) {
		this.ok = ok;
	}
	
	/**
	 * Gets the error code.
	 *
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return errorCode;
	}
	
	/**
	 * Sets the error message.
	 *
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	
	/**
	 * Sets the error code.
	 *
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

}
