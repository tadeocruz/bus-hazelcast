package mx.isban.flujogd.common.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import mx.isban.flujogd.common.bean.AtributoBean;
import mx.isban.flujogd.common.bean.DocumentoBean;
import mx.isban.flujogd.common.bean.DocumentoBeanDetalle;
import mx.isban.flujogd.common.bean.DocumentoBeanDetalle2;
import mx.isban.flujogd.common.bean.FolderBean;
import mx.isban.flujogd.common.bean.FolderBeanDetalle;
import mx.isban.flujogd.common.bean.XMLManagerResponse;

/**
 * The Class XMLManager.
 *
 * @author Alvaro Zamorano azamorano@serviciosexternos.isban.mx
 * Clase XMLManager
 */
public class XMLManager {

	// Inicializamos un log para la bitacora
	private static final Logger LOG = LogManager.getLogger(XMLManager.class);
	
	// Inicializamos un string para guardar el tipo de dato
	private static final String TIPO_DATO = "tipoDato";
	
	// Inicializamos un string para guardar el nivel 1
	private static final String NIVEL_1 = "FolderClienteSantander";
	
	// Inicializamos un string para guardar el nivel 2
	private static final String NIVEL_2 = "Familiadeproductos";
	
	// Inicializamos un string para guardar el nivel 3
	private static final String NIVEL_3 = "Producto";
	
	// Inicializamos un string para guardar el nivel 4
	private static final String NIVEL_4 = "Servicio";
	
	// Inicializamos un string para guardar el atributo
	private static final String ATRIBUTO = "atributo";
	
	// Inicializamos un string con el atributo ISO-8859-1
	private static final String ISO88591 = "ISO-8859-1"; 


	/**
	 * Recupera los valores de un xml.
	 *
	 * @param xml         - El xml ha ser parseado
	 * @param rootName    - En donde se comienza a buscar el elemento
	 * @param elementName - El elemento que se esta buscando
	 * @return Una de lista de elementos encontrados bajo el nodo root
	 */
	public XMLManagerResponse getValuesElement(String xml, String rootName, String elementName) {
		// Inicializamos un xml manager response
		XMLManagerResponse response = new XMLManagerResponse();
		// Inicializamos un boolean para guardar el estatus de la respuesta
		boolean successElement = false;
		// Inicializamos una lista para guardar los valores
		List<String> values = null;
		// Inicializamos un document builder factory
		DocumentBuilderFactory factory = null;
		// Inicializamos un document builder
		DocumentBuilder builder = null;
		// Inicializamos un document
		Document document = null;
		// Inicializamos una lista de nodos
		NodeList nodeList = null;
		// Inicializamos un input source
		InputSource is = null;
		try {
			values = new ArrayList<>();
			// Obtener una nueva instancia del document builder factory
			factory = DocumentBuilderFactory.newInstance();
			// Crear un builder a partir de la instancia
			builder = factory.newDocumentBuilder();
			is = new InputSource();
			// codificacion Utf8

			// Colocar el xml en el flujo de caracteres
			is.setCharacterStream(new StringReader(xml));
			is.setEncoding(ISO88591);
			// Parsear el flujo de caracteres
			document = builder.parse(is);
			// Obtener el nombre base
			// Guardar el nombre en la lista de nodos
			nodeList = document.getElementsByTagName(rootName);
			for (int i = 0; i < nodeList.getLength(); i++) {
				// Crear un nodo para guardar un item de la lista
				// Tomar un item de la lista de nodos
				Node node = nodeList.item(i);
				// Crear un mapa de nodos
				// Tomar los atrbutos del nodo
				NamedNodeMap maps = node.getAttributes();
				// Crear un nodo para guardar el nombre del elemento del mapa
				// Tomar el nombre del elemento del mapa
				Node nodeElement = maps.getNamedItem(elementName);
				// Crear un string para almacenar el valor del elemento
				String valueElement = nodeElement.getNodeValue();
				// Agregar el valor a la lista de valores
				values.add(valueElement);
			}
			successElement = true;
			// Mostrar errores de parseo del XML
		} catch (ParserConfigurationException e) {
			LOG.error(e);
			response.setErrorMsg(ExpedienteErrorEnum.ERROR_XML_ERROR_PARSEO.getName());
		} catch (SAXException e) {
			LOG.error(e);
			response.setErrorMsg(ExpedienteErrorEnum.ERROR_XML_ERROR_PARSEO.getName());
		} catch (IOException e) {
			LOG.error(e);
			response.setErrorMsg(ExpedienteErrorEnum.ERROR_XML_ERROR_PARSEO.getName());
		}
		// Colocar los valores en la respuesta
		response.setValues(values);
		// Colocar el estatus de la respuesta
		response.setSuccess(successElement);
		// Regresar la respuesta
		return response;
	}

	/**
	 * Recupera los documentos de un XML.
	 *
	 * @param path     Ruta del documento
	 * @param nameFile Nombre del documento
	 * @return Documentos generados y su estatus
	 */
	public XMLManagerResponse getDocumentos(String path, String nameFile) {
		// Inicializamos un xml manager response
		XMLManagerResponse response = new XMLManagerResponse();
		// Inicializamos un boolean para guardar el estatus de la respuesta
		boolean success = false;
		// Inicializamos una lista para guardar los documentos
		List<DocumentoBean> documentos = null;
		// Inicializamos un document builder factory
		DocumentBuilderFactory factory = null;
		// Inicializamos un document builder
		DocumentBuilder builder = null;
		// Inicializamos un document
		Document document = null;
		// Inicializamos una lista de nodos
		NodeList nodeList = null;
		// Inicializamos un input source
		InputSource is = null;
		// Inicializamos un string para guardar el xml
		String xml = null;
		try {
			documentos = new ArrayList<DocumentoBean>();
			// Obtener el contenido del xml
			xml = getContenidoXML(path + File.separator + nameFile);
			// Crear una nueva instancia del document builder factory
			factory = DocumentBuilderFactory.newInstance();
			// Crear un builder a partir de la instancia
			builder = factory.newDocumentBuilder();
			is = new InputSource();

			// Colocar el xml en un flujo de caracteres
			is.setCharacterStream(new StringReader(xml));
			// codificacion ISO-8859-1
			is.setEncoding(ISO88591);
			// Parsear el flujo de caracteres
			document = builder.parse(is);
			// Obtener la ruta
			// Guardar la ruta en la lista de nodos
			nodeList = document.getElementsByTagName("path");
			for (int i = 0; i < nodeList.getLength(); i++) {
				// Crear un nodo para guardar un item de la lista
				// Tomar un item de la lista de nodos
				Node node = nodeList.item(i);
				DocumentoBean bean = new DocumentoBean();
				DocumentoBeanDetalle detalle = new DocumentoBeanDetalle();
				DocumentoBeanDetalle2 detalle2 = new DocumentoBeanDetalle2();
				// Colocar el primer detalle del bean
				bean.setDetalle(detalle);
				// Colocar el segundo detalle del bean
				bean.setDetalle2(detalle2);
				// Colocar la ruta del detalle
				bean.getDetalle().setPath(node.getFirstChild().getNodeValue());
				// Crear un nodo para guardar el nodo padre
				// Tomar el nodo padre
				Node par = node.getParentNode();
				// Crear un mapa de nodos
				// Tomar los atributos del nodo padre
				NamedNodeMap map2 = par.getAttributes();
				bean.setIdDocumento(map2.getNamedItem("id").getNodeValue());
				// Colocar el detalle del mapa en el bean
				bean.getDetalle().setNombreDocumento(map2.getNamedItem("binario").getNodeValue());
				bean.getDetalle().setMetadato(map2.getNamedItem("metadato").getNodeValue());
				bean.getDetalle().setExtension(bean.getDetalle().getNombreDocumento()
						.substring(bean.getDetalle().getNombreDocumento().length() - 3));
				// Recuperamos los atributos del documento
				bean.getDetalle().setAtributos(getAtributos(path, bean.getDetalle().getMetadato()));
				// Agregar el bean a la lista de documentos
				documentos.add(bean);
			}
			success = true;
			// Mostrar errores de parseo del xml
		} catch (ParserConfigurationException e) {
			LOG.error(e);
			response.setErrorMsg(ExpedienteErrorEnum.ERROR_XML_ERROR_PARSEO.getName());
		} catch (SAXException e) {
			LOG.error(e);
			response.setErrorMsg(ExpedienteErrorEnum.ERROR_XML_ERROR_PARSEO.getName());
		} catch (FileNotFoundException e) {
			LOG.error("No se encontro el archivo :: " + nameFile + " Cause :: ",  e);
			response.setErrorMsg(ExpedienteErrorEnum.ERROR_XML_EXP_NO_EXISTE.getName());
		} catch (IOException e) {
			LOG.error(e);
			response.setErrorMsg(ExpedienteErrorEnum.ERROR_XML_ERROR_PARSEO.getName());
		}
		// Colocar el estatus de la respuesta
		response.setSuccess(success);
		// Colocar los documentos de la respuesta
		response.setDocumentos(documentos);
		// Regresar la respuesta
		return response;
	}

	/**
	 * Recupera los folders de un documento.
	 *
	 * @param path     Ruta
	 * @param nameFile Nombre del archivo
	 * @return Folders generados y su estatus
	 */
	public XMLManagerResponse getFolders(String path, String nameFile) {
		XMLManagerResponse response = new XMLManagerResponse();
		boolean success = false;
		Map<String, FolderBean> mapa = new TreeMap<>();
		DocumentBuilderFactory factory = null;
		DocumentBuilder builder = null;
		Document document = null;
		NodeList nodeList = null;
		InputSource is = null;
		String xml = null;
		String pathFilenet = "";
		try {
			xml = getContenidoXML(path + File.separator + nameFile);
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			is = new InputSource();

			is.setCharacterStream(new StringReader(xml));
			// codificacion ISO-8859-1
			is.setEncoding(ISO88591);
			document = builder.parse(is);
			nodeList = document.getElementsByTagName(ATRIBUTO);
			for (int i = 0; i < nodeList.getLength(); i++) {
				pathFilenet = validateNode(nodeList.item(i), mapa, pathFilenet);
			}
			success = true;
		} catch (ParserConfigurationException e) {
			LOG.error(e);
			response.setErrorMsg(ExpedienteErrorEnum.ERROR_XML_ERROR_PARSEO.getName());
		} catch (SAXException e) {
			LOG.error(e);
			response.setErrorMsg(ExpedienteErrorEnum.ERROR_XML_ERROR_PARSEO.getName());
		} catch (IOException e) {
			LOG.error(e);
			response.setErrorMsg(ExpedienteErrorEnum.ERROR_XML_ERROR_PARSEO.getName());
		}
		response.setSuccess(success);
		response.setFolders(new ArrayList<FolderBean>(mapa.values()));
		return response;
	}

	/**
	 * Valida si el nodo ya existe.
	 *
	 * @param node        Nodo
	 * @param mapa        Mapa de folders
	 * @param pathFilenet Ruta de filenet
	 * @return La ruta de filenet validada
	 * @throws UnsupportedEncodingException the unsupported encoding exception
	 * @throws DOMException the DOM exception
	 */
	private String validateNode(Node node, Map<String, FolderBean> mapa, String pathFilenet) {
		Node parent = node.getParentNode();
		NamedNodeMap map2 = parent.getAttributes();
		String id = map2.getNamedItem("id").getNodeValue();
		FolderBean folderBean = mapa.get(id);
		String value8859 = "";
		
		if (folderBean == null) {
			// Verificamos si es un subfolder
			String referencia = map2.getNamedItem("referencia").getNodeValue();
			FolderBean folderParent = mapa.get(referencia);
			String pathParent = "";
			if (folderParent != null) {
				pathParent = folderParent.getDetalle().getPath() + File.separator;
			}
			folderBean = new FolderBean();
			folderBean.setReferencia(referencia);
			folderBean.setTipo(map2.getNamedItem("tipo").getNodeValue());
			folderBean.setNombre(map2.getNamedItem("nombre").getNodeValue());
			folderBean.setId(id);
			FolderBeanDetalle detalle = new FolderBeanDetalle();
			folderBean.setDetalle(detalle);
			folderBean.getDetalle().setPath(pathParent + folderBean.getNombre());
			AtributoBean beanNode = new AtributoBean();
			NamedNodeMap map = node.getAttributes();
			beanNode.setNombre(map.getNamedItem("id").getNodeValue());
			beanNode.setTipo(map.getNamedItem(TIPO_DATO).getNodeValue());
			Node nodeValue = node.getFirstChild();
			
			if (nodeValue != null) {
				value8859 = nodeValue.getNodeValue();
			}
			beanNode.setValor(value8859);
			pathFilenet = validarNivel(folderBean, beanNode, pathFilenet);
			folderBean.getDetalle().addAtributo(beanNode);
			mapa.put(id, folderBean);
		} else {
			AtributoBean bean = new AtributoBean();
			NamedNodeMap map = node.getAttributes();
			bean.setNombre(map.getNamedItem("id").getNodeValue());
			bean.setTipo(map.getNamedItem(TIPO_DATO).getNodeValue());
			Node nodeValue = node.getFirstChild();
			
			if (nodeValue != null) {
				value8859 = nodeValue.getNodeValue();
				
			}
			bean.setValor(value8859);
			pathFilenet = validarNivel(folderBean, bean, pathFilenet);
			folderBean.getDetalle().addAtributo(bean);
		}
		return pathFilenet;
	}

	/**
	 * validarNivel.
	 *
	 * @param folderBean  Vaina del folder
	 * @param bean        Atributos del bean
	 * @param pathFilenet Ruta de filenet
	 * @return La ruta de filenet validada
	 */
	private String validarNivel(FolderBean folderBean, AtributoBean bean, String pathFilenet) {
		if (NIVEL_1.equals(folderBean.getTipo()) && "BUC".equals(bean.getNombre())) {
				folderBean.getDetalle().setPathFilenet(File.separator);
				pathFilenet = File.separator + bean.getValor() + File.separator;
		} else if (NIVEL_2.equals(folderBean.getTipo()) && "IDFamilia".equals(bean.getNombre())) {
				folderBean.getDetalle().setPathFilenet(pathFilenet);
				pathFilenet = pathFilenet + formatString(2, bean.getValor(), true) + File.separator;
		} else if (NIVEL_3.equals(folderBean.getTipo()) && "IdProducto".equals(bean.getNombre())) {
				folderBean.getDetalle().setPathFilenet(pathFilenet);
				pathFilenet = pathFilenet + bean.getValor();
			if ("IdSubproducto".equals(bean.getNombre())) {
				pathFilenet = pathFilenet + bean.getValor() + File.separator;
			}
		} else if (NIVEL_4.equals(folderBean.getTipo()) && "idServicio".equals(bean.getNombre())) {
				folderBean.getDetalle().setPathFilenet(pathFilenet);
				pathFilenet = pathFilenet + bean.getValor();
		}

		return pathFilenet;
	}

	/**
	 * Recupera los atributos de un XML.
	 *
	 * @param path     Ruta del xml
	 * @param nameFile Nombre del archivo
	 * @return Atributos del archivo
	 * @throws ParserConfigurationException Error en la configuracion del parseo
	 * @throws SAXException                 Excepcion sax
	 */
	private List<AtributoBean> getAtributos(String path, String nameFile)
			throws ParserConfigurationException, SAXException {
		List<AtributoBean> atributos = null;
		DocumentBuilderFactory factory = null;
		DocumentBuilder builder = null;
		Document document = null;
		NodeList nodeList = null;
		InputSource is = null;
		String xml = null;
		String value8859 = "";
		try {
			xml = getContenidoXML(path + File.separator + nameFile);
			atributos = new ArrayList<>();
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			is = new InputSource();
			is.setCharacterStream(new StringReader(xml));
			// codificacion ISO-8859-1
			is.setEncoding(ISO88591);
			document = builder.parse(is);
			nodeList = document.getElementsByTagName(ATRIBUTO);
			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);
				NamedNodeMap map2 = node.getAttributes();
				AtributoBean bean = new AtributoBean();
				bean.setNombre(map2.getNamedItem("id").getNodeValue());
				bean.setTipo(map2.getNamedItem(TIPO_DATO).getNodeValue());
				if (node.getFirstChild() == null) {
					bean.setValor("");
				} else {
					
					value8859 = node.getFirstChild().getNodeValue();
					bean.setValor(value8859);
				}
				atributos.add(bean);
			}
		} catch (FileNotFoundException e) {
			LOG.error("Error al recuperar los atributos del archivo - " + path + File.separator + nameFile);
			LOG.error(e);
		} catch (IOException e) {
			LOG.error("Error al recuperar los atributos del archivo - " + path + File.separator + nameFile);
			LOG.error(e);
		}
		return atributos;
	}

	/**
	 * Carga un XML localizado en cierta ruta.
	 *
	 * @param ruta Ruta del xml
	 * @return Contenido del xml
	 * @throws FileNotFoundException Error al buscar el archivo
	 */
	private String getContenidoXML(String ruta) throws FileNotFoundException {
		StringBuilder contenido = new StringBuilder();
		try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(ruta), "UTF-8"))) {
				String linea;
				while ((linea = br.readLine()) != null) {
					String strLinea = linea;

					contenido.append(strLinea + "\n");
				}
		} catch (IOException e) {
			LOG.error(e);
		}
		return contenido.toString();
	}

	/**
	 * getBuc.
	 *
	 * @param path     Ruta del archivo
	 * @param nameFile Nombre del archivo
	 * @return Id del archivo
	 */
	public String getBuc(String path, String nameFile) {
		DocumentBuilderFactory factory = null;
		DocumentBuilder builder = null;
		Document document = null;
		NodeList nodeList = null;
		InputSource is = null;
		String xml = null;
		String buc = "";
		try {
			xml = getContenidoXML(path + File.separator + nameFile);
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			is = new InputSource();
			// codificacion ISO-8859-1
			is.setEncoding(ISO88591);
			is.setCharacterStream(new StringReader(xml));
			document = builder.parse(is);
			nodeList = document.getElementsByTagName(ATRIBUTO);
			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);
				NamedNodeMap map = node.getAttributes();
				if ("BUC".equals(map.getNamedItem("id").getNodeValue())) {
					Node nodeValue = node.getFirstChild();
					buc = nodeValue.getNodeValue();
				}
			}
		} catch (ParserConfigurationException e) {
			LOG.error(e);
		} catch (SAXException e) {
			LOG.error(e);
		} catch (IOException e) {
			LOG.error(e);
		}
		return buc;
	}

	/**
	 * formatString.
	 *
	 * @param longitud Longitud del string
	 * @param format   Formato del string
	 * @param left     Validacion de izquierda
	 * @return String con el formato correcto
	 */
	private static String formatString(int longitud, String format, boolean left) {
		StringBuilder formatTmp = new StringBuilder(format);
		for (int i = (longitud - format.length()); i > 0; i--) {
			// Dar formato al string de acuerdo a la condicional
			if (left) {
				formatTmp.append("0" + format);
			} else {				
				formatTmp.append(format + "0");
			}
		}
		// Regresar el string con el formato adecuado
		return formatTmp.toString();
	}

}
