package mx.isban.flujogd.common.bean;

import java.io.Serializable;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal LoteBean
 *
 */
public class LoteBean implements Serializable{
	
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 8524154215191695542L;
	
	private String idLote;
	private String idStatus;
	private String usuarioCreacion;
	private String usuarioActualizacion;
	private LoteBeanDetalle detalle;
	/**
	 * @return the idLote
	 */
	public String getIdLote() {
		return idLote;
	}
	/**
	 * @param idLote the idLote to set
	 */
	public void setIdLote(String idLote) {
		this.idLote = idLote;
	}
	/**
	 * @return the idStatus
	 */
	public String getIdStatus() {
		return idStatus;
	}
	/**
	 * @param idStatus the idStatus to set
	 */
	public void setIdStatus(String idStatus) {
		this.idStatus = idStatus;
	}
	/**
	 * @return the usuarioCreacion
	 */
	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}
	/**
	 * @param usuarioCreacion the usuarioCreacion to set
	 */
	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}
	/**
	 * @return the usuarioActualizacion
	 */
	public String getUsuarioActualizacion() {
		return usuarioActualizacion;
	}
	/**
	 * @param usuarioActualizacion the usuarioActualizacion to set
	 */
	public void setUsuarioActualizacion(String usuarioActualizacion) {
		this.usuarioActualizacion = usuarioActualizacion;
	}
	/**
	 * @return the detalle
	 */
	public LoteBeanDetalle getDetalle() {
		return detalle;
	}
	/**
	 * @param detalle the detalle to set
	 */
	public void setDetalle(LoteBeanDetalle detalle) {
		this.detalle = detalle;
	}

}
