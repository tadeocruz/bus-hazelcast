package mx.isban.flujogd.common.util;

/**
 * 
 * @author Alvaro
 *
 */
public enum DocumentoStatusEnum {
	
	//Tipo de estatus - documento valido correcto
	STATUS_DOC_VALID_OK("DOC_VALID_OK"),
	//Tipo de estatus - documento valido con error
	STATUS_DOC_VALID_ERROR("DOC_VALID_ERROR"),
	//Tipo de estatus - md5 valido correcto
	STATUS_MD5_VALID_OK("MD5_VALID_OK"),
	//Tipo de estatus - md5 valido con error
	STATUS_MD5_VALID_ERROR("MD5_VALID_ERROR"),
	//Tipo de estatus - documento guardado correcto
	STATUS_STORED_OK("STORED_OK"),
	//Tipo de estatus - documento guardado con error
	STATUS_STORED_ERROR("STORED_ERROR"),
	//Tipo de estatus - documento duplicado
	STATUS_DUPLICATE("DUPLICATE");
	
	//Inicializamos un string para guardar el nombre
	private String name;

	DocumentoStatusEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
}
