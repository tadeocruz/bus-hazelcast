package mx.isban.flujogd.common.bean;

import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal CifrasControlBean
 *
 */
public class CifrasControlBean implements Serializable{

	//Inicializamos un long para el serial
	private static final long serialVersionUID = -5205696650765216773L;

	//Inicializamos un input stream
	private transient InputStream reporteInStre;
	//Inicializamos una lista para guardar los id de expedientes
	private List<Integer> listIdsExpedientes;

	/**
	 * cifrascontrolbean
	 * @param reporteInStre		Reporte
	 * @param listIdsExpedientes	Lista de ids de expedientes
	 */
	public CifrasControlBean(InputStream reporteInStre, List<Integer> listIdsExpedientes) {
		super();
		//Guardar el valor del input stream
		this.reporteInStre = reporteInStre;
		//Guardar la lista de id de expedientes
		this.listIdsExpedientes = new ArrayList<>(listIdsExpedientes);
	}

	/**
	 * @return the reporteInStre
	 */
	public InputStream getReporteInStre() {
		return reporteInStre;
	}

	/**
	 * @param reporteInStre the reporteInStre to set
	 */
	public void setReporteInStre(InputStream reporteInStre) {
		this.reporteInStre = reporteInStre;
	}

	/**
	 * @return the listIdsExpedientes
	 */
	public List<Integer> getListIdsExpedientes() {
		return new ArrayList<>(this.listIdsExpedientes);
	}

	/**
	 * @param listIdsExpedientes the listIdsExpedientes to set
	 */
	public void setListIdsExpedientes(List<Integer> listIdsExpedientes) {
		this.listIdsExpedientes = new ArrayList<>(listIdsExpedientes);
	}
}
