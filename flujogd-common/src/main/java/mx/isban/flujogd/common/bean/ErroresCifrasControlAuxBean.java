/**
 * 
 */
package mx.isban.flujogd.common.bean;

import java.io.Serializable;

/**
 * The Class ErroresCifrasControlAuxBean.
 * Objeto que contendra el numero de errores para las cifras de control
 *
 * @author jccalderon
 * Stefanini
 * 01/08/2019
 */
public class ErroresCifrasControlAuxBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6441468250717103596L;

	/** The num error archivo INCM. */
	private int numErrorArchivoINCM;
	
	/** The num proceso cancelado. */
	private int numProcesoCancelado;

	/**
	 * @return the numErrorArchivoINCM
	 */
	public int getNumErrorArchivoINCM() {
		return numErrorArchivoINCM;
	}

	/**
	 * @param numErrorArchivoINCM the numErrorArchivoINCM to set
	 */
	public void setNumErrorArchivoINCM(int numErrorArchivoINCM) {
		this.numErrorArchivoINCM = numErrorArchivoINCM;
	}

	/**
	 * @return the numProcesoCancelado
	 */
	public int getNumProcesoCancelado() {
		return numProcesoCancelado;
	}

	/**
	 * @param numProcesoCancelado the numProcesoCancelado to set
	 */
	public void setNumProcesoCancelado(int numProcesoCancelado) {
		this.numProcesoCancelado = numProcesoCancelado;
	}
	
	
}
