package mx.isban.flujogd.common.bean;

import java.io.Serializable;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal NotificationBean3
 *
 */
public class NotificationBean3 implements Serializable{
	
	/**
	 * 
	 */
	//Inicializamos un long para el serial
	private static final long serialVersionUID = 1753067063886797026L;
	//Inicializamos un string para guardar el buc
	private String buc;
	//Inicializamos un string para guardar la ruta del zip
	private String pathZip;
	//Inicializamos un string para guardar la ruta donde se descomprime el zip
	private String pathUnzip;
	//Inicializamos un string para guardar el nombre del archivo
	private String fileName;
	/**
	 * @return the buc
	 */
	public String getBuc() {
		return buc;
	}
	/**
	 * @param buc the buc to set
	 */
	public void setBuc(String buc) {
		this.buc = buc;
	}
	/**
	 * @return the pathZip
	 */
	public String getPathZip() {
		return pathZip;
	}
	/**
	 * @param pathZip the pathZip to set
	 */
	public void setPathZip(String pathZip) {
		this.pathZip = pathZip;
	}
	/**
	 * @return the pathUnzip
	 */
	public String getPathUnzip() {
		return pathUnzip;
	}
	/**
	 * @param pathUnzip the pathUnzip to set
	 */
	public void setPathUnzip(String pathUnzip) {
		this.pathUnzip = pathUnzip;
	}
	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}
	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

}
