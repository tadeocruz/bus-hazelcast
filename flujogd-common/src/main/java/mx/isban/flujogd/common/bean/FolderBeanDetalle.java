package mx.isban.flujogd.common.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Bean para transporte de datos
 * Flujo hazelcast con funciones especificas pero independiente de los subscriptores
 *
 */
public class FolderBeanDetalle implements Serializable{
	
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 5057305729149040485L;
	
	private String path;
	private List<AtributoBean> atributos = new ArrayList<>();
	private String pathFilenet;

	/**
	 * @return the atributos
	 */
	public List<AtributoBean> getAtributos() {
		return new ArrayList<>(this.atributos);
	}
	/**
	 * @param atributos the atributos to set
	 */
	public void setAtributos(List<AtributoBean> atributos) {
		this.atributos = new ArrayList<>(atributos);
	}
	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}
	/**
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}
	/**
	 * @return the pathFilenet
	 */
	public String getPathFilenet() {
		return pathFilenet;
	}
	/**
	 * @param pathFilenet the pathFilenet to set
	 */
	public void setPathFilenet(String pathFilenet) {
		this.pathFilenet = pathFilenet;
	}
	
	/**
	 * addatributo
	 * @param bean	Vaina del atributo
	 */
	public void addAtributo(AtributoBean bean) {
		this.atributos.add(bean);
	}

}
