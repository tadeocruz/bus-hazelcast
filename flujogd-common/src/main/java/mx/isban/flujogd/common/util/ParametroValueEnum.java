package mx.isban.flujogd.common.util;

/**
 * 
 * @author Alvaro
 *
 */
public enum ParametroValueEnum {
	
	//Tipo de parametro - activo
	PARAMETRO_VALUE_ACTIVO("1"),
	//Tipo de parametro - inactivo
	PARAMETRO_VALUE_INACTIVO("0");
	
	//Inicializamos un string para guardar el nombre
	private String name;

	ParametroValueEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
}
