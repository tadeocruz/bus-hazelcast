package mx.isban.flujogd.common.bean;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * The Class ExpedienteBeanDetalle.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal ExpedienteBeanDetalle
 */
public class ExpedienteBeanDetalle implements Serializable {

	/** The Constant serialVersionUID. */
	//Inicializamos un long para el serial
	private static final long serialVersionUID = 7093317025232792358L;

	/** The siguiente queue. */
	//Inicialidamos un string para almacenar el documento que sigue en la cola
	private String siguienteQueue;
	
	/** The id flujo. */
	//Inicializamos un integer para almacenar el id del flujo
	private Integer idFlujo;
	
	/** The documentos. */
	//Inicializamos una lista para almacenar los documentos
	private List<DocumentoBean> documentos;
	
	/** The flujos. */
	//Inicializamos un mapa para almacenar los flujos
	private Map<Integer, FlujoBean> flujos;
	
	/** Propiedades del expediente */
	private ExpedienteBeanDetalle2 detalle2;
	
	/**
	 * Gets the documentos.
	 *
	 * @return the documentos
	 */
	public List<DocumentoBean> getDocumentos() {
		return new ArrayList<>(this.documentos);
	}
	
	/**
	 * Sets the documentos.
	 *
	 * @param documentos the documentos to set
	 */
	public void setDocumentos(List<DocumentoBean> documentos) {
		this.documentos = new ArrayList<>(documentos);
	}
	
	/**
	 * Gets the flujos.
	 *
	 * @return the flujos
	 */
	public Map<Integer, FlujoBean> getFlujos() {
		return flujos;
	}
	
	/**
	 * Sets the flujos.
	 *
	 * @param flujos the flujos to set
	 */
	public void setFlujos(Map<Integer, FlujoBean> flujos) {
		this.flujos = flujos;
	}
	
	/**
	 * Gets the siguiente queue.
	 *
	 * @return the siguienteQueue
	 */
	public String getSiguienteQueue() {
		return siguienteQueue;
	}
	
	/**
	 * Sets the siguiente queue.
	 *
	 * @param siguienteQueue the siguienteQueue to set
	 */
	public void setSiguienteQueue(String siguienteQueue) {
		this.siguienteQueue = siguienteQueue;
	}
	
	/**
	 * Gets the id flujo.
	 *
	 * @return the idFlujo
	 */
	public Integer getIdFlujo() {
		return idFlujo;
	}
	
	/**
	 * Sets the id flujo.
	 *
	 * @param idFlujo the idFlujo to set
	 */
	public void setIdFlujo(Integer idFlujo) {
		this.idFlujo = idFlujo;
	}
	
	/**
	 * Gets the detalle 2.
	 *
	 * @return the detalle2
	 */
	public ExpedienteBeanDetalle2 getDetalle2() {
		return detalle2;
	}
	
	/**
	 * Sets the detalle 2.
	 *
	 * @param detalle2 the detalle2 to set
	 */
	public void setDetalle2(ExpedienteBeanDetalle2 detalle2) {
		this.detalle2 = detalle2;
	}
	

}