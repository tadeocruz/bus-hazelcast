package mx.isban.flujogd.common.util;

/**
 * 
 * @author Alvaro
 *
 */
public enum LoteStatusEnum {
	
	//Tipo de estatus - solicitado
	STATUS_SOLICITADO("SOLICITADO"),
	//Tipo de estatus - procesado
	STATUS_PROCESADO("PROCESADO"),
	//Tipo de estatus - depurado
	STATUS_DEPURADO("DEPURADO"),
	//Tipo de estatus - procesado con errores
	STATUS_PROCESADO_CON_ERRORES("PROCESADOCONERRORES");
	
	//Inicializamos un string para guardar el nombre
	private String name;

	LoteStatusEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
}
