/**
 * 
 */
package mx.isban.flujogd.common.generic;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Clase de utileria para codigo en comun entre los procesos.
 *
 * @author jccalderon
 * Stefanini
 * 30/07/2019
 */
public class CommonsUtils {
	//Inicializamos un log para la bitacora
		private static final Logger LOG = LogManager.getLogger(CommonsUtils.class);
		
/**
 * Metodo para generar el listado de las ip para el servicio de hazelcast
 * Gets the lista ip.
 *
 * @param args the args
 * @return the lista ip
 */
public List<String> getListaIp (String[] args){
	//Guardar los parametros
	String ips = args[0];
	String port = args[1];
	String path = args[2];
	String componente = args[3];
	String nombreSubscriptor = args[4];
	//Mostrar en bitacora los parametros
	LOG.info("Parametros recuperados");
	LOG.info("Ips: " + ips);
	LOG.info("Port: " + port);
	LOG.info("Path: " + path);
	LOG.info("Componente: " + componente);
	LOG.info("Subscriptor: " + nombreSubscriptor);
	//Crear una lista para las ips
	List<String> ipList = new ArrayList<>();
	//Separar ls ips por comas
	//Guardar las ips en un arreglo de strings
	String[] ipsSplit = ips.split(",");
	for(int i = 0; i < ipsSplit.length; i++) {
		//Agregar las ips del arreglo en la lista
		ipList.add(ipsSplit[i]);
	}

	return ipList;
	
}

/**
 * Validar null to 0.
 * Metodo para validar que el valor no sea null y convertirlo a 
 *
 * @param valor the valor
 * @return the string
 */
public String validarNullTo0(String valor) {
	if(valor==null) {
		valor = "0";
	}
	return valor;
}
}
