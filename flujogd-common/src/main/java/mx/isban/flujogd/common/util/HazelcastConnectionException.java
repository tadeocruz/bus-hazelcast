package mx.isban.flujogd.common.util;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase HazelcastConnectionException
 *
 */
public class HazelcastConnectionException extends Exception{
	
	/**
	 * serialVersionUID
	 */
	//Inicializamos un long para guardar el serial
	private static final long serialVersionUID = 1398697226818360231L;

	/**
	 * hazelcastconnectionexception.
	 *
	 * @param message Mensaje a notificar
	 * @param e excepcion hazelcast conexion
	 */
	public HazelcastConnectionException(String message, RuntimeException e) {
		//Mensaje de excepcion
		super(message);
	}

}
