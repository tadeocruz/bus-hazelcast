package mx.isban.flujogd.common.util;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase DocumentoUtil
 *
 */
public class DocumentoUtil {
	
	/**
	 * getNameWithoutExt
	 * @param fileName	Nombre del archivo
	 * @return	Cadena perteneciente al nombre del archivo
	 */
	public String getNameWithoutExt(String fileName) {
		int ini = fileName.indexOf('.');
		return fileName.substring(0, ini);
	}

}
