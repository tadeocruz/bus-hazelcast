package mx.isban.flujogd.common.util;

/**
 * 
 * @author Alvaro
 *
 */
public enum NetConfigStatusEnum {
	
	//Tipo de configuracion de red - activa
	NET_CONFIG_ACTIVA("1"),
	//Tipo de configuracion de red - inactiva
	NET_CONFIG_INACTIVA("0"),
	//Tipo de configuracion de red - todas
	NET_CONFIG_TODAS("2");
	
	//Inicializamos un string para guardar el nombre
	private String name;

	NetConfigStatusEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
}
