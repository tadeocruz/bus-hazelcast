package mx.isban.flujogd.common.util;

/**
 * 
 * @author Alvaro
 *
 */
public enum ParametroBatchEnum {
	
	//Tipo de lote de parametros - numero de hilos
	BATCH_PARAMETRO_NUMBER_THREADS("number_threads"),
	//Tipo de lote de parametros - ruta funcional
	BATCH_PARAMETRO_WORKING_PATH("working_path");
	
	//Inicializamos un string para guardar el nombre
	private String name;

	ParametroBatchEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
}
