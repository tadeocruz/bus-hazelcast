/**
 * 
 */
package mx.isban.flujogd.common.generic;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.config.Config;
import com.hazelcast.core.HazelcastInstance;

import mx.isban.flujogd.common.util.HazelcastConnectionException;

/**
 * Clase de utileria para hazelcast
 * The Class HazelcastGenericUtils.
 *
 * @author jccalderon
 */
public class HazelcastGenericUtils {

	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(HazelcastGenericUtils.class);

	
	/**
	 * Metodo para obtener la instancia de Hazelcast
	 * Gets the client hazelcast.
	 *
	 * @param timeSleep the time sleep
	 * @param config the config
	 * @return the client hazelcast
	 * @throws HazelcastConnectionException the hazelcast connection exception
	 */
	public HazelcastInstance getClientHazelcast(long timeSleep, ClientConfig config) throws HazelcastConnectionException {
		HazelcastInstance client = null; 
		try {
			Thread.sleep(timeSleep);
			client = HazelcastClient.newHazelcastClient(config);
			LOG.info("Termina creacion cliente hazelcast");
		} catch (RuntimeException e) {
			LOG.error(e);
			throw new HazelcastConnectionException("Error de conexion", e);
		} catch (InterruptedException e) {
			LOG.error(e);
			Thread.currentThread().interrupt();
		}
		return client;
	}
	
	/**
	 * Metodo para obtener la configuracion del servicio hazelcast
	 * Sets the config hazelcast.
	 *
	 * @param port the port
	 * @param ips the ips
	 * @param conf the conf
	 * @return the config
	 */
	public Config setConfigHazelcast(String port, List<String> ips, Config conf) {
		Config config = new Config();
		config = conf;
		config.getNetworkConfig().setPort(new Integer(port));
		Integer lastPort = Integer.parseInt(port) + 99;
		String rango = port+"-"+lastPort.toString();
		LOG.info("Rango de puertos "+rango);
		config.getNetworkConfig().addOutboundPortDefinition(rango);
		config.getNetworkConfig().getJoin().getMulticastConfig().setEnabled(false);
		config.getNetworkConfig().getJoin().getAwsConfig().setEnabled(false);
		config.getNetworkConfig().getJoin().getTcpIpConfig().setEnabled(true);
		List<String> members = new ArrayList<>();
		for(String ip:ips) {
			members.add(ip);
		}
		config.getNetworkConfig().getJoin().getTcpIpConfig().setMembers(members);
		
		return config;
	}
}
