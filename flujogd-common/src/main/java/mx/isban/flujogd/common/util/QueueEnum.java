package mx.isban.flujogd.common.util;

/**
 * 
 * @author Alvaro
 *
 */
public enum QueueEnum {
	
	//Tipo de cola - envio
	QUEUE_DISPATCHER("flujogd-dispatcher"),
	//Tipo de cola - sftp
	QUEUE_SFTP("flujogd-sftp"),
	//Tipo de cola - checksum
	QUEUE_CHECKSUM("flujogd-checksum"),
	//Tipo de cola - zip
	QUEUE_ZIP("flujogd-zip"),
	//Tipo de cola - validacion
	QUEUE_VALIDATION("flujogd-validation"),
	//Tipo de cola - filenet
	QUEUE_FILENET("flujogd-filenet"),
	//Tipo de cola - notificacion
	QUEUE_NOTIFICATION("flujogd-notification"),
	//Tipo de cola - terminacion
	QUEUE_KILLER("queue-killer"),
	//Tipo de cola - expediente
	QUEUE_EXPEDIENTE("flujogd-expediente");
	
	//Inicializamos un string para guardar el nombre
	private String name;

	QueueEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
}
