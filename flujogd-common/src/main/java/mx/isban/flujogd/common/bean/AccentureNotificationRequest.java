package mx.isban.flujogd.common.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal AccentureNotificationRequest
 *
 */
public class AccentureNotificationRequest implements Serializable{
	
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 3203915021861700800L;
	private String referenciaExterna;
	private String requestId;
	private String buc;
	private String fechaProceso;
	private List<AccentureDocumentoRequest> archivosList;
	
	/**
	 * @return the referenciaExterna
	 */
	public String getReferenciaExterna() {
		return referenciaExterna;
	}
	/**
	 * @param referenciaExterna the referenciaExterna to set
	 */
	public void setReferenciaExterna(String referenciaExterna) {
		this.referenciaExterna = referenciaExterna;
	}
	/**
	 * @return the requestId
	 */
	public String getRequestId() {
		return requestId;
	}
	/**
	 * @param requestId the requestId to set
	 */
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	/**
	 * @return the buc
	 */
	public String getBuc() {
		return buc;
	}
	/**
	 * @param buc the buc to set
	 */
	public void setBuc(String buc) {
		this.buc = buc;
	}
	/**
	 * @return the fechaProceso
	 */
	public String getFechaProceso() {
		return fechaProceso;
	}
	/**
	 * @param fechaProceso the fechaProceso to set
	 */
	public void setFechaProceso(String fechaProceso) {
		this.fechaProceso = fechaProceso;
	}
	/**
	 * @return the archivos
	 */
	public List<AccentureDocumentoRequest> getArchivos() {
		List<AccentureDocumentoRequest> copia = new ArrayList<>();
		//Agregar archivos a la lista
		copia.addAll(archivosList);
		return copia;
	}
	/**
	 * @param archivos the archivos to set
	 */
	public void setArchivos(List<AccentureDocumentoRequest> archivos) {
		List<AccentureDocumentoRequest> copia = new ArrayList<>();
		//Agregar archivos a la lista
		copia.addAll(archivos);
		this.archivosList = copia;
	}
	
}
