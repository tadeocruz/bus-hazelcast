package mx.isban.flujogd.common.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase XMLManagerResponse
 *
 */
public class XMLManagerResponse implements Serializable{
	
	/**
	 * serialVersionUID
	 */
	//Inicializamos un long para el serial
	private static final long serialVersionUID = -4695601020482553543L;
	//Inicializamos un boolean para guardar el status de la respuesta
	private boolean success;
	//Inicializamos un string para guardar el mensaje de error
	private String errorMsg;
	//Inicializamos una lista para guardar los documentos
	private List<DocumentoBean> documentos;
	//Inicializamos una lista para guardar los valores
	private List<String> values;
	//Inicializamos una lista para guardar los folders
	private List<FolderBean> folders;
	
	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}
	/**
	 * @param success the success to set
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}
	/**
	 * @return the errorMsg
	 */
	public String getErrorMsg() {
		return errorMsg;
	}
	/**
	 * @param errorMsg the errorMsg to set
	 */
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	/**
	 * @return the documentos
	 */
	public List<DocumentoBean> getDocumentos() {
		List<DocumentoBean> copia = new ArrayList<>();
		copia.addAll(documentos);
		return copia;
	}
	/**
	 * @param documentos the documentos to set
	 */
	public void setDocumentos(List<DocumentoBean> documentos) {
		List<DocumentoBean> copia = new ArrayList<>();
		copia.addAll(documentos);
		this.documentos = copia;
	}
	/**
	 * @return the values
	 */
	public List<String> getValues() {
		List<String> copia = new ArrayList<>();
		copia.addAll(values);
		return copia;
	}
	/**
	 * @param values the values to set
	 */
	public void setValues(List<String> values) {
		List<String> copia = new ArrayList<>();
		copia.addAll(values);
		this.values = copia;
	}
	/**
	 * @return the folders
	 */
	public List<FolderBean> getFolders() {
		List<FolderBean> copia = new ArrayList<>();
		copia.addAll(folders);
		return copia;
	}
	/**
	 * @param folders the folders to set
	 */
	public void setFolders(List<FolderBean> folders) {
		List<FolderBean> copia = new ArrayList<>();
		copia.addAll(folders);
		this.folders = copia;
	}
	
}
