package mx.isban.flujogd.common.exceptions;

/**
 * Excepción BusinessException
 * 
 * @author DELL
 *
 */
public class BusinessException extends Exception {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1398697226818360231L;

	/**
	 * Excepción utilizada para avisar que hubo un error con la validación del
	 * documento
	 *
	 * @param message Mensaje a notificar
	 */
	public BusinessException(String message) {
		super(message);
	}

	/**
	 * Excepción utilizada para notificar un mensaje en específico y propagar la
	 * excepción proporcionada
	 * 
	 * @param message mensaje a notificar
	 * @param e       excepción que ocurrió
	 */
	public BusinessException(String message, Throwable e) {
		super(message, e);
	}

}
