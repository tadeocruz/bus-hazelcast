package mx.isban.flujogd.common.bean;

import java.io.Serializable;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal AccentureResponseNotificacion
 *
 */
public class AccentureNotificacionResponse implements Serializable{
	
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 70384436256450721L;
	private String message;
	private String code;
	private String body;
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * @return the body
	 */
	public String getBody() {
		return body;
	}
	/**
	 * @param body the body to set
	 */
	public void setBody(String body) {
		this.body = body;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AccentureNotificacionResponse [message=" + message + ", code=" + code + ", body=" + body + "]";
	}

}
