package mx.isban.flujogd.common.util;

import java.util.Iterator;
import java.util.Map;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.bean.FlujoBean;

/**
 * 
 * @author Alvaro
 *
 */
public class ExpedienteUtil {
	
	/**
	 * Determina la siguiente queue a la que debe de ser lanzado el expediente
	 * Es requisito que la implementaci�n del map sea hecha con un TreeMap para garantizar el �rden de las queue's
	 * @param expedienteBean	Vaina de expediente
	 */
	public void determinaSiguienteQueue(ExpedienteBean expedienteBean) {
		expedienteBean.getDetalle().setSiguienteQueue("");
		//Inicializamos un mapa para guardar los flujos
		Map<Integer, FlujoBean> flujos = expedienteBean.getDetalle().getFlujos();
		Iterator<Integer> iter = flujos.keySet().iterator();
		while(iter.hasNext()) {
			Integer key = iter.next();
			//Obtener la llave del flujo
			FlujoBean bean = flujos.get(key);
			if(!bean.isProcesada()) {
				//Colocar los detalles de la cola
				expedienteBean.getDetalle().setSiguienteQueue(bean.getQueue());
				break;
			}
		}
	}
	
	/**
	 * Cambia el estatus de la queue
	 * @param expedienteBean	Vaina del expediente
	 */
	public void cambiaEstausQueue(ExpedienteBean expedienteBean) {
		//Inicializamos un para para guardar los flujos
		Map<Integer, FlujoBean> flujos = expedienteBean.getDetalle().getFlujos();
		Iterator<Integer> iter = flujos.keySet().iterator();
		while(iter.hasNext()) {
			Integer key = iter.next();
			//Obtener la llave del flujo
			FlujoBean bean = flujos.get(key);
			if(bean.getQueue().equals(expedienteBean.getDetalle().getSiguienteQueue())) {
				//Colocar los detalles de la cola
				bean.setProcesada(true);
				break;
			}
		}
	}

}
