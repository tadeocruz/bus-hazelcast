package mx.isban.flujogd.common.util;

/**
 * 
 * @author Alvaro
 *
 */
public enum ExpedienteProcessTypeEnum {
	
	//Tipo de proceso - nuevo
	PROCESO_TYPE_NUEVO("NUEVO"),
	//Tipo de proceso - reproceso total
	PROCESO_TYPE_REPROCESO_TOTAL("REPROCESO_TOTAL"),
	//Tipo de proceso - reproceso parcial
	PROCESO_TYPE_REPROCESO_PARCIAL("REPROCESO_PARCIAL"),
	//Tipo de proceso - actualizacion
	PROCESO_TYPE_ACTUALIZACION("ACTUALIZACION"),
	//Tipo de proceso - cancelacion
	PROCESO_TYPE_CANCELACION("CANCELACION");
	
	//Inicializamos un string para el nombre
	private String name;

	ExpedienteProcessTypeEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
}
