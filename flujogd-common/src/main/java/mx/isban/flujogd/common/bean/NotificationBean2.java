package mx.isban.flujogd.common.bean;

import java.io.Serializable;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal NotificationBean2
 *
 */
public class NotificationBean2 implements Serializable{
	
	/**
	 * 
	 */
	//Inicializamos un long para el serial
	private static final long serialVersionUID = -2818204070271061598L;
	//Inicializamos un string para guardar la referencia externa
	private String refExterna;
	//Inicializamos un string para guardar el id de la peticion
	private String reqId;
	//Inicializamos un string para guardar el id del error
	private String errorId;
	//Inicializamos un string para guardar el mensaje de error
	private String errorMsg;
	private NotificationBean3 bean3;
	/**
	 * @return the refExterna
	 */
	public String getRefExterna() {
		return refExterna;
	}
	/**
	 * @param refExterna the refExterna to set
	 */
	public void setRefExterna(String refExterna) {
		this.refExterna = refExterna;
	}
	/**
	 * @return the reqId
	 */
	public String getReqId() {
		return reqId;
	}
	/**
	 * @param reqId the reqId to set
	 */
	public void setReqId(String reqId) {
		this.reqId = reqId;
	}
	/**
	 * @return the errorId
	 */
	public String getErrorId() {
		return errorId;
	}
	/**
	 * @param errorId the errorId to set
	 */
	public void setErrorId(String errorId) {
		this.errorId = errorId;
	}
	/**
	 * @return the errorMsg
	 */
	public String getErrorMsg() {
		return errorMsg;
	}
	/**
	 * @param errorMsg the errorMsg to set
	 */
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	/**
	 * @return the bean3
	 */
	public NotificationBean3 getBean3() {
		return bean3;
	}
	/**
	 * @param bean3 the bean3 to set
	 */
	public void setBean3(NotificationBean3 bean3) {
		this.bean3 = bean3;
	}

}
