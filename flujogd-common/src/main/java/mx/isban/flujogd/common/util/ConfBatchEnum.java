package mx.isban.flujogd.common.util;

/**
 * 
 * @author Alvaro
 *
 */
public enum ConfBatchEnum {
	
	//Tipo de lote - configuracion general
	BATCH_CONF_GENERAL("conf_batch_general"),
	//Tipo de lote - configuracion del stfp
	BATCH_CONF_SFTP("conf_batch_sftp"),
	//Tipo de lote - configuracion de filenet
	BATCH_CONF_FILENET("conf_batch_filenet");
	
	//Inicializamos un string para guardar el nombre
	private String name;

	ConfBatchEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
}
