package mx.isban.flujogd.common.util;

/**
 * 
 * @author Alvaro
 *
 */
public enum DocumentoTypeEnum {
	
	//Tipo de documento - ine
	TYPE_INE("INE"),
	//Tipo de documento - licencia para conducir
	TYPE_LICENCIA_CONDUCIR("LIC_COND"),
	//Tipo de documento - curp
	TYPE_CURP("CURP"),
	//Tipo de documento - pasaporte
	TYPE_PASAPORTE("PASAPORTE"),
	//Tipo de documento - comprobante de domicilio
	TYPE_COMP_DOMICILIO("COM_DOM");
	
	//Inicializamos un string para guardar el nombre
	private String name;

	DocumentoTypeEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
}
