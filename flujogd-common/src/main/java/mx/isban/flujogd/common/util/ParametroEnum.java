package mx.isban.flujogd.common.util;

/**
 * Enum ParametroEnum
 * 
 * @author Alvaro
 *
 */
public enum ParametroEnum {

	//Tipo de parametro - usuario del sftp
	PARAMETRO_SFTP_USER("sftp_user"),
	//Tipo de parametro - contrasena del sftp
	PARAMETRO_SFTP_PASS("sftp_pass"),
	//Tipo de parametro - host del sftp
	PARAMETRO_SFTP_HOST("sftp_host"),
	//Tipo de parametro - puerto del sftp
	PARAMETRO_SFTP_PORT("sftp_port"),
	//Tipo de parametro - ruta fuente del sftp
	PARAMETRO_SFTP_SRC_PATH("sftp_src_path"),
	//Tipo de parametro - ruta de salida del sftp
	PARAMETRO_SFTP_OUTPUT_PATH("sftp_output_path"),
	//Tipo de parametro - ruta a la llave privada del sftp
	PARAMETRO_SFTP_PATH_PRIVATE_KEY("sftp_path_priv_key"),
	//Tipo de parametro - frase de seguridad de la contrasena del sftp
	PARAMETRO_SFTP_PASSPHRASE("sftp_passphrase"),
	//Tipo de parametro - dias de resguardo del sftp
	PARAMETRO_SFTP_DIAS_RESGUARDO("sftp_dias_resguardo"),
	//Tipo de parametro - ruta destino de descompresion ZIP
	PARAMETRO_ZIP_TARGET_PATH("zip_target_path"),
	//Tipo de parametro - numero de flujos en la red general
	PARAMETRO_GRAL_RED_NUMERO_FLUJOS("red_num_flujos"),
	//Tipo de parametro - hazelcast activo general
	PARAMETRO_GRAL_HAZELCAST_ACTIVO("hazelcast_activo"),
	//Tipo de parametro - ruta del hazelcast general
	PARAMETRO_GRAL_HAZELCAST_PATH("hazelcast_path"),
	//Tipo de parametro - descriptor de archivos general
	PARAMETRO_GRAL_FILE_DESCRIPTOR("file_descriptor"),
	//Tipo de parametro - hora de apagado de hazelcast general
	PARAMETRO_GRAL_HAZELCAST_HOUR_SHUTDOWN("hazelcast_hour_shutdown"),
	//Tipo de parametro - tamano maximo archivo
	PARAMETRO_GRAL_FILE_MAX_SIZE("file_max_size"),
	//Tipo de parametro - java path cacerts
	PARAMETRO_GRAL_JAVA_PATH_CACERTS("java_path_cacerts"),
	//Tipo de parametro - java key Store P
	PARAMETRO_GRAL_JAVA_KEY_STORE_P("java_key_store_P"),
	//Tipo de parametro - java path cacerts
	PARAMETRO_GRAL_CONFIG_LOCAL_NET("config_local_net"),
	//Tipo de parametro - ruta de filenet
	PARAMETRO_FILENET_PATH_IDC("filenet_path_idc"),
	//Tipo de parametro - usuario de filenet
	PARAMETRO_FILENET_USER("filenet_user"),
	//Tipo de parametro - contrasena de filenet
	PARAMETRO_FILENET_PASSWORD("filenet_password"),
	//Tipo de parametro - uri de filenet
	PARAMETRO_FILENET_URI("filenet_uri"),
	//Tipo de parametro - jaas de filenet
	PARAMETRO_FILENET_JAAS("filenet_jaas"),
	//Tipo de parametro - almacenamiento de filenet
	PARAMETRO_FILENET_STORE("filenet_store"),
	//Secret Data
	PARAMETRO_FILENET_SECRET_DATA("filenet_secret_data"),
	//Tipo de parametro - url de las notificaciones
	PARAMETRO_NOTIFICATION_URL_NOTI("vivere_noti_url"),
	//Tipo de parametro - url del token de las notificaciones
	PARAMETRO_NOTIFICATION_URL_TOKEN("vivere_token_url"),
	//Tipo de parametro - token de challenge de las notificaciones
	PARAMETRO_NOTIFICATION_TOKEN_CHALLENGE("vivere_token_challenge"),
	//Tipo de parametro - token del canal de las notificaciones
	PARAMETRO_NOTIFICATION_TOKEN_CHANNEL("vivere_token_channel"),
	//Tipo de parametro - token de la contrasena de las notificaciones
	PARAMETRO_NOTIFICATION_TOKEN_PASSWORD("vivere_token_password"),
	//Tipo de parametro - token del nombre de usuario de las notificaciones
	PARAMETRO_NOTIFICATION_TOKEN_USERNAME("vivere_token_username"),
	//Tipo de parametro - token de la version de las notificaciones
	PARAMETRO_NOTIFICATION_TOKEN_VERSION("vivere_token_version"),
	
	PARAMETRO_NOTIFICATION_PROXY_NAM_HOST("vivere_prx_nam_host"), 
	/**
	 * Tipo de parámetro que indica si el flujo de validación debe realizar la
	 * validación con ced o con la tradicional
	 */
	PARAMETRO_GRAL_VALIDAR_CED("validar_ced"),
	/**
	 * Url del ced al cuál se le pide la validación a realizar por tipo documental
	 */
	PARAMETRO_CED_VALIDATION_URL("ced_validation_url"),
	PARAMETRO_CED_CANAL("ced_canal"),
	PARAMETRO_CED_APP("ced_app_origen"),
	PARAMETRO_CED_NIVEL("ced_nivel"),
	PARAMETRO_CED_PRODUCTO("ced_producto"),
	PARAMETRO_CED_TIPO_CONSULTA("ced_tipo_consulta"),
	PARAMETRO_CED_TIPO_OPERACION("ced_tipo_operacion"),
	/**
	 * Url del servicio de expedientes sucursales al cuàl pedir el token de
	 * autenticaciòn
	 */
	PARAMETRO_EXP_SUC_TOKEN_URL("es_token_url"),
	/**
	 * Url del servicio de expedientes sucursales al cuál se realiza la notificación
	 */
	PARAMETRO_EXP_SUC_VALIDATION_URL("es_notification_url"),	
	PARAMETRO_NOTIFICATION_PROXY_VAL_HOST("vivere_prx_val_host"),
	PARAMETRO_NOTIFICATION_PROXY_NAM_PORT("vivere_prx_nam_port"), 
	PARAMETRO_NOTIFICATION_PROXY_VAL_PORT("vivere_prx_val_port"),
	PARAMETRO_NOTIFICATION_PROXY_NAM_DISABLED_SCHEMES("vivere_prx_nam_dis_sch"), 
	PARAMETRO_NOTIFICATION_PROXY_VAL_DISABLED_SCHEMES("vivere_prx_val_dis_sch");

	//Inicializamos un string para guardar el nombre
	private String name;

	ParametroEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
