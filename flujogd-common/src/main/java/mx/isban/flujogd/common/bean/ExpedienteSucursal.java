package mx.isban.flujogd.common.bean;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * ErrorDocumentoBean es una clase que tiene como finalidad almacenar los
 * valores necesarios para informar un error en la validación de documentos en
 * caso de que lo exista
 * 
 * @author DELL
 *
 */
@Getter
@Setter
public class ExpedienteSucursal implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 4751912756896050091L;

	/**
	 * Lista de errores relacionados con los documentos, ya sea que estos no existen
	 * o no tienen el xml correspondiente
	 */
	private List<ErrorDocumentoBean> erroresDocumento;
	
	/**
	 * Lista de errores relacionados con la persistencia de documentos a filenet
	 */
	private List<ErrorDocumentoBean> erroresFilenet;

	/**
	 * Número de cuenta al cuál está asociado el objeto
	 */
	private Long cuenta;

	/**
	 * Aplicativo identificador para saber a quién se realiza la notificación final
	 */
	private String aplicativo;

	/**
	 * Indica si existió un error con la existencia de documentos(no existe el pdf)
	 * o la paridad de estos, es decir... para cada pdf corresponde un xml con los
	 * metadatos Y si el xml no se encuentra entonces es un error de paridad
	 */
	private boolean errorExistenciaParidad;

}
