package mx.isban.flujogd.common.util;

/**
 * 
 * @author Alvaro
 *
 */
public enum NotificacionStatusEnum {
	
	//Tipo de estatus de la notificacion - no enviada
	NOTIFICACION_STATUS_NO_ENVIADA("NO_ENVIADA"),
	//Tipo de estatus de la notificacion - enviada
	NOTIFICACION_STATUS_ENVIADA("ENVIADA"),;
	
	//Inicializamos un string para guardar el nombre
	private String name;

	NotificacionStatusEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
}
