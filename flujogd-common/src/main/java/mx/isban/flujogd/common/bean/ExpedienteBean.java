package mx.isban.flujogd.common.bean;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * Clase principal ExpedienteBean la cual contiene las propeidades necesarias
 * para pasar por el flujo del bus masivo
 * 
 * @author Alvaro Zamorano azamorano@serviciosexternos.isban.mx
 *
 */
@Getter
@Setter
public class ExpedienteBean implements Serializable {

	/**
	 * Inicializamos un long para el serial
	 */
	private static final long serialVersionUID = 7093317025232792358L;

	/**
	 * Inicializamos un string para almacenar el id de expediente
	 */
	private String idExpediente;

	/**
	 * Inicializamos un string para almacenar la ruta del zip del expediente
	 */
	private String pathZIPExpediente;

	/**
	 * Inicializamos un string para almacenar la ruta donde se va a descomprimir el
	 * expediente
	 */
	private String pathUNZIPExpediente;

	/**
	 * Inicializamos un string para almacenar el nombre del expediente
	 */
	private String nombreExpediente;

	/**
	 * Propiedad con los elementos necesarios para enviar una notificación al
	 * servicio de expedientesSucursales
	 */
	private ExpedienteSucursal expedienteSucursal;

	/**
	 * Detalles del expediente
	 */
	private ExpedienteBeanDetalle detalle;

}