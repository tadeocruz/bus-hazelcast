package mx.isban.flujogd.common.util;

/**
 * 
 * @author Alvaro
 *
 */
public enum ConfEnum {
	
	//Tipo de configuracion - general
	CONF_GENERAL("conf_general"),
	//Tipo de configuracion - sftp
	CONF_SFTP("conf_sftp"),
	//Tipo de configuracion - filenet
	CONF_FILENET("conf_filenet"),
	//Tipo de configuracion - notificacion del web sphere
	CONF_NOTI_WS("conf_noti_ws");
	
	//Inicializamos un string para guardar el nombre
	private String name;

	ConfEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
}
