package mx.isban.flujogd.common.util;

/**
 * 
 * @author Alvaro
 *
 */
public enum QueueStatusEnum {
	
	//Tipo de estatus - correcto
	STATUS_OK("OK"),
	//Tipo de estatus - error
	STATUS_ERROR("ERROR");
	
	//Inicializar un string para guardar el nombre
	private String name;

	QueueStatusEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
}
