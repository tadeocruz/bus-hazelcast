package mx.isban.flujogd.common.bean;

import java.io.Serializable;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal ParametroBean
 *
 */
public class ParametroBean implements Serializable{
	
	/**
	 * Serial Version UID
	 */
	//Inicializamos un long para el serial
	private static final long serialVersionUID = 8198653481401885462L;
	
	//Inicializamos un string para guardar el nombre del parametro
	private String nombreParametro;
	//Inicializamos un string para guardar el valor del parametro
	private String valorParametro;
	//Inicializamos un string para saber si el parametro esta encriptado
	private String estaEncriptado;
	
	/**
	 * @return the nombreParametro
	 */
	public String getNombreParametro() {
		return nombreParametro;
	}
	/**
	 * @param nombreParametro the nombreParametro to set
	 */
	public void setNombreParametro(String nombreParametro) {
		this.nombreParametro = nombreParametro;
	}
	/**
	 * @return the valorParametro
	 */
	public String getValorParametro() {
		return valorParametro;
	}
	/**
	 * @param valorParametro the valorParametro to set
	 */
	public void setValorParametro(String valorParametro) {
		this.valorParametro = valorParametro;
	}
	/**
	 * @return the estaEncriptado
	 */
	public String getEstaEncriptado() {
		return estaEncriptado;
	}
	/**
	 * @param estaEncriptado the estaEncriptado to set
	 */
	public void setEstaEncriptado(String estaEncriptado) {
		this.estaEncriptado = estaEncriptado;
	}

}
