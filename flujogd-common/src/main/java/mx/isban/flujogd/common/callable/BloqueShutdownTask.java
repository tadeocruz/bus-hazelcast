package mx.isban.flujogd.common.callable;

import java.io.Serializable;
import java.util.concurrent.Callable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hazelcast.cluster.ClusterState;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.HazelcastInstanceAware;

/**
 * 
 * @author Alvaro
 *
 */
public class BloqueShutdownTask implements Callable<Object>, Serializable, HazelcastInstanceAware{
	
	//Inicializamos un log para la bitacora
	private static final Logger LOG = LogManager.getLogger(BloqueShutdownTask.class);
	
	/**
	 * 
	 */
	//Inicializamos un long para el serial
	private static final long serialVersionUID = 1845527693555859790L;

	//Inicializamos un hazelcast instance
	private transient HazelcastInstance hazelcastInstance;
	
	@Override
	public void setHazelcastInstance(HazelcastInstance hazelcastInstance) {
		this.hazelcastInstance = hazelcastInstance;
	}

	@Override
	public Object call() throws Exception {
		//Mostrar en bitacora el mensaje de apagado
		LOG.info("BloqueShutdownTask - shutdown");
		//Cambiar el status del cluster
		hazelcastInstance.getCluster().changeClusterState(ClusterState.PASSIVE);
		//Terminar el cluster
		hazelcastInstance.getCluster().shutdown();
		//Mostrar en bitaco el mensaje de terminado
		LOG.info(":::DONE:::");
		//Regresar el resultado
		return 0;
	}

}
