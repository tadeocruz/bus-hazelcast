package mx.isban.flujogd.common.util;

/**
 * Enum para estatus de los espedientes
 * The Enum LoteExpedienteDetalleEnum.
 */
public enum LoteExpedienteDetalleEnum {
	
	/** The encontrdado. */
	ENCONTRDADO("ENCONTRADO"),
	
	/** The no encontrado. */
	NO_ENCONTRADO("NOENCONTRADO"),
	
	/** The error. */
	ERROR("ERROR");
	
	
	/** The name. */
	//Inicializamos un string para guardar el nombre
	private String name;

	/**
	 * Instantiates a new lote expediente detalle enum.
	 *
	 * @param name the name
	 */
	LoteExpedienteDetalleEnum(String name) {
		this.name = name;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
}
