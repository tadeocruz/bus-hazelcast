package mx.isban.flujogd.filenet.manager;

import java.util.List;

import mx.isban.flujogd.common.bean.FilenetBean;
import mx.isban.flujogd.common.bean.FilenetBeanDetalle;
import mx.isban.flujogd.common.bean.ParametroBean;
import mx.isban.flujogd.common.generic.FileNetUtils;
import mx.isban.flujogd.common.util.QueueEnum;
import mx.isban.flujogd.persistence.PersistenceManager;
import mx.isban.flujogd.persistence.impl.PersistenceManagerImpl;

/**
 * Clase IdcUtils
 * 
 * @author Alvaro Zamorano azamorano@serviciosexternos.isban.mx
 *
 */
public class IdcUtils {
	
	/**
	 * PersistenceManager
	 */
	private PersistenceManager persistenceManager = new PersistenceManagerImpl();
	
	/**
	 * FileNetUtils
	 */
	private FileNetUtils utils = new FileNetUtils();

	/**
	 * retrieveIDCPath
	 * 
	 * @param idExpediente Id del expediente
	 * @return Vaina de filenet
	 */
	public FilenetBean retrieveIDCPath(String idExpediente) {				
		// Guardar los parametros del expediente en una lista
		List<ParametroBean> parametros = persistenceManager.retrieveParametrosBloque(idExpediente, QueueEnum.QUEUE_FILENET.getName());
		FilenetBean filenetBean = null;
		if (parametros != null) {
			filenetBean = new FilenetBean();
			FilenetBeanDetalle detalle = new FilenetBeanDetalle();
			// Colocar el detalle del bean
			filenetBean.setDetalle(detalle);
			// Recorrer todos los parametros y enviarlos al bean de conexion para filenet
			for (ParametroBean bean : parametros) {
				utils.setParametersFileNet(bean, filenetBean);
			}
		}
		return filenetBean;
	}

}
