package mx.isban.flujogd.filenet;

import java.io.File;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.system.ApplicationPidFileWriter;
import org.springframework.scheduling.annotation.EnableAsync;

import mx.isban.flujogd.common.generic.CommonsUtils;
import mx.isban.flujogd.filenet.configuration.HazelcastFilenetConfig;
import mx.isban.flujogd.filenet.listener.ClusterMembershipListener;

/**
 * The Class FlujoGDFilenetApplication. Metodo principal que ejecutara el
 * proceso de Filenet.
 * 
 * @author Alvaro Zamorano azamorano@serviciosexternos.isban.mx Clase
 *         FlujoGDFilenetApplication
 */
@SpringBootApplication
@EnableAsync
@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class })
public class FlujoGDFilenetApplication implements CommandLineRunner {

	/** Inicializamos un log para guardar la bitacora */
	private static final Logger LOG = LogManager.getLogger(FlujoGDFilenetApplication.class);

	/**
	 * main Metodo principal que inicia el flujo Filenet.
	 * 
	 * @param args Argumentos
	 */
	public static void main(String[] args) {
		CommonsUtils utils = new CommonsUtils();
		// Validar que existan los parametros requeridos para el proceso
		if (args.length < 5) {
			// Mostrar en bitacora la falta de parametros
			LOG.error(
					"No es posible inicar el componente sin los parametros 'IPs','port','path','componente' y 'Nombre subscriptor'");
			return;
		} else {
			// Colocar la lista de ips en la configuracion de hazelcast filenet
			HazelcastFilenetConfig.setIps(utils.getListaIp(args));
			// Colocar el puerto en la configuracion de hazelcast filenet
			HazelcastFilenetConfig.setPort(args[1]);
			// Colocar el nombre del subscriptor en la ejecucion del cluster de membresias
			ClusterMembershipListener.setNombreSubscriptorFilenet(args[4]);
			// Colocar el componente en la ejecucion del cluster de membresias
			ClusterMembershipListener.setComponenteFilenet(args[3]);
			SpringApplication sp = new SpringApplication(FlujoGDFilenetApplication.class);
			// Agregar los parametros a la ejecucion
			sp.addListeners(new ApplicationPidFileWriter(args[2] + File.separator + args[3] + ".pid"));
			// Correr los argumentos
			sp.run(args);
		}
	}

	/**
	 * Metodo nativo hazelcast que dispara el proceso de arranque.
	 *
	 * @param args the args Argumento variable de tipo String
	 * @throws Exception the exception Lanza error de tipo exception
	 */
	@Override
	public void run(String... args) throws Exception {
		LOG.info("FlujoGDFilenetApplication - run");
	}

}
