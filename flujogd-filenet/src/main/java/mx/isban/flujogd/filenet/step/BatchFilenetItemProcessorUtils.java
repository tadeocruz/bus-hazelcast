package mx.isban.flujogd.filenet.step;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hazelcast.core.IExecutorService;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.bean.NotificationBean;
import mx.isban.flujogd.common.bean.NotificationBean2;
import mx.isban.flujogd.common.bean.NotificationBean3;
import mx.isban.flujogd.common.bean.ParametroBean;
import mx.isban.flujogd.common.util.ExpedienteStatusEnum;
import mx.isban.flujogd.common.util.NotificationManager;
import mx.isban.flujogd.common.util.ParametroEnum;
import mx.isban.flujogd.common.util.XMLManager;
import mx.isban.flujogd.filenet.configuration.HazelcastFilenetConfig;
import mx.isban.flujogd.persistence.PersistenceManagerExpediente;
import mx.isban.flujogd.persistence.PersistenceManagerSubscriptor;
import mx.isban.flujogd.persistence.callable.BloquePersistenceTask;
import mx.isban.flujogd.persistence.callable.ExpedientePersistenceTask;
import mx.isban.flujogd.persistence.callable.NotificacionPersistenceTask;
import mx.isban.flujogd.persistence.impl.PersistenceManagerExpedienteImpl;
import mx.isban.flujogd.persistence.impl.PersistenceManagerSubscriptorImpl;

/**
 * Clase BatchFilenetItemProcessor
 * 
 * @author Alvaro Zamorano azamorano@serviciosexternos.isban.mx
 *
 */
public class BatchFilenetItemProcessorUtils {

	
	/**
	 * Inicializamos un log para guardar la bitacora 
	 */
	private static final Logger LOG = LogManager.getLogger(BatchFilenetItemProcessorUtils.class);
	
	private static final Integer IS_FILENET_PRCESADO = 1;
	
	/**
	 * Crear un string para guardar el executor service 
	 */
	private static final String EXECUTOR_SERVICE = "executorService";
	
	/**
	 * PersistenceManagerSubscriptor
	 */
	private PersistenceManagerSubscriptor persistenceManager = new PersistenceManagerSubscriptorImpl();
	
	/**
	 * PersistenceManagerExpediente
	 */
	private PersistenceManagerExpediente persistence = new PersistenceManagerExpedienteImpl();
	
	/**
	 * XMLManager
	 */
	private XMLManager xmlManager = new XMLManager();

	/**
	 * recupearBuc
	 * 
	 * @param expedienteBean Vaina del expediente
	 * @return Buc del expediente
	 */
	public String recupearBuc(ExpedienteBean expedienteBean) {				
		// Crear un string para guardar el nombre del subscriptor
		String nombreSubscriptor = persistenceManager.retrieveSubscriptorByIdExpediente(expedienteBean.getIdExpediente());
		// Crear un parametro bean para guardar la descripcion del subscriptor
		ParametroBean hazelcastPathParam = persistenceManager.retrieveParam(nombreSubscriptor, ParametroEnum.PARAMETRO_GRAL_FILE_DESCRIPTOR.getName());
		// Crear un string para guardar el buc del expediente
		return xmlManager.getBuc(expedienteBean.getPathUNZIPExpediente(), hazelcastPathParam.getValorParametro());
	}

	/**
	 * updatestatusbloqueexpediente
	 * 
	 * @param hazelcastFilenetConfig Configuracion de hazelcast de filenet
	 * @param expedienteBean         Vaina del expediente
	 * @param statusBloque           Estatus del bloque
	 * @param queueName              Nombre del a cola
	 * @param nombreError            Nombre del error
	 * @param buc                    Buc del expediente
	 * @return Estatus de la actualizacion del bloque
	 */
	public boolean updateStausBloqueExpediente(HazelcastFilenetConfig hazelcastFilenetConfig, ExpedienteBean expedienteBean, String statusBloque, String queueName,
			String nombreError, String buc) {
		
		IExecutorService executorService = hazelcastFilenetConfig.getInstanceFilenet().getExecutorService(EXECUTOR_SERVICE);
		Future<Object> future = executorService.submit(new BloquePersistenceTask(expedienteBean.getIdExpediente(), statusBloque, queueName, nombreError));
		
		boolean ok = false;
		
		try {
			ok = (Boolean) future.get();
			if (ok) {
				// Crear un string para guardar el estatus del expediente
				String estatusExpediente = ExpedienteStatusEnum.STATUS_ARCHIVO_IN_CM.getName();
				if (nombreError != null) {
					estatusExpediente = ocurrioUnError(hazelcastFilenetConfig, persistence, expedienteBean, queueName, nombreError, buc);
				}
				ok = updateStausExpediente(hazelcastFilenetConfig, expedienteBean.getIdExpediente(), estatusExpediente, nombreError);
			}
		} catch (InterruptedException e) {
			LOG.error(e.getMessage(), e);
			Thread.currentThread().interrupt();
		} catch (ExecutionException e) {
			LOG.error(e.getMessage(), e);
		}
		// Regresar el estatus del bloque
		return ok;
	}

	/**
	 * updateStausExpediente
	 * 
	 * @param idExpediente
	 * @param status
	 * @param idError
	 * @return
	 */
	private boolean updateStausExpediente(HazelcastFilenetConfig hazelcastFilenetConfig, String idExpediente, String status, String idError) {
		IExecutorService executorServiceFilenet = hazelcastFilenetConfig.getInstanceFilenet().getExecutorService(EXECUTOR_SERVICE);
		Future<Object> future = executorServiceFilenet.submit(new ExpedientePersistenceTask(idExpediente, status, idError));
		boolean okFilenet = false;
		try {
			// Obtener el estatus del expediente
			okFilenet = (Boolean) future.get();
		} catch (InterruptedException e) {
			LOG.error(e.getMessage(), e);
			Thread.currentThread().interrupt();
		} catch (ExecutionException e) {
			LOG.error(e.getMessage(), e);
		}
		return okFilenet;
	}

	/**
	 * updateStausExpediente
	 * 
	 * @param idExpediente
	 * @param status
	 * @param idError
	 * @return
	 */
	private boolean insertNotificacion(HazelcastFilenetConfig hazelcastFilenetConfig, ExpedienteBean expedienteBeanFilenet, String nombreError) {
		IExecutorService executorServiceFilenet = hazelcastFilenetConfig.getInstanceFilenet().getExecutorService(EXECUTOR_SERVICE);
		NotificationBean bean = new NotificationBean();
		NotificationBean2 bean2 = new NotificationBean2();
		NotificationBean3 bean3 = new NotificationBean3();
		bean2.setBean3(bean3);
		bean.setBean2(bean2);
		bean.setIdExpediente(expedienteBeanFilenet.getIdExpediente());
		bean.setRequest(null);
		bean.setResponseCode(null);
		bean.setResponseMsg(null);
		bean2.setRefExterna(null);
		bean2.setReqId(null);
		bean2.setErrorId(nombreError);
		bean2.setErrorMsg(nombreError);
		bean3.setBuc(null);
		bean3.setPathZip(expedienteBeanFilenet.getPathZIPExpediente());
		bean3.setFileName(expedienteBeanFilenet.getNombreExpediente());
		bean3.setPathUnzip(expedienteBeanFilenet.getPathUNZIPExpediente());
		Future<Object> future = executorServiceFilenet.submit(new NotificacionPersistenceTask(bean));
		boolean okFilenet = false;
		try {
			okFilenet = (Boolean) future.get();
		} catch (InterruptedException e) {
			LOG.error(e.getMessage(), e);
			Thread.currentThread().interrupt();
		} catch (ExecutionException e) {
			LOG.error(e.getMessage(), e);
		}
		return okFilenet;
	}

	/**
	 * ocurrioUnError
	 * 
	 * @param persistence
	 * @param expedienteBean
	 * @param queueName
	 * @param nombreError
	 * @return
	 */
	private String ocurrioUnError(HazelcastFilenetConfig hazelcastFilenetConfig, PersistenceManagerExpediente persistence, ExpedienteBean expedienteBean, String queueName,
			String nombreError, String buc) {
		String estatusExpediente = ExpedienteStatusEnum.STATUS_ERROR_ARCHIVO_IN_CM.getName();
		// Nueva validacion para numero de reintentos y envio de notificaciones
		Integer intentos = persistence.retrieveIntentosTipoError(expedienteBean.getIdExpediente(), queueName, nombreError);
		Integer reintentosPermitidos = persistence.retrieveReintentosByNombreError(nombreError);
		LOG.info("Intentos: {}", intentos);
		LOG.info("Reintentos: {}", reintentosPermitidos);
		if (intentos >= reintentosPermitidos) {
			LOG.info("Se superaron los intentos y el expediente sera notificado");
			// Validar estatus cuando alguno de los docs no se logro impactar en filenet
			estatusExpediente = ExpedienteStatusEnum.STATUS_PROCESO_CANCELADO.getName();
			NotificationManager notification = new NotificationManager();
			notification.preparaEnvioNotificacion(expedienteBean, nombreError, nombreError, buc);
			insertNotificacion(hazelcastFilenetConfig, expedienteBean, nombreError);
		}
		return estatusExpediente;
	}

}
