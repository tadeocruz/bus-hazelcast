package mx.isban.flujogd.filenet.step;

import java.text.ParseException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.item.ItemReader;

import com.hazelcast.core.IQueue;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.util.QueueEnum;
import mx.isban.flujogd.filenet.configuration.HazelcastFilenetConfig;

/**
 * Clase inicial BatchFilenetItemReader Reader para el filenet, el cuál se
 * encarga de obtener los expedientes a procesar
 * 
 * @author Alvaro Zamorano azamorano@serviciosexternos.isban.mx
 *
 */
public class BatchFilenetItemReader implements ItemReader<ExpedienteBean> {

	/**
	 * Inicializar un log para guardar la bitacora
	 */
	private static final Logger LOG = LogManager.getLogger(BatchFilenetItemReader.class);

	/**
	 * Crear un hazelcast filenet config
	 */
	private HazelcastFilenetConfig hazelcastFilenetConfig;

	/**
	 * Constructor
	 * 
	 * @param hazelcastFilenetConfig Configuracion de hazelcast de filenet
	 */
	public BatchFilenetItemReader(HazelcastFilenetConfig hazelcastFilenetConfig) {
		this.hazelcastFilenetConfig = hazelcastFilenetConfig;
	}

	/**
	 * Metodo que lee de la queue el siguiente dato a ser procesado
	 */
	@Override
	public ExpedienteBean read() throws InterruptedException, ParseException {
		// Inicializar un iqueue para guardar la cola de filenet
		IQueue<ExpedienteBean> queueFilenet = hazelcastFilenetConfig.getQueue(QueueEnum.QUEUE_FILENET.getName());
		try {
			// Tomar un elemento de la cola
			ExpedienteBean expedienteBean = queueFilenet.take();
			if (expedienteBean != null) {
				// Mostrar en bitacora el expediente recuperado
				LOG.info("=== SE RECUPERA EXPEDIENTE: {} - {} - {}", expedienteBean.getIdExpediente(), expedienteBean.getNombreExpediente(),
						expedienteBean.getDetalle().getDetalle2().getReferenciaExterna());
				return expedienteBean;
			}
		} catch (InterruptedException e1) {
			LOG.error(e1);
			Thread.currentThread().interrupt();
		}
		return null;
	}
}
