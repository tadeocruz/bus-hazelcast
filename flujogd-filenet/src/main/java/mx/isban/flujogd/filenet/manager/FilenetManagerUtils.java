package mx.isban.flujogd.filenet.manager;

import java.io.File;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.bean.ParametroBean;
import mx.isban.flujogd.common.bean.XMLManagerResponse;
import mx.isban.flujogd.common.util.ParametroEnum;
import mx.isban.flujogd.common.util.QueueEnum;
import mx.isban.flujogd.common.util.XMLManager;
import mx.isban.flujogd.persistence.PersistenceManagerBloqueDetail;
import mx.isban.flujogd.persistence.PersistenceManagerSubscriptor;
import mx.isban.flujogd.persistence.impl.PersistenceManagerBloqueDetailImpl;
import mx.isban.flujogd.persistence.impl.PersistenceManagerSubscriptorImpl;

/**
 * Clase FilenetManagerUtils
 * 
 * @author Alvaro Zamorano azamorano@serviciosexternos.isban.mx
 *
 */
public class FilenetManagerUtils {

	/**
	 * crearpatch
	 * 
	 * @param expedienteBean Vaina del expediente
	 */
	public void crearPath(ExpedienteBean expedienteBean) {
		PersistenceManagerBloqueDetail persistenceManagerFilenet = new PersistenceManagerBloqueDetailImpl();
		// Colocar la ruta del zip del expediente
		expedienteBean.setPathZIPExpediente(persistenceManagerFilenet
				.retrieveParametroBloque(expedienteBean.getIdExpediente(), QueueEnum.QUEUE_SFTP.getName(), ParametroEnum.PARAMETRO_SFTP_OUTPUT_PATH.getName()).get(0)
				.getValorParametro());
		// Inicializar un string para guardar la ruta para descomprimir el zip
		String pathUnzip = expedienteBean.getPathZIPExpediente() + File.separator + expedienteBean.getNombreExpediente();
		// Modificar la ruta para descomprimir el zip
		pathUnzip = pathUnzip.substring(0, pathUnzip.length() - 4);
		// Colocar la ruta para descomprimir el zip
		expedienteBean.setPathUNZIPExpediente(pathUnzip);
	}

	/**
	 * recuperardocumentos
	 * 
	 * @param expedienteBean Vaina del expediente
	 * @return Documentos del expediente
	 */
	public XMLManagerResponse recuperarDocumentos(ExpedienteBean expedienteBean) {
		XMLManager xmlManager = new XMLManager();
		PersistenceManagerSubscriptor persistenceManagerFilenet = new PersistenceManagerSubscriptorImpl();
		// Inicializar un string para guardar el nombre del subscriptor
		String nombreSubscriptor = persistenceManagerFilenet.retrieveSubscriptorByIdExpediente(expedienteBean.getIdExpediente());
		// Inicializar un parametro bean para guardar los parametros del subscriptor
		ParametroBean hazelcastPathParam = persistenceManagerFilenet.retrieveParam(nombreSubscriptor, ParametroEnum.PARAMETRO_GRAL_FILE_DESCRIPTOR.getName());
		// Obtener los documentos del expediente
		return xmlManager.getDocumentos(expedienteBean.getPathUNZIPExpediente(), hazelcastPathParam.getValorParametro());
	}

	/**
	 * recuperarfolders
	 * 
	 * @param expedienteBean Vaina del expediente
	 * @return Folders del expediente
	 */
	public XMLManagerResponse recuperarFolders(ExpedienteBean expedienteBean) {
		PersistenceManagerSubscriptor persistenceManagerFilenet = new PersistenceManagerSubscriptorImpl();
		// Inicializar un string para guardar el nombre del subscriptor
		String nombreSubscriptor = persistenceManagerFilenet.retrieveSubscriptorByIdExpediente(expedienteBean.getIdExpediente());
		// Inicializar un parametro bean para guardar los parametros del subscriptor
		ParametroBean hazelcastPathParam = persistenceManagerFilenet.retrieveParam(nombreSubscriptor, ParametroEnum.PARAMETRO_GRAL_FILE_DESCRIPTOR.getName());
		// Crear un xml manager
		XMLManager xmlManager = new XMLManager();
		// Obtener los folders del expediente
		return xmlManager.getFolders(expedienteBean.getPathUNZIPExpediente(), hazelcastPathParam.getValorParametro());
	}

}
