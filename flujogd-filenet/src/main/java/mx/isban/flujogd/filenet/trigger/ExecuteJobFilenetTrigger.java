package mx.isban.flujogd.filenet.trigger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Clase ExecuteJobFilenetTrigger Job para el filenet el cuál es desatado por un
 * trigger y ejecuta el proceso con base en los parámetros proporcionados
 * 
 * @author Alvaro Zamorano azamorano@serviciosexternos.isban.mx
 *
 */
@Component
public class ExecuteJobFilenetTrigger {

	/**
	 * Inicializar un log para guardar la bitacora
	 */
	private static final Logger LOG = LogManager.getLogger(ExecuteJobFilenetTrigger.class);

	/**
	 * Crear un job launcher
	 */
	@Autowired
	private JobLauncher jobLauncherFilenet;

	/**
	 * Crear un job
	 */
	@Autowired
	private Job processJobFilenet;

	/**
	 * Ejecuta el job batch del filenet para hacer el resguardo con base en los
	 * parámetros proporcionados
	 */
	public void executeJobFilenet() {
		try {
			// Configurar los parametros del job
			JobParameters jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis()).toJobParameters();
			// Correr el job con los parametros
			jobLauncherFilenet.run(processJobFilenet, jobParameters);
		} catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException | JobParametersInvalidException e) {
			LOG.error(e.getMessage(), e);
		}
	}

}
