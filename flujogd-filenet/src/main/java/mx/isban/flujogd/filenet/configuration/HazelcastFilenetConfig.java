package mx.isban.flujogd.filenet.configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.hazelcast.config.Config;
import com.hazelcast.config.ListenerConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IQueue;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.bean.MemberBean;
import mx.isban.flujogd.common.generic.HazelcastGenericUtils;
import mx.isban.flujogd.filenet.listener.ClusterMembershipListener;

/**
 * Clase HazelcastFilenetConfig
 * 
 * @author Alvaro Zamorano azamorano@serviciosexternos.isban.mx
 *
 */
@Configuration
public class HazelcastFilenetConfig {
	/**
	 * ips
	 */
	// Crear una lista de strings para guardar las ips
	private static List<String> ips;

	/**
	 * port
	 */
	// Crear un string para guardar el puerto
	private static String port;

	/**
	 * Constructor
	 */
	public HazelcastFilenetConfig() {
		ClusterMembershipListener.setHazelcastConfig(this);
	}

	/**
	 * 
	 * @return
	 */
	@Bean
	public HazelcastInstance getInstanceFilenet() {
		return Hazelcast.newHazelcastInstance(getHazelcastFilenetConfig());
	}

	/**
	 * getqueue
	 * 
	 * @param name Nombre de la cola
	 * @return Instancia de la cola
	 */
	public IQueue<ExpedienteBean> getQueue(String name) {
		return getInstanceFilenet().getQueue(name);
	}

	/**
	 * getmapmembers
	 * 
	 * @param name Nombre del mapa de miembros
	 * @return Instancia del mapa de miembros
	 */
	public Map<String, MemberBean> getMapMembers(String name) {
		return getInstanceFilenet().getMap(name);
	}

	/**
	 * getmapexpedientes
	 * 
	 * @param name Nombre del mapa de expedientes
	 * @return Instancia del mapa de expedientes
	 */
	public Map<String, Boolean> getMapExpedientes(String name) {
		return getInstanceFilenet().getMap(name);
	}

	/**
	 * getmapnotificaciones
	 * 
	 * @param name Nombre del mapa de notificaciones
	 * @return Instancia del mapa de notificaciones
	 */
	public Map<String, Boolean> getMapNotificaciones(String name) {
		return getInstanceFilenet().getMap(name);
	}

	/**
	 * Config cluster network and discovery mechanism
	 * 
	 * @return Config
	 */
	@Bean
	public Config getHazelcastFilenetConfig() {
		HazelcastGenericUtils util = new HazelcastGenericUtils();
		Config config = new Config();
		config.addListenerConfig(new ListenerConfig("mx.isban.flujogd.filenet.listener.ClusterMembershipListener"));
		config.setInstanceName("FlujoGD-Filenet");
		return util.setConfigHazelcast(port, ips, config);
	}

	/**
	 * @param ips the ips to set
	 */
	public static void setIps(List<String> ips) {
		List<String> copia = new ArrayList<>();
		copia.addAll(ips);
		HazelcastFilenetConfig.ips = copia;
	}

	/**
	 * @param port the port to set
	 */
	public static void setPort(String port) {
		HazelcastFilenetConfig.port = port;
	}

}
