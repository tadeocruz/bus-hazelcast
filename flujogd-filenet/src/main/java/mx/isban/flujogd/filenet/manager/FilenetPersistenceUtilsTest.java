package mx.isban.flujogd.filenet.manager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.filenet.api.exception.ExceptionCode;

import lombok.extern.log4j.Log4j2;
import mx.isban.flujogd.common.bean.DocumentoBean;
import mx.isban.flujogd.common.util.DocumentoStatusEnum;

@Log4j2
public class FilenetPersistenceUtilsTest {

	@Before
	public void setUp() throws Exception {
		System.setProperty("INTERFACES", "./");
	}

	@Test
	public void test() {
		
		String ref = "TF-92130387762";
		String errorMsg = "Error al Salvar en Filenet :: FNRCE0051E: E_OBJECT_NOT_FOUND: The requested item was not found. Folder /60610986965 not found";
		
		String status = DocumentoStatusEnum.STATUS_STORED_OK.getName();
		
		DocumentoBean documento = new DocumentoBean();
		List<DocumentoBean> documentosErroneos = new ArrayList<DocumentoBean>();
		
		//Crear un string para guardar el estatus del documento
		if(errorMsg!=null) {
		
			log.info("Para la referencia: {}, Se evalua el mensaje de error: {}", ref, errorMsg);
			
			boolean res = ( ref.startsWith("TF") || ref.startsWith("CB") );
			
			boolean res2 = errorMsg.contains (ExceptionCode.E_OBJECT_NOT_FOUND.getKey() );
			
			String key = ExceptionCode.E_OBJECT_NOT_FOUND.getKey();
			
			//si es de terminal financiero
			if( ( ref.startsWith("TF") || ref.startsWith("CB") ) && 
					errorMsg.contains(ExceptionCode.E_OBJECT_NOT_FOUND.getKey())
					) {
				//no hace nada, es un error controlado, no se agrega el mensaje de error
				log.error("No se agrega el mensaje a lista de errores");
			}else {
				
//				documento.getDetalle2().setErrorMsg(errorMsg);
				documentosErroneos.add(documento);
				status = DocumentoStatusEnum.STATUS_STORED_ERROR.getName();	
				log.error("Se agrega el mensaje a la lista de errores");
			}
			
		}
		
//		assertTrue(DocumentoStatusEnum.STATUS_STORED_OK.equals(status));
		assertTrue(documentosErroneos.size()<=0);
		
	}

}
