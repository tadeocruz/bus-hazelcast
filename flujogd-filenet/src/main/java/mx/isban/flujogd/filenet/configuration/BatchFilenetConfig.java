package mx.isban.flujogd.filenet.configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.support.transaction.ResourcelessTransactionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.filenet.listener.JobFilenetListener;
import mx.isban.flujogd.filenet.step.BatchFilenetItemProcessor;
import mx.isban.flujogd.filenet.step.BatchFilenetItemReader;
import mx.isban.flujogd.filenet.step.BatchFilenetItemWriter;

/**
 * Clase BatchFilenetConfig
 * 
 * @author Alvaro Zamorano azamorano@serviciosexternos.isban.mx
 *
 */
@EnableBatchProcessing(modular = true)
@Configuration
public class BatchFilenetConfig {

	// Inicializamos un log para guardar la bitacora
	private static final Logger LOG = LogManager.getLogger(BatchFilenetConfig.class);

	@Autowired
	// Crear un job builder factory
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	// Crear un step builder factory
	private StepBuilderFactory stepBuilderFactory;

	@Autowired
	// Crear un hazelcast filenet config
	private HazelcastFilenetConfig hazelcastFilenetConfig;

	/**
	 * @return Job
	 */
	@Bean
	public Job processJob() {
		// Mostrar en bitacora el inicio del job
		LOG.info("execute job");
		return jobBuilderFactory.get("jobBuilderFactory").incrementer(new RunIdIncrementer())
				// Disparador
				.listener(listener())
				// Orden del flujo
				.flow(orderStep()).end()
				// Compilar
				.build();
	}

	/**
	 * To create a step, reader, processor and writer has been passed serially
	 * 
	 * @return Step del job
	 */
	@Bean
	public Step orderStep() {
		// Mostrar el flujo del job
		LOG.info("step de job");
		return stepBuilderFactory.get("orderStep1").<ExpedienteBean, ExpedienteBean>chunk(1)
				// Lector
				.reader(new BatchFilenetItemReader(this.hazelcastFilenetConfig))
				// Procesamiento
				.processor(new BatchFilenetItemProcessor(this.hazelcastFilenetConfig)).writer(new BatchFilenetItemWriter(this.hazelcastFilenetConfig))
				// Compilar
				.build();
	}

	/**
	 * @return {@link JobExecutionListener}
	 */
	@Bean
	public JobExecutionListener listener() {
		return new JobFilenetListener();
	}

	/**
	 * @return {@link ResourcelessTransactionManager}
	 */
	@Bean
	public ResourcelessTransactionManager transactionManager() {
		return new ResourcelessTransactionManager();
	}
}
