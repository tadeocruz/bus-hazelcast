package mx.isban.flujogd.filenet.listener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hazelcast.core.ItemEvent;
import com.hazelcast.core.ItemListener;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.filenet.trigger.ExecuteJobFilenetTrigger;

/**
 * Clase QueueFilenetListener
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 *
 */
@Component
public class QueueFilenetListener implements ItemListener<ExpedienteBean> {

	//Inicializar un log para guardar la bitacora
	private static final Logger LOG = LogManager.getLogger(QueueFilenetListener.class);

	@Autowired
	//Crear un execue job filenet trigger
	private ExecuteJobFilenetTrigger jobBatch;

	/**
	 * itemAdded
	 * @param item
	 */
	@Override
	public void itemAdded(ItemEvent<ExpedienteBean> item) {
		LOG.info("Se detecta objeto agregado a la queue");
		jobBatch.executeJobFilenet();
	}

	/**
	 * itemRemoved
	 * @param item
	 */
	@Override
	public void itemRemoved(ItemEvent<ExpedienteBean> item) {
		LOG.info("Se detecta objeto removido de la queue");
	}

}