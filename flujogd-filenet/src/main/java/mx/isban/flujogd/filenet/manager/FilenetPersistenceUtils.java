package mx.isban.flujogd.filenet.manager;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.filenet.api.exception.ExceptionCode;

import lombok.extern.log4j.Log4j2;
import mx.isban.apigdmx.dg.Document;
import mx.isban.apigdmx.dg.Folder;
import mx.isban.apigdmx.dg.PropertiesBean;
import mx.isban.flujogd.common.bean.AtributoBean;
import mx.isban.flujogd.common.bean.DocumentoBean;
import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.bean.FolderBean;
import mx.isban.flujogd.common.bean.XMLManagerResponse;
import mx.isban.flujogd.common.util.DocumentoStatusEnum;
import mx.isban.flujogd.common.util.ExpedienteProcessTypeEnum;
import mx.isban.flujogd.filenet.apigdmx.client.ApiGdMxClient;
import mx.isban.flujogd.persistence.PersistenceManagerDocumento;
import mx.isban.flujogd.persistence.impl.PersistenceManagerDocumentoImpl;

/**
 * Clase de utileria para la persistencia de datos y conexion hacia filenet
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase FilenetPersistenceUtils
 * --------------------------------------
 * Modificado por : Jccalderon
 * Stefanini
 * 23/07/2019
 * Se agrega el envio de la aplicacion origen al filenet
 */
@Log4j2
public class FilenetPersistenceUtils {
	
	//Inicializar un log para guardar la bitacora
	private static final Logger LOG = LogManager.getLogger(FilenetPersistenceUtils.class);
	
	/**
	 * salvarDocumentos
	 * @param expedienteBean	Vaina del expediente
	 * @param apiClient	Cliente del api
	 * @return	Lista de documentos con errores
	 */
	public List<DocumentoBean> salvarDocumentos(ExpedienteBean expedienteBean,ApiGdMxClient apiClient, boolean salvarFolder) {
		
		PersistenceManagerDocumento pmd = new PersistenceManagerDocumentoImpl();
		List<DocumentoBean> documentosErroneos = new ArrayList<>();
		//Recuperamos de DB los documentos que esten validados correctamente
		Map<String,DocumentoBean> documentosDB = pmd.retrieveDocumentosByExpediente(expedienteBean.getIdExpediente());
		FilenetManagerUtils utils = new FilenetManagerUtils();
		//Obtener los documentos del expediente
		XMLManagerResponse response = utils.recuperarDocumentos(expedienteBean);
		//Guardar los documentos en una lista
		List<DocumentoBean> documentos = response.getDocumentos();
		for(DocumentoBean documento:documentos) {
			
			DocumentoBean docDB = documentosDB.get(documento.getDetalle().getNombreDocumento());
			
			if(docDB!=null) {
				documento.setIdDocumento(docDB.getIdDocumento());
				documento.setAsociarFolder(salvarFolder);
				
				//Enviar los parametros del expediente
				docDB.getDetalle2().setErrorMsg(procesarDocumento(expedienteBean, documento, documentosErroneos, apiClient, pmd));
			}
		}
		//Seteamos la lista de documentos al expediente
		expedienteBean.getDetalle().setDocumentos(new ArrayList<DocumentoBean>(documentosDB.values()));
		//Regresar lista de documentos con errores
		return documentosErroneos;
	}
	
	/**
	 * procesarDocumento
	 * @param expedienteBean	Vaina del expediente
	 * @param documento	Documento a evaluar
	 * @param documentosErroneos	Lista de documentos con errores
	 * @param apiClient	Cliente del api
	 * @param pmd	Documento del persistance manager
	 */
	private String procesarDocumento(ExpedienteBean expedienteBean,DocumentoBean documento,List<DocumentoBean> documentosErroneos,ApiGdMxClient apiClient,PersistenceManagerDocumento pmd){
		String status = DocumentoStatusEnum.STATUS_STORED_OK.getName();
		
		String ref =  expedienteBean.getDetalle().getDetalle2().getReferenciaExterna();
		
		try (FileInputStream fileInputStream =  new FileInputStream( new File( expedienteBean.getPathUNZIPExpediente()+File.separator+documento.getDetalle().getNombreDocumento() ) )){
			
			String name = documento.getDetalle().getNombreDocumento();
			List<PropertiesBean> metaData = new ArrayList<>();
			
			log.info("Procesando el documento: {}", name);
			
			String[] arr = name.split("_");
			String objectType;
			
			if(arr.length > 2) {
				
				objectType = arr[2];
				
				// Se agrega la propiedad de aplicacion origen
				PropertiesBean beanApp = new PropertiesBean("Formato", expedienteBean.getDetalle().getDetalle2().getDetalle3().getOrigen(), "STRING", null);
				metaData.add(beanApp);
				
			}else {
				
				objectType = arr[1];
				objectType = objectType.replace(".pdf", "");
			
			}
			
			Document document = new Document( name, objectType, fileInputStream, null );
			
			for(AtributoBean atributo:documento.getDetalle().getAtributos()) {
				//Obtener las propiedades del expediente
				PropertiesBean bean = new PropertiesBean(atributo.getNombre(), atributo.getValor(), atributo.getTipo(), null);
				metaData.add(bean);
			}
			
			String errorMsg = null;
			//Colocar los metadatos del documento
			document.setMetadata(metaData);
			
			if(ExpedienteProcessTypeEnum.PROCESO_TYPE_NUEVO.getName().equals(expedienteBean.getDetalle().getDetalle2().getDetalle3().getTipoProceso())) {
				errorMsg = apiClient.saveOrUpdateDocument(document,documento.getDetalle().getPath(), documento.isAsociarFolder());
			}else {
				errorMsg = apiClient.updateDocument(document,documento.getDetalle().getPath()+File.separator+documento.getDetalle().getNombreDocumento());
			}

			//Crear un string para guardar el estatus del documento
			if(errorMsg!=null) {
			
				log.info("Para la referencia: {}, Se evalua el mensaje de error: {}", ref, errorMsg);
				//si es de terminal financiero
				if( ( ref.startsWith("TF") || ref.startsWith("CB") ) && 
						errorMsg.contains(ExceptionCode.E_OBJECT_NOT_FOUND.getKey())
						) {
					//no hace nada, es un error controlado, no se agrega el mensaje de error
					log.error("No se agrega el mensaje a lista de errores");
				}else {
					
					documento.getDetalle2().setErrorMsg(errorMsg);
					documentosErroneos.add(documento);
					status = DocumentoStatusEnum.STATUS_STORED_ERROR.getName();	
					log.error("Se agrega el mensaje a la lista de errores");
				}
				
			}
			
			
			//Actualizar el estatus del documento
			pmd.updateStatusDocumento(documento.getIdDocumento(), status);
			//Si el expediente se inserto/actualizo correctamente en filenet se pone el valor en vacio para avisar que no ocurrio algun error
			if(status.equals(DocumentoStatusEnum.STATUS_STORED_OK.getName())) {
				status = "";
			}
		} catch (IOException | TimeoutException e) {
			LOG.error(e.getMessage(), e);
		} 
		return status;
	}
	
	/**
	 * salvarFolders
	 * @param expedienteBean	Vaina del expediente
	 * @param apiClient	Cliente del api
	 * @return	Estatus del expediente
	 * @throws TimeoutException	Excepcion de falta de tiempo
	 */
	public boolean salvarFolders(ExpedienteBean expedienteBean, ApiGdMxClient apiClient) throws TimeoutException {
		boolean okExp = true;
		FilenetManagerUtils utils = new FilenetManagerUtils();
		XMLManagerResponse response2 =  utils.recuperarFolders(expedienteBean);
		//Lista de folders del expediente
		List<FolderBean> folders = response2.getFolders();
		for(FolderBean folderBean:folders) {
			Folder folder = new Folder(folderBean.getNombre(),folderBean.getTipo(),null);
			List<PropertiesBean> metaData = new ArrayList<>();
			for(AtributoBean atributo:folderBean.getDetalle().getAtributos()) {
				PropertiesBean bean = new PropertiesBean(atributo.getNombre(), atributo.getValor(), atributo.getTipo(), null);
				metaData.add(bean);
			}
			//Colocar los metadatos del folder
			folder.setMetadata(metaData);
			int result = apiClient.saveOrUpdateFolder(folder,folderBean.getDetalle().getPathFilenet());
			if(result==0) {
				okExp = false;
				return okExp;
			}
		}
		//Regresar el estatus del expediente
		return okExp;
	}

}
