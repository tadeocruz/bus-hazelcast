package mx.isban.flujogd.filenet.configuration;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import mx.isban.flujogd.common.util.QueueEnum;
import mx.isban.flujogd.filenet.listener.QueueFilenetListener;

/**
 * Clase QueueFilenetConfig
 * 
 * @author Alvaro Zamorano azamorano@serviciosexternos.isban.mx
 *
 */
@Configuration
public class QueueFilenetConfig {

	/**
	 * log
	 */
	// Inicializamos un log para guardar la bitacora
	private static final Logger LOG = LogManager.getLogger(QueueFilenetConfig.class);

	@Autowired
	// Crear un hazelcast filenet config
	private HazelcastFilenetConfig hazelcastConfig;

	@Autowired
	// Crear un queue filenet listener
	private QueueFilenetListener queueInitialListener;

	/**
	 * postConstruct
	 */
	@PostConstruct
	public void postConstruct() {
		hazelcastConfig.getQueue(QueueEnum.QUEUE_FILENET.getName()).addItemListener(queueInitialListener, true);
		LOG.info("FlujoGD-Filenet  - Listener Start");
	}
}
