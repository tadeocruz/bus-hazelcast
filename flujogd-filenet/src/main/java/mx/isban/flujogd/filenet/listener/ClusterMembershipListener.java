package mx.isban.flujogd.filenet.listener;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hazelcast.core.MemberAttributeEvent;
import com.hazelcast.core.MembershipEvent;
import com.hazelcast.core.MembershipListener;

import mx.isban.flujogd.common.bean.MemberBean;
import mx.isban.flujogd.common.bean.NetConfigBean;
import mx.isban.flujogd.common.util.InetAddressUtil;
import mx.isban.flujogd.common.util.MapEnum;
import mx.isban.flujogd.filenet.configuration.HazelcastFilenetConfig;
import mx.isban.flujogd.persistence.PersistenceManagerSubscriptor;
import mx.isban.flujogd.persistence.impl.PersistenceManagerSubscriptorImpl;

/**
 * Clase ClusterMembershipListener
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 *
 */
public class ClusterMembershipListener implements MembershipListener{
	
	//Inizializamos un log para guardar la bitacora
	private static final Logger LOG = LogManager.getLogger(ClusterMembershipListener.class);
	
	/**
	 * nombreSubscriptor
	 */
	//Creamos un string para guardar el nombre del subscriptor
	private static String nombreSubscriptor;
	/**
	 * componente
	 */
	//Creamos un string para guardar el componente
	private static String componente;
	/**
	 * hazelcastConfig
	 */
	//Creamos un hazelcast filenet config
	private static HazelcastFilenetConfig hazelcastConfig;

	/**
	 * memberAdded
	 * @param membershipEvent
	 */
	@Override
	public void memberAdded(MembershipEvent membershipEvent) {
		//Obtener el uuid del miembro
		String uuid = membershipEvent.getMember().getUuid();
		//Obtener el uuid del miembro local
		String uuidLocal = membershipEvent.getCluster().getLocalMember().getUuid();
		//Mostrar en bitacora el miembro agregado
		LOG.info("Se grego un nuevo miembro: "+uuid);
		if(uuid.equals(uuidLocal)) {
			LOG.info("flujogd-filenet: "+uuid);
			//Notificar al miembro
			notifyMemberAddFilenet(uuid);
		}
	}

	/**
	 * memberRemoved
	 * @param membershipEvent
	 */
	@Override
	public void memberRemoved(MembershipEvent membershipEvent) {
		LOG.info("Se elimino el miembro: "+membershipEvent.getMember().getUuid());
		notifyMemberRemovedFilenet(membershipEvent.getMember().getUuid());
	}
	
	/**
	 * Notifica al cluster que flujo fue agregado
	 * @param uuid
	 */
	private void notifyMemberAddFilenet(String uuid) {
		LOG.info("Notificamos al cluster que flujo fue agregado");
		PersistenceManagerSubscriptor persistenceManagerFilenet = new PersistenceManagerSubscriptorImpl();
		InetAddressUtil inetUtilFilenet = new InetAddressUtil();
		String ipNetFilenet = inetUtilFilenet.getLocalNet();
		if(!"".equals(ipNetFilenet)) {
			//Configurar el bean con los datos del subscriptor
			NetConfigBean netConfigBeanFilenet = persistenceManagerFilenet.retrieveNetConfigByIp(nombreSubscriptor,ipNetFilenet);
			if(netConfigBeanFilenet!=null){
				//Obtener el mapa de expedientes en proceso
				Map<String, MemberBean> members = hazelcastConfig.getMapMembers(MapEnum.MAP_MEMBER_STATUS.getName());
				MemberBean memberFilenet = new MemberBean();
				//Configurar el miembro
				memberFilenet.setActive(true);
				memberFilenet.setIp(netConfigBeanFilenet.getIp());
				memberFilenet.setPort(netConfigBeanFilenet.getPort());
				memberFilenet.setName(componente);
				memberFilenet.setUuid(uuid);
				members.put(uuid, memberFilenet);
				LOG.info("La notificacion se realizo con exito");
			}
		}else {
			LOG.info("Ocurrio un problema al recuperar la IP de este servidor, sin ella no es posible notificar la baja del nodo");
		}
	}

	/**
	 * Notifica al cluster que flujo se dio de baja
	 * @param uuid
	 */
	private void notifyMemberRemovedFilenet(String uuid) {
		//Mostrar notificacion en bitacora
		LOG.info("Notificamos al cluster que flujo se dio de baja");
		PersistenceManagerSubscriptor persistenceManagerFilenet = new PersistenceManagerSubscriptorImpl();
		InetAddressUtil inetUtilFilenet = new InetAddressUtil();
		String ipNetFilenet = inetUtilFilenet.getLocalNet();
		if(!"".equals(ipNetFilenet)) {
			//Configurar el bean con los datos del subscriptor
			NetConfigBean netConfigBeanFilenet = persistenceManagerFilenet.retrieveNetConfigByIp(nombreSubscriptor,ipNetFilenet);
			if(netConfigBeanFilenet!=null){
				//Obtener el mapa del estatus del miembro
				Map<String, MemberBean> members = hazelcastConfig.getMapMembers(MapEnum.MAP_MEMBER_STATUS.getName());
				MemberBean memberFilenet = members.get(uuid);
				if(memberFilenet!=null) {
					//Configurar el miembro
					memberFilenet.setActive(false);
					members.replace(uuid, memberFilenet);
				}
				LOG.info("La notificacion se realizo con exito");
			}
		}else {
			LOG.info("Ocurrio un problema al recuperar la IP de este servidor, sin ella no es posible notificar la baja del nodo");
		}
	}

	/**
	 * memberAttributeChanged
	 * @param memberAttributeEvent
	 */
	@Override
	public void memberAttributeChanged(MemberAttributeEvent memberAttributeEvent) {
		LOG.info("Cambio un atributo de uno de los miembros");
	}

	/**
	 * @param nombreSubscriptor the nombreSubscriptor to set
	 */
	public static void setNombreSubscriptorFilenet(String nombreSubscriptor) {
		ClusterMembershipListener.nombreSubscriptor = nombreSubscriptor;
	}

	/**
	 * @param componente the componente to set
	 */
	public static void setComponenteFilenet(String componente) {
		ClusterMembershipListener.componente = componente;
	}

	/**
	 * @param hazelcastConfig the hazelcastConfig to set
	 */
	public static void setHazelcastConfig(HazelcastFilenetConfig hazelcastConfig) {
		ClusterMembershipListener.hazelcastConfig = hazelcastConfig;
	}

}
