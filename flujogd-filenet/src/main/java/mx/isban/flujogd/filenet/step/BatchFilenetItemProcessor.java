package mx.isban.flujogd.filenet.step;

import java.security.KeyStoreException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.item.ItemProcessor;

import mx.isban.apigdmx.exception.ApiGdInfraestructureException;
import mx.isban.flujogd.common.bean.DocumentoBean;
import mx.isban.flujogd.common.bean.ErrorDocumentoBean;
import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.util.ExpedienteErrorEnum;
import mx.isban.flujogd.common.util.ExpedienteProcessTypeEnum;
import mx.isban.flujogd.common.util.ExpedienteUtil;
import mx.isban.flujogd.common.util.NotificationManager;
import mx.isban.flujogd.common.util.QueueEnum;
import mx.isban.flujogd.common.util.QueueStatusEnum;
import mx.isban.flujogd.filenet.apigdmx.client.ApiGdMxClient;
import mx.isban.flujogd.filenet.configuration.HazelcastFilenetConfig;
import mx.isban.flujogd.filenet.manager.FilenetManagerUtils;
import mx.isban.flujogd.filenet.manager.FilenetPersistenceUtils;
import mx.isban.flujogd.filenet.manager.IdcUtils;

/**
 * Clase BatchFilenetItemProcessor
 * 
 * @author Alvaro Zamorano azamorano@serviciosexternos.isban.mx
 *
 */
public class BatchFilenetItemProcessor implements ItemProcessor<ExpedienteBean, ExpedienteBean> {

	// Inicializar un log para guardar la bitacora
	private static final Logger LOG = LogManager.getLogger(BatchFilenetItemProcessor.class);
	
	// Inicializar un batch filenet item processor utils
	private BatchFilenetItemProcessorUtils batchUtils = new BatchFilenetItemProcessorUtils();
	
	// Crear un hazelcast filenet config
	private HazelcastFilenetConfig hazelcastFilenetConfig;
	
	/**
	 * IDentificador del flujo vivere
	 */
	public static final Integer ID_FLUJO_VIVERE = 1;

	/**
	 * Constructor
	 * 
	 * @param hazelcastFilenetConfig Configuracion de hazelcast de filenet
	 */
	public BatchFilenetItemProcessor(HazelcastFilenetConfig hazelcastFilenetConfig) {
		this.hazelcastFilenetConfig = hazelcastFilenetConfig;
	}

	/**
	 * process
	 * 
	 * @param expedienteBean Vaina del expediente
	 * @return Esatus del proceso
	 * @throws Exception Excepcion general
	 */
	@Override
	public synchronized ExpedienteBean process(ExpedienteBean expedienteBean) throws Exception {
		
		FilenetManagerUtils utils = new FilenetManagerUtils();
		FilenetPersistenceUtils persistenceUtils = new FilenetPersistenceUtils();
		IdcUtils idcUtils = new IdcUtils();
		
		if (expedienteBean.getPathUNZIPExpediente() == null) {
			// Crear la ruta del expediente
			utils.crearPath(expedienteBean);
		}
		
		boolean okExp = true;
		boolean salvarFolder = false;
		ApiGdMxClient apiClient = null;
		
		try {
			apiClient = new ApiGdMxClient(idcUtils.retrieveIDCPath(expedienteBean.getIdExpediente()));
			// Impactamos los folders si es nuevo el expediente
			
			if (ExpedienteProcessTypeEnum.PROCESO_TYPE_NUEVO.getName().equals(expedienteBean.getDetalle().getDetalle2().getDetalle3().getTipoProceso())) {
				
				if( expedienteBean.getDetalle().getIdFlujo() <= 10 ) {
					salvarFolder = true;
					okExp = persistenceUtils.salvarFolders(expedienteBean, apiClient);
				}
			}
			
		} catch (KeyStoreException | ApiGdInfraestructureException | TimeoutException e) {
			// Mostrar el error en bitacora
			LOG.error("Error al realziar la conexion con ApiGdMxClient");
			LOG.error(e.getMessage(), e);
			return error(expedienteBean, ExpedienteErrorEnum.ERROR_GDOCUMENTAL_CONEXION.getName(), null);
		}
		
		if (okExp) {
			return impactaDocumentos(expedienteBean, apiClient, salvarFolder);
		} else {
			
			// Notificacion de que los folders del expediente estan mal formados
			String buc = batchUtils.recupearBuc(expedienteBean);
			return error(expedienteBean, ExpedienteErrorEnum.ERROR_GDOCUMENTAL_GUARDAR.getName(), buc);
		}
		
	}

	/**
	 * impactaDocumentos
	 * 
	 * @param expedienteBean Vaina del expediente
	 * @param apiClient      Cliente del api
	 * @param salvarFolder indica si se guarda el folder
	 * @return Estatus del proceso
	 */
	private ExpedienteBean impactaDocumentos(ExpedienteBean expedienteBean, ApiGdMxClient apiClient, boolean salvarFolder) {
		
		FilenetPersistenceUtils persistenceUtils = new FilenetPersistenceUtils();
		String buc = batchUtils.recupearBuc(expedienteBean);
		
		// Impactamos los documentos
		List<DocumentoBean> documentosErroneos = persistenceUtils.salvarDocumentos(expedienteBean, apiClient, salvarFolder);
		
		if (documentosErroneos.isEmpty()) {
			
			batchUtils.updateStausBloqueExpediente(hazelcastFilenetConfig, expedienteBean, QueueStatusEnum.STATUS_OK.getName(), QueueEnum.QUEUE_FILENET.getName(), null, null);
			ExpedienteUtil expedienteUtil = new ExpedienteUtil();
			
			// Configurar el expediente
			expedienteUtil.cambiaEstausQueue(expedienteBean);
			expedienteUtil.determinaSiguienteQueue(expedienteBean);
			NotificationManager notification = new NotificationManager();
			notification.preparaEnvioNotificacion(expedienteBean, "", "", buc);
			
			return expedienteBean;
			
		} else {
			
			List<ErrorDocumentoBean> erroresFilenet = new ArrayList<>();
			
			documentosErroneos.forEach( i -> {
				ErrorDocumentoBean err = new ErrorDocumentoBean(i.getTipoDocumento(), i.getDetalle2().getErrorMsg());
				erroresFilenet.add(err);
				
				expedienteBean.getExpedienteSucursal().setErroresFilenet(erroresFilenet);
			});
			
			// Notificamos el o los documentos que no se pudieron impactar
			return error(expedienteBean, ExpedienteErrorEnum.ERROR_GDOCUMENTAL_GUARDAR.getName(), buc);
		}
	}

	/**
	 * error
	 * 
	 * @param expedienteBean Vaina del expediente
	 * @param errorName      Nombre del error
	 * @return Error del bean
	 */
	private ExpedienteBean error(ExpedienteBean expedienteBean, String nombreError, String buc) {
		// Obtener el detalle del expediente
		expedienteBean.getDetalle().setSiguienteQueue("");
		LOG.error(nombreError);
		// Actualizar el estatus del expediente
		batchUtils.updateStausBloqueExpediente(hazelcastFilenetConfig, expedienteBean, QueueStatusEnum.STATUS_ERROR.getName(), QueueEnum.QUEUE_FILENET.getName(), nombreError, buc);
		// Regresar el bean
		return expedienteBean;
	}

}
