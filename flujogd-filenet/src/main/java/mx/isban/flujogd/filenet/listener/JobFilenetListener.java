package mx.isban.flujogd.filenet.listener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;

/**
 * Clase JobFilenetListener
 * 
 * @author Alvaro Zamorano azamorano@serviciosexternos.isban.mx
 *
 */
public class JobFilenetListener extends JobExecutionListenerSupport {

	/**
	 * LOG
	 */
	private static final Logger LOG = LogManager.getLogger(JobFilenetListener.class);

	/**
	 * afterJob
	 * 
	 * @param jobExecution
	 */
	@Override
	public void afterJob(JobExecution jobExecution) {
		if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
			LOG.info("is done");
		}
	}
}
