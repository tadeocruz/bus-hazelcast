package mx.isban.flujogd.filenet.apigdmx.client;

import java.security.KeyStoreException;
import java.util.Properties;
import java.util.concurrent.TimeoutException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mx.isban.apigdmx.IConnection;
import mx.isban.apigdmx.commons.ConnectionCommonExtendedProperties;
import mx.isban.apigdmx.commons.ConnectionCommonProperties;
import mx.isban.apigdmx.connection.ConnectionFactoryImplFileNet;
import mx.isban.apigdmx.dg.Document;
import mx.isban.apigdmx.dg.Folder;
import mx.isban.apigdmx.exception.ApiGdBusinessException;
import mx.isban.apigdmx.exception.ApiGdInfraestructureException;
import mx.isban.apigdmx.session.SessionBacthImplFileNet;
import mx.isban.flujogd.common.bean.FilenetBean;
import mx.isban.idc.core.appserver.IsbKeyStorePublicOps;
import mx.isban.idc.core.appserver.IsbProvider;

/**
 * Clase para las conexiones hacia filenet
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase ApiGdMxClient
 *
 */
public class ApiGdMxClient {

	//Inicializamos un log para guardar la bitacora
	private static final Logger LOG = LogManager.getLogger(ApiGdMxClient.class);

	//Crear un connection factory impl filenet
	private ConnectionFactoryImplFileNet connectionFactoryFilenet;
	//Crear un iconnection
	private IConnection connection;

	/**
	 * Constructor
	 * @param bean	Vaina de filenet
	 * @throws KeyStoreException	Excepcion general
	 * @throws ApiGdInfraestructureException	Excepcion de infraestructura
	 */
	public ApiGdMxClient(FilenetBean bean) throws KeyStoreException, ApiGdInfraestructureException {
		String password = null;
			//El pathIDC debe de venir NULO no VACIO
			if(bean.getPathIdc()!=null) {
				LOG.info("LA CONEXION HACIA FILENET SERA POR IDC");
				//Configurar las propiedades
				System.setProperty("java.naming.factory.initial","mx.isban.idc.jndi.provider.JndiProvider");
				System.setProperty("JNDI_F_PATH",bean.getPathIdc());
				IsbKeyStorePublicOps pub = IsbProvider.getInstance();
				password = new String(pub.getSecretData(bean.getDetalle().getSecretData()));
			}else {
				LOG.info("LA CONEXION HACIA FILENET SERA POR PASSWORD DESDE DB");
				password = bean.getDetalle().getPassword();
			}
		
		LOG.info("FILENET USER: "+bean.getUser());
		LOG.trace("FILENET IDC: "+bean.getPathIdc());
		LOG.trace("FILENET SECRET DATA: "+bean.getDetalle().getSecretData());
		LOG.trace("FILENET CLAVE UNICA: "+password);
		LOG.info("FILENET URI: "+bean.getUri());
		LOG.info("FILENET JASS: "+bean.getJaas());
		LOG.info("FILENET OBJECT STORE: "+bean.getDetalle().getStore());

		//Crear la conexion
		connectionFactoryFilenet = new ConnectionFactoryImplFileNet();
		ConnectionCommonProperties connectionCommonProperties = new ConnectionCommonProperties();
		ConnectionCommonExtendedProperties connectionCommonExtendedProperties = new ConnectionCommonExtendedProperties();

		//Configurar la conexion
		connectionCommonProperties.setPassword(password);
		connectionCommonProperties.setUser(bean.getUser());
		connectionCommonExtendedProperties.setUri(bean.getUri());
		connectionCommonExtendedProperties.setJaasName(bean.getJaas());
		connectionCommonExtendedProperties.setObjectStoreName(bean.getDetalle().getStore());

		//Agregar propiedades a la conexion
		connectionFactoryFilenet.setConnectionCommonExtendedProperties(connectionCommonExtendedProperties);
		connectionFactoryFilenet.setConnectionCommonProperties(connectionCommonProperties);

		//Iniciar la conexion
		connection = connectionFactoryFilenet.getConnection();
	}

	/**
	 * saveOrUpdateDocument
	 * @param document	Documento a evaluar
	 * @param path	Ruta del documento
	 * @param asociarFolder indica si se asocia el documento al folder
	 * @return	Mensaje de respuesta
	 */
	public String saveOrUpdateDocument(Document document, String path, boolean asociarFolder) {
		SessionBacthImplFileNet batch = null;
		String errorMsg = null;
		LOG.info("Directorio a guardar el documento "+ document+" :: "+path);
		try { 
			batch = new SessionBacthImplFileNet(connection);
			if(batch.saveOrUpdate( document, path, new Properties (), asociarFolder )==0) {
				errorMsg = "Error 0: No se pudo realizar impacto en Filenet";
			}
		}catch (TimeoutException | ApiGdBusinessException e) {
			errorMsg = e.getMessage();
			LOG.error(e.getMessage(), e);
		} 
		return errorMsg;
	}

	/**
	 * saveOrUpdateFolder
	 * @param folder	Folder a evaluar
	 * @param path	Ruta del folder
	 * @return	Estatus del proceso
	 * @throws TimeoutException	Excepcion de falta de tiempo
	 */
	public int saveOrUpdateFolder(Folder folder,String path) throws TimeoutException {
		//Inicializar un batch
		SessionBacthImplFileNet batch = new SessionBacthImplFileNet(connection);
		int ok = 1;
		try {
			//Guardar el batch
			ok = batch.saveOrUpdate( folder, path, new Properties (), true );
		}catch (ApiGdBusinessException e) {
			LOG.error(e.getMessage(), e);
		}
		//Regresar el estatus
		return ok;
	}
	
	/**
	 * saveOrUpdateDocument
	 * @param document	Documento a actualizar
	 * @param path	Ruta del documento
	 * @return	Mensaje de respuesta
	 * @throws TimeoutException	Excepcion de falta de tiempo
	 */
	public String updateDocument(Document document, String path) throws TimeoutException {
		SessionBacthImplFileNet batch = new SessionBacthImplFileNet(connection);
		String errorMsg = null;
		try {
			if(batch.update( document, path, new Properties () )==0) {
				errorMsg = "Error 0: No se pudo realizar actualizacion en Filenet";
			}
		}catch (ApiGdBusinessException e) {
			errorMsg = e.getMessage();
			LOG.error(e.getMessage(), e);
		}
		return errorMsg;
	}

}
