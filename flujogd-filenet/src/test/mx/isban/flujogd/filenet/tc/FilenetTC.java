package mx.isban.flujogd.filenet.tc;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeoutException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.filenet.api.constants.TypeID;

import mx.isban.apigdmx.IConnection;
import mx.isban.apigdmx.commons.ConnectionCommonExtendedProperties;
import mx.isban.apigdmx.commons.ConnectionCommonProperties;
import mx.isban.apigdmx.connection.ConnectionFactoryImplFileNet;
import mx.isban.apigdmx.dg.Folder;
import mx.isban.apigdmx.dg.PropertiesBean;
import mx.isban.apigdmx.exception.ApiGdBusinessException;
import mx.isban.apigdmx.exception.ApiGdInfraestructureException;
import mx.isban.apigdmx.session.SessionBacthImplFileNet;
import mx.isban.flujogd.common.bean.AtributoBean;
import mx.isban.flujogd.common.bean.DocumentoBean;
import mx.isban.flujogd.common.bean.XMLManagerResponse;
import mx.isban.flujogd.common.util.XMLManager;

public class FilenetTC {

	private static final Logger LOG = LogManager.getLogger(FilenetTC.class);

	public static void main(String[] args){
		/*
		ConnectionFactoryImplFileNet connectionFactory = new ConnectionFactoryImplFileNet();

		ConnectionCommonProperties connectionCommonProperties = new ConnectionCommonProperties();
		ConnectionCommonExtendedProperties connectionCommonExtendedProperties = new ConnectionCommonExtendedProperties();

		
		 * connectionCommonProperties.setPassword("Delo854pu#7YAx");//password
		 * connectionCommonProperties.setUser("deupopfu");//filenet
		 * connectionCommonExtendedProperties.setUri(
		 * "https://wsytrdlm2mxr204.dev.mx.corp:1443/wsi/FNCEWS40MTOM");
		 * connectionCommonExtendedProperties.setObjectStoreName("os_optimus_dev");
		 * connectionCommonExtendedProperties.setJaasName("FileNetP8WSI");
		 

		connectionCommonProperties.setPassword("password");
		connectionCommonProperties.setUser("filenet");
		connectionCommonExtendedProperties.setUri("http://ibmfilenet:9080/wsi/FNCEWS40MTOM/");
		connectionCommonExtendedProperties.setJaasName("FileNetP8WSI");
		connectionCommonExtendedProperties.setObjectStoreName("OS1");

		connectionFactory.setConnectionCommonExtendedProperties(connectionCommonExtendedProperties);
		connectionFactory.setConnectionCommonProperties(connectionCommonProperties);

		IConnection connection = connectionFactory.getConnection();
		SessionBacthImplFileNet batch = new SessionBacthImplFileNet(connection);

		// Folder docto = new Folder("00811949", "Producto", null);

		XMLManager xmlManager = new XMLManager();
		// SessionBacthImplFileNet batch = new SessionBacthImplFileNet(connection);
		// List<PropertiesBean> metaData = new ArrayList<>();

		String path = "C:\\Users\\Administrador\\Downloads\\00811949";
		String nameFile = "expediente.xml";

		XMLManagerResponse response = xmlManager.getDocumentos(path, nameFile);
		List<DocumentoBean> documentos = response.getDocumentos();

		String no = "";
		for (DocumentoBean documento : documentos) {
			for (AtributoBean atributo : documento.getDetalle().getAtributos()) {
				// Obtener las propiedades del expediente
				System.out.println(atributo.getValor());
				if ("Super Nómina".equals(atributo.getValor())) {
					no = atributo.getValor();
				}
				PropertiesBean bean1 = new PropertiesBean(atributo.getNombre(), atributo.getValor(), atributo.getTipo(),
						null);
				// metaData.add(bean1);
			}
		}

		Folder docto = new Folder("010058", "Producto", null);
		FileInputStream fileInputStream = null;

		List<PropertiesBean> metaData = new ArrayList<PropertiesBean>();

		PropertiesBean bean1 = new PropertiesBean("BUC", "00811949", null, null);
		metaData.add(bean1);
		PropertiesBean bean2 = new PropertiesBean("Familia", "01", null, null);
		metaData.add(bean2);

		PropertiesBean bean3 = new PropertiesBean("IDFamilia", "01", null, null);
		metaData.add(bean3);
		PropertiesBean bean4 = new PropertiesBean("Subproducto", "0058", null, null);
		metaData.add(bean4);
		PropertiesBean bean5 = new PropertiesBean("NomComunProducto", "01", null, null);
		metaData.add(bean5);
		PropertiesBean bean6 = new PropertiesBean("IdProducto", "01", null, null);
		metaData.add(bean6);
		PropertiesBean bean7 = new PropertiesBean("Producto", no, null, null);
		metaData.add(bean7);
		PropertiesBean bean8 = new PropertiesBean("ProductoActivo", "true", TypeID.BOOLEAN.toString(), null);
		metaData.add(bean8);
		PropertiesBean bean9 = new PropertiesBean("CarpetaDocumentalProducto", "12345678", null, null);
		metaData.add(bean9);
		PropertiesBean bean10 = new PropertiesBean("IdSubproducto", "0058", null, null);
		metaData.add(bean10);
		PropertiesBean bean11 = new PropertiesBean("IDPersonalidad", "F", null, null);
		metaData.add(bean11);
		PropertiesBean bean12 = new PropertiesBean("IdSubproducto", "0058", null, null);
		metaData.add(bean12);
		PropertiesBean bean13 = new PropertiesBean("NivelRiesgo", "A1", null, null);
		metaData.add(bean13);
		PropertiesBean bean14 = new PropertiesBean("Nombres", "Juan Carlos", null, null);
		metaData.add(bean14);
		PropertiesBean bean15 = new PropertiesBean("Paterno", "Díaz", null, null);
		metaData.add(bean15);
		PropertiesBean bean16 = new PropertiesBean("Materno", "Ramirez", null, null);
		metaData.add(bean16);
		PropertiesBean bean17 = new PropertiesBean("RFC", "dirj810916387", null, null);
		metaData.add(bean17);
		PropertiesBean bean18 = new PropertiesBean("FechaNacimiento", "1981-09-16", TypeID.DATE.toString(), null);
		metaData.add(bean18);
		PropertiesBean bean19 = new PropertiesBean("Refundido", "true", TypeID.BOOLEAN.toString(), null);
		metaData.add(bean19);
		PropertiesBean bean20 = new PropertiesBean("Condicion", "PRO", null, null);
		metaData.add(bean20);
		PropertiesBean bean21 = new PropertiesBean("Nacionalidad", "Mexicana", null, null);
		metaData.add(bean21);
		PropertiesBean bean22 = new PropertiesBean("FechaActualizacion", "2018-10-23 12:04:02", TypeID.DATE.toString(),
				null);
		metaData.add(bean22);
		PropertiesBean bean23 = new PropertiesBean("Actualizado", "true", TypeID.BOOLEAN.toString(), null);
		metaData.add(bean23);

		docto.setMetadata(metaData);
		batch.saveOrUpdate(docto, "/00811949/01", new Properties());

		LOG.info("done...");
	}
*/
	}
}
