package mx.isban.flujogd.zip;

import java.io.File;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.bean.ParametroBean;
import mx.isban.flujogd.common.util.ParametroEnum;
import mx.isban.flujogd.persistence.PersistenceManagerSubscriptor;
import mx.isban.flujogd.persistence.impl.PersistenceManagerSubscriptorImpl;
import mx.isban.flujogd.zip.bean.ZipResponseBean;
import mx.isban.flujogd.zip.manager.ZIPManager;

public class ZipTC {
	
	private static final Logger log = LogManager.getLogger(ZipTC.class);
	
	public static void main(String[] args) {
//		ZipTC sipTC = new ZipTC();
		log.info("inicia...");
//		PersistenceManager pm = new PersistenceManagerImpl();
//		List<ExpedienteBean> expedientes = pm.retrieveAllExpedientes();
//		ExpedienteBean expedienteBean = expedientes.get(0);
//		ExpedienteBean expedienteBean = new ExpedienteBean();
//		expedienteBean.setIdExpediente("13433");
//		expedienteBean.setPathZIPExpediente("D:/Temporal");
//		expedienteBean.setNombreExpediente("51829902.zip");
//		sipTC.validaTamano(expedienteBean);
		ZIPManager zipManager = new ZIPManager();
//		String pathZip = expedienteBean.getPathZIPExpediente()+"/"+expedienteBean.getNombreExpediente();
		String pathZip = "\\\\192.168.15.50\\planOPTIMUS\00448351.zip";
		ZipResponseBean zipResponse = zipManager.unzip(pathZip);
		if(zipResponse.isOk()) 
			log.info("El expediente se descomprimio con exito");
		else{
			log.error("El expediente No se pudo descomprimir");
			log.error(zipResponse.getErrorMessage());
		}
	}
	
	/**
	 * validaTamano
	 * @param expedienteBean
	 * @return
	 */
	public boolean validaTamano(ExpedienteBean expedienteBean) {
		PersistenceManagerSubscriptor persistenceManager = new PersistenceManagerSubscriptorImpl();
		String nombreSubscriptor = persistenceManager.retrieveSubscriptorByIdExpediente(expedienteBean.getIdExpediente());
		ParametroBean maxSize = persistenceManager.retrieveParam(nombreSubscriptor, ParametroEnum.PARAMETRO_GRAL_FILE_MAX_SIZE.getName());
		String fileName = expedienteBean.getPathZIPExpediente()+"/"+expedienteBean.getNombreExpediente();
		File file = new File(fileName);
		if((file.length() / 1024) <= new Long(maxSize.getValorParametro()).longValue()) {
			return true;
		}else {
			return false;
		}
	}

}
