package mx.isban.flujogd.zip.listener;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hazelcast.core.MemberAttributeEvent;
import com.hazelcast.core.MembershipEvent;
import com.hazelcast.core.MembershipListener;

import mx.isban.flujogd.common.bean.MemberBean;
import mx.isban.flujogd.common.bean.NetConfigBean;
import mx.isban.flujogd.common.util.InetAddressUtil;
import mx.isban.flujogd.common.util.MapEnum;
import mx.isban.flujogd.persistence.PersistenceManagerSubscriptor;
import mx.isban.flujogd.persistence.impl.PersistenceManagerSubscriptorImpl;
import mx.isban.flujogd.zip.configuration.HazelcastZipConfig;

/**
 * The listener interface for receiving clusterMembership events.
 * The class that is interested in processing a clusterMembership
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addClusterMembershipListener<code> method. When
 * the clusterMembership event occurs, that object's appropriate
 * method is invoked.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase ClusterMembershipListener
 */
public class ClusterMembershipListener implements MembershipListener{
	
	/** Inicializamos un log para guardar la bitacora. */
	private static final Logger LOG = LogManager.getLogger(ClusterMembershipListener.class);
	
	/** Crear un string para guardar el nombre del subscriptor */
	private static String nombreSubscriptor;
	
	/** Crear un string para guardar el componente. */
	private static String componente;
	
	/** Crear un hazelcast dispatcher config. */
	private static HazelcastZipConfig hazelcastConfig;

	/**
	 * memberAdded.
	 *
	 * @param membershipEvent the membership event
	 */
	@Override
	public void memberAdded(MembershipEvent membershipEvent) {
		//Obtener el uuid del miembro
		String uuid = membershipEvent.getMember().getUuid();
		String uuidLocal = membershipEvent.getCluster().getLocalMember().getUuid();
		//Obtener el uuid del miembro
		LOG.info("Se grego un nuevo miembro: "+uuid);
		if(uuid.equals(uuidLocal)) {
			LOG.info("flujogd-zip: "+uuid);
			//Notificar al miembro
			notifyMemberAddZip(uuid);
		}
	}

	/**
	 * memberRemoved.
	 * Metodo nativo de hazelcast para remover los miembros
	 *
	 * @param membershipEvent the membership event
	 */
	@Override
	public void memberRemoved(MembershipEvent membershipEvent) {
		LOG.info("Se elimino el miembro: "+membershipEvent.getMember().getUuid());
		notifyMemberRemovedZip(membershipEvent.getMember().getUuid());
	}
	
	/**
	 * Notifica al cluster que flujo fue agregado.
	 *
	 * @param uuid the uuid
	 */
	private void notifyMemberAddZip(String uuid) {
		LOG.info("Notificamos al cluster que flujo fue agregado");
		PersistenceManagerSubscriptor persistenceManagerZip = new PersistenceManagerSubscriptorImpl();
		InetAddressUtil inetUtilZip = new InetAddressUtil();
		String ipNetZip = inetUtilZip.getLocalNet();
		if(!"".equals(ipNetZip)) {
			//Configurar el bean con los datos del subscriptor
			NetConfigBean netConfigBeanZip = persistenceManagerZip.retrieveNetConfigByIp(nombreSubscriptor,ipNetZip);
			if(netConfigBeanZip!=null){
				Map<String, MemberBean> members = hazelcastConfig.getMapMembers(MapEnum.MAP_MEMBER_STATUS.getName());
				MemberBean memberZip = new MemberBean();
				memberZip.setActive(true);
				memberZip.setIp(netConfigBeanZip.getIp());
				memberZip.setPort(netConfigBeanZip.getPort());
				memberZip.setName(componente);
				memberZip.setUuid(uuid);
				members.put(uuid, memberZip);
				LOG.info("La notificacion se realizo con exito");
			}
		}else {
			LOG.info("Ocurrio un problema al recuperar la IP de este servidor, sin ella no es posible notificar la baja del nodo");
		}
	}

	/**
	 * Notifica al cluster que flujo se dio de baja.
	 *
	 * @param uuid the uuid
	 */
	private void notifyMemberRemovedZip(String uuid) {
		LOG.info("Notificamos al cluster que flujo se dio de baja");
		PersistenceManagerSubscriptor persistenceManagerZip = new PersistenceManagerSubscriptorImpl();
		InetAddressUtil inetUtilZip = new InetAddressUtil();
		String ipNetZip = inetUtilZip.getLocalNet();
		if(!"".equals(ipNetZip)) {
			//Configurar el bean con los datos del subscriptor
			NetConfigBean netConfigBeanZip = persistenceManagerZip.retrieveNetConfigByIp(nombreSubscriptor,ipNetZip);
			if(netConfigBeanZip!=null){
				Map<String, MemberBean> members = hazelcastConfig.getMapMembers(MapEnum.MAP_MEMBER_STATUS.getName());
				MemberBean memberZip = members.get(uuid);
				if(memberZip!=null) {
					memberZip.setActive(false);
					members.replace(uuid, memberZip);
				}
				LOG.info("La notificacion se realizo con exito");
			}
		}else {
			LOG.info("Ocurrio un problema al recuperar la IP de este servidor, sin ella no es posible notificar la baja del nodo");
		}
	}

	/**
	 * memberAttributeChanged.
	 *
	 * @param memberAttributeEvent the member attribute event
	 */
	@Override
	public void memberAttributeChanged(MemberAttributeEvent memberAttributeEvent) {
		LOG.info("Cambio un atributo de uno de los miembros");
	}

	/**
	 * Sets the nombre subscriptor zip.
	 *
	 * @param nombreSubscriptor the nombreSubscriptor to set
	 */
	public static void setNombreSubscriptorZip(String nombreSubscriptor) {
		ClusterMembershipListener.nombreSubscriptor = nombreSubscriptor;
	}

	/**
	 * Sets the componente zip.
	 *
	 * @param componente the componente to set
	 */
	public static void setComponenteZip(String componente) {
		ClusterMembershipListener.componente = componente;
	}

	/**
	 * Sets the hazelcast config.
	 *
	 * @param hazelcastConfig the hazelcastConfig to set
	 */
	public static void setHazelcastConfig(HazelcastZipConfig hazelcastConfig) {
		ClusterMembershipListener.hazelcastConfig = hazelcastConfig;
	}

}
