package mx.isban.flujogd.zip.configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.hazelcast.config.Config;
import com.hazelcast.config.ListenerConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IQueue;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.bean.MemberBean;
import mx.isban.flujogd.common.generic.HazelcastGenericUtils;
import mx.isban.flujogd.zip.listener.ClusterMembershipListener;

/**
 * The Class HazelcastZipConfig.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase HazelcastZipConfig
 */
@Configuration
public class HazelcastZipConfig {

	/** ips de los servicios hazelcast */
	private static List<String> ips;
	
	/** port de los servicios de hazelcast */
	private static String port;
	
	/**
	 * Constructor.
	 */
	public HazelcastZipConfig() {
		ClusterMembershipListener.setHazelcastConfig(this);
	}
	
	
	/**
	 * Gets the single instance of HazelcastZipConfig.
	 *
	 * @return single instance of HazelcastZipConfig
	 */
	@Bean
	public HazelcastInstance getInstanceZip() {
		return Hazelcast.newHazelcastInstance(getHazelcastConfig());
	}

	/**
	 * Gets the queue.
	 *
	 * @param name the name
	 * @return the queue
	 */
	public IQueue<ExpedienteBean> getQueue(String name) {
		return getInstanceZip().getQueue(name);
	}
	
	/**
	 * Gets the map members.
	 *
	 * @param name the name
	 * @return the map members
	 */
	public Map<String, MemberBean> getMapMembers(String name) {
		return getInstanceZip().getMap(name);
	}
	
	/**
	 * Gets the map expedientes.
	 *
	 * @param name the name
	 * @return the map expedientes
	 */
	public Map<String, Boolean> getMapExpedientes(String name) {
		return getInstanceZip().getMap(name);
	}
	
	/**
	 * Gets the map notificaciones.
	 *
	 * @param name the name
	 * @return the map notificaciones
	 */
	public Map<String, Boolean> getMapNotificaciones(String name) {
		return getInstanceZip().getMap(name);
	}

	/**
	 * Config cluster network and discovery mechanism.
	 *
	 * @return Config
	 */
	@Bean
	public Config getHazelcastConfig() {
		Config configZip = new Config();
		HazelcastGenericUtils util = new HazelcastGenericUtils();
		configZip.addListenerConfig(new ListenerConfig("mx.isban.flujogd.zip.listener.ClusterMembershipListener"));
		configZip.setInstanceName("FlujoGD-Zip");
		return util.setConfigHazelcast(port, ips, configZip);
	}

	/**
	 * Sets the ips.
	 *
	 * @param ips the ips to set
	 */
	public static void setIps(List<String> ips) {
		List<String> copiaZip = new ArrayList<>();
		copiaZip.addAll(ips);
		HazelcastZipConfig.ips = copiaZip;
	}

	/**
	 * Sets the port.
	 *
	 * @param port the port to set
	 */
	public static void setPort(String port) {
		HazelcastZipConfig.port = port;
	}

}
