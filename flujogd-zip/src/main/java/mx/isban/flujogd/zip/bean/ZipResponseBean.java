package mx.isban.flujogd.zip.bean;

import java.io.Serializable;

/**
 * The Class ZipResponseBean.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase Bean
 * Clase utilizada para transporte de informacion del expediente en .zip
 */
public class ZipResponseBean implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The ok. */
	private boolean ok;
	
	/** The error message. */
	private String errorMessage;
	
	/** The path unzip. */
	private String pathUnzip;
	
	/**
	 * Constructor.
	 *
	 * @param ok the ok
	 */
	public ZipResponseBean(boolean ok) {
		this.ok = ok;
	}
	
	
	/**
	 * Checks if is ok.
	 *
	 * @return the ok
	 */
	public boolean isOk() {
		return ok;
	}
	
	/**
	 * Sets the ok.
	 *
	 * @param ok the ok to set
	 */
	public void setOk(boolean ok) {
		this.ok = ok;
	}
	
	/**
	 * Gets the error message.
	 *
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}
	
	/**
	 * Sets the error message.
	 *
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	/**
	 * Gets the path unzip.
	 *
	 * @return the pathUnzip
	 */
	public String getPathUnzip() {
		return pathUnzip;
	}

	/**
	 * Sets the path unzip.
	 *
	 * @param pathUnzip the pathUnzip to set
	 */
	public void setPathUnzip(String pathUnzip) {
		this.pathUnzip = pathUnzip;
	}

}
