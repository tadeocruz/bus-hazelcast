package mx.isban.flujogd.zip.step;

import java.io.File;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.item.ItemProcessor;

import com.hazelcast.core.IExecutorService;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.bean.NotificationBean;
import mx.isban.flujogd.common.bean.NotificationBean2;
import mx.isban.flujogd.common.bean.NotificationBean3;
import mx.isban.flujogd.common.util.ExpedienteErrorEnum;
import mx.isban.flujogd.common.util.ExpedienteStatusEnum;
import mx.isban.flujogd.common.util.ExpedienteUtil;
import mx.isban.flujogd.common.util.NotificationManager;
import mx.isban.flujogd.common.util.ParametroEnum;
import mx.isban.flujogd.common.util.QueueEnum;
import mx.isban.flujogd.common.util.QueueStatusEnum;
import mx.isban.flujogd.persistence.PersistenceManagerBloqueDetail;
import mx.isban.flujogd.persistence.PersistenceManagerExpediente;
import mx.isban.flujogd.persistence.callable.BloquePersistenceTask;
import mx.isban.flujogd.persistence.callable.ExpedientePersistenceTask;
import mx.isban.flujogd.persistence.callable.NotificacionPersistenceTask;
import mx.isban.flujogd.persistence.impl.PersistenceManagerBloqueDetailImpl;
import mx.isban.flujogd.persistence.impl.PersistenceManagerExpedienteImpl;
import mx.isban.flujogd.zip.bean.ZipResponseBean;
import mx.isban.flujogd.zip.configuration.HazelcastZipConfig;
import mx.isban.flujogd.zip.manager.ZIPManager;

/**
 * The Class BatchZipItemProcessor.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase BatchZipItemProcessor
 */
public class BatchZipItemProcessor implements ItemProcessor<ExpedienteBean, ExpedienteBean>{

	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(BatchZipItemProcessor.class);
	
	/** The Constant EXECUTOR_SERVICE. */
	private static final String EXECUTOR_SERVICE  = "executorService";

	/** The hazelcast zip config. */
	private HazelcastZipConfig hazelcastZipConfig;
	/**
	 * Constructor.
	 *
	 * @param hazelcastZipConfig the hazelcast zip config
	 */
	public BatchZipItemProcessor(HazelcastZipConfig hazelcastZipConfig){
		this.hazelcastZipConfig = hazelcastZipConfig;
	}


	/**
	 * Metodo para procesar los expedientes en formato .zip
	 * process.
	 *
	 * @param expedienteBean the expediente bean
	 * @return the expediente bean
	 * @throws Exception the exception
	 */
	@Override
	public synchronized ExpedienteBean process(ExpedienteBean expedienteBean) throws Exception {
		PersistenceManagerBloqueDetail persistenceManager = new PersistenceManagerBloqueDetailImpl();
		if(expedienteBean.getPathZIPExpediente()==null) {
			expedienteBean.setPathZIPExpediente(persistenceManager.retrieveParametroBloque(expedienteBean.getIdExpediente(),  QueueEnum.QUEUE_SFTP.getName(), ParametroEnum.PARAMETRO_SFTP_OUTPUT_PATH.getName()).get(0).getValorParametro());
		}
		String target = obtenerTarget(expedienteBean, persistenceManager);
		// Obtener instancia con la configuracion de hazelcast
		BatchZipItemProcessorUtil util = new BatchZipItemProcessorUtil(hazelcastZipConfig);
		// Validar el tamanio del archivo para poder ser procesado
		if(util.validaTamano(expedienteBean)) {
			ZIPManager zipManager = new ZIPManager();
			// Extraer los documentos del archivo .zip
			ZipResponseBean zipResponse = zipManager.unzip(expedienteBean.getPathZIPExpediente(), target, expedienteBean.getNombreExpediente(), File.separator);
			if(zipResponse.isOk()) {
				// Validar proceso de extraccion de documentos
				if(updateStausBloqueExpediente(expedienteBean, QueueStatusEnum.STATUS_OK.getName(), QueueEnum.QUEUE_ZIP.getName(), null)) {
					LOG.info("Se realizo unzip con exito al expediente");
					expedienteBean.setPathUNZIPExpediente(zipResponse.getPathUnzip());
					ExpedienteUtil expedienteUtil = new ExpedienteUtil();
					expedienteUtil.cambiaEstausQueue(expedienteBean);
					expedienteUtil.determinaSiguienteQueue(expedienteBean);
				}else {
					expedienteBean.getDetalle().setSiguienteQueue("");
				}
			}else {
				error(expedienteBean, ExpedienteErrorEnum.ERROR_UNZIP.getName());
			}
		}else {
			error(expedienteBean, ExpedienteErrorEnum.ERROR_EXPEDIENTE_TAMANO_MAX.getName());
		}
		return expedienteBean;
	}


	/**
	 * Obtener target.
	 * Metodo para obtener la ruta de trabajo para unZip
	 *
	 * @param expedienteBean the expediente bean
	 * @param persistenceManager the parametros
	 * @return the string
	 */
	private String obtenerTarget(ExpedienteBean expedienteBean, PersistenceManagerBloqueDetail persistenceManager) {
		try {			
			return persistenceManager.retrieveParametroBloque(expedienteBean.getIdExpediente(),  QueueEnum.QUEUE_ZIP.getName(), ParametroEnum.PARAMETRO_ZIP_TARGET_PATH.getName()).get(0).getValorParametro();
		}catch (IndexOutOfBoundsException e) {
			LOG.error("Error al obtener la ruta de unZip ", e);
		}
		return expedienteBean.getPathUNZIPExpediente();
	}

	/**
	 * Metodo para informa en caso de existir algun error en la extraccion de los documentos
	 * error.
	 *
	 * @param expedienteBean the expediente bean
	 * @param nombreError the nombre error
	 */
	private void error(ExpedienteBean expedienteBean,String nombreError){
		expedienteBean.getDetalle().setSiguienteQueue("");
		LOG.error(nombreError);
		// Ejecutar la actualizacion del estatus del expediente
		updateStausBloqueExpediente(expedienteBean, QueueStatusEnum.STATUS_ERROR.getName(), QueueEnum.QUEUE_ZIP.getName(), nombreError);
	}

	/**
	 * Metodo para actualizar los estatus de los bloques procesados
	 * updateStausBloqueExpediente.
	 *
	 * @param expedienteBean the expediente bean
	 * @param statusBloque the status bloque
	 * @param queueName the queue name
	 * @param nombreError the nombre error
	 * @return true, if successful
	 */
	private boolean updateStausBloqueExpediente(ExpedienteBean expedienteBean, String statusBloque, String queueName, String nombreError) {
		PersistenceManagerExpediente persistenceZip = new PersistenceManagerExpedienteImpl();
		// Intanciar con la configuracion de hazelcast
		IExecutorService executorServiceZip = this.hazelcastZipConfig.getInstanceZip().getExecutorService( EXECUTOR_SERVICE );
		Future<Object> future = executorServiceZip.submit( new BloquePersistenceTask(expedienteBean.getIdExpediente(), statusBloque, queueName,nombreError) );
		boolean okZip = false;
		try {
			okZip = (Boolean)future.get(); 
			if(okZip){
				String estatusExpediente = ExpedienteStatusEnum.STATUS_ARCHIVO_VALIDO.getName();
				if(nombreError!=null) {
					estatusExpediente = ocurrioUnError(persistenceZip, expedienteBean, queueName, nombreError);
				}
				okZip = updateStausExpediente(expedienteBean.getIdExpediente(), estatusExpediente, nombreError);
			}
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(e);
		}
		return okZip;
	}

	/**
	 * Metodo para actualizar el estatus del expediente procesado
	 * updateStausExpediente.
	 *
	 * @param idExpediente the id expediente
	 * @param status the status
	 * @param idError the id error
	 * @return true, if successful
	 */
	private boolean updateStausExpediente(String idExpediente, String status, String idError) {
		IExecutorService executorServiceZip = this.hazelcastZipConfig.getInstanceZip().getExecutorService( EXECUTOR_SERVICE );
		Future<Object> future = executorServiceZip.submit( new ExpedientePersistenceTask(idExpediente, status, idError) );
		boolean okZip = false;
		try {
			okZip = (Boolean)future.get();
		} catch (InterruptedException e) {
			LOG.error(e);
			Thread.currentThread().interrupt();
		} catch (ExecutionException e) {
			LOG.error(e);
		}
		return okZip;
	}

	/**
	 * Metodo para insertar la notificacion de los expedientes procesados
	 * updateStausExpediente.
	 *
	 * @param expedienteBean the expediente bean
	 * @param nombreError the nombre error
	 * @return true, if successful
	 */
	private boolean insertNotificacion(ExpedienteBean expedienteBeanZip, String nombreError) {
		IExecutorService executorServiceZip = this.hazelcastZipConfig.getInstanceZip().getExecutorService( EXECUTOR_SERVICE );
		NotificationBean bean = new NotificationBean();
		NotificationBean2 bean2 = new NotificationBean2();
		NotificationBean3 bean3 = new NotificationBean3();
		bean2.setBean3(bean3);
		bean.setBean2(bean2);
		bean.setIdExpediente(expedienteBeanZip.getIdExpediente());
		bean.setRequest(null);
		bean.setResponseCode(null);
		bean.setResponseMsg(null);
		bean2.setRefExterna(null);
		bean2.setReqId(null);
		bean2.setErrorId(nombreError);
		bean2.setErrorMsg(nombreError);
		bean3.setBuc(null);
		bean3.setPathZip(expedienteBeanZip.getPathZIPExpediente());
		bean3.setFileName(expedienteBeanZip.getNombreExpediente());
		bean3.setPathUnzip(expedienteBeanZip.getPathUNZIPExpediente());
		Future<Object> future = executorServiceZip.submit( new NotificacionPersistenceTask(bean) );
		boolean okZip = false;
		try {
			okZip = (Boolean)future.get();
		} catch (InterruptedException e) {
			LOG.error(e);
			Thread.currentThread().interrupt();
		} catch (ExecutionException e) {
			LOG.error(e);
		}
		return okZip;
	}

	/**
	 * Metodo para informar cuando ocurre algun error en la extraccion de los expedientes
	 * ocurrioUnError.
	 *
	 * @param persistence the persistence
	 * @param expedienteBean the expediente bean
	 * @param queueName the queue name
	 * @param nombreError the nombre error
	 * @return the string
	 */
	private String ocurrioUnError(PersistenceManagerExpediente persistence,ExpedienteBean expedienteBean,String queueName,String nombreError) {
		String estatusExpedienteZip = ExpedienteStatusEnum.STATUS_ERROR_VALIDACION.getName();
		//Nueva validacion para numero de reintentos y envio de notificaciones
		Integer intentosZip = persistence.retrieveIntentosTipoError(expedienteBean.getIdExpediente(), queueName, nombreError);
		Integer reintentosPermitidos = persistence.retrieveReintentosByNombreError(nombreError);
		if(intentosZip>=reintentosPermitidos) {
			estatusExpedienteZip = ExpedienteStatusEnum.STATUS_PROCESO_CANCELADO.getName();
			NotificationManager notification = new NotificationManager();
			notification.preparaEnvioNotificacion(expedienteBean, nombreError, nombreError,expedienteBean.getNombreExpediente().substring(0, expedienteBean.getNombreExpediente().length()-4));
			insertNotificacion(expedienteBean,nombreError);
		}
		return estatusExpedienteZip;
	}
}
