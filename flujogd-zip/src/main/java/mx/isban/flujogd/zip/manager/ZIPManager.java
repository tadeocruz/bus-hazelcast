package mx.isban.flujogd.zip.manager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mx.isban.flujogd.zip.bean.ZipResponseBean;

/**
 * Clase para la extraccion de los documentos desde un archivo .zip
 * The Class ZIPManager.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase ZIPManager
 */
public class ZIPManager {

	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(ZIPManager.class);

	/**
	 * Metodo que desempaqueta un archivo zip.
	 *
	 * @param fileSource the file source
	 * @param fileTarget the file target
	 * @param fileName the file name
	 * @param fileSeparator the file separator
	 * @return the zip response bean
	 */
	public ZipResponseBean unzip(String fileSource, String fileTarget, String fileName, String fileSeparator) {
		ZipEntry zipEntry = null;
		ZipResponseBean response = new ZipResponseBean(true);
		String pathOutput = null;
		// Obtener el archivo .zip
		try (FileInputStream fis = new FileInputStream(fileSource + fileSeparator + fileName);
				ZipInputStream zipInputStream =  new ZipInputStream(fis)){
			pathOutput = fileTarget + fileSeparator + fileName.substring(0, fileName.length()-4);
			response.setPathUnzip(pathOutput);
			zipEntry = zipInputStream.getNextEntry();
			while(zipEntry!=null){
				zipEntry = validateZipEntry(zipEntry, pathOutput, zipInputStream);
			}
		} catch (FileNotFoundException e) {
			// En caso de excepcion, se informa el error
			response.setOk(false);
			response.setErrorMessage(e.getMessage());
			LOG.error(e);
		} catch (IOException e) {
			// En caso de excepcion, se informa el error
			response.setOk(false);
			response.setErrorMessage(e.getMessage());
			LOG.error(e);
		}
		return response;
	}
	
	/**
	 * Metodo que valida si la entrada es una carpeta o un archivo y realiza lo correspondiente.
	 *
	 * @param zipEntry the zip entry
	 * @param pathOutput the path output
	 * @param zipInputStream the zip input stream
	 * @return the zip entry
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private ZipEntry validateZipEntry(ZipEntry zipEntry,String pathOutput, ZipInputStream zipInputStream) throws IOException {
		byte[] buffer = new byte[1024];
		String fileName = zipEntry.getName();
		// Obtener el archivo .zip
		File newFile = new File(pathOutput+File.separator+fileName);
		// Si el directorio existe se crea la nueva ruta
		if(zipEntry.isDirectory()) {
			if(!newFile.exists()) {
				newFile.mkdir();
			}
		}else {
			new File(newFile.getParent()).mkdirs();
			try(FileOutputStream fos = new FileOutputStream(newFile) ){
				int len;
				while ((len = zipInputStream.read(buffer)) > 0) {
					fos.write(buffer, 0, len);
				}
			} catch (FileNotFoundException e) {
				LOG.error(e);
			} catch (IOException e) {
				LOG.error(e);
			}
		}
		return zipInputStream.getNextEntry();
	}

}
