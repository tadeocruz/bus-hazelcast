package mx.isban.flujogd.zip.step;

import java.io.File;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.bean.ParametroBean;
import mx.isban.flujogd.common.util.ParametroEnum;
import mx.isban.flujogd.persistence.PersistenceManagerSubscriptor;
import mx.isban.flujogd.persistence.impl.PersistenceManagerSubscriptorImpl;
import mx.isban.flujogd.zip.configuration.HazelcastZipConfig;

/**
 * The Class BatchZipItemProcessorUtil.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase BatchZipItemProcessor
 */
public class BatchZipItemProcessorUtil{
	
	/** The hazelcast zip config. */
	@SuppressWarnings("unused")
	private HazelcastZipConfig hazelcastZipConfig;

	/**
	 * Constructor.
	 *
	 * @param hazelcastZipConfig the hazelcast zip config
	 */
	public BatchZipItemProcessorUtil(HazelcastZipConfig hazelcastZipConfig){
		this.hazelcastZipConfig = hazelcastZipConfig;
	}

	
	/**
	 * Metodo para validar el tamano del expediente en formato .zip
	 * validaTamano.
	 *
	 * @param expedienteBean the expediente bean
	 * @return true, if successful
	 */
	public boolean validaTamano(ExpedienteBean expedienteBean) {
		PersistenceManagerSubscriptor persistenceManager = new PersistenceManagerSubscriptorImpl();
		String nombreSubscriptor = persistenceManager.retrieveSubscriptorByIdExpediente(expedienteBean.getIdExpediente());
		ParametroBean maxSize = persistenceManager.retrieveParam(nombreSubscriptor, ParametroEnum.PARAMETRO_GRAL_FILE_MAX_SIZE.getName());
		String fileName = expedienteBean.getPathZIPExpediente()+File.separator+expedienteBean.getNombreExpediente();
		File file = new File(fileName);
		return (file.length() / 1024) <= new Long(maxSize.getValorParametro()).longValue();
	}

}
