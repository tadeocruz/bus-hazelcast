package mx.isban.flujogd.zip.step;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.item.ItemReader;

import com.hazelcast.core.IQueue;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.util.QueueEnum;
import mx.isban.flujogd.zip.configuration.HazelcastZipConfig;

/**
 * The Class BatchZipItemReader.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase inicial BatchZipItemReader
 */
public class BatchZipItemReader implements  ItemReader<ExpedienteBean>{

	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(BatchZipItemReader.class);
	
	/** The hazelcast zip config. */
	private HazelcastZipConfig hazelcastZipConfig;
	
	/** The expediente bean. */
	private ExpedienteBean expedienteBean;
	
	/**
	 * Constructor.
	 *
	 * @param hazelcastZipConfig the hazelcast zip config
	 */
	public BatchZipItemReader(HazelcastZipConfig hazelcastZipConfig){
		this.hazelcastZipConfig = hazelcastZipConfig;
	}
	

	/**
	 * Metodo que lee de la queue el siguiente dato a ser procesado.
	 *
	 * @return the expediente bean
	 * @throws Exception the exception
	 */
	@Override
	public ExpedienteBean read() throws Exception{
		IQueue<ExpedienteBean> queueZip = hazelcastZipConfig.getQueue(QueueEnum.QUEUE_ZIP.getName());
		try {
			expedienteBean = queueZip.take();
			if (expedienteBean != null) {
				LOG.info("=== SE RECUPERA EXPEDIENTE: "+expedienteBean.getIdExpediente() + " - "+ expedienteBean.getNombreExpediente() + " - " + expedienteBean.getDetalle().getDetalle2().getReferenciaExterna());
				return expedienteBean;
			}else{
				return null;
			}
		} catch (InterruptedException e1) {
			LOG.error(e1);
			Thread.currentThread().interrupt();
		}
		return null;
	}
}
