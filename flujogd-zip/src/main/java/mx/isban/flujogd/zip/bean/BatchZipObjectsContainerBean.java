package mx.isban.flujogd.zip.bean;

import lombok.Getter;
import mx.isban.flujogd.persistence.PersistenceManagerBloqueDetail;
import mx.isban.flujogd.persistence.PersistenceManagerExpediente;
import mx.isban.flujogd.persistence.impl.PersistenceManagerBloqueDetailImpl;
import mx.isban.flujogd.persistence.impl.PersistenceManagerExpedienteImpl;
import mx.isban.flujogd.zip.configuration.HazelcastZipConfig;
import mx.isban.flujogd.zip.manager.ZIPManager;
import mx.isban.flujogd.zip.step.BatchZipItemProcessorUtil;

/**
 * BatchZipObjectsContainerBean class
 * 
 * @author DELL
 *
 */
@Getter
public class BatchZipObjectsContainerBean {
	/**
	 * PersistenceManagerBloqueDetail
	 */
	private PersistenceManagerBloqueDetail pmb = new PersistenceManagerBloqueDetailImpl();
	
	/**
	 * BatchZipItemProcessorUtil
	 */
	private BatchZipItemProcessorUtil util;
	
	/**
	 * ZIPManager
	 */
	private ZIPManager zipManager = new ZIPManager();
	
	/**
	 * PersistenceManagerExpediente
	 */
	private PersistenceManagerExpediente persistenceZip = new PersistenceManagerExpedienteImpl();

	/**
	 * Constructor con hz
	 * 
	 * @param hazelcastZipConfig the hazelcastZipConfig
	 */
	public BatchZipObjectsContainerBean(HazelcastZipConfig hazelcastZipConfig) {
		super();
		util = new BatchZipItemProcessorUtil(hazelcastZipConfig);
	}
	
	
}
