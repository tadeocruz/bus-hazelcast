package mx.isban.flujogd.zip.trigger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Clase para lanzar los procesos que ejecutaron la extraccion de documentos
 * The Class ExecuteJobZipTrigger.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase ExecuteJobZipTrigger
 */
@Component
public class ExecuteJobZipTrigger {
	
	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(ExecuteJobZipTrigger.class);
	
	/** The job launcher. */
	@Autowired
	private JobLauncher jobLauncher;
	
	/** The process job. */
	@Autowired
	private Job processJob;

	/**
	 * Metodo que ejecuta el job batch.
	 * executeJob
	 * Sin retorno de informacion
	 */
	public void executeJob() {
		// Iniciar proceso
		try {
			// inicializar el job con los parametros de ejecucion
			JobParameters jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis()).toJobParameters();
			// Ejecutar job
			jobLauncher.run(processJob, jobParameters);
		} catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException | JobParametersInvalidException e) {
			// En caso de error, notificarlo via consola
			LOG.error(e);
		}
	}

}
