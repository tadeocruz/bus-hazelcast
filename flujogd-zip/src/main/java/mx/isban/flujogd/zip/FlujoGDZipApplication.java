package mx.isban.flujogd.zip;


import java.io.File;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.system.ApplicationPidFileWriter;
import org.springframework.scheduling.annotation.EnableAsync;

import mx.isban.flujogd.common.generic.CommonsUtils;
import mx.isban.flujogd.zip.configuration.HazelcastZipConfig;
import mx.isban.flujogd.zip.listener.ClusterMembershipListener;

/**
 * The Class FlujoGDZipApplication.
 * Metodo principal que ejecutara el proceso de dispatcher.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase FlujoGDZipApplication
 */
@SpringBootApplication
@EnableAsync
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
public class FlujoGDZipApplication implements CommandLineRunner {
	
	//Inicializamos un log para la bitacora
	private static final Logger LOG = LogManager.getLogger(FlujoGDZipApplication.class);
	
	/**
	 * Metodo principal que inicia el flujo zip.
	 *
	 * @param args the arguments
	 */
    public static void main(String[] args) {
		CommonsUtils utils = new CommonsUtils();
		// Validar que existan los parametros requeridos para el proceso
    	if(args.length<5) {
			//Mostrar en bitacora un error de componentes faltantes
    		LOG.error("No es posible inicar el componente sin los parametros 'IPs','port','path','componente' y 'Nombre subscriptor'");
    		return;
    	}else{
			//Colocar la ip de la configuracion de hazelcast zip
			HazelcastZipConfig.setIps(utils.getListaIp(args));
			//Colocar el puerto de la configuracion de hazelcast zip
			HazelcastZipConfig.setPort(args[1]);
			//Colocar el nombre del subscriptor del cluster de membresias
			ClusterMembershipListener.setNombreSubscriptorZip(args[4]);
			//Colocar el componente del cluster de membresias
			ClusterMembershipListener.setComponenteZip(args[3]);
			SpringApplication sp = new SpringApplication(FlujoGDZipApplication.class);
			//Agregar los disparadores
			sp.addListeners(new ApplicationPidFileWriter(args[2]+File.separator+args[3]+".pid"));
			//Correr los argumentos
			sp.run(args);
		}
    }

    /**
     * Metodo nativo hazelcast que dispara el proceso de arranque.
     *
     * @param args the args
     * @throws Exception the exception
     */
	@Override
	public void run(String... args) throws Exception {
		LOG.info("FlujoGDZipApplication - run");
	}

}
