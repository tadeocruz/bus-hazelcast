package mx.isban.flujogd.zip.listener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hazelcast.core.ItemEvent;
import com.hazelcast.core.ItemListener;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.zip.trigger.ExecuteJobZipTrigger;

/**
 * The listener interface for receiving queueZip events.
 * The class that is interested in processing a queueZip
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addQueueZipListener<code> method. When
 * the queueZip event occurs, that object's appropriate
 * method is invoked.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase QueueZipListener
 */
@Component
public class QueueZipListener implements ItemListener<ExpedienteBean> {
	
	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(QueueZipListener.class);
	
    /** The job batch. */
    @Autowired
    private ExecuteJobZipTrigger jobBatch;

    /**
     * itemAdded.
     *
     * @param item the item
     */
    @Override
    public void itemAdded(ItemEvent<ExpedienteBean> item) {
    	LOG.info("Se detecta objeto agregado a la queue");
    	jobBatch.executeJob();
    }

    /**
     * itemRemoved.
     *
     * @param item the item
     */
    @Override
    public void itemRemoved(ItemEvent<ExpedienteBean> item) {
    	LOG.info("Se detecta objeto removido de la queue");
    }

}