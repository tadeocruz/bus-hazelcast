package mx.isban.flujogd.zip.configuration;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import mx.isban.flujogd.common.util.QueueEnum;
import mx.isban.flujogd.zip.listener.QueueZipListener;

/**
 * Clase para la configuracion de las queues para unZip
 * The Class QueueZipConfig.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase QueueZipConfig
 */
@Configuration
public class QueueZipConfig {
	
	/** log. */
	private static final Logger LOG = LogManager.getLogger(QueueZipConfig.class);
	
	/** The hazelcast config. */
	@Autowired
	private HazelcastZipConfig hazelcastConfig;
	
	/** The queue initial listener. */
	@Autowired
	private QueueZipListener queueInitialListener;

	/**
	 * postConstruct.
	 */
	@PostConstruct
	public void postConstruct() {
		hazelcastConfig.getQueue( QueueEnum.QUEUE_ZIP.getName() ).addItemListener( queueInitialListener, true );
		LOG.info( "FlujoGD-Zip  - Listener Start" );
	}
}
