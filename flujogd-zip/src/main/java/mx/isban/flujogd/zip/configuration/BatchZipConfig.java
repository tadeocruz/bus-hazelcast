package mx.isban.flujogd.zip.configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.support.transaction.ResourcelessTransactionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.zip.listener.JobZipListener;
import mx.isban.flujogd.zip.step.BatchZipItemProcessor;
import mx.isban.flujogd.zip.step.BatchZipItemReader;
import mx.isban.flujogd.zip.step.BatchZipItemWriter;

/**
 * Clase de configuracion para el proceso de unZip
 * The Class BatchZipConfig.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase BatchZipConfig
 */
@EnableBatchProcessing(modular = true)
@Configuration
public class BatchZipConfig {

	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(BatchZipConfig.class);

	/** The job builder factory. */
	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	/** The step builder factory. */
	@Autowired
	private StepBuilderFactory stepBuilderFactory;
	
	/** The hazelcast zip config. */
	@Autowired
	private HazelcastZipConfig hazelcastZipConfig;

	/**
	 * Metodo para lanzar la ejcucion del proceso
	 * Process job.
	 *
	 * @return Job
	 */
	@Bean
	public Job processJob() {
		LOG.info("execute job");
		// Devolver ejecucion del proceso
		return jobBuilderFactory.get("jobBuilderFactory")
				.incrementer(new RunIdIncrementer())
				.listener(listener())
				.flow(orderStep())
				.end()
				.build();
	}

	/**
	 * Metodo para obtener los pasos del proceso
	 * To create a step, reader, processor and writer has been passed serially.
	 *
	 * @return the step
	 */
	@Bean
	public Step orderStep() {
		LOG.info("step de job");
		return stepBuilderFactory.get("orderStep1")
				.<ExpedienteBean, ExpedienteBean> chunk(1)
				.reader(new BatchZipItemReader(this.hazelcastZipConfig))
				.processor(new BatchZipItemProcessor(this.hazelcastZipConfig))
				.writer(new BatchZipItemWriter(this.hazelcastZipConfig))
				.build();
	}

	/**
	 * Listener.
	 *
	 * @return {@link JobExecutionListener}
	 */
	@Bean
	public JobExecutionListener listener() {
		return new JobZipListener();
	}

	/**
	 * Transaction manager.
	 *
	 * @return {@link ResourcelessTransactionManager}
	 */
	@Bean
	public ResourcelessTransactionManager transactionManager() {
		return new ResourcelessTransactionManager();
	}
}
