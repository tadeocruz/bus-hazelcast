package mx.isban.flujogd.injector.tc;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.client.config.ClientNetworkConfig;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IQueue;

import mx.isban.flujogd.common.bean.NetConfigBean;
import mx.isban.flujogd.common.bean.ParametroBean;
import mx.isban.flujogd.common.bean.SubscriptorBean;
import mx.isban.flujogd.common.util.HazelcastConnectionException;
import mx.isban.flujogd.common.util.MapEnum;
import mx.isban.flujogd.common.util.ParametroEnum;
import mx.isban.flujogd.common.util.ParametroValueEnum;
import mx.isban.flujogd.common.util.QueueEnum;
import mx.isban.flujogd.persistence.PersistenceManagerSubscriptor;
import mx.isban.flujogd.persistence.impl.PersistenceManagerSubscriptorImpl;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase InjectorComponent
 *
 */
public class InjectorComponentTC {

	/**
	 * LOG
	 */
	private static final Logger LOG = LogManager.getLogger(InjectorComponentTC.class);

	/**
	 * Inicia la inyeccion de los expediente por subscriptor
	 */
	public static void main(String[] args) {
		InjectorComponentTC tc = new InjectorComponentTC();
		tc.recuperarExpedientesEnMapa();
		System.exit(1);
	}
	
	private void recuperarExpedientesEnMapa() {
//		IQueue<String> queueKamikaze;
//		HazelcastInstance client = null;
//		try {
//			client = crearClienteHazelcast("192.168.15.34", "5900",1000);
//			if(client!=null) {
//				//Se recuperan expedientes del mapa
//				LOG.info("Inicia proceso de recuperacion de expedientes del mapa");
//				Map<String, Boolean> expedientesMap = client.getMap("expedientesInProcess");
//				if(expedientesMap.isEmpty()) {
//					LOG.info("No hay expedientes en el mapa");
//				}else {
//					for (Map.Entry<String, Boolean> entry : expedientesMap.entrySet()) {
//						System.out.println("Key : " + entry.getKey() + " Value : " + entry.getValue());
//					}
//				}
//			}
//		} catch (HazelcastConnectionException e) {
//			LOG.error(e);
//		}
//		LOG.info("Termina proceso de inyeccion");
	}
	
	private void injector() {
		LOG.info("Inicia TC");
		InjectorComponentTC injectorComponentTC = new InjectorComponentTC();
		PersistenceManagerSubscriptor persistenceManager = new PersistenceManagerSubscriptorImpl();
		//Obtenemos todos los subscriptores
		List<SubscriptorBean> subscriptores = persistenceManager.retrieveAllSubscriptores();
		LOG.info("Subscriptores recuperados: " + subscriptores.size());
		for(SubscriptorBean subscriptor:subscriptores){
			LOG.info("Inicia revision flujo: " + subscriptor.getNombreSubscriptor());
			String nombreSubscriptor = subscriptor.getNombreSubscriptor();
			//Debe hazelcast estar activo?
			ParametroBean hazelcastActivoParam = persistenceManager.retrieveParam(nombreSubscriptor, ParametroEnum.PARAMETRO_GRAL_HAZELCAST_ACTIVO.getName());
			//Recuperamos al configuracion para la maquina en donde esta siendo ejecutado el flujogd-started
			InetAddress iAddress = null;
			try {
				iAddress = InetAddress.getLocalHost();
				LOG.info("IP: "+iAddress.getHostAddress());
			} catch (UnknownHostException e) {
				LOG.error(e);
			}
			if(iAddress!=null) {
				injectorComponentTC.validaKamikaze(persistenceManager, nombreSubscriptor, iAddress, hazelcastActivoParam);
			}else {
				LOG.info("Ocurrio un problema al recuperar la IP de este servidor, sin ella no es posible ejecutar el flujo hazelcast");
			}
		}
		LOG.info("Termina Injector");
	}
	
	/**
	 * validaInyectarExpedientes
	 * @param persistenceManager
	 * @param nombreSubscriptor
	 * @param iAddress
	 * @param hazelcastActivoParam
	 */
	private void validaKamikaze(PersistenceManagerSubscriptor persistenceManager,String nombreSubscriptor,InetAddress iAddress,ParametroBean hazelcastActivoParam) {
		NetConfigBean localNetConfigBean = persistenceManager.retrieveNetConfigByIp(nombreSubscriptor,iAddress.getHostAddress().trim());
		if(localNetConfigBean!=null){
			if(hazelcastActivoParam.getValorParametro().equals(ParametroValueEnum.PARAMETRO_VALUE_ACTIVO.getName())){
				invocarKamikaze(localNetConfigBean.getIp(), localNetConfigBean.getPort(), nombreSubscriptor);
			}
		}else {
			LOG.info("Este servidor IP no esta habilitado para ejecutar un flujo hazelcast, si se requiere es necesario agregarlo a la parametria");
		}
	}
	
	/**
	 * invocarKamikaze
	 * @param ip
	 * @param port
	 * @param nombreSubscriptor
	 */
	private void invocarKamikaze(String ip,String port,String nombreSubscriptor) {
		IQueue<String> queueKamikaze;
		HazelcastInstance client = null;
		try {
			client = crearClienteHazelcast(ip, port,30000);
			if(client!=null) {
				//Se inyectan los expediente
				LOG.info("Inicia proceso de inyeccion de expedientes");
				queueKamikaze = client.getQueue(QueueEnum.QUEUE_KILLER.getName());
				queueKamikaze.put("echo");
			}
		} catch (InterruptedException e) {
			LOG.error(e);
			Thread.currentThread().interrupt();
		} catch (HazelcastConnectionException e) {
			LOG.error(e);
		}
		LOG.info("Termina proceso de inyeccion");
	}

	/**
	 * Crea una instancia hazelcast
	 * @param ip
	 * @param port
	 * @param timeSleep
	 * @return
	 * @throws HazelcastConnectionException 
	 * @throws InterruptedException 
	 */
	private HazelcastInstance crearClienteHazelcast(String ip,String port,long timeSleep) throws HazelcastConnectionException {
		LOG.info("Inicia creacion cliente hazelcast");
		ClientConfig config = new ClientConfig();
		ClientNetworkConfig cnc = new ClientNetworkConfig();
		cnc.addAddress(ip+":"+port);
		config.setNetworkConfig(cnc);
		HazelcastInstance client = null; 
		try {
			Thread.sleep(timeSleep);
			client = HazelcastClient.newHazelcastClient(config);
			LOG.info("Termina creacion cliente hazelcast");
		} catch (RuntimeException e) {
			LOG.error(e);
			throw new HazelcastConnectionException("Error de conexion");
		} catch (InterruptedException e) {
			LOG.error(e);
			Thread.currentThread().interrupt();
		}
		return client;
	}

}
