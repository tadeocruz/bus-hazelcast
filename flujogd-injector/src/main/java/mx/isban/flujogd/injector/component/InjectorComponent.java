package mx.isban.flujogd.injector.component;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.client.config.ClientNetworkConfig;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IQueue;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.bean.NetConfigBean;
import mx.isban.flujogd.common.bean.ParametroBean;
import mx.isban.flujogd.common.bean.SubscriptorBean;
import mx.isban.flujogd.common.generic.HazelcastGenericUtils;
import mx.isban.flujogd.common.util.HazelcastConnectionException;
import mx.isban.flujogd.common.util.InetAddressUtil;
import mx.isban.flujogd.common.util.MapEnum;
import mx.isban.flujogd.common.util.ParametroEnum;
import mx.isban.flujogd.common.util.ParametroValueEnum;
import mx.isban.flujogd.common.util.QueueEnum;
import mx.isban.flujogd.persistence.PersistenceManager;
import mx.isban.flujogd.persistence.PersistenceManagerNotificacion;
import mx.isban.flujogd.persistence.PersistenceManagerSubscriptor;
import mx.isban.flujogd.persistence.impl.PersistenceManagerImpl;
import mx.isban.flujogd.persistence.impl.PersistenceManagerNotificacionImpl;
import mx.isban.flujogd.persistence.impl.PersistenceManagerSubscriptorImpl;

/**
 * Clase para el Componente injector
 * 
 * @author Alvaro Zamorano 
 * azamorano@serviciosexternos.isban.mx 
 * Clase InjectorComponent
 * ------------------------------------
 * Modificado by Julio Cesar Sanchez
 * Stefanini
 */
public class InjectorComponent {

	/**
	 * LOG
	 */
	private static final Logger LOG = LogManager.getLogger(InjectorComponent.class);

	/**
	 * Inicia la inyeccion de los expediente por subscriptor
	 */
	public void execute() {
		LOG.info("Inicia Injector");
		PersistenceManagerSubscriptor persistenceManager = new PersistenceManagerSubscriptorImpl();
		// Obtenemos todos los subscriptores
		List<SubscriptorBean> subscriptores = persistenceManager.retrieveAllSubscriptores();
		LOG.info("Subscriptores recuperados: " + subscriptores.size());
		for (SubscriptorBean subscriptor : subscriptores) {
			LOG.info("Inicia revision flujo: " + subscriptor.getNombreSubscriptor());
			String nombreSubscriptor = subscriptor.getNombreSubscriptor();
			// Debe hazelcast estar activo?
			ParametroBean hazelcastActivoParam = persistenceManager.retrieveParam(nombreSubscriptor,
					ParametroEnum.PARAMETRO_GRAL_HAZELCAST_ACTIVO.getName());
			// Recuperamos al configuracion para la maquina en donde esta siendo ejecutado
			// el flujogd-started
			InetAddressUtil inetUtil = new InetAddressUtil();
			String ipNet = inetUtil.getLocalNet();
			if (!"".equals(ipNet)) {
				validaInyeccion(persistenceManager, nombreSubscriptor, ipNet, hazelcastActivoParam);
			} else {
				LOG.info("Ocurrio un problema al recuperar la IP de este servidor, sin ella no es posible ejecutar el flujo hazelcast");
			}
		}
		LOG.info("Termina Injector");
	}

	/**
	 * validaInyectarExpedientes
	 * 
	 * @param persistenceManager
	 * @param nombreSubscriptor
	 * @param iAddress
	 * @param hazelcastActivoParam
	 */
	private void validaInyeccion(PersistenceManagerSubscriptor persistenceManager, String nombreSubscriptor,
			String iAddress, ParametroBean hazelcastActivoParam) {
		NetConfigBean localNetConfigBean = persistenceManager.retrieveNetConfigByIp(nombreSubscriptor, iAddress);
		if (localNetConfigBean != null) {
			if (hazelcastActivoParam.getValorParametro().equals(ParametroValueEnum.PARAMETRO_VALUE_ACTIVO.getName())) {
				inyectarExpedientesYNotificaciones(localNetConfigBean.getIp(), localNetConfigBean.getPort(),
						nombreSubscriptor);
			}
		} else {
			ParametroBean configLocalNet = persistenceManager.retrieveParam(nombreSubscriptor,
					ParametroEnum.PARAMETRO_GRAL_CONFIG_LOCAL_NET.getName());
			localNetConfigBean = persistenceManager.retrieveNetConfigByIp(nombreSubscriptor,
					configLocalNet.getValorParametro());
			if (localNetConfigBean != null) {
				if (hazelcastActivoParam.getValorParametro()
						.equals(ParametroValueEnum.PARAMETRO_VALUE_ACTIVO.getName())) {
					inyectarExpedientesYNotificaciones(localNetConfigBean.getIp(), localNetConfigBean.getPort(),
							nombreSubscriptor);
				}
			} else {
				LOG.info(
						"Este servidor IP no esta habilitado para ejecutar un flujo hazelcast, si se requiere es necesario agregarlo a la parametria");
			}
		}
	}

	/**
	 * Inyecta los expedientes de un subscriptor que esten en estatus 1,3,5 y 7
	 * 
	 * @param ip
	 * @param port
	 * @param nombreSubscriptor
	 */
	private void inyectarExpedientesYNotificaciones(String ip, String port, String nombreSubscriptor) {
		HazelcastInstance client = null;
		try {
			client = crearClienteHazelcast(ip, port, 30000);
			if (client != null) {
				// Se inyectan los expediente
				LOG.info("Inicia proceso de inyeccion de expedientes");
				PersistenceManager pm = new PersistenceManagerImpl();
				List<ExpedienteBean> expedientes = pm.retrieveAllExpedientesBySubscriptor(nombreSubscriptor);
				iterarExpedientes(expedientes, client);
				// Se inyectan las notificaciones
				LOG.info("Inicia proceso de inyeccion de notificaciones");
				PersistenceManagerNotificacion pmN = new PersistenceManagerNotificacionImpl();
				List<ExpedienteBean> notificaciones = pmN.retrieveAllNotificacionesBySubscriptor(nombreSubscriptor);
				iterarNotificaciones(notificaciones, client);
			}
		} catch (InterruptedException e) {
			LOG.error(e);
			Thread.currentThread().interrupt();
		} catch (HazelcastConnectionException e) {
			LOG.error(e);
		}
		LOG.info("Termina proceso de inyeccion");
	}

	/**
	 * iterarExpedientes
	 * 
	 * @param expedientes Parametro de tipo list para los expedientes
	 * @param client Parametro de tipo HazelcastInstance para los clientes
	 * @param pm
	 * @throws InterruptedException 
	 */
	private void iterarExpedientes(List<ExpedienteBean> expedientes, HazelcastInstance client)
			throws InterruptedException {
		IQueue<ExpedienteBean> queue = client.getQueue(QueueEnum.QUEUE_DISPATCHER.getName());
		for (ExpedienteBean bean : expedientes) {
			queue.put(bean);
		}
	}

	/**
	 * Metodo para obteber las notificaciones de expedientes procesados
	 * iterarNotificaciones
	 * 
	 * @param notificaciones
	 * @param client
	 * @throws InterruptedException
	 */
	private void iterarNotificaciones(List<ExpedienteBean> notificaciones, HazelcastInstance client)
			throws InterruptedException {
		IQueue<ExpedienteBean> queue2 = client.getQueue(QueueEnum.QUEUE_NOTIFICATION.getName());
		Map<String, Boolean> notificacionesMap = client.getMap(MapEnum.MAP_NOTIFICACIONES_IN_PROCESS.getName());
		// Obtener las notificaciones
		for (ExpedienteBean bean : notificaciones) {
			if (notificacionesMap.get(bean.getIdExpediente()) == null) {
				queue2.put(bean);
				notificacionesMap.put(bean.getIdExpediente(), true);
				LOG.info("Se agrega notificacion en la queue: " + bean.getIdExpediente());
			} else {
				LOG.info("No se agrego la notificacion a la queue ya que actualmente esta siendo procesada: "
						+ bean.getIdExpediente());
			}
		}
	}

	/**
	 * Metodo que generara el cliente de hazelcast
	 * Crea una instancia hazelcast
	 * 
	 * @param ip del servicio hazcelcast
	 * @param port puerto del servicio
	 * @param timeSleep tiempo de espera
	 * @return
	 * @throws HazelcastConnectionException
	 * @throws InterruptedException
	 */
	private HazelcastInstance crearClienteHazelcast(String ip, String port, long timeSleep)
			throws HazelcastConnectionException {
		// Obtener instancia de utileria para hazelcast
		HazelcastGenericUtils hazelcastUtils = new HazelcastGenericUtils();
		LOG.info("Inicia creacion cliente hazelcast");
		ClientConfig config = new ClientConfig();
		// Setear datos de configuracion para el servicio hazelcast
		ClientNetworkConfig cnc = new ClientNetworkConfig();
		cnc.addAddress(ip + ":" + port);
		config.setNetworkConfig(cnc);
		// Aqui de agrega la configuracion de name y password para hazelcast
		return hazelcastUtils.getClientHazelcast(timeSleep,config); 
	}

}
