package mx.isban.flujogd.injector;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.scheduling.annotation.EnableAsync;

import mx.isban.flujogd.injector.component.InjectorComponent;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase FlujoGDinjectorApplication
 *
 */
@SpringBootApplication
@EnableAsync
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
public class FlujoGDInjectorApplication implements CommandLineRunner{
	
	//Inicializamos un log para guardar la bitacora
	private static final Logger LOG = LogManager.getLogger(FlujoGDInjectorApplication.class);
	
	/**
	 * main
	 * @param args	Argumentos
	 */
	public static void main(String[] args) {
		SpringApplication sp = new SpringApplication(FlujoGDInjectorApplication.class);
		//Ejecutar los argumentos
		sp.run(args);
		System.exit(0);
	}

	@Override
	public void run(String... args) throws Exception {
		//Mostrar en bitacor el inicio del proceso
		LOG.info("Inicia InjectorComponent");
		InjectorComponent batchComponent = new InjectorComponent();
		//Ejecutar el proceso
		batchComponent.execute();
	}

}
