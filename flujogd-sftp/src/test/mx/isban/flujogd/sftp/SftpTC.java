package mx.isban.flujogd.sftp;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mx.isban.flujogd.common.bean.CifrasControlBean;
import mx.isban.flujogd.common.bean.ParametroBean;
import mx.isban.flujogd.common.bean.SFTPBean;
import mx.isban.flujogd.common.bean.SFTPBeanDetalle;
import mx.isban.flujogd.common.util.ConfEnum;
import mx.isban.flujogd.common.util.ParametroEnum;
import mx.isban.flujogd.persistence.PersistenceManager;
import mx.isban.flujogd.persistence.PersistenceManagerCifrasControl;
import mx.isban.flujogd.persistence.impl.PersistenceManagerCifrasControlImpl;
import mx.isban.flujogd.persistence.impl.PersistenceManagerImpl;
import mx.isban.flujogd.sftp.manager.SFTPManager;

public class SftpTC {

	private static final Logger LOG = LogManager.getLogger(SftpTC.class);

	public static void main(String[] args) {
		LOG.info("inicia...");
		PersistenceManager pm = new PersistenceManagerImpl();
//		List<ExpedienteBean> expedientes = pm.retrieveAllExpedientes();
//		ExpedienteBean expedienteBean = expedientes.get(0);
		SFTPBean sftpBean = retrieveSftpBean(ConfEnum.CONF_SFTP.getName());
		SFTPManager sftpManager = new SFTPManager(sftpBean.getHost(), sftpBean.getPort(), sftpBean.getUser(), sftpBean.getPass(),sftpBean.getDetalle().getPathPrivateKey(),sftpBean.getDetalle().getPassphrase());
//		SftpResponseBean sftpResponse = sftpManager.retrieve(expedienteBean.getNombreExpediente(), sftpBean.getDetalle().getSrcPath(), sftpBean.getDetalle().getOutputPath());
//		if(sftpResponse.isOk()) 
//			LOG.info("El expediente se copio con exito");
//		else
//			LOG.info("El expediente No se pudo copiar");
		save(sftpManager);
	}

	private static SFTPBean retrieveSftpBean(String config){
		PersistenceManager persistenceManager = new PersistenceManagerImpl();
		List<ParametroBean> parametros = persistenceManager.retrieveParamByConf(config);
		SFTPBean sftpBean = null;
		if(parametros != null){
			sftpBean = new SFTPBean();
			SFTPBeanDetalle detalle = new SFTPBeanDetalle();
			sftpBean.setDetalle(detalle);
			for(ParametroBean bean:parametros){
				if(bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_SFTP_HOST.getName())) {
					sftpBean.setHost(bean.getValorParametro());
				}else if(bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_SFTP_PORT.getName())) {
					sftpBean.setPort(bean.getValorParametro());
				}else if(bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_SFTP_USER.getName())) {
					sftpBean.setUser(bean.getValorParametro());
				}else if(bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_SFTP_PASS.getName())) {
					sftpBean.setPass(bean.getValorParametro());
				}else if(bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_SFTP_SRC_PATH.getName())) {
					sftpBean.getDetalle().setSrcPath(bean.getValorParametro());
				}else if(bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_SFTP_OUTPUT_PATH.getName())) {
					sftpBean.getDetalle().setOutputPath(bean.getValorParametro());
				}else if(bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_SFTP_PATH_PRIVATE_KEY.getName())) {
					sftpBean.getDetalle().setPathPrivateKey(bean.getValorParametro());
				}else if(bean.getNombreParametro().equals(ParametroEnum.PARAMETRO_SFTP_PASSPHRASE.getName())) {
					sftpBean.getDetalle().setPassphrase(bean.getValorParametro());
				}
			}
		}
		return sftpBean;
	}
	
	/**
	 * 
	 * @param manager
	 */
	private static void save(SFTPManager manager) {
		
		PersistenceManagerCifrasControl pm  = new PersistenceManagerCifrasControlImpl();
		LOG.info("SAVE");
		//File initialFile = new File("D:/Temporal/stop.txt");
	    try {
			//InputStream is = new FileInputStream(initialFile);
	    	
	    	CifrasControlBean beanRes = pm.notificarCifrasCtrl("VIVERE");
			
			manager.save("prueba.txt", beanRes.getReporteInStre());
			
			pm.updateExpedientes(beanRes.getListIdsExpedientes());
			
		} catch (Exception e) {
			LOG.error(e);
		}
	    LOG.info("DONE...");
	}

}
