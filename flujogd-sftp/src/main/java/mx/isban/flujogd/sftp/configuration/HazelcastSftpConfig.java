package mx.isban.flujogd.sftp.configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.hazelcast.config.Config;
import com.hazelcast.config.ListenerConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IQueue;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.bean.MemberBean;
import mx.isban.flujogd.common.generic.HazelcastGenericUtils;
import mx.isban.flujogd.sftp.listener.ClusterMembershipListener;

/**
 * The Class HazelcastSftpConfig.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase HazelcastSftpConfig
 */
@Configuration
public class HazelcastSftpConfig {
	
	/** ips. */
	private static List<String> ips;
	
	/** port. */
	private static String port;
	/**
	 * Constructor.
	 */
	public HazelcastSftpConfig() {
		ClusterMembershipListener.setHazelcastConfig(this);
	}
	
	
	/**
	 * Gets the single instance of HazelcastSftpConfig.
	 *
	 * @return single instance of HazelcastSftpConfig
	 */
	@Bean
	public HazelcastInstance getInstanceSFTP() {
		return Hazelcast.newHazelcastInstance(getHazelcastSftpConfig());
	}

	/**
	 * Gets the queue.
	 *
	 * @param name the name
	 * @return the queue
	 */
	public IQueue<ExpedienteBean> getQueue(String name) {
		return getInstanceSFTP().getQueue(name);
	}
	
	/**
	 * Gets the map members.
	 *
	 * @param name the name
	 * @return the map members
	 */
	public Map<String, MemberBean> getMapMembers(String name) {
		return getInstanceSFTP().getMap(name);
	}
	
	/**
	 * Gets the map expedientes.
	 *
	 * @param name the name
	 * @return the map expedientes
	 */
	public Map<String, Boolean> getMapExpedientes(String name) {
		return getInstanceSFTP().getMap(name);
	}
	
	/**
	 * Gets the map notificaciones.
	 *
	 * @param name the name
	 * @return the map notificaciones
	 */
	public Map<String, Boolean> getMapNotificaciones(String name) {
		return getInstanceSFTP().getMap(name);
	}

	/**
	 * Config cluster network and discovery mechanism.
	 *
	 * @return Config
	 */
	@Bean
	public Config getHazelcastSftpConfig() {
		HazelcastGenericUtils util = new HazelcastGenericUtils();
		Config config = new Config();
		config.addListenerConfig(new ListenerConfig("mx.isban.flujogd.sftp.listener.ClusterMembershipListener"));
		config.setInstanceName("FlujoGD-Sftp");
		return util.setConfigHazelcast(port, ips, config);
	}

	/**
	 * Sets the ips.
	 *
	 * @param ips the ips to set
	 */
	public static void setIps(List<String> ips) {
		List<String> copia = new ArrayList<>();
		copia.addAll(ips);
		HazelcastSftpConfig.ips = copia;
	}

	/**
	 * Sets the port.
	 *
	 * @param port the port to set
	 */
	public static void setPort(String port) {
		HazelcastSftpConfig.port = port;
	}

}
