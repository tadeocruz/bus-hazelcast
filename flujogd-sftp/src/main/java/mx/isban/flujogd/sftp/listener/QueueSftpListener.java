package mx.isban.flujogd.sftp.listener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hazelcast.core.ItemEvent;
import com.hazelcast.core.ItemListener;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.sftp.trigger.ExecuteJobSftpTrigger;

/**
 * The listener interface for receiving queueSftp events.
 * The class that is interested in processing a queueSftp
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addQueueSftpListener<code> method. When
 * the queueSftp event occurs, that object's appropriate
 * method is invoked.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase QueueSftpListener
 */
@Component
public class QueueSftpListener implements ItemListener<ExpedienteBean> {
	
	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(QueueSftpListener.class);
	
    /** The job batch. */
    @Autowired
    private ExecuteJobSftpTrigger jobBatch;

    /**
     * itemAdded.
     *
     * @param item the item
     */
    @Override
    public void itemAdded(ItemEvent<ExpedienteBean> item) {
    	LOG.info("Se detecta objeto agregado a la queue - "+ item.getItem().getIdExpediente() + " - " + item.getItem().getNombreExpediente() + " - " + item.getItem().getDetalle().getDetalle2().getReferenciaExterna());
    	jobBatch.executeJobSftp();
    }

    /**
     * itemRemoved.
     *
     * @param item the item
     */
    @Override
    public void itemRemoved(ItemEvent<ExpedienteBean> item) {
    	LOG.info("Se detecta objeto removido de la queue");
    }

}