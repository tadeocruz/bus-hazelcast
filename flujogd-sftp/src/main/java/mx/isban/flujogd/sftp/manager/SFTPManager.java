package mx.isban.flujogd.sftp.manager;

import java.io.InputStream;
import java.util.List;

import mx.isban.flujogd.common.bean.ParametroBean;
import mx.isban.flujogd.common.bean.SFTPBean;
import mx.isban.flujogd.common.bean.SFTPBeanDetalle;
import mx.isban.flujogd.common.bean.SftpResponseBean;
import mx.isban.flujogd.common.generic.SFTPManagerUtils;
import mx.isban.flujogd.common.generic.StartedGenericUtils;
import mx.isban.flujogd.common.util.QueueEnum;
import mx.isban.flujogd.persistence.PersistenceManager;
import mx.isban.flujogd.persistence.impl.PersistenceManagerImpl;

/**
 * The Class SFTPManager.
 */
public class SFTPManager {
	/** The user. */
	private String user = null;
	
	/** The pass. */
	private String pass = null;
	
	/** The host. */
	private String host = null;
	
	/** The port. */
	private String port = null;
	
	
	/** The path private key. */
	private String pathPrivateKey = null;
	
	/** The passphrase. */
	private String passphrase = null;
	
	/**
	 * Instantiates a new SFTP manager.
	 *
	 * @param host the host
	 * @param port the port
	 * @param user the user
	 * @param pass the pass
	 */
	public SFTPManager(String host, String port, String user, String pass) {
		this.user = user;
		this.pass = pass;
		this.host = host;
		this.port = port;
	}
	
	/**
	 * Instantiates a new SFTP manager.
	 *
	 * @param host the host
	 * @param port the port
	 * @param user the user
	 * @param pass the pass
	 * @param pathPrivateKey the path private key
	 * @param passphrase the passphrase
	 */
	public SFTPManager(String host, String port, String user, String pass, String pathPrivateKey, String passphrase) {
		this.user = user;
		this.pass = pass;
		this.host = host;
		this.port = port;
		this.pathPrivateKey = pathPrivateKey;
		this.passphrase = passphrase;
	}
	
	/**
	 * Retrieve.
	 *
	 * @param fileName the file name
	 * @param srcPath the src path
	 * @param outputPath the output path
	 * @return the sftp response bean
	 */
	public SftpResponseBean retrieve(String fileName, String srcPath, String outputPath) {
		SFTPManagerUtils utils = new SFTPManagerUtils();
		return utils.getFileSFTP(fileName, srcPath, outputPath, this.pathPrivateKey, this.user, this.pass, this.host, this.port, this.passphrase);
	}
	
	
	/**
	 * retrieveSftpBean.
	 *
	 * @param idExpediente the id expediente
	 * @return the SFTP bean
	 */
	public static SFTPBean retrieveSftpBean(String idExpediente){
		StartedGenericUtils utils = new StartedGenericUtils();
		PersistenceManager persistenceManager = new PersistenceManagerImpl();
		List<ParametroBean> parametros = persistenceManager.retrieveParametrosBloque(idExpediente, QueueEnum.QUEUE_SFTP.getName());
		SFTPBean sftpBean = null;
		if(parametros != null){
			sftpBean = new SFTPBean();
			SFTPBeanDetalle detalle = new SFTPBeanDetalle();
			sftpBean.setDetalle(detalle);
			for(ParametroBean bean:parametros){
				utils.getValue(bean, sftpBean);
			}
		}
		return sftpBean;
	}
	
	/**
	 * save.
	 *
	 * @param fileName the file name
	 * @param is the is
	 * @return the sftp response bean
	 */
	public SftpResponseBean save(String fileName, InputStream is) {
		SFTPManagerUtils utils = new SFTPManagerUtils();
		return utils.saveFileSFTP(fileName, is, this.pathPrivateKey, this.user, this.pass, this.host, this.port, this.passphrase);
	}

}
