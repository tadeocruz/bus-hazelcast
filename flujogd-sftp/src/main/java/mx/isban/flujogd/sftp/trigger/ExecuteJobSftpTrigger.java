package mx.isban.flujogd.sftp.trigger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Clase para ejecutar el proceso de SFTP
 * The Class ExecuteJobSftpTrigger.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase ExecuteJobSftpTrigger
 */
@Component
public class ExecuteJobSftpTrigger {
	
	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(ExecuteJobSftpTrigger.class);
	
	/** The job launcher sftp. */
	@Autowired
	private JobLauncher jobLauncherSftp;
	
	/** The process job sftp. */
	@Autowired
	private Job processJobSftp;

	/**
	 * Metodo para ejecutar proceso sftp
	 * Ejecuta el job batch sftp.
	 */
	public void executeJobSftp() {
		try {
			JobParameters jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis()).toJobParameters();
			jobLauncherSftp.run(processJobSftp, jobParameters);
		} catch (JobParametersInvalidException | JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException e) {
			LOG.error(e);
		}
	}

}
