package mx.isban.flujogd.sftp.step;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.item.ItemProcessor;

import com.hazelcast.core.IExecutorService;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.bean.NotificationBean;
import mx.isban.flujogd.common.bean.NotificationBean2;
import mx.isban.flujogd.common.bean.NotificationBean3;
import mx.isban.flujogd.common.bean.SFTPBean;
import mx.isban.flujogd.common.bean.SftpResponseBean;
import mx.isban.flujogd.common.util.ExpedienteErrorEnum;
import mx.isban.flujogd.common.util.ExpedienteStatusEnum;
import mx.isban.flujogd.common.util.ExpedienteUtil;
import mx.isban.flujogd.common.util.NotificationManager;
import mx.isban.flujogd.common.util.QueueEnum;
import mx.isban.flujogd.common.util.QueueStatusEnum;
import mx.isban.flujogd.persistence.PersistenceManagerExpediente;
import mx.isban.flujogd.persistence.callable.BloquePersistenceTask;
import mx.isban.flujogd.persistence.callable.ExpedientePersistenceTask;
import mx.isban.flujogd.persistence.callable.NotificacionPersistenceTask;
import mx.isban.flujogd.persistence.impl.PersistenceManagerExpedienteImpl;
import mx.isban.flujogd.sftp.configuration.HazelcastSftpConfig;
import mx.isban.flujogd.sftp.manager.SFTPManager;

/**
 * Clase para ejecutar el proceso de transferencia sftp de los expedientes
 * The Class BatchSftpItemProcessor.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase BatchSftpItemProcessor
 */
public class BatchSftpItemProcessor implements ItemProcessor<ExpedienteBean, ExpedienteBean>{

	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(BatchSftpItemProcessor.class);

	/** The Constant EXECUTOR_SERVICE. */
	private static final String EXECUTOR_SERVICE  = "executorService";

	/** The hazelcast sftp config. */
	private HazelcastSftpConfig hazelcastSftpConfig;
	
	/**
	 * Constructor.
	 *
	 * @param hazelcastSftpConfig the hazelcast sftp config
	 */
	public BatchSftpItemProcessor(HazelcastSftpConfig hazelcastSftpConfig) {
		this.hazelcastSftpConfig = hazelcastSftpConfig;
	}


	/**
	 * process.
	 * Metodo para realizar la transferencia de los archivos mediante sftp
	 *
	 * @param expedienteBean the expediente bean
	 * @return the expediente bean
	 * @throws Exception the exception
	 */
	@Override
	public  synchronized ExpedienteBean process(ExpedienteBean expedienteBean) throws Exception {
		SFTPBean sftpBean = SFTPManager.retrieveSftpBean(expedienteBean.getIdExpediente());
		if(sftpBean==null) {
			return error(expedienteBean, ExpedienteErrorEnum.ERROR_SFTP_CONEXION.getName());
		}
		SFTPManager sftpManager = new SFTPManager(sftpBean.getHost(), sftpBean.getPort(), sftpBean.getUser(), sftpBean.getPass(),sftpBean.getDetalle().getPathPrivateKey(),sftpBean.getDetalle().getPassphrase());
		SftpResponseBean sftpResponse = sftpManager.retrieve(expedienteBean.getNombreExpediente(), sftpBean.getDetalle().getSrcPath(), sftpBean.getDetalle().getOutputPath());
		if(sftpResponse.isOk()) {
			LOG.info("El expediente se copio con exito");
			updateStausBloqueExpediente(expedienteBean, QueueStatusEnum.STATUS_OK.getName(), QueueEnum.QUEUE_SFTP.getName(), null);
			LOG.info("EL ESTATUS DEL EXPEDIENTE SE ACTUALIZO CON EXITO - "+ expedienteBean.getIdExpediente() + " - "+ expedienteBean.getNombreExpediente() + " - " + expedienteBean.getDetalle().getDetalle2().getReferenciaExterna());
			expedienteBean.setPathZIPExpediente(sftpBean.getDetalle().getOutputPath());
			ExpedienteUtil expedienteUtil = new ExpedienteUtil();
			expedienteUtil.cambiaEstausQueue(expedienteBean);
			expedienteUtil.determinaSiguienteQueue(expedienteBean);
			return expedienteBean;
		}else {
			return error(expedienteBean, sftpResponse.getErrorCode());
		}
	}

	/**
	 * Metodo para controlar la siguiente queue en caso de error
	 * error.
	 *
	 * @param expedienteBean the expediente bean
	 * @param nombreError the nombre error
	 * @return the expediente bean
	 */
	private ExpedienteBean error(ExpedienteBean expedienteBean,String nombreError){
		expedienteBean.getDetalle().setSiguienteQueue("");
		LOG.error(nombreError);
		updateStausBloqueExpediente(expedienteBean, QueueStatusEnum.STATUS_ERROR.getName(), QueueEnum.QUEUE_SFTP.getName(), nombreError);
		return expedienteBean;
	}

	/**
	 * Metodo para actualizar el estatus de los bloques de los expedientes
	 * updateStausBloqueExpediente.
	 *
	 * @param expedienteBean the expediente bean
	 * @param statusBloque the status bloque
	 * @param queueName the queue name
	 * @param nombreError the nombre error
	 * @return true, if successful
	 */
	private boolean updateStausBloqueExpediente(ExpedienteBean expedienteBean, String statusBloque, String queueName, String nombreError) {
		PersistenceManagerExpediente persistence = new PersistenceManagerExpedienteImpl();
		// Obtener instancia de hazelcast
		IExecutorService executorService = this.hazelcastSftpConfig.getInstanceSFTP().getExecutorService( EXECUTOR_SERVICE );
		Future<Object> future = executorService.submit( new BloquePersistenceTask(expedienteBean.getIdExpediente(), statusBloque, queueName,nombreError) );
		boolean ok = false;
		try {
			ok = (Boolean)future.get(); 
			if(ok){
				String estatusExpediente = ExpedienteStatusEnum.STATUS_RECIBIDO.getName();
				if(nombreError!=null) {
					estatusExpediente = ocurrioUnError(persistence, expedienteBean, queueName, nombreError);
				}
				ok = updateStausExpediente(expedienteBean.getIdExpediente(), estatusExpediente, nombreError);
			}
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(e);
		}
		return ok;
	}

	/**
	 * Metodo para controlar y notificar los errores en el proceso de sftp
	 * ocurrioUnError.
	 *
	 * @param persistence the persistence
	 * @param expedienteBean the expediente bean
	 * @param queueName the queue name
	 * @param nombreError the nombre error
	 * @return the string
	 */
	private String ocurrioUnError(PersistenceManagerExpediente persistence,ExpedienteBean expedienteBean,String queueName,String nombreError) {
		String estatusExpediente = ExpedienteStatusEnum.STATUS_ERROR_RECEPCION.getName();
		//Nueva validacion para numero de reintentos y envio de notificaciones
		Integer intentos = persistence.retrieveIntentosTipoError(expedienteBean.getIdExpediente(), queueName, nombreError);
		Integer reintentosPermitidos = persistence.retrieveReintentosByNombreError(nombreError);
		if(intentos>=reintentosPermitidos) {
			estatusExpediente = ExpedienteStatusEnum.STATUS_PROCESO_CANCELADO.getName();
			NotificationManager notification = new NotificationManager();
			notification.preparaEnvioNotificacion(expedienteBean, nombreError, nombreError,expedienteBean.getNombreExpediente().substring(0, expedienteBean.getNombreExpediente().length()-4));
			insertNotificacion(expedienteBean,nombreError);
		}
		return estatusExpediente;
	}

	/**
	 * Metodo para actuliar el estatis de los expedientes procesdos por sftp
	 * updateStausExpediente.
	 *
	 * @param idExpediente the id expediente
	 * @param status the status
	 * @param idError the id error
	 * @return true, if successful
	 */
	private boolean updateStausExpediente(String idExpediente, String status, String idError) {
		IExecutorService executorServiceSftp = this.hazelcastSftpConfig.getInstanceSFTP().getExecutorService( EXECUTOR_SERVICE );
		Future<Object> future = executorServiceSftp.submit( new ExpedientePersistenceTask(idExpediente, status, idError) );
		boolean okSftp = false;
		try {
			okSftp = (Boolean)future.get();
		} catch (InterruptedException e) {
			LOG.error(e);
			Thread.currentThread().interrupt();
		} catch (ExecutionException e) {
			LOG.error(e);
		}
		return okSftp;
	}

	/**
	 * Metodo para insertar las notificaciones del sftp
	 * updateStausExpediente.
	 *
	 * @param expedienteBean the expediente bean
	 * @param nombreError the nombre error
	 * @return true, if successful
	 */
	private boolean insertNotificacion(ExpedienteBean expedienteBeanSftp, String nombreError) {
		// Obtener instancia de hazelcast
		IExecutorService executorServiceSftp = this.hazelcastSftpConfig.getInstanceSFTP().getExecutorService( EXECUTOR_SERVICE );
		NotificationBean bean = new NotificationBean();
		NotificationBean2 bean2 = new NotificationBean2();
		NotificationBean3 bean3 = new NotificationBean3();
		bean2.setBean3(bean3);
		bean.setBean2(bean2);
		bean.setIdExpediente(expedienteBeanSftp.getIdExpediente());
		bean.setRequest(null);
		bean.setResponseCode(null);
		bean.setResponseMsg(null);
		bean2.setRefExterna(null);
		bean2.setReqId(null);
		bean2.setErrorId(nombreError);
		bean2.setErrorMsg(nombreError);
		bean3.setBuc(null);
		bean3.setPathZip(expedienteBeanSftp.getPathZIPExpediente());
		bean3.setFileName(expedienteBeanSftp.getNombreExpediente());
		bean3.setPathUnzip(expedienteBeanSftp.getPathUNZIPExpediente());
		Future<Object> future = executorServiceSftp.submit( new NotificacionPersistenceTask(bean) );
		boolean okSftp = false;
		try {
			okSftp = (Boolean)future.get();
		} catch (InterruptedException e) {
			LOG.error(e);
			Thread.currentThread().interrupt();
		} catch (ExecutionException e) {
			LOG.error(e);
		}
		return okSftp;
	}

}
