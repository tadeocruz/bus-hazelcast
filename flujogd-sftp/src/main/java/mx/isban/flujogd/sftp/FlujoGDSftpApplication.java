package mx.isban.flujogd.sftp;


import java.io.File;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.system.ApplicationPidFileWriter;
import org.springframework.scheduling.annotation.EnableAsync;

import mx.isban.flujogd.common.generic.CommonsUtils;
import mx.isban.flujogd.sftp.configuration.HazelcastSftpConfig;
import mx.isban.flujogd.sftp.listener.ClusterMembershipListener;

/**
 * The Class FlujoGDSftpApplication.
 * Metodo principal que ejecutara el proceso de sftp
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase FlujoGDSftpApplication
 */
@SpringBootApplication
@EnableAsync
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
public class FlujoGDSftpApplication implements CommandLineRunner {
	
	//Inicializamos un log para la bitacora
	private static final Logger LOG = LogManager.getLogger(FlujoGDSftpApplication.class);
	
    /**
     * The main method.
     * Metodo que lanzara la ejecucion del proceso sftp
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
		CommonsUtils utils = new CommonsUtils();
		// Validar que existan los parametros requeridos
    	if(args.length<5) {
			//Mostrar en bitacora un error de componentes faltantes
    		LOG.error("No es posible inicar el componente sin los parametros 'IPs','port','path','componente' y 'Nombre subscriptor'");
    		return;
    	}else{
			HazelcastSftpConfig.setIps(utils.getListaIp(args));
			HazelcastSftpConfig.setPort(args[1]);
			ClusterMembershipListener.setNombreSubscriptorSftp(args[4]);
			ClusterMembershipListener.setComponenteSftp(args[3]);
			SpringApplication sp = new SpringApplication(FlujoGDSftpApplication.class);
			sp.addListeners(new ApplicationPidFileWriter(args[2]+File.separator+args[3]+".pid"));
			sp.run(args);
		}
    }

    /**
     * Metodo nativo hazelcast que dispara el proceso de arranque.
     *
     * @param args the args
     * @throws Exception the exception
     */
	@Override
	public void run(String... args) throws Exception {
		LOG.info("FlujoGDSftpApplication - run");
	}

}
