package mx.isban.flujogd.sftp.configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.support.transaction.ResourcelessTransactionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.sftp.listener.JobSftpListener;
import mx.isban.flujogd.sftp.step.BatchSftpItemProcessor;
import mx.isban.flujogd.sftp.step.BatchSftpItemReader;
import mx.isban.flujogd.sftp.step.BatchSftpItemWriter;

/**
 * The Class BatchSftpConfig.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase BatchSftpConfig
 */
@EnableBatchProcessing(modular = true)
@Configuration
public class BatchSftpConfig {

	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(BatchSftpConfig.class);

	/** The job builder factory. */
	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	/** The step builder factory. */
	@Autowired
	private StepBuilderFactory stepBuilderFactory;
	
	/** The hazelcast sftp config. */
	@Autowired
	private HazelcastSftpConfig hazelcastSftpConfig;

	/**
	 * Metodo para procesar las tareas sftp
	 * Process job.
	 *
	 * @return Job
	 */
	@Bean
	public Job processJob() {
		LOG.info("execute job");
		// Devolver procesoS
		return jobBuilderFactory.get("jobBuilderFactory")
				.incrementer(new RunIdIncrementer())
				.listener(listener())
				.flow(orderStep())
				.end()
				.build();
	}

	/**
	 * Metodo para crear los pasos para procesar sftp
	 * To create a step, reader, processor and writer has been passed serially.
	 *
	 * @return the step
	 */
	@Bean
	public Step orderStep() {
		LOG.info("step de job");
		// Devolver pasos
		return stepBuilderFactory.get("orderStep1")
				.<ExpedienteBean, ExpedienteBean> chunk(1)
				.reader(new BatchSftpItemReader(this.hazelcastSftpConfig))
				.processor(new BatchSftpItemProcessor(this.hazelcastSftpConfig))
				.writer(new BatchSftpItemWriter(this.hazelcastSftpConfig))
				.build();
	}

	/**
	 * Listener.
	 *
	 * @return {@link JobExecutionListener}
	 */
	@Bean
	public JobExecutionListener listener() {
		return new JobSftpListener();
	}

	/**
	 * Transaction manager.
	 *
	 * @return {@link ResourcelessTransactionManager}
	 */
	@Bean
	public ResourcelessTransactionManager transactionManager() {
		return new ResourcelessTransactionManager();
	}
}
