package mx.isban.flujogd.sftp.configuration;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import mx.isban.flujogd.common.util.QueueEnum;
import mx.isban.flujogd.sftp.listener.QueueSftpListener;

/**
 * The Class QueueSftpConfig.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase QueueSftpConfig
 */
@Configuration
public class QueueSftpConfig {
	
	/** log. */
	private static final Logger LOG = LogManager.getLogger(QueueSftpConfig.class);
	
	/** The hazelcast config. */
	@Autowired
	private HazelcastSftpConfig hazelcastConfig;
	
	/** The queue initial listener. */
	@Autowired
	private QueueSftpListener queueInitialListener;

	/**
	 * postConstruct.
	 */
	@PostConstruct
	public void postConstruct() {
		hazelcastConfig.getQueue( QueueEnum.QUEUE_SFTP.getName() ).addItemListener( queueInitialListener, true );
		LOG.info( "FlujoGD-Sftp  - Listener Start" );
	}
}
