package mx.isban.flujogd.sftp.step;

import java.text.ParseException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;

import com.hazelcast.core.IQueue;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.util.QueueEnum;
import mx.isban.flujogd.sftp.configuration.HazelcastSftpConfig;

/**
 * The Class BatchSftpItemReader.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase inicial BatchSftpItemReader
 */
public class BatchSftpItemReader implements ItemReader<ExpedienteBean>{

	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(BatchSftpItemReader.class);
	
	/** The hazelcast sftp config. */
	private HazelcastSftpConfig hazelcastSftpConfig;
	
	/** The expediente bean. */
	private ExpedienteBean expedienteBean;

	/**
	 * Constructor.
	 *
	 * @param hazelcastSftpConfig the hazelcast sftp config
	 */
	public BatchSftpItemReader(HazelcastSftpConfig hazelcastSftpConfig){
		this.hazelcastSftpConfig = hazelcastSftpConfig;
	}


	/**
	 * Read.
	 *
	 * @return the expediente bean
	 * @throws InterruptedException the interrupted exception
	 * @throws ParseException the parse exception
	 * @throws NonTransientResourceException the non transient resource exception
	 */
	@Override
	public ExpedienteBean read() throws ParseException {
		IQueue<ExpedienteBean> queueSftp = this.hazelcastSftpConfig.getQueue(QueueEnum.QUEUE_SFTP.getName());
		try {
			expedienteBean = queueSftp.take();
			if (expedienteBean != null) {
				LOG.info("=== SE RECUPERA EXPEDIENTE: "+expedienteBean.getIdExpediente() + " - "+ expedienteBean.getNombreExpediente() + " - " + expedienteBean.getDetalle().getDetalle2().getReferenciaExterna());
				return expedienteBean;
			}else{
				return null;
			}
		} catch (InterruptedException e1) {
			LOG.error(e1);
			Thread.currentThread().interrupt();
		}
		return null;
	}

}
