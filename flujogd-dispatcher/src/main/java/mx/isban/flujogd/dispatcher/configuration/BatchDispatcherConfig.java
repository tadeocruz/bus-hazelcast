package mx.isban.flujogd.dispatcher.configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.support.transaction.ResourcelessTransactionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.dispatcher.listener.JobDispatcherListener;
import mx.isban.flujogd.dispatcher.step.BatchDispatcherItemProcessor;
import mx.isban.flujogd.dispatcher.step.BatchDispatcherItemReader;
import mx.isban.flujogd.dispatcher.step.BatchDispatcherItemWriter;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase BatchDispatcherConfig
 *
 */
@EnableBatchProcessing(modular = true)
@Configuration
public class BatchDispatcherConfig {

	//Inicializamos un log para la bitacora
	private static final Logger LOG = LogManager.getLogger(BatchDispatcherConfig.class);

	@Autowired
	//Inicializamos un job builder factory
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	//Inicializamos un step builder factory
	private StepBuilderFactory stepBuilderFactory;
	
	@Autowired
	//Inicializamos un hazelcast dispatcher config
	private HazelcastDispatcherConfig hazelcastDispatcherConfig;

	/**
	 * @return Job
	 */
	@Bean
	public Job processJob() {
		//Mostrar en bitacora el inicio del job
		LOG.info("execute job");
		return jobBuilderFactory.get("jobBuilderFactory")
				//Incremento
				.incrementer(new RunIdIncrementer())
				//Disparador
				.listener(listener())
				//Orden del flujo
				.flow(orderStep())
				.end()
				.build();
	}

	/**
	 * To create a step, reader, processor and writer has been passed serially
	 * 
	 * @return	Step del job
	 */
	@Bean
	public Step orderStep() {
		//Mostrar en bitacora el proceso
		LOG.info("step de job");
		return stepBuilderFactory.get("orderStep1")
				.<ExpedienteBean, ExpedienteBean> chunk(1)
				//Lector del dispatcher
				.reader(new BatchDispatcherItemReader(this.hazelcastDispatcherConfig))
				//Proceso
				.processor(new BatchDispatcherItemProcessor(this.hazelcastDispatcherConfig))
				.writer(new BatchDispatcherItemWriter(this.hazelcastDispatcherConfig))
				//Compilar
				.build();
	}

	/**
	 * @return {@link JobExecutionListener}
	 */
	@Bean
	public JobExecutionListener listener() {
		return new JobDispatcherListener();
	}

	/**
	 * @return {@link ResourcelessTransactionManager}
	 */
	@Bean
	public ResourcelessTransactionManager transactionManager() {
		return new ResourcelessTransactionManager();
	}
}
