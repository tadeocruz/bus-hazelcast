package mx.isban.flujogd.dispatcher.listener;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hazelcast.core.MemberAttributeEvent;
import com.hazelcast.core.MembershipEvent;
import com.hazelcast.core.MembershipListener;

import mx.isban.flujogd.common.bean.MemberBean;
import mx.isban.flujogd.common.bean.NetConfigBean;
import mx.isban.flujogd.common.util.InetAddressUtil;
import mx.isban.flujogd.common.util.MapEnum;
import mx.isban.flujogd.dispatcher.configuration.HazelcastDispatcherConfig;
import mx.isban.flujogd.persistence.PersistenceManagerSubscriptor;
import mx.isban.flujogd.persistence.impl.PersistenceManagerSubscriptorImpl;

/**
 * The listener interface for receiving clusterMembership events.
 * The class that is interested in processing a clusterMembership
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addClusterMembershipListener<code> method. When
 * the clusterMembership event occurs, that object's appropriate
 * method is invoked.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase ClusterMembershipListener
 */
public class ClusterMembershipListener implements MembershipListener{

	/** Inicializamos un log para guardar la bitacora */
	private static final Logger LOG = LogManager.getLogger(ClusterMembershipListener.class);

	/** Crear un string para guardar el nombre del subscriptor */
	private static String nombreSubscriptor;
	
	/** Crear un string para guardar el componente. */
	private static String componente;
	
	/** Crear un hazelcast dispatcher config. */
	private static HazelcastDispatcherConfig hazelcastConfig;

	/**
	 * memberAdded.
	 *
	 * @param membershipEvent the membership event
	 */
	@Override
	public void memberAdded(MembershipEvent membershipEvent) {
		//Obtener el uuid del miembro
		String uuid = membershipEvent.getMember().getUuid();
		String uuidLocal = membershipEvent.getCluster().getLocalMember().getUuid();
		//Obtener el uuid del miembro
		LOG.info("Se grego un nuevo miembro: "+uuid);
		if(uuid.equals(uuidLocal)) {
			LOG.info("flujogd-dispatcher: "+uuid);
			//Notificar al miembro
			notifyMemberAddDispatcher(uuid);
		}
	}

	/**
	 * memberRemoved.
	 * Metodo nativo de hazelcast para remover los miembros
	 *
	 * @param membershipEvent the membership event
	 */
	@Override
	public void memberRemoved(MembershipEvent membershipEvent) {
		LOG.info("Se elimino el miembro: "+membershipEvent.getMember().getUuid());
		notifyMemberRemovedDispatcher(membershipEvent.getMember().getUuid());
	}

	/**
	 * Notifica al cluster que flujo fue agregado.
	 *
	 * @param uuid the uuid
	 */
	private void notifyMemberAddDispatcher(String uuid) {
		//Mostrar en bitacora el flujo agregado
		LOG.info("Notificamos al cluster que flujo fue agregado");
		PersistenceManagerSubscriptor persistenceManagerDispatcher = new PersistenceManagerSubscriptorImpl();
		InetAddressUtil inetUtilDispatcher = new InetAddressUtil();
		String ipNetDispatcher = inetUtilDispatcher.getLocalNet();
		if(!"".equals(ipNetDispatcher)) {
			//Configurar el bean con los datos del subscriptor
			NetConfigBean netConfigBeanDispatcher = persistenceManagerDispatcher.retrieveNetConfigByIp(nombreSubscriptor,ipNetDispatcher);
			if(netConfigBeanDispatcher!=null){
				//Obtener el nombre de los expedientes en proceso
				Map<String, MemberBean> members = hazelcastConfig.getMapMembers(MapEnum.MAP_MEMBER_STATUS.getName());
				MemberBean memberDispatcher = new MemberBean();
				//Configurar el miembro
				memberDispatcher.setActive(true);
				memberDispatcher.setIp(netConfigBeanDispatcher.getIp());
				memberDispatcher.setPort(netConfigBeanDispatcher.getPort());
				memberDispatcher.setName(componente);
				memberDispatcher.setUuid(uuid);
				members.put(uuid, memberDispatcher);
				LOG.info("La notificacion se realizo con exito");
			}
		}else {
			LOG.info("Ocurrio un problema al recuperar la IP de este servidor, sin ella no es posible notificar la baja del nodo");
		}
	}

	/**
	 * Notifica al cluster que flujo se dio de baja.
	 *
	 * @param uuid the uuid
	 */
	private void notifyMemberRemovedDispatcher(String uuid) {
		//Mostrar en bitacora el flujo terminado
		LOG.info("Notificamos al cluster que flujo se dio de baja");
		PersistenceManagerSubscriptor persistenceManager = new PersistenceManagerSubscriptorImpl();
		InetAddressUtil inetUtilDispatcher = new InetAddressUtil();
		String ipNetDispatcher = inetUtilDispatcher.getLocalNet();
		if(!"".equals(ipNetDispatcher)) {
			//Configurar el bean con los datos del subscriptor
			NetConfigBean netConfigBeanDispatcher = persistenceManager.retrieveNetConfigByIp(nombreSubscriptor,ipNetDispatcher);
			if(netConfigBeanDispatcher!=null){
				//Guardar el estatus del mapa 
				Map<String, MemberBean> members = hazelcastConfig.getMapMembers(MapEnum.MAP_MEMBER_STATUS.getName());
				MemberBean memberDispatcher = members.get(uuid);
				if(memberDispatcher!=null) {
					//Configurar el miembro
					memberDispatcher.setActive(false);
					members.replace(uuid, memberDispatcher);
				}
				LOG.info("La notificacion se realizo con exito");
			}
		}else {
			LOG.info("Ocurrio un problema al recuperar la IP de este servidor, sin ella no es posible notificar la baja del nodo");
		}
	}

	/**
	 * memberAttributeChanged.
	 *
	 * @param memberAttributeEvent the member attribute event
	 */
	@Override
	public void memberAttributeChanged(MemberAttributeEvent memberAttributeEvent) {
		LOG.info("Cambio un atributo de uno de los miembros");
	}

	/**
	 * Sets the nombre subscriptor dispatcher.
	 *
	 * @param nombreSubscriptor the nombreSubscriptor to set
	 */
	public static void setNombreSubscriptorDispatcher(String nombreSubscriptor) {
		ClusterMembershipListener.nombreSubscriptor = nombreSubscriptor;
	}

	/**
	 * Sets the componente dispatcher.
	 *
	 * @param componente the componente to set
	 */
	public static void setComponenteDispatcher(String componente) {
		ClusterMembershipListener.componente = componente;
	}

	/**
	 * Sets the hazelcast config.
	 *
	 * @param hazelcastConfig the hazelcastConfig to set
	 */
	public static void setHazelcastConfig(HazelcastDispatcherConfig hazelcastConfig) {
		ClusterMembershipListener.hazelcastConfig = hazelcastConfig;
	}

	

}
