package mx.isban.flujogd.dispatcher.configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.hazelcast.config.Config;
import com.hazelcast.config.ListenerConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IQueue;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.bean.MemberBean;
import mx.isban.flujogd.common.generic.HazelcastGenericUtils;
import mx.isban.flujogd.dispatcher.listener.ClusterMembershipListener;
import mx.isban.flujogd.dispatcher.listener.QueueKillerListener;

/**
 * The Class HazelcastDispatcherConfig.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase HazelcastDispatcherConfig
 */
@Configuration
public class HazelcastDispatcherConfig {
	
	/** port. */
	//Crear un string para guardar el puerto
	private static String port;

	/** ips. */
	//Crear una lista de strings para guardar las ips
	private static List<String> ips;
	
	
	/**
	 * HazelcastDispatcherConfig.
	 */
	public HazelcastDispatcherConfig() {
		ClusterMembershipListener.setHazelcastConfig(this);
		QueueKillerListener.setHazelcastConfig(this);
	}
	
	/**
	 * Gets the single instance of HazelcastDispatcherConfig.
	 *
	 * @return single instance of HazelcastDispatcherConfig
	 */
	@Bean
	public HazelcastInstance getInstanceDispatcher() {
		return Hazelcast.newHazelcastInstance(getHazelcastConfig());
	}

	/**
	 * getqueue.
	 *
	 * @param name Nombre del expediente
	 * @return Instancia del expediente
	 */
	public IQueue<ExpedienteBean> getQueue(String name) {
		return getInstanceDispatcher().getQueue(name);
	}
	
	/**
	 * getmapmembers.
	 *
	 * @param name Nombre del mapa
	 * @return Instancia del mapa
	 */
	public Map<String, MemberBean> getMapMembers(String name) {
		return getInstanceDispatcher().getMap(name);
	}
	
	/**
	 * getmapexpedientes.
	 *
	 * @param name Nombre del mapa
	 * @return Instancia del mapa
	 */
	public Map<String, Boolean> getMapExpedientes(String name) {
		return getInstanceDispatcher().getMap(name);
	}
	
	/**
	 * getmapnotificaciones.
	 *
	 * @param name Nombre del mapa
	 * @return Instancia del mapa
	 */
	public Map<String, Boolean> getMapNotificaciones(String name) {
		return getInstanceDispatcher().getMap(name);
	}
	
	/**
	 * getqueuekamikaze.
	 *
	 * @param name Nombre de la cola
	 * @return Instancia de la cola
	 */
	public IQueue<String> getQueueKamikaze(String name) {
		return getInstanceDispatcher().getQueue(name);
	}

	/**
	 * Config cluster network and discovery mechanism.
	 *
	 * @return Config
	 */
	@Bean
	public Config getHazelcastConfig() {
		Config configDispatcher = new Config();
		HazelcastGenericUtils util = new HazelcastGenericUtils();
		configDispatcher.addListenerConfig(new ListenerConfig("mx.isban.flujogd.dispatcher.listener.ClusterMembershipListener"));
		configDispatcher.setInstanceName("FlujoGD-Dispatcher");
		return util.setConfigHazelcast(port, ips, configDispatcher);
	}

	/**
	 * Sets the ips.
	 *
	 * @param ips the ips to set
	 */
	public static void setIps(List<String> ips) {
		List<String> copiaDispatcher = new ArrayList<>();
		copiaDispatcher.addAll(ips);
		HazelcastDispatcherConfig.ips = copiaDispatcher;
	}

	/**
	 * Sets the port.
	 *
	 * @param port the port to set
	 */
	public static void setPort(String port) {
		HazelcastDispatcherConfig.port = port;
	}

}
