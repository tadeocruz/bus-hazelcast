package mx.isban.flujogd.dispatcher.listener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.hazelcast.core.IExecutorService;
import com.hazelcast.core.ItemEvent;
import com.hazelcast.core.ItemListener;

import mx.isban.flujogd.common.callable.BloqueShutdownTask;
import mx.isban.flujogd.dispatcher.configuration.HazelcastDispatcherConfig;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase principal QueueKamikazeListener
 *
 */
@Component
public class QueueKillerListener implements ItemListener<String> {
	
	private static final Logger LOG = LogManager.getLogger(QueueKillerListener.class);
	
	/**
	 * hazelcastConfig
	 */
	private static HazelcastDispatcherConfig hazelcastConfig;

    /**
   	 * itemAdded
   	 * @param item
   	 */
    @Override
    public void itemAdded(ItemEvent<String> item) {
    	LOG.info( "KAMIKAZE: "+item.getItem() );
    	IExecutorService executorService = hazelcastConfig.getInstanceDispatcher().getExecutorService("executorService");
    	executorService.submit( new BloqueShutdownTask() );
    }

    /**
	 * itemRemoved
	 * @param item
	 */
    @Override
    public void itemRemoved(ItemEvent<String> item) {
    	LOG.info("Se detecta objeto removido de la queue");
    }
    
    /**
	 * @param hazelcastConfig the hazelcastConfig to set
	 */
	public static void setHazelcastConfig(HazelcastDispatcherConfig hazelcastConfig) {
		QueueKillerListener.hazelcastConfig = hazelcastConfig;
	}

}