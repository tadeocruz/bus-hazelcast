package mx.isban.flujogd.dispatcher.trigger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * The Class ExecuteJobDispatcherTrigger.
 * Clase que ejecutara las tareas de Validation
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase ExecuteJobDispatcherTrigger
 */
@Component
public class ExecuteJobDispatcherTrigger {
	
	/** Inicializar un log para guardar la bitacora */
	private static final Logger LOG = LogManager.getLogger(ExecuteJobDispatcherTrigger.class);
	
	/** Crear un job launcher */
	@Autowired
	private JobLauncher jobLauncherDispatcher;

	/** Crear un job */
	@Autowired
	private Job processJobDispatcher;

	/**
	 * Ejecuta el job batch.
	 */
	public void executeJobDispatcher() {
		try {
			//Configurar los parametros del job
			JobParameters jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis()).toJobParameters();
			//Correr el job
			jobLauncherDispatcher.run(processJobDispatcher, jobParameters);
		} catch (JobParametersInvalidException | JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException e) {
			LOG.error(e);
		}
	}

}
