	package mx.isban.flujogd.dispatcher.step;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.item.ItemProcessor;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.util.ExpedienteUtil;
import mx.isban.flujogd.common.util.MapEnum;
import mx.isban.flujogd.dispatcher.configuration.HazelcastDispatcherConfig;
import mx.isban.flujogd.persistence.PersistenceManager;
import mx.isban.flujogd.persistence.impl.PersistenceManagerImpl;

/**
 * The Class BatchDispatcherItemProcessor.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase BatchDispatcherItemProcessor
 */
public class BatchDispatcherItemProcessor implements ItemProcessor<ExpedienteBean, ExpedienteBean>{
	
	/** LOG. */
	private static final Logger LOG = LogManager.getLogger(BatchDispatcherItemProcessor.class);
	
	/** The hazelcast dispatcher config. */
	private HazelcastDispatcherConfig hazelcastDispatcherConfig;
	
	/**
	 * Construtor.
	 *
	 * @param hazelcastDispatcherConfig the hazelcast dispatcher config
	 */
	public BatchDispatcherItemProcessor(HazelcastDispatcherConfig hazelcastDispatcherConfig) {
		this.hazelcastDispatcherConfig = hazelcastDispatcherConfig;
	}
	
	
	/**
	 * process.
	 *
	 * @param expedienteBean the expediente bean
	 * @return the expediente bean
	 * @throws Exception the exception
	 */
	@Override
	public synchronized ExpedienteBean process(ExpedienteBean expedienteBean) throws Exception {
		ExpedienteUtil expedienteUtil = new ExpedienteUtil();
		//Validamos que el expediente no este siendo procesado en otra queue
		Map<String, Boolean> expedientesMap = this.hazelcastDispatcherConfig.getMapExpedientes(MapEnum.MAP_EXPEDIENTES_IN_PROCESS.getName());
		//La llave del expediente en el mapa es BUC-TIPOPROCESO
//		String key = expedienteBean.getNombreExpediente().substring(0, expedienteBean.getNombreExpediente().length()-4)+"-"+expedienteBean.getDetalle().getDetalle2().getDetalle3().getTipoProceso();
		String key = expedienteBean.getIdExpediente();
		if(expedientesMap.get(key)==null) {
			
			//Se obtiene el flujo del expediente
			PersistenceManager pm = new PersistenceManagerImpl();
			pm.llenarFlujo(expedienteBean);
			
			LOG.info("INJECTOR - SE AGREGA EXPEDIENTE AL CLUSTER - "+ expedienteBean.getIdExpediente() + " - "+ expedienteBean.getNombreExpediente() + " - " + expedienteBean.getDetalle().getDetalle2().getDetalle3().getTipoProceso());
			expedienteUtil.determinaSiguienteQueue(expedienteBean);
			expedienteBean.getDetalle().getDetalle2().getDetalle3().setKeyMap(key);
			expedientesMap.put(key, true);
		}else {
			expedienteBean.getDetalle().setSiguienteQueue("");
			LOG.info("INJECTOR - NO SE AGREGA EXPEDIENTE AL CLUSTER YA QUE ACTUALMENTE ESTA SIENDO PROCESADA EN OTRA QUEUE - "+ expedienteBean.getIdExpediente());
		}
		return expedienteBean;
	}

}
