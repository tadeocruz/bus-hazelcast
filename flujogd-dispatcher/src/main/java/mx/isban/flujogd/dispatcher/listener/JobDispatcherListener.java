package mx.isban.flujogd.dispatcher.listener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.stereotype.Component;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase JobDispatcherListener
 *
 */
@Component
public class JobDispatcherListener extends JobExecutionListenerSupport{
	
	//Inicializamos un log para guardar la bitacora
	private static final Logger LOG = LogManager.getLogger(JobDispatcherListener.class);
	
	/**
	 * afterJob
	 * @param jobExecution
	 */
	@Override
	public void afterJob(JobExecution jobExecution) {
		if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
			LOG.info("is done");
		}
	}
}
