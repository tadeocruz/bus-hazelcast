package mx.isban.flujogd.dispatcher;


import java.io.File;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.system.ApplicationPidFileWriter;
import org.springframework.scheduling.annotation.EnableAsync;

import mx.isban.flujogd.common.generic.CommonsUtils;
import mx.isban.flujogd.dispatcher.configuration.HazelcastDispatcherConfig;
import mx.isban.flujogd.dispatcher.listener.ClusterMembershipListener;

/**
 * Metodo principal que ejecutara el proceso de dispatcher.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase FlujoGDDispatcherApplication
 */
@SpringBootApplication
@EnableAsync
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
public class FlujoGDDispatcherApplication implements CommandLineRunner {

	//Inicializamos un log para la bitacora
	private static final Logger LOG = LogManager.getLogger(FlujoGDDispatcherApplication.class);

	/**
	 * main.
	 * Metodo que lanzara la ejecucion del proceso dispatcher
	 *
	 * @param args Argumentos
	 */
	public static void main(String[] args) {
		CommonsUtils utils = new CommonsUtils();
		if(args.length<5) {
			//Mostrar en bitacora un error de componentes faltantes
    		LOG.error("No es posible inicar el componente sin los parametros 'IPs','port','path','componente' y 'Nombre subscriptor'");
    		return;
    	}else{
			//Colocar la ip de la configuracion de hazelcast dispatcher
			HazelcastDispatcherConfig.setIps(utils.getListaIp(args));
			//Colocar el puerto de la configuracion de hazelcast dispatcher
			HazelcastDispatcherConfig.setPort(args[1]);
			//Colocar el nombre del subscriptor del cluster de membresias
			ClusterMembershipListener.setNombreSubscriptorDispatcher(args[4]);
			//Colocar el componente del cluster de membresias
			ClusterMembershipListener.setComponenteDispatcher(args[3]);
			SpringApplication sp = new SpringApplication(FlujoGDDispatcherApplication.class);
			//Agregar los disparadores
			sp.addListeners(new ApplicationPidFileWriter(args[2]+File.separator+args[3]+".pid"));
			//Correr los argumentos
			sp.run(args);
		}
	}

	/**
	 * Metodo nativo hazelcast que dispara el proceso de arranque.
	 *
	 * @param args the args
	 * @throws Exception the exception
	 */
	@Override
	public void run(String... args) throws Exception {
		LOG.info("FlujoGDDispatcherApplication - run");
	}

}
