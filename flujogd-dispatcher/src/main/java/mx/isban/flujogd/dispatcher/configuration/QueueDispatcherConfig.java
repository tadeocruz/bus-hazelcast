package mx.isban.flujogd.dispatcher.configuration;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import mx.isban.flujogd.common.util.QueueEnum;
import mx.isban.flujogd.dispatcher.listener.QueueDispatcherListener;
import mx.isban.flujogd.dispatcher.listener.QueueKillerListener;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase QueueDispatcherConfig
 *
 */
@Configuration
public class QueueDispatcherConfig {
	
	/**
	 * log
	 */
	//Inicializamos un log para guardar la bitacora
	private static final Logger LOG = LogManager.getLogger(QueueDispatcherConfig.class);
	
	@Autowired
	//Crear un hazelcast dispatcher config
	private HazelcastDispatcherConfig hazelcastConfig;
	
	@Autowired
	//Crear un queue dispatcher listener
	private QueueDispatcherListener queueDispatcherListener;
	
	@Autowired
	//Crear un queue killer listener
	private QueueKillerListener queueKamikazeListener;

	/**
	 * postConstruct
	 */
	@PostConstruct
	public void postConstruct() {
		hazelcastConfig.getQueue( QueueEnum.QUEUE_DISPATCHER.getName() ).addItemListener( queueDispatcherListener, true );
		hazelcastConfig.getQueueKamikaze( QueueEnum.QUEUE_KILLER.getName() ).addItemListener( queueKamikazeListener, true );
		LOG.info( "FlujoGD-Dispatcher  - Listener Start" );
	}
}
