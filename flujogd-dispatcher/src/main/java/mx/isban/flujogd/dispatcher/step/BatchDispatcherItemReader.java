package mx.isban.flujogd.dispatcher.step;

import java.text.ParseException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.item.ItemReader;
import com.hazelcast.core.IQueue;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.util.QueueEnum;
import mx.isban.flujogd.dispatcher.configuration.HazelcastDispatcherConfig;

/**
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase inicial BatchDispatcherItemReader
 *
 */
public class BatchDispatcherItemReader implements ItemReader<ExpedienteBean>{

	//Inicializar un log para guardar la bitacora
	private static final Logger LOG = LogManager.getLogger(BatchDispatcherItemReader.class);
	
	//Crear un expediente bean
	private ExpedienteBean expedienteBean;

	//Crear un hazelcast dispatcher config
	private HazelcastDispatcherConfig hazelcastDispatcherConfig;
	
	/**
	 * Constructor
	 * @param hazelcastDispatcherConfig - Mapa hazelcast neceario para obtener la queue dispatcher
	 */
	public BatchDispatcherItemReader(HazelcastDispatcherConfig hazelcastDispatcherConfig){
		this.hazelcastDispatcherConfig = hazelcastDispatcherConfig;
	}


	/**
	 * Metodo que lee de la queue el siguiente dato a ser procesado
	 */
	@Override
	public synchronized ExpedienteBean read() throws InterruptedException, ParseException {
		//Obtener el expediente en la cola
		IQueue<ExpedienteBean> queue = this.hazelcastDispatcherConfig.getQueue(QueueEnum.QUEUE_DISPATCHER.getName());
		try {
			expedienteBean = queue.take();
			if (expedienteBean != null) {
				//Mostrar en bitaco el expediente recuperado
				LOG.info("=== SE RECUPERA EXPEDIENTE: "+expedienteBean.getIdExpediente());
				return expedienteBean;
			}else{
				return null;
			}
		} catch (InterruptedException e1) {
			LOG.error(e1);
			Thread.currentThread().interrupt();
		}
		//Regresar nulo
		return null;
	}

}
