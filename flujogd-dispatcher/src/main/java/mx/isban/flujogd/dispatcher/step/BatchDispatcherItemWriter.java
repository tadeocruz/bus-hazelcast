package mx.isban.flujogd.dispatcher.step;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.item.ItemWriter;

import com.hazelcast.core.IQueue;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.util.MapEnum;
import mx.isban.flujogd.common.util.QueueEnum;
import mx.isban.flujogd.dispatcher.configuration.HazelcastDispatcherConfig;

/**
 * Clase para notificar las queues de procesamiento
 * 
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase BatchDispatcherItemWriter
 *
 */
public class BatchDispatcherItemWriter implements ItemWriter<ExpedienteBean>{
	
	//Inicializar el log para guardar la bitacora
	private static final Logger LOG = LogManager.getLogger(BatchDispatcherItemWriter.class);
	
	//Crear un hazelcast dispatcher config
	private HazelcastDispatcherConfig hazelcastDispatcherConfig;

	/**
	 * Constructor
	 * @param hazelcastDispatcherConfig	Configuracion del hazelcast dispatcher
	 */
	public BatchDispatcherItemWriter(HazelcastDispatcherConfig hazelcastDispatcherConfig) {
		this.hazelcastDispatcherConfig = hazelcastDispatcherConfig;
	}
	

	/**
	 * Metodo para notificar la siguiente queue a procesar
	 * write
	 * @param items
	 */
	@Override
	public synchronized void write(List<? extends ExpedienteBean> items) throws Exception {
		//Obtener el item que sigue en la cola
		String nextQueue = items.get(0).getDetalle().getSiguienteQueue();
		if(!"".equals(nextQueue)) {
			LOG.info("Siguiente queue: " + nextQueue);
			if(QueueEnum.QUEUE_NOTIFICATION.getName().equals(nextQueue)) {
				//Obtener e mapa de notificaciones
				Map<String, Boolean> notificacionesMap = this.hazelcastDispatcherConfig.getMapNotificaciones(MapEnum.MAP_NOTIFICACIONES_IN_PROCESS.getName());
				if(notificacionesMap.get(items.get(0).getIdExpediente())==null) {
					IQueue<ExpedienteBean> queue = hazelcastDispatcherConfig.getQueue(nextQueue);
					queue.put(items.get(0));
					notificacionesMap.put(items.get(0).getIdExpediente(), true);
				}
			}else {
				IQueue<ExpedienteBean> queue = hazelcastDispatcherConfig.getQueue(nextQueue);
				queue.put(items.get(0));
			}
		}else {
			LOG.info("No hay otra queue a procesar");
		}
	}
	
}
