package mx.isban.flujogd.validation.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.util.ExpedienteUtil;
import mx.isban.flujogd.common.util.QueueEnum;
import mx.isban.flujogd.common.util.QueueStatusEnum;
import mx.isban.flujogd.validation.configuration.HazelcastValidationConfig;

/**
 * Clase con las propiedades y métodos comúnes para las clases de validación
 * 
 * @author DELL
 *
 */
public class ValidationsObjectsContainer {

	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(ValidationsObjectsContainer.class);

	/** The hazelcast validation config. */
	private HazelcastValidationConfig hazelcastValidationConfig;

	/**
	 * validationUtils object
	 */
	private BatchValidationItemProcessorUtils validationUtils;

	/**
	 * expedienteUtil objeto
	 */
	private ExpedienteUtil expedienteUtil = new ExpedienteUtil();

	/**
	 * Constructor con hazelcast config
	 * 
	 * @param hazelcastValidationConfig hazelcast config
	 */
	public ValidationsObjectsContainer(HazelcastValidationConfig hazelcastValidationConfig) {
		this.hazelcastValidationConfig = hazelcastValidationConfig;
		validationUtils = new BatchValidationItemProcessorUtils(this.hazelcastValidationConfig);
	}

	/**
	 * Constructor con los parámetros proporcionados
	 * 
	 * @param hazelcastValidationConfig HazelcastValidationConfig object
	 * @param validationUtils           BatchValidationItemProcessorUtils object
	 * @param expedienteUtil            ExpedienteUtil object
	 */
	public ValidationsObjectsContainer(HazelcastValidationConfig hazelcastValidationConfig, BatchValidationItemProcessorUtils validationUtils, ExpedienteUtil expedienteUtil) {
		this.hazelcastValidationConfig = hazelcastValidationConfig;
		this.validationUtils = validationUtils;
		this.expedienteUtil = expedienteUtil;
	}

	/**
	 * Metodo para actualizar los expedientes en caso de error error.
	 *
	 * @param expedienteBean the expediente bean
	 * @param nombreError    the nombre error
	 * @return the expediente bean
	 */
	public ExpedienteBean error(ExpedienteBean expedienteBean, String nombreError) {
		expedienteBean.getDetalle().setSiguienteQueue("");
		LOG.error(nombreError);
		validationUtils.updateStausBloqueExpediente(expedienteBean, QueueStatusEnum.STATUS_ERROR.getName(), QueueEnum.QUEUE_VALIDATION.getName(), nombreError);
		return expedienteBean;
	}

	/**
	 * Método para obtener el objeto de BatchValidationItemProcessorUtils
	 * 
	 * @return BatchValidationItemProcessorUtils object
	 */
	public BatchValidationItemProcessorUtils getValidationUtils() {
		return validationUtils;
	}

	/**
	 * Método para obtener el objeto de ExpedienteUtil
	 * 
	 * @return ExpedienteUtil object
	 */
	public ExpedienteUtil getExpedienteUtil() {
		return expedienteUtil;
	}
}
