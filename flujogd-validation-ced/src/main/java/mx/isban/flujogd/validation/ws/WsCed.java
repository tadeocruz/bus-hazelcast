package mx.isban.flujogd.validation.ws;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import mx.isban.flujogd.common.bean.ParametroBean;
import mx.isban.flujogd.common.bean.ValidacionMetadato;
import mx.isban.flujogd.common.exceptions.BusinessException;
import mx.isban.flujogd.common.util.ParametroEnum;
import mx.isban.flujogd.persistence.PersistenceManagerSubscriptor;
import mx.isban.flujogd.persistence.impl.PersistenceManagerSubscriptorImpl;

/**
 * Clase WsCed para comunicarse con el servicio de Ced el cuál se encarga de
 * proporcionar la información necesaria para validar los metadatos
 * 
 * @author DELL
 *
 */
public class WsCed {

	/**
	 * url del ced para obtener un token de sesión
	 */
	private String urlToken;

	/**
	 * Url para obtener las reglas de validación de metadatos desde el ced
	 */
	private String urlCedMetadatos;

	/**
	 * objeto para realizar parseo
	 */
	private JSONParser parser = new JSONParser();

	/**
	 * objeto para realizar las peticiones a servicios rest
	 */
	private RestTemplate restTemplate = new RestTemplate();

	/**
	 * Objeto de persistencia en base de datos, utilizado para recuperar la url del
	 * token y la url del ced
	 */
	private PersistenceManagerSubscriptor persistenceManager = new PersistenceManagerSubscriptorImpl();

	/**
	 * Constructor Por default
	 */
	public WsCed() {
	}

	/**
	 * Constructor con lo necesario para manejar el restTemplate y el persistence
	 * manager
	 * 
	 * @param restTemplate       resttemplate object
	 * @param persistenceManager persistenceManager object
	 */
	public WsCed(RestTemplate restTemplate, PersistenceManagerSubscriptor persistenceManager) {
		super();
		this.restTemplate = restTemplate;
		this.persistenceManager = persistenceManager;
	}

	/**
	 * Método para obtener la lista de validación de metadatos correspondiente al
	 * nombre de subscriptor y tipo documental proporcionado
	 * 
	 * @param documentalTipe    tipo documental del que deseamos obtener las
	 *                          validaciones
	 * @param nombreSubscriptor nombre del subscriptor
	 * @return List<ValidacionMetadato> lista de validaciones de metadatos
	 * @throws ParseException    en caso de ocurrir un error al parsear la respuesta
	 *                           del ced
	 * @throws BusinessException en caso de que la respuesta del ced no sea un
	 *                           código ok
	 */
	public List<ValidacionMetadato> getParamsFromCedByDocumentalTipe(String documentalTipe, String nombreSubscriptor) throws BusinessException {
		prepareObjects(nombreSubscriptor);
		JSONArray metadataParams;
		try {
			metadataParams = getJsonArrayParamsFromCedByDocumentalTipe(documentalTipe, nombreSubscriptor);
		} catch (ParseException e) {
			throw new BusinessException("ocurrió un error al recuperar las validaciones del ced", e);
		}

		List<ValidacionMetadato> list = new ArrayList<>();
		for (int i = 0; i < metadataParams.size(); i++) {
			JSONObject objParam = (JSONObject) metadataParams.get(i);
			JSONObject metaValid = (JSONObject) objParam.get("param");
			list.add(new ValidacionMetadato(metaValid.get("tipo").toString(), metaValid.get("validacion").toString(), Integer.parseInt(metaValid.get("requerido").toString()),
					metaValid.get("symbolicname").toString()));
		}
		return list;
	}

	private void prepareObjects(String nombreSubscriptor) {
		ParametroBean urlTokenCedParam = persistenceManager.retrieveParam(nombreSubscriptor, ParametroEnum.PARAMETRO_CED_TOKEN_URL.getName());
		urlToken = urlTokenCedParam.getValorParametro();

		ParametroBean urlGetMetadatosConfigurationParam = persistenceManager.retrieveParam(nombreSubscriptor, ParametroEnum.PARAMETRO_CED_VALIDATION_URL.getName());
		urlCedMetadatos = urlGetMetadatosConfigurationParam.getValorParametro();

	}

	/**
	 * Método para obtener el array de parámetros de validación desde el CED
	 * 
	 * @param documentalTipe tipo documental por el cuál se solicita los parámetros
	 *                       de validación
	 * 
	 * @return JsonArray con los parámetros de validación
	 * @throws ParseException    en caso de ocurrir un error al parsear la respuesta
	 *                           del ced
	 * @throws BusinessException en caso de que la respuesta del ced no sea un
	 *                           código ok
	 */
	private JSONArray getJsonArrayParamsFromCedByDocumentalTipe(String documentalTipe, String nombreSubscriptor) throws ParseException, BusinessException {
		String token = getToken();

		HashMap<String, String> bodyRequest = new HashMap<>();
		bodyRequest.put("appOrigen", nombreSubscriptor);
		bodyRequest.put("tipoDocumento", documentalTipe);

		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", token);

		HttpEntity<HashMap<String, String>> request = new HttpEntity<>(bodyRequest, headers);
		ResponseEntity<String> response = restTemplate.postForEntity(urlCedMetadatos, request, String.class);
		if (response.getStatusCode().is2xxSuccessful()) {
			JSONObject jsonObject = (JSONObject) parser.parse(response.getBody());
			JSONObject remoteResponse = (JSONObject) jsonObject.get("response");
			JSONArray data = (JSONArray) remoteResponse.get("data");
			JSONObject paramsRequired = (JSONObject) data.get(0);
			return (JSONArray) paramsRequired.get("paramsRequired");
		}
		throw new BusinessException(response.getBody());
	}

	/**
	 * Método para obtener un token de autenticación y poder consumir el servicio
	 * del ced
	 * 
	 * @return token de autenticación
	 * @throws ParseException    en caso de ocurrir un error al parsear la respuesta
	 *                           del ced
	 * @throws BusinessException en caso de que la respuesta del ced no sea un
	 *                           código ok
	 */
	private String getToken() throws ParseException, BusinessException {
		ResponseEntity<String> response = restTemplate.postForEntity(urlToken, null, String.class);
		JSONObject jsonObject = (JSONObject) parser.parse(response.getBody());
		if (response.getStatusCode().is2xxSuccessful()) {
			return jsonObject.get("access-token").toString();
		}
		throw new BusinessException(response.getBody());
	}
}
