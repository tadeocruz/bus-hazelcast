package mx.isban.flujogd.validation.trigger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * The Class ExecuteJobValidationTrigger es la Clase que ejecutara/desatará el
 * job para la validación de expedientes de acuerdo con los jobparameters
 * proporcionados (En este caso únicamente se pasa el tiempo en milisegundos al
 * JobBuilder).
 * 
 * @author DELL
 *
 */
@Component
public class ExecuteJobValidationTrigger {

	/** Inicializar un log para guardar la bitacora */
	private static final Logger LOG = LogManager.getLogger(ExecuteJobValidationTrigger.class);

	/** Crear un job launcher */
	@Autowired
	private JobLauncher jobLauncherValidation;

	/** Crear un job */
	@Autowired
	private Job processJobValidation;

	/**
	 * Ejecuta el job batch de acuerdo con los jobparameters proporcionados (En este
	 * caso únicamente se pasa el tiempo en milisegundos al JobBuilder).
	 */
	public void executeJobValidation() {
		try {
			JobParameters jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis()).toJobParameters();
			jobLauncherValidation.run(processJobValidation, jobParameters);
		} catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException | JobParametersInvalidException e) {
			LOG.error(e.getMessage(), e);
		}
	}

}
