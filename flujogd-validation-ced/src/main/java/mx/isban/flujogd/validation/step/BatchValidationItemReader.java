package mx.isban.flujogd.validation.step;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.item.ItemReader;

import com.hazelcast.core.IQueue;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.util.QueueEnum;
import mx.isban.flujogd.validation.configuration.HazelcastValidationConfig;

/**
 * The Class BatchValidationItemReader.
 * 
 * @author DELL
 *
 */
public class BatchValidationItemReader implements ItemReader<ExpedienteBean> {

	/**
	 * Información del expediente a mostrar en consola
	 */
	private static final String INFO_EXPEDIENTE = "=== SE RECUPERA EXPEDIENTE: %s - %s - %s";

	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(BatchValidationItemReader.class);

	/** The hazelcast validation config. */
	private HazelcastValidationConfig hazelcastValidationConfig;

	/**
	 * Constructor.
	 *
	 * @param hazelcastValidationConfig the hazelcast validation config
	 */
	public BatchValidationItemReader(HazelcastValidationConfig hazelcastValidationConfig) {
		this.hazelcastValidationConfig = hazelcastValidationConfig;
	}

	/**
	 * Metodo que lee de la queue de validación el siguiente dato a ser procesado,
	 * si encuentra expedientes al menos un expediente por procesar entonces lo
	 * informa en consola y lo devuelve para que sea procesado, en caso contrario no
	 * devuelve nada.
	 *
	 * @return the expediente bean
	 * @throws Exception the exception
	 */
	@Override
	public ExpedienteBean read() throws Exception {
		IQueue<ExpedienteBean> queue = this.hazelcastValidationConfig.getQueue(QueueEnum.QUEUE_VALIDATION.getName());
		try {
			ExpedienteBean exp = queue.take();
			if (exp != null) {
				LOG.info(INFO_EXPEDIENTE, exp.getIdExpediente(), exp.getNombreExpediente(), exp.getDetalle().getDetalle2().getReferenciaExterna());
				return exp;
			} else {
				LOG.info("No hay expedientes por procesar");
				return null;
			}
		} catch (InterruptedException e) {
			LOG.error(e.getMessage(), e);
			Thread.currentThread().interrupt();
		}
		return null;
	}
}
