package mx.isban.flujogd.validation.listener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hazelcast.core.ItemEvent;
import com.hazelcast.core.ItemListener;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.validation.trigger.ExecuteJobValidationTrigger;

/**
 * The listener interface for receiving queueValidation events.
 * The class that is interested in processing a queueValidation
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addQueueValidationListener<code> method. When
 * the queueValidation event occurs, that object's appropriate
 * method is invoked.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase QueueValidationListener
 */
@Component
public class QueueValidationListener implements ItemListener<ExpedienteBean> {

	/** The Constant LOG. */
	private static final Logger LOG = LogManager.getLogger(QueueValidationListener.class);

	/** The job batch. */
	@Autowired
	private ExecuteJobValidationTrigger jobBatch;

	/**
	 * itemAdded.
	 *
	 * @param item the item
	 */
	@Override
	public void itemAdded(ItemEvent<ExpedienteBean> item) {
		LOG.info("Se detecta objeto agregado a la queue");
		jobBatch.executeJobValidation();
	}

	/**
	 * itemRemoved.
	 *
	 * @param item the item
	 */
	@Override
	public void itemRemoved(ItemEvent<ExpedienteBean> item) {
		LOG.info("Se detecta objeto removido de la queue");
	}

}