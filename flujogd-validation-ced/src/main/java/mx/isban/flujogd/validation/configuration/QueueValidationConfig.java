package mx.isban.flujogd.validation.configuration;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import mx.isban.flujogd.common.util.QueueEnum;
import mx.isban.flujogd.validation.listener.QueueValidationListener;

/**
 * The Class QueueValidationConfig.
 *
 * @author Alvaro Zamorano
 * azamorano@serviciosexternos.isban.mx
 * Clase QueueValidationConfig
 */
@Configuration
public class QueueValidationConfig {
	
	/** log. */
	private static final Logger LOG = LogManager.getLogger(QueueValidationConfig.class);
	
	/** The hazelcast config. */
	@Autowired
	private HazelcastValidationConfig hazelcastConfig;
	
	/** The queue validation listener. */
	@Autowired
	private QueueValidationListener queueValidationListener;

	/**
	 * postConstruct.
	 */
	@PostConstruct
	public void postConstruct() {
		// Obtener la queue de validacion
		hazelcastConfig.getQueue( QueueEnum.QUEUE_VALIDATION.getName() ).addItemListener( queueValidationListener, true );
		LOG.info( "FlujoGD-Validation  - Listener Start" );
	}
}
