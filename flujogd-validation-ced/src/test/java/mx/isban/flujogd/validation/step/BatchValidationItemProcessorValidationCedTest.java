package mx.isban.flujogd.validation.step;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.hazelcast.core.IExecutorService;

import mx.isban.flujogd.common.bean.ExpedienteBean;
import mx.isban.flujogd.common.bean.ExpedienteBeanDetalle;
import mx.isban.flujogd.common.bean.ExpedienteBeanDetalle2;
import mx.isban.flujogd.common.bean.ExpedienteBeanDetalle3;
import mx.isban.flujogd.common.bean.FlujoBean;
import mx.isban.flujogd.common.bean.ParametroBean;
import mx.isban.flujogd.common.bean.ValidacionMetadato;
import mx.isban.flujogd.common.exceptions.BusinessException;
import mx.isban.flujogd.common.util.ExpedienteProcessTypeEnum;
import mx.isban.flujogd.common.util.ExpedienteUtil;
import mx.isban.flujogd.common.util.ParametroEnum;
import mx.isban.flujogd.common.util.QueueEnum;
import mx.isban.flujogd.common.util.XMLManager;
import mx.isban.flujogd.persistence.PersistenceManagerBloqueDetail;
import mx.isban.flujogd.persistence.PersistenceManagerDocumento;
import mx.isban.flujogd.persistence.PersistenceManagerExpediente;
import mx.isban.flujogd.persistence.PersistenceManagerSubscriptor;
import mx.isban.flujogd.validation.configuration.HazelcastValidationConfig;
import mx.isban.flujogd.validation.utils.BatchValidationItemProcessorUtils;
import mx.isban.flujogd.validation.utils.CedValidation;
import mx.isban.flujogd.validation.utils.ValidationManagerUtils;
import mx.isban.flujogd.validation.utils.ValidationsObjectsContainer;
import mx.isban.flujogd.validation.ws.WsCed;

public class BatchValidationItemProcessorValidationCedTest {

	private static final String SUBSCRIPTOR = "EXP_SUC";

	@Mock
	private HazelcastValidationConfig hazelCastConfig;

	@Mock
	private ValidationManagerUtils utils;

	@Mock
	private PersistenceManagerExpediente pme;

	@Mock
	private PersistenceManagerDocumento pmd;

	@Mock
	private PersistenceManagerSubscriptor persistenceManagerSub;

	@Mock
	private PersistenceManagerBloqueDetail persistenceManagerValidation;

	@Mock
	private WsCed ws;

	@Mock
	private ValidationsObjectsContainer validation;

	@Mock
	private BatchValidationItemProcessorUtils validationUtils;

	@Mock
	private IExecutorService executorService;
	
	@Mock
	private DocumentBuilder dBuilder;
	
	private BatchValidationItemProcessor procesor;

	@BeforeEach
	public void Setup() {
		MockitoAnnotations.initMocks(this);
		utils = new ValidationManagerUtils(new XMLManager(), persistenceManagerSub, persistenceManagerValidation);
		validation = new ValidationsObjectsContainer(hazelCastConfig, validationUtils, new ExpedienteUtil());
		procesor = new BatchValidationItemProcessor(persistenceManagerSub, utils, new CedValidation(ws, hazelCastConfig, validation, null));
	}

	@Test
	public void CuandoSeValidaUnExpedienteYEsValidoEntoncesLaListaDeErroresEsVacia() throws Exception {
		List<ValidacionMetadato> validacionesMetadatoIne = new ArrayList<>();
		validacionesMetadatoIne.add(new ValidacionMetadato("Varchar(12)",
				"\\^([A-ZÑ&amp;]{3,4}) ?(?:- ?)?(\\\\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\\\\d|3[01])) ?(?:- ?)?([A-Z\\\\d]{2})([A\\\\d])\\$", 0, "RFC"));
		validacionesMetadatoIne.add(new ValidacionMetadato("Char(8)", "^[0-9]{8}$", 0, "BUC"));

		when(persistenceManagerSub.retrieveSubscriptorByIdExpediente(any(String.class))).thenReturn(SUBSCRIPTOR);
		when(persistenceManagerSub.retrieveParam(SUBSCRIPTOR, ParametroEnum.PARAMETRO_GRAL_VALIDAR_CED.getName()))
				.thenReturn(new ParametroBean(ParametroEnum.PARAMETRO_GRAL_VALIDAR_CED.getName(), "true", "false"));
		when(persistenceManagerSub.retrieveParam(SUBSCRIPTOR, ParametroEnum.PARAMETRO_GRAL_FILE_DESCRIPTOR.getName()))
				.thenReturn(new ParametroBean(ParametroEnum.PARAMETRO_GRAL_VALIDAR_CED.getName(), "expediente.xml", "false"));

		when(ws.getParamsFromCedByDocumentalTipe("INE", SUBSCRIPTOR)).thenReturn(validacionesMetadatoIne);

		when(validationUtils.updateStausBloqueExpediente(any(ExpedienteBean.class), any(String.class), any(String.class), any(String.class))).thenReturn(true);

		Map<Integer, FlujoBean> flujos = new HashMap<>();
		flujos.put(0, new FlujoBean(QueueEnum.QUEUE_VALIDATION.getName()));
		flujos.put(1, new FlujoBean(QueueEnum.QUEUE_FILENET.getName()));

		ExpedienteBean expedienteBean = new ExpedienteBean();
		expedienteBean.setIdExpediente("98124");
		expedienteBean.setPathUNZIPExpediente("src/test/resources");
		expedienteBean.setNombreExpediente("TEST-001");
		expedienteBean.setDetalle(new ExpedienteBeanDetalle());

		expedienteBean.getDetalle().setFlujos(flujos);

		ExpedienteBean result = procesor.process(expedienteBean);
		assertTrue(result.getExpedienteSucursal().getErroresDocumento().isEmpty());
		assertNotNull(result.getExpedienteSucursal().getCuenta());
		assertNotNull(result.getExpedienteSucursal().getAplicativo());
	}

	@Test
	public void CuandoSeValidaUnExpedienteYAlMenosUnDocumentoNoCuentaConUnMetadatoRequeridoEntoncesLaListaDeErroresNoEstaVacia() throws Exception {
		List<ValidacionMetadato> validacionesMetadatoIne = new ArrayList<>();
		validacionesMetadatoIne.add(new ValidacionMetadato("Varchar(12)",
				"\\^([A-ZÑ&amp;]{3,4}) ?(?:- ?)?(\\\\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\\\\d|3[01])) ?(?:- ?)?([A-Z\\\\d]{2})([A\\\\d])\\$", 0, "RFC"));
		validacionesMetadatoIne.add(new ValidacionMetadato("Char(8)", "^[0-9]{8}$", 1, "BUC"));

		when(persistenceManagerSub.retrieveSubscriptorByIdExpediente(any(String.class))).thenReturn(SUBSCRIPTOR);
		when(persistenceManagerSub.retrieveParam(SUBSCRIPTOR, ParametroEnum.PARAMETRO_GRAL_VALIDAR_CED.getName()))
				.thenReturn(new ParametroBean(ParametroEnum.PARAMETRO_GRAL_VALIDAR_CED.getName(), "true", "false"));
		when(persistenceManagerSub.retrieveParam(SUBSCRIPTOR, ParametroEnum.PARAMETRO_GRAL_FILE_DESCRIPTOR.getName()))
				.thenReturn(new ParametroBean(ParametroEnum.PARAMETRO_GRAL_VALIDAR_CED.getName(), "expediente.xml", "false"));

		when(ws.getParamsFromCedByDocumentalTipe("INE", SUBSCRIPTOR)).thenReturn(validacionesMetadatoIne);

		when(validationUtils.updateStausBloqueExpediente(any(ExpedienteBean.class), any(String.class), any(String.class), any(String.class))).thenReturn(true);

		Map<Integer, FlujoBean> flujos = new HashMap<>();
		flujos.put(0, new FlujoBean(QueueEnum.QUEUE_VALIDATION.getName()));
		flujos.put(1, new FlujoBean(QueueEnum.QUEUE_FILENET.getName()));

		ExpedienteBean expedienteBean = new ExpedienteBean();
		expedienteBean.setIdExpediente("98124");
		expedienteBean.setPathUNZIPExpediente("src/test/resources");
		expedienteBean.setNombreExpediente("TEST-001");
		expedienteBean.setDetalle(new ExpedienteBeanDetalle());

		expedienteBean.getDetalle().setFlujos(flujos);

		ExpedienteBean result = procesor.process(expedienteBean);
		assertFalse(result.getExpedienteSucursal().getErroresDocumento().isEmpty());
	}

	@Test
	public void CuandoSeValidaUnExpedienteYAlMenosUnDocumentoTieneUnValorInvalidoEnUnMetadatoEntoncesLaListaDeErroresNoEstaVacia() throws Exception {
		List<ValidacionMetadato> validacionesMetadatoIne = new ArrayList<>();
		validacionesMetadatoIne.add(new ValidacionMetadato("Varchar(12)",
				"\\^([A-ZÑ&amp;]{3,4}) ?(?:- ?)?(\\\\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\\\\d|3[01])) ?(?:- ?)?([A-Z\\\\d]{2})([A\\\\d])\\$", 0, "RFC"));
		validacionesMetadatoIne.add(new ValidacionMetadato("Char(8)", "^[0-9]{8}$", 1, "NombreArchivo"));

		when(persistenceManagerSub.retrieveSubscriptorByIdExpediente(any(String.class))).thenReturn(SUBSCRIPTOR);
		when(persistenceManagerSub.retrieveParam(SUBSCRIPTOR, ParametroEnum.PARAMETRO_GRAL_VALIDAR_CED.getName()))
				.thenReturn(new ParametroBean(ParametroEnum.PARAMETRO_GRAL_VALIDAR_CED.getName(), "true", "false"));
		when(persistenceManagerSub.retrieveParam(SUBSCRIPTOR, ParametroEnum.PARAMETRO_GRAL_FILE_DESCRIPTOR.getName()))
				.thenReturn(new ParametroBean(ParametroEnum.PARAMETRO_GRAL_VALIDAR_CED.getName(), "expediente.xml", "false"));

		when(ws.getParamsFromCedByDocumentalTipe("INE", SUBSCRIPTOR)).thenReturn(validacionesMetadatoIne);

		when(validationUtils.updateStausBloqueExpediente(any(ExpedienteBean.class), any(String.class), any(String.class), any(String.class))).thenReturn(true);

		Map<Integer, FlujoBean> flujos = new HashMap<>();
		flujos.put(0, new FlujoBean(QueueEnum.QUEUE_VALIDATION.getName()));
		flujos.put(1, new FlujoBean(QueueEnum.QUEUE_FILENET.getName()));

		ExpedienteBean expedienteBean = new ExpedienteBean();
		expedienteBean.setIdExpediente("98124");
		expedienteBean.setPathUNZIPExpediente("src/test/resources");
		expedienteBean.setNombreExpediente("TEST-001");
		expedienteBean.setDetalle(new ExpedienteBeanDetalle());

		expedienteBean.getDetalle().setFlujos(flujos);

		ExpedienteBean result = procesor.process(expedienteBean);
		assertFalse(result.getExpedienteSucursal().getErroresDocumento().isEmpty());
	}

	@Test
	public void CuandoSeValidaUnExpedienteYAlMenosUnDocumentoPdfNoExisteEntoncesLaListaDeErroresNoEstaVacia() throws Exception {
		List<ValidacionMetadato> validacionesMetadatoIne = new ArrayList<>();
		validacionesMetadatoIne.add(new ValidacionMetadato("Varchar(12)",
				"\\^([A-ZÑ&amp;]{3,4}) ?(?:- ?)?(\\\\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\\\\d|3[01])) ?(?:- ?)?([A-Z\\\\d]{2})([A\\\\d])\\$", 0, "RFC"));
		validacionesMetadatoIne.add(new ValidacionMetadato("Char(8)", "^[0-9]{8}$", 0, "NombreArchivo"));

		when(persistenceManagerSub.retrieveSubscriptorByIdExpediente(any(String.class))).thenReturn(SUBSCRIPTOR);
		when(persistenceManagerSub.retrieveParam(SUBSCRIPTOR, ParametroEnum.PARAMETRO_GRAL_VALIDAR_CED.getName()))
				.thenReturn(new ParametroBean(ParametroEnum.PARAMETRO_GRAL_VALIDAR_CED.getName(), "true", "false"));
		when(persistenceManagerSub.retrieveParam(SUBSCRIPTOR, ParametroEnum.PARAMETRO_GRAL_FILE_DESCRIPTOR.getName()))
				.thenReturn(new ParametroBean(ParametroEnum.PARAMETRO_GRAL_VALIDAR_CED.getName(), "expediente-no-pdf.xml", "false"));

		when(ws.getParamsFromCedByDocumentalTipe("INE", SUBSCRIPTOR)).thenReturn(validacionesMetadatoIne);

		when(validationUtils.updateStausBloqueExpediente(any(ExpedienteBean.class), any(String.class), any(String.class), any(String.class))).thenReturn(true);

		Map<Integer, FlujoBean> flujos = new HashMap<>();
		flujos.put(0, new FlujoBean(QueueEnum.QUEUE_VALIDATION.getName()));
		flujos.put(1, new FlujoBean(QueueEnum.QUEUE_FILENET.getName()));

		ExpedienteBean expedienteBean = new ExpedienteBean();
		expedienteBean.setIdExpediente("98124");
		expedienteBean.setPathUNZIPExpediente("src/test/resources");
		expedienteBean.setNombreExpediente("TEST-001");
		expedienteBean.setDetalle(new ExpedienteBeanDetalle());

		expedienteBean.getDetalle().setFlujos(flujos);

		ExpedienteBean result = procesor.process(expedienteBean);
		assertFalse(result.getExpedienteSucursal().getErroresDocumento().isEmpty());
	}

	@Test
	public void CuandoSeValidaUnExpedienteYAlMenosUnDocumentoNoTieneElDocumentoXmlRelacionadoEntoncesLaListaDeErroresNoEstaVacia() throws Exception {
		List<ValidacionMetadato> validacionesMetadatoIne = new ArrayList<>();
		validacionesMetadatoIne.add(new ValidacionMetadato("Varchar(12)",
				"\\^([A-ZÑ&amp;]{3,4}) ?(?:- ?)?(\\\\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\\\\d|3[01])) ?(?:- ?)?([A-Z\\\\d]{2})([A\\\\d])\\$", 0, "RFC"));
		validacionesMetadatoIne.add(new ValidacionMetadato("Char(8)", "^[0-9]{8}$", 0, "NombreArchivo"));

		when(persistenceManagerSub.retrieveSubscriptorByIdExpediente(any(String.class))).thenReturn(SUBSCRIPTOR);
		when(persistenceManagerSub.retrieveParam(SUBSCRIPTOR, ParametroEnum.PARAMETRO_GRAL_VALIDAR_CED.getName()))
				.thenReturn(new ParametroBean(ParametroEnum.PARAMETRO_GRAL_VALIDAR_CED.getName(), "true", "false"));
		when(persistenceManagerSub.retrieveParam(SUBSCRIPTOR, ParametroEnum.PARAMETRO_GRAL_FILE_DESCRIPTOR.getName()))
				.thenReturn(new ParametroBean(ParametroEnum.PARAMETRO_GRAL_VALIDAR_CED.getName(), "expediente-no-xml.xml", "false"));

		when(ws.getParamsFromCedByDocumentalTipe("INE", SUBSCRIPTOR)).thenReturn(validacionesMetadatoIne);

		when(validationUtils.updateStausBloqueExpediente(any(ExpedienteBean.class), any(String.class), any(String.class), any(String.class))).thenReturn(true);

		Map<Integer, FlujoBean> flujos = new HashMap<>();
		flujos.put(0, new FlujoBean(QueueEnum.QUEUE_VALIDATION.getName()));
		flujos.put(1, new FlujoBean(QueueEnum.QUEUE_FILENET.getName()));

		ExpedienteBean expedienteBean = new ExpedienteBean();
		expedienteBean.setIdExpediente("98124");
		expedienteBean.setPathUNZIPExpediente("src/test/resources");
		expedienteBean.setNombreExpediente("TEST-001");
		expedienteBean.setDetalle(new ExpedienteBeanDetalle());

		expedienteBean.getDetalle().setFlujos(flujos);

		ExpedienteBean result = procesor.process(expedienteBean);
		assertFalse(result.getExpedienteSucursal().getErroresDocumento().isEmpty());
	}

	@Test
	public void CuandoOcurreUnErrorEnElLlamadoAlCedEntoncesSeRegresaUnError() throws Exception {
		List<ValidacionMetadato> validacionesMetadatoIne = new ArrayList<>();
		validacionesMetadatoIne.add(new ValidacionMetadato("Varchar(12)",
				"\\^([A-ZÑ&amp;]{3,4}) ?(?:- ?)?(\\\\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\\\\d|3[01])) ?(?:- ?)?([A-Z\\\\d]{2})([A\\\\d])\\$", 0, "RFC"));
		validacionesMetadatoIne.add(new ValidacionMetadato("Char(8)", "^[0-9]{8}$", 0, "NombreArchivo"));
		
		List<ParametroBean> parametrosBloque = new ArrayList<>();
		parametrosBloque.add(new ParametroBean(ParametroEnum.PARAMETRO_GRAL_VALIDAR_CED.getName(), "src/test/resources", "false"));

		when(persistenceManagerSub.retrieveSubscriptorByIdExpediente(any(String.class))).thenReturn(SUBSCRIPTOR);
		when(persistenceManagerSub.retrieveParam(SUBSCRIPTOR, ParametroEnum.PARAMETRO_GRAL_VALIDAR_CED.getName()))
				.thenReturn(new ParametroBean(ParametroEnum.PARAMETRO_GRAL_VALIDAR_CED.getName(), "true", "false"));
		when(persistenceManagerSub.retrieveParam(SUBSCRIPTOR, ParametroEnum.PARAMETRO_GRAL_FILE_DESCRIPTOR.getName()))
				.thenReturn(new ParametroBean(ParametroEnum.PARAMETRO_GRAL_VALIDAR_CED.getName(), "expediente.xml", "false"));

		when(ws.getParamsFromCedByDocumentalTipe("INE", SUBSCRIPTOR)).thenThrow(new BusinessException("Ocurrió un error al llamar al ced"));

		when(validationUtils.updateStausBloqueExpediente(any(ExpedienteBean.class), any(String.class), any(String.class), any(String.class))).thenReturn(true);
		
		when(persistenceManagerValidation.retrieveParametroBloque("98124",QueueEnum.QUEUE_SFTP.getName(), ParametroEnum.PARAMETRO_SFTP_OUTPUT_PATH.getName()))
		.thenReturn(parametrosBloque );

		Map<Integer, FlujoBean> flujos = new HashMap<>();
		flujos.put(0, new FlujoBean(QueueEnum.QUEUE_VALIDATION.getName()));
		flujos.put(1, new FlujoBean(QueueEnum.QUEUE_FILENET.getName()));

		ExpedienteBeanDetalle3 detalle3 = new ExpedienteBeanDetalle3();
		detalle3.setTipoProceso(ExpedienteProcessTypeEnum.PROCESO_TYPE_NUEVO.getName());

		ExpedienteBeanDetalle2 detalle2 = new ExpedienteBeanDetalle2();
		detalle2.setDetalle3(detalle3);

		ExpedienteBeanDetalle detalle = new ExpedienteBeanDetalle();
		detalle.setFlujos(flujos);
		detalle.setDetalle2(detalle2);

		ExpedienteBean expedienteBean = new ExpedienteBean();
		expedienteBean.setIdExpediente("98124");
		expedienteBean.setNombreExpediente("TEST-001");
		expedienteBean.setDetalle(detalle);
		expedienteBean.setNombreExpediente(".zip");

		ExpedienteBean result = procesor.process(expedienteBean);
		assertEquals(expedienteBean.getDetalle().getSiguienteQueue(), result.getDetalle().getSiguienteQueue());
	}


	@Test
	public void CuandoSeValidaUnExpedienteYOcurreUnErrorConElDocumentBuilderEntoncesSeDevuelveUnErrorLaListaDeErroresEsVacia() throws Exception {
		BatchValidationItemProcessor procesor = new BatchValidationItemProcessor(persistenceManagerSub, utils, new CedValidation(ws, hazelCastConfig, validation, dBuilder));
		List<ValidacionMetadato> validacionesMetadatoIne = new ArrayList<>();
		validacionesMetadatoIne.add(new ValidacionMetadato("Varchar(12)",
				"\\^([A-ZÑ&amp;]{3,4}) ?(?:- ?)?(\\\\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\\\\d|3[01])) ?(?:- ?)?([A-Z\\\\d]{2})([A\\\\d])\\$", 0, "RFC"));
		validacionesMetadatoIne.add(new ValidacionMetadato("Char(8)", "^[0-9]{8}$", 0, "BUC"));
		
		when(dBuilder.parse(any(File.class))).thenThrow(new IOException());

		when(persistenceManagerSub.retrieveSubscriptorByIdExpediente(any(String.class))).thenReturn(SUBSCRIPTOR);
		when(persistenceManagerSub.retrieveParam(SUBSCRIPTOR, ParametroEnum.PARAMETRO_GRAL_VALIDAR_CED.getName()))
				.thenReturn(new ParametroBean(ParametroEnum.PARAMETRO_GRAL_VALIDAR_CED.getName(), "true", "false"));
		when(persistenceManagerSub.retrieveParam(SUBSCRIPTOR, ParametroEnum.PARAMETRO_GRAL_FILE_DESCRIPTOR.getName()))
				.thenReturn(new ParametroBean(ParametroEnum.PARAMETRO_GRAL_VALIDAR_CED.getName(), "expediente.xml", "false"));

		when(ws.getParamsFromCedByDocumentalTipe("INE", SUBSCRIPTOR)).thenReturn(validacionesMetadatoIne);

		when(validationUtils.updateStausBloqueExpediente(any(ExpedienteBean.class), any(String.class), any(String.class), any(String.class))).thenReturn(true);

		Map<Integer, FlujoBean> flujos = new HashMap<>();
		flujos.put(0, new FlujoBean(QueueEnum.QUEUE_VALIDATION.getName()));
		flujos.put(1, new FlujoBean(QueueEnum.QUEUE_FILENET.getName()));

		ExpedienteBean expedienteBean = new ExpedienteBean();
		expedienteBean.setIdExpediente("98124");
		expedienteBean.setPathUNZIPExpediente("src/test/resources");
		expedienteBean.setNombreExpediente("TEST-001");
		expedienteBean.setDetalle(new ExpedienteBeanDetalle());

		expedienteBean.getDetalle().setFlujos(flujos);

		ExpedienteBean result = procesor.process(expedienteBean);
		assertTrue(result.getDetalle().getSiguienteQueue().isEmpty());
	}
}
