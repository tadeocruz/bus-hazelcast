package mx.isban.flujogd.validation.ws;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import mx.isban.flujogd.common.bean.ParametroBean;
import mx.isban.flujogd.common.bean.ValidacionMetadato;
import mx.isban.flujogd.common.exceptions.BusinessException;
import mx.isban.flujogd.common.util.ParametroEnum;
import mx.isban.flujogd.persistence.PersistenceManagerSubscriptor;

public class WsCedTest {

	private static final String reponse = "{\r\n" + "    \"respuesta\": {\r\n" + "        \"codigo\": \"A001\",\r\n" + "        \"mensaje\": \"Consulta Exitosa.\"\r\n"
			+ "    },\r\n" + "    \"response\": {\r\n" + "        \"data\": [\r\n" + "            {\r\n" + "                \"paramsRequired\": [\r\n" + "                    {\r\n"
			+ "                        \"param\": {\r\n" + "                            \"tipo\": \"Varchar(12)\",\r\n"
			+ "                            \"validacion\": \"\\\\^([A-ZÑ&amp;]{3,4}) ?(?:- ?)?(\\\\\\\\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\\\\\\\\d|3[01])) ?(?:- ?)?([A-Z\\\\\\\\d]{2})([A\\\\\\\\d])\\\\$\",\r\n"
			+ "                            \"valordefecto\": 1,\r\n" + "                            \"displayname\": \"RFC \",\r\n"
			+ "                            \"symbolicname\": \"RFC\",\r\n" + "                            \"requerido\": 0\r\n" + "                        }\r\n"
			+ "                    },\r\n" + "                    {\r\n" + "                        \"param\": {\r\n" + "                            \"tipo\": \"Char(8)\",\r\n"
			+ "                            \"validacion\": \"^[0-9]{8}$\",\r\n" + "                            \"valordefecto\": 1,\r\n"
			+ "                            \"displayname\": \"BUC \",\r\n" + "                            \"symbolicname\": \"BUC\",\r\n"
			+ "                            \"requerido\": 1\r\n" + "                        }\r\n" + "                    },\r\n" + "                    {\r\n"
			+ "                        \"param\": {\r\n" + "                            \"tipo\": \"Varchar(25)\",\r\n"
			+ "                            \"validacion\": \"\\\\^[0-9, \\\\]\\\\{12\\\\}\\\\$\",\r\n" + "                            \"valordefecto\": 1,\r\n"
			+ "                            \"displayname\": \"CodigoBarras \",\r\n" + "                            \"symbolicname\": \"CodigoBarras\",\r\n"
			+ "                            \"requerido\": 0\r\n" + "                        }\r\n" + "                    },\r\n" + "                    {\r\n"
			+ "                        \"param\": {\r\n" + "                            \"tipo\": \"Boolean\",\r\n"
			+ "                            \"validacion\": \"\\\\^[01]\\\\$\",\r\n" + "                            \"valordefecto\": 1,\r\n"
			+ "                            \"displayname\": \"DocActualizado \",\r\n" + "                            \"symbolicname\": \"DocActualizado\",\r\n"
			+ "                            \"requerido\": 0\r\n" + "                        }\r\n" + "                    }\r\n" + "                ],\r\n"
			+ "                \"properties\": {\r\n" + "                    \"ID_QUERY\": \"PosicionFinanciera\",\r\n"
			+ "                    \"displayname\": \"INFO_FINANCIERA-Posición Financiera\",\r\n" + "                    \"requerido\": \"0\",\r\n"
			+ "                    \"nivel\": \"Apertura Cuentas - Empresa\"\r\n" + "                }\r\n" + "            }\r\n" + "        ],\r\n"
			+ "        \"responseData\": {}\r\n" + "    }\r\n" + "}\r\n" + "";

	private static final String MS_TOKEN_RESPONSE = "{" + "    \"access-token\": \"eb44fa09-9eba-4ac3-a60a-4275e1b0106d\"," + "    \"token_type\": \"bearer\","
			+ "    \"expires_in\": 599," + "    \"scope\": \"basic\"" + "}";

	@Mock
	private RestTemplate restTemplate;

	@Mock
	private PersistenceManagerSubscriptor persistenceManager;

	private WsCed ws;

	@BeforeEach
	public void Setup() {
		MockitoAnnotations.initMocks(this);
		ws = new WsCed(restTemplate, persistenceManager);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void cuandoSeObtienenLosParametrosPAraValidarLosMetadatosYElREsultadoEsCorrectoEntoncesSeDevuelveLaListaDeValidacionesMetadato() throws BusinessException {

		when(persistenceManager.retrieveParam("EXP_SUC", ParametroEnum.PARAMETRO_CED_TOKEN_URL.getName()))
				.thenReturn(new ParametroBean(ParametroEnum.PARAMETRO_CED_TOKEN_URL.getName(), "urlToken", "false"));

		when(persistenceManager.retrieveParam("EXP_SUC", ParametroEnum.PARAMETRO_CED_VALIDATION_URL.getName()))
				.thenReturn(new ParametroBean(ParametroEnum.PARAMETRO_CED_VALIDATION_URL.getName(), "urlCed", "false"));

		when(restTemplate.postForEntity(anyString(), any(), any(Class.class))).thenReturn(new ResponseEntity<String>(MS_TOKEN_RESPONSE, HttpStatus.OK));
		when(restTemplate.postForEntity(anyString(), any(HttpEntity.class), any(Class.class))).thenReturn(new ResponseEntity<>(reponse, HttpStatus.OK));

		List<ValidacionMetadato> result = ws.getParamsFromCedByDocumentalTipe("INE", "EXP_SUC");
		assertTrue(!result.isEmpty());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void cuandoSeObtienenLosParametrosPAraValidarLosMetadatosYNoSePuedeObtenerEltokenDeSesionEntoncesSeLanzaUnaBusinessException() throws BusinessException {

		when(persistenceManager.retrieveParam("EXP_SUC", ParametroEnum.PARAMETRO_CED_TOKEN_URL.getName()))
				.thenReturn(new ParametroBean(ParametroEnum.PARAMETRO_CED_TOKEN_URL.getName(), "urlToken", "false"));

		when(persistenceManager.retrieveParam("EXP_SUC", ParametroEnum.PARAMETRO_CED_VALIDATION_URL.getName()))
				.thenReturn(new ParametroBean(ParametroEnum.PARAMETRO_CED_VALIDATION_URL.getName(), "urlCed", "false"));

		when(restTemplate.postForEntity(anyString(), any(), any(Class.class))).thenReturn(new ResponseEntity<String>("Internal server error", HttpStatus.INTERNAL_SERVER_ERROR));
		when(restTemplate.postForEntity(anyString(), any(HttpEntity.class), any(Class.class))).thenReturn(new ResponseEntity<>(reponse, HttpStatus.OK));

		assertThrows(BusinessException.class, () -> {
			ws.getParamsFromCedByDocumentalTipe("INE", "EXP_SUC");
		});
	}

	@SuppressWarnings("unchecked")
	@Test
	public void cuandoSeObtienenLosParametrosPAraValidarLosMetadatosYNoSePuedeObtenerLasValidacionesEntoncesSeLanzaUnaBusinessException() throws BusinessException {

		when(persistenceManager.retrieveParam("EXP_SUC", ParametroEnum.PARAMETRO_CED_TOKEN_URL.getName()))
				.thenReturn(new ParametroBean(ParametroEnum.PARAMETRO_CED_TOKEN_URL.getName(), "urlToken", "false"));

		when(persistenceManager.retrieveParam("EXP_SUC", ParametroEnum.PARAMETRO_CED_VALIDATION_URL.getName()))
				.thenReturn(new ParametroBean(ParametroEnum.PARAMETRO_CED_VALIDATION_URL.getName(), "urlCed", "false"));

		when(restTemplate.postForEntity(anyString(), any(), any(Class.class))).thenReturn(new ResponseEntity<String>(MS_TOKEN_RESPONSE, HttpStatus.OK));
		when(restTemplate.postForEntity(anyString(), any(HttpEntity.class), any(Class.class)))
				.thenReturn(new ResponseEntity<>("Internal server error", HttpStatus.INTERNAL_SERVER_ERROR));

		assertThrows(BusinessException.class, () -> {
			ws.getParamsFromCedByDocumentalTipe("INE", "EXP_SUC");
		});
	}

}
